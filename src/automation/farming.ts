/*******************************************************************************************************************************
 * Farming Feature
 *******************************************************************************************************************************/

import { farmin_time } from "../constants";
import HelperFunctions from "../utils/helpers";
import Log from "../utils/log";

var Farming: FarmingType = {
  globalTimeout: undefined,
  cityTimeout: undefined,
  openWindowTimeout: undefined,
  // Liste des towns et leurs farmTowns
  citiesWithTowns: new Map(),
  cityTowns: [],

  activate: function () {
    $(".bull_eye_buttons .island_view").trigger("click");
    Log.log("Farming", `Farming Started`);
    if ($(".advisor_frame.captain div").hasClass("captain_active")) {
      Farming.captain();
    } else {
      Farming.start();
    }
    var min: number = farmin_time[window.tr.settings.tr_farmin_timer] + (window.tr.settings.tr_farmin_wait_timer + 100) * 6,
      max: number = farmin_time[window.tr.settings.tr_farmin_timer] + (window.tr.settings.tr_farmin_wait_timer + 100) * 6 + 3000;
    var rand = HelperFunctions.randomize(min, max);

    Farming.globalTimeout = setTimeout(Farming.activate, rand);
  },
  captain: function () {
    const wnd = window.FarmTownOverviewWindowFactory.openFarmTownOverview();
    setTimeout(function () {
      $(".checkbox.select_all").trigger("click");
    }, 500);
    setTimeout(function () {
      $("#fto_claim_button").trigger("click");
    }, 1000);
    setTimeout(function () {
      $(".btn_confirm.button_new").trigger("click");
    }, 2000);
    setTimeout(function () {
      wnd.close();
    }, 2500);
  },
  checkReady: function (town: any) {
    return false;
  },
  start: function () {
    var firstVerif: boolean = false;

    Log.log("Farming", `<span class="debut">⭐ Generating Village List for City</span>`);

    for (let i = 0; i < window.MM.getCollections().Town[0].models.length; i++) {
      if (window.MM.getCollections().Town[0].models[i].attributes.on_small_island) continue;

      for (let j = 0; j < window.MM.getCollections().FarmTown[0].models.length; j++) {
        let city = window.MM.getCollections().Town[0].models[i].attributes;

        let farmTown = window.MM.getCollections().FarmTown[0].models[j].attributes;
        if (city.island_x != farmTown.island_x || city.island_y != farmTown.island_y) continue;

        for (let k = 0; k < window.MM.getCollections().FarmTownPlayerRelation[0].models.length; k++) {
          let relation = window.MM.getCollections().FarmTownPlayerRelation[0].models[k].attributes;
          if (farmTown.id != relation.farm_town_id || relation.relation_status == 0) continue;
          firstVerif = false;

          // Si le tableau des farmTowns de la town n'a pas été créé

          if (Farming.citiesWithTowns.get(city.id) == undefined) {
            firstVerif = true;

            Farming.citiesWithTowns.set(city.id, [farmTown.id]);
          } else {
            if (Farming.citiesWithTowns.get(city.id).includes(farmTown.id)) continue;
            Farming.citiesWithTowns.set(city.id, [...Farming.citiesWithTowns.get(city.id), farmTown.id]);
          }
        }
      }
    }

    var cityId: number = window.Game.townId;

    window.HelperTown.townSwitch(cityId);

    if (!Farming.citiesWithTowns.has(cityId)) {
      cityId = Farming.citiesWithTowns.keys().next().value;

      window.HelperTown.townSwitch(cityId);
    }

    Farming.cityTowns = [...Farming.citiesWithTowns.get(cityId)];

    Farming.cityTimeout = setInterval(function () {
      if (Farming.cityTowns.length < 1 && !window.tr.automation.farmTowns.current) {
        if (Farming.citiesWithTowns.size > 1) {
          Farming.citiesWithTowns.delete(cityId);

          cityId = Farming.citiesWithTowns.keys().next().value;
          console.log("Move to city", cityId);

          window.HelperTown.townSwitch(cityId);

          Farming.cityTowns = [...Farming.citiesWithTowns.get(cityId)];
        } else {
          clearInterval(Farming.cityTimeout);

          // Si la fenêtre est ouvrte, on la ferme
          if (window.tr.automation.windows.farmTown.open) {
            // @ts-ignore
            document.querySelector(".window_curtain .farm_town .btn_wnd.close")?.click();
            Log.log("Farming", `<span class="fin">🔻 Village Window Closed</span>`);
          }
          Log.log("Farming", "Farming over");
        }
      } else if (!window.tr.automation.farmTowns.current) {
        window.tr.automation.farmTowns.current = true;
        Farming.farmTown(Farming.cityTowns[0], firstVerif, cityId);
      }
    }, window.tr.settings.tr_farmin_wait_timer);
  },
  farmTown: function (id: number, firstVerif: boolean, cityId: number) {
    Log.log("Farming", "Farming Village " + id);
    let cpt = 0;
    window.tr.automation.windows.farmTown.open = false;

    window.FarmTownWindowFactory.openWindow(id);

    Farming.openWindowTimeout = setInterval(function () {
      const res = document.querySelector(".window_curtain .farm_town .window_content .action_wrapper");
      cpt++;
      if (res != null) {
        clearInterval(Farming.openWindowTimeout);
        window.tr.automation.windows.farmTown.open = true;
        Log.log("Farming", `<span class="debut">🔆 Village Window Opened.</span>`);

        // @ts-ignore
        var card = document.querySelector(".window_curtain .farm_towns .action_wrapper").children[window.tr.settings.tr_farmin_timer];
        if (window.tr.settings.tr_stopfarmin) {
          // @ts-ignore
          let v = parseInt(card.children[1].innerText);

          let v_w = v + window.ITowns.towns[cityId].resources().wood <= window.ITowns.towns[cityId].resources().storage;

          let v_s = v + window.ITowns.towns[cityId].resources().stone <= window.ITowns.towns[cityId].resources().storage;

          let v_i = v + window.ITowns.towns[cityId].resources().iron <= window.ITowns.towns[cityId].resources().storage;

          if (!v_w || !v_s || !v_i) {
            if (Farming.citiesWithTowns.size > 1) {
              Farming.citiesWithTowns.delete(cityId);

              cityId = Farming.citiesWithTowns.keys().next().value;
              console.log("Move to city", cityId);

              window.HelperTown.townSwitch(cityId);

              Farming.cityTowns = [...Farming.citiesWithTowns.get(cityId)];
            }
            Farming.cityTowns = [];
            window.tr.automation.farmTowns.current = false;
          } else {
            // @ts-ignore
            card.children[3]?.click();
            Log.log("Farming", `Ressources Collected`);

            // @ts-ignore
            document.querySelector(".window_curtain .confirmation .btn_confirm")?.click();

            Farming.cityTowns.shift();
            window.tr.automation.farmTowns.current = false;
            Log.log("Farming", `${Farming.cityTowns.length} Farm Towns Left`);
          }
        } else {
          // @ts-ignore
          card.children[3]?.click();
          Log.log("Farming", `Ressources Collected`);

          Farming.cityTowns.shift();
          window.tr.automation.farmTowns.current = false;
          Log.log("Farming", `${Farming.cityTowns.length} Farm Towns Left`);
        }
      }

      // Si 10 tentatives sans succés
      else if (cpt >= 5 && !firstVerif) {
        clearInterval(Farming.openWindowTimeout);
        Farming.cityTowns.shift();
        window.tr.automation.farmTowns.current = false;
        Log.log("Farming", `<span class="error">⚠ Cannot detect Village Window!</span>`);
      }

      // Si première vérification manquée
      else if (cpt >= 10 && firstVerif) {
        clearInterval(Farming.openWindowTimeout);
        Farming.cityTowns.shift();

        // On supprime le village du tableau principal
        const pos = Farming.citiesWithTowns.get(cityId).indexOf(id);
        Farming.citiesWithTowns.get(cityId).splice(pos, 1);

        window.tr.automation.farmTowns.current = false;
        Log.log("Farming", `<span class="error">No Village detected for this City.</span>`);
      }
    }, window.tr.automation.windows.timeVerif);
  },
  deactivate: function () {
    window.tr.automation.farmTowns.current = false;
    if (!$(".advisor_frame.captain div").hasClass("captain_active")) {
      clearInterval(Farming.openWindowTimeout);
      clearInterval(Farming.cityTimeout);
      clearTimeout(Farming.globalTimeout);
    }
  }
};

export default Farming;
