/*******************************************************************************************************************************
 * Auto Culture Feature
 *******************************************************************************************************************************/

import TrStorage from "../storage";
import TownIcons from "../ui/town_icons";
import HelperFunctions from "../utils/helpers";
import UI from "../utils/ui";
import ModuleHub from "./hub";

var Culture: CultureType = {
  tr_storage: new TrStorage(),
  interval: null,
  town: {},
  iTown: {},
  activate: function () {
    $(
      '<style id="tr_culture" type="text/css">' +
        // Window
        "#tr_culture a { float:left; background-repeat:no-repeat; background-size:25px; line-height:1; } " +
        "#tr_culture .box_content { text-align:center; font-style:normal; } " +
        // Content
        "#tr_culture .hidden { display:none; } " +
        "#tr_culture table { width:500px; } " +
        "#culture_table_wrapper { min-height: 473px; max-height: 473px; overflow-y: auto; overflow-x: hidden; } " +
        "#tr_culture .box_content { background:url(https://www.tuto-de-david1327.com/medias/images/casque-1.png) 94% 94% no-repeat; background-size:140px; } " +
        ".h25 {height: 25px}" +
        ".h16 {height: 16px}" +
        ".w16 {width: 16px}" +
        ".col {float: left}" +
        ".col.w20 {width: 20px}" +
        ".col.w25 {width: 25px}" +
        ".col.w30 {width: 30px}" +
        ".col.w35 {width: 5%}" +
        ".col.w40 {width: 9%}" +
        ".col.w50 {width: 12.5%}" +
        ".col.w175 {width: 175px}" +
        ".col.w200 {width: 200px}" +
        ".town_name_culture {display: flex; flex-direction: column; align-items: flex-start;}" +
        ".casted_spell_town {float: left;margin-top: 4px}" +
        "#tr_culture .scroll_content {overflow-x: hidden;overflow-y: auto;text-align: center;margin: 0}" +
        "#tr_culture .game_list li {display: flex;align-items: center;justify-content: space-around}" +
        "#units_show {float: right;width: 12px;height: 12px;background: url(https://gpnl.innogamescdn.com/images/game/temp/toggle_sprite.png) -12px -24px no-repeat}" +
        ".units_div {position: relative;left: 0;width: 800px}" +
        ".hideoverflow {overflow-x: hidden}" +
        ":first-child+html .hideoverflow {position: relative}" +
        ".hidden_border_cols {width: 25px;height: 25px;border-style: none;border-left: 0!important;padding-right: 1px}" +
        ".col.header {width: 25px;height: 25px;padding: 0;float: none}" +
        ".col.header.main {width: 20px;height: 20px;background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/main_20x20.png);margin: 0 auto}" +
        ".col.header.wood {background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/wood_25x25.png);margin: 0 auto}" +
        ".col.header.stone {background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/stone_25x25.png);margin: 0 auto}" +
        ".col.header.iron {background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/iron_25x25.png);margin: 0 auto}" +
        ".col.header.storage {background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/storage_25x25.png);margin: 0 auto}" +
        ".col.header.casts {background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/favor_25x25.png);margin: 0 auto}" +
        ".col.header.research_points {background-image: url(https://gpnl.innogamescdn.com/images/game/academy/points_25x25.png);margin: 0 auto}" +
        ".col.header.free_trade {background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/free_trade_25x25.png);margin: 0 auto}" +
        ".col.header.culture {background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/culture_25x25.png);margin: 0 auto}" +
        ".col.header.attackin {background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/attack_in.png);margin: 0 auto}" +
        ".col.header.attackout {background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/attack_out.png);margin: 0 auto}" +
        ".game_list li:hover {background-image: url(https://gpnl.innogamescdn.com/images/game/border/brown.png)}" +
        ".towns_overview_inner_unit_table,#bot_townsoverview .towns_overview_inner_unit_table td {border-style: none}" +
        ".towns_overview_inner_unit_table div {width: 25px}" +
        ".no_construction {width: 16px;height: 16px;background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/gears_inactive.png);margin: 0 auto}" +
        ".construction {width: 16px;height: 16px;background-image: url(https://gpnl.innogamescdn.com/images/game/overviews/gears.gif);margin: 0 auto}" +
        ".celebration {float: left;width: 16px;height: 16px;cursor: pointer}" +
        ".celebration.party {background: url(https://gpnl.innogamescdn.com/images/game/overviews/celebration_bg_new.png) 0px -110px no-repeat;margin: 0 auto}" +
        ".celebration.games {background: url(https://gpnl.innogamescdn.com/images/game/overviews/celebration_bg_new.png) 0px -140px no-repeat;margin: 0 auto}" +
        ".celebration.triumph {background: url(https://gpnl.innogamescdn.com/images/game/overviews/celebration_bg_new.png) 0px -201px no-repeat;margin: 0 auto}" +
        ".celebration.theater {background: url(https://gpnl.innogamescdn.com/images/game/overviews/celebration_bg_new.png) 0px -170px no-repeat;margin: 0 auto}" +
        ".toolbar_activities .middle .activity.culture .icon, .toolbar_activities .middle .activity.culture .icon.active {background: url(https://raw.githubusercontent.com/alexandre2311/rex1/master/images/items-sprite.png) no-repeat -30px -1px; width: 26px;height: 27px;}" +
        ".aphrodite_festival {background: url(https://gpen.innogamescdn.com/images/game/autogenerated/powers/powers_30x30_part2_b8a6ca8.png) no-repeat -690px -30px;margin: 0 auto;width: 30px;height: 30px;}" +
        "</style>"
    ).appendTo("head");

    Culture.addButton();

    // Create Window Type
    UI.createWindowType("TR_CULTURE", "Culture", 680, 605, true, ["center", "center", 100, 100]);
    //Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON).setHeight('800');
  },
  checkReady: function (town: any) {
    var _town = window.ITowns.towns[town.id];
    if (_town.hasConqueror()) {
      return false;
    }

    if (!ModuleHub.modules.Culture.isOn) {
      return false;
    }

    if (town.modules.Culture.isReadyTime >= window.Timestamp.now()) {
      return town.modules.Culture.isReadyTime;
    }
    if (window.tr.culture[town.id] !== undefined && ((window.tr.culture[town.id].party && Culture.checkAvailable(town.id).party) || (window.tr.culture[town.id].triumph && Culture.checkAvailable(town.id).triumph) || (window.tr.culture[town.id].theater && Culture.checkAvailable(town.id).theater))) {
      return true;
    }
    return false;
  },
  addButton: function () {
    $('<div id="tr_culture_divider" class="divider"></div><div id="tr_culture_button" class="activity_wrap"><div class="activity culture"><div class="hover_state"><div class="icon"></div></div></div><div class="ui_highlight" data-type="bubble_menu"></div></div>').appendTo(".tb_activities.toolbar_activities .middle");

    $("#tr_culture_button").on("click", function () {
      if (!window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_CULTURE)) {
        Culture.openWindow();
      } else {
        Culture.closeWindow();
      }
    });
  },
  openWindow: function () {
    var content =
      '<div id="tr_culture" style="margin-bottom:5px; font-style:italic;"><div class="game_border_top"></div><div class="game_border_bottom"></div><div class="game_border_left"></div><div class="game_border_right"></div><div class="game_border_corner corner1"></div><div class="game_border_corner corner2"></div><div class="game_border_corner corner3"></div><div class="game_border_corner corner4"></div><div class="box_content"></div></div>';

    window.Layout.wnd.Create(window.GPWindowMgr.TYPE_TR_CULTURE).setContent(content);
    Culture.addContent();

    // Close button event - uncheck available units button
    /*if (typeof window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_CULTURE).getJQCloseButton() != 'undefined') {
      //window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_CULTURE).getJQCloseButton().get(0).onclick = function () {
          $('#tr_settings_button').removeClass("checked");
          $('.ico_comparison').get(0).style.marginTop = "5px";
      //};
      };*/
  },
  closeWindow: function () {
    window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_CULTURE).close();
  },
  addContent: function () {
    var content = '<ul class="game_list"><li class="even">';
    content += '<div class="towninfo small tag_header col w50" id="header_town"></div>';
    content += '<div class="towninfo small tag_header col w40" id="header_wood"><div class="col header wood"></div></div>';
    content += '<div class="towninfo small tag_header col w40" id="header_stone"><div class="col header stone"></div></div>';
    content += '<div class="towninfo small tag_header col w40" id="header_iron"><div class="col header iron"></div></div>';
    content += '<div class="towninfo small tag_header col w50" id="header_party"><div class="col header celebration party"></div></div>';
    content += '<div class="towninfo small tag_header col w50" id="header_triumph"><div class="col header celebration triumph"></div></div>';
    content += '<div class="towninfo small tag_header col w50" id="header_theater"><div class="col header celebration theater"></div></div>';
    content += '</li></ul><div id="culture_table_wrapper">';
    content += '<ul class="game_list scroll_content">';
    var i = 0;

    var towns: [string, any][] = Object.entries(window.ITowns.getTowns());
    for (const townE of towns) {
      var town: any = townE[1];
      if (!window.tr.culture.hasOwnProperty(town.id))
        window.tr.culture[town.id] = {
          party: false,
          triumph: false,
          theater: false
        };
      var resources = town.resources();
      content += '<li class="' + (i % 2 ? "even" : "odd") + ' bottom" id="ov_town_' + town.id + '">';
      content += '<div class="towninfo small townsoverview col w50">';
      content += '<div class="town_name_culture">';
      content += '<span style="display: inline-flex;align-items: center;gap: 2px;">';
      if (window.tr.autoTownTypes.hasOwnProperty(town.id)) {
        content += '<div style="background: url(https://dio-david1327.github.io/img/dio/btn/town-icons.png) repeat;background-position: ' + TownIcons.types[window.tr.manuTownTypes[town.id] || window.tr.autoTownTypes[town.id]] * -25 + 'px -27px;height: 18px;width:18px;"></div>';
      }
      content += '<a href="#' + town.getLinkFragment() + '" class="gp_town_link">' + town.name + "</a></span>";
      content += "<span>(" + town.getPoints() + " Ptn.)</span>";
      content += "</div></div>";
      content += '<div class="towninfo small townsoverview col w40">';
      content += '<div class="wood' + (resources.wood == resources.storage ? " town_storage_full" : "") + '">';
      content += resources.wood;
      content += "</div>";
      content += "</div>";
      content += '<div class="towninfo small townsoverview col w40">';
      content += '<div class="stone' + (resources.stone == resources.storage ? " town_storage_full" : "") + '">';
      content += resources.stone;
      content += "</div>";
      content += "</div>";
      content += '<div class="towninfo small townsoverview col w40">';
      content += '<div class="iron' + (resources.iron == resources.storage ? " town_storage_full" : "") + '">';
      content += resources.iron;
      content += "</div>";
      content += "</div>";
      content += '<div class="towninfo small townsoverview col w50">';
      content += '<div class="culture_party_row" id="culture_party_' + town.id + '">';
      content += "</div>";
      content += "</div>";
      content += '<div class="towninfo small townsoverview col w50">';
      content += '<div class="culture_triumph_row" id="culture_triumph_' + town.id + '">';
      content += "</div>";
      content += "</div>";
      content += '<div class="towninfo small townsoverview col w50">';
      content += '<div class="culture_theater_row" id="culture_theater_' + town.id + '">';
      content += "</div>";
      content += "</div>";
      content += "</li>";
      i++;
    }
    content += "</ul></div>";
    content += '<div class="game_list_footer">';
    content += '<div style="display: inline-flex; align-items: center; gap: 7px; float: left;" id="header_aphrodite_festival"><div class="aphrodite_festival"></div><div class="cbx_caption">Enable Aphrodite\'s Charitable Festival</div></div>';
    content += "</div>";

    $(".header_party .celebration").mousePopup(new MousePopup('<div class="dio_icon b"></div> City Festivals'));
    $(".header_triumph .celebration").mousePopup(new MousePopup('<div class="dio_icon b"></div> Victory procession'));
    $(".header_theater .celebration").mousePopup(new MousePopup('<div class="dio_icon b"></div> Theater Plays'));

    let tr_storage = new TrStorage();
    var ct = $(content);
    for (const town of towns) {
      var townId = town[1].id;
      // var availability = Culture.checkAvailable(townId);
      let ui_checkboxes = [];

      ui_checkboxes.push(
        UI.checkbox({
          id: "clt_party_" + townId,
          name: "clt_party_" + townId,
          checked: window.tr.culture[townId].party != undefined ? window.tr.culture[townId].party : false
          //disabled: !availability.party
        }).appendTo(ct.find("#culture_party_" + townId))
      );

      ui_checkboxes.push(
        UI.checkbox({
          id: "clt_triumph_" + townId,
          name: "clt_triumph_" + townId,
          checked: window.tr.culture[townId].triumph != undefined ? window.tr.culture[townId].triumph : false
          // disabled: !availability.triumph
        }).appendTo(ct.find("#culture_triumph_" + townId))
      );

      ui_checkboxes.push(
        UI.checkbox({
          id: "clt_theater_" + townId,
          name: "clt_theater_" + townId,
          checked: window.tr.culture[townId].theater != undefined ? window.tr.culture[townId].theater : false
          //  disabled: !availability.theater
        }).appendTo(ct.find("#culture_theater_" + townId))
      );
    }

    $(ct).appendTo("#tr_culture .box_content");

    $("#culture_table_wrapper .checkbox_new").click(function () {
      // @ts-ignore
      let idArray = this.id.split("_");
      // @ts-ignore
      if ($(this).hasClass("checked")) {
        // @ts-ignore
        window.tr.culture[parseInt(idArray[2])][idArray[1]] = false;
        tr_storage.save("culture", window.tr.culture);

        // @ts-ignore
        $(this).removeClass("checked");
      } else {
        // @ts-ignore
        window.tr.culture[parseInt(idArray[2])][idArray[1]] = true;
        tr_storage.save("culture", window.tr.culture);

        // @ts-ignore
        $(this).toggleClass("checked");
      }
    });
  },
  checkAvailable: function (townId: number) {
    var cultureOptions = {
      party: false,
      triumph: false,
      theater: false
    };

    var buildings = window.ITowns.towns[townId].buildings().attributes;

    var resources = window.ITowns.towns[townId].resources();
    if (buildings.academy >= 30 && resources.wood >= 15000 && resources.stone >= 18000 && resources.iron >= 15000) {
      cultureOptions.party = true;
    }
    if (buildings.theater == 1 && resources.wood >= 10000 && resources.stone >= 12000 && resources.iron >= 10000) {
      cultureOptions.theater = true;
    }
    if (window.MM.getModelByNameAndPlayerId("PlayerKillpoints").getUnusedPoints() >= 300) {
      cultureOptions.triumph = true;
    }
    return cultureOptions;
  },
  startCulture: function (town: any) {
    if (!Culture.checkEnabled()) {
      return false;
    }
    Culture.town = town;
    Culture.iTown = window.ITowns.towns[Culture.town.id];

    if (ModuleHub.currentTown != Culture.town.key) {
      console.log(Culture.town.name + " move to town.", 2);
      ModuleHub.currentTown = Culture.town.key;
    }
    window.HelperTown.townSwitch(Culture.town.id);
    Culture.start();
  },
  start: function () {
    if (!Culture.checkEnabled()) {
      return false;
    }

    Culture.interval = setTimeout(function () {
      if (window.tr.culture[Culture.town.id] !== undefined) {
        console.log(Culture.town.name + " getting event information.", 2);
        window.PlaceWindowFactory.openPlaceWindow("culture");
        window.PlaceWindowFactory.createBuildingPlaceWindow();

        if (!Culture.checkEnabled()) {
          return false;
        }

        var boxes = [
          {
            name: "triumph",
            waiting: 19200,
            element: $("#place_triumph")
          },
          {
            name: "party",
            waiting: 57600,
            element: $("#place_party")
          },
          {
            name: "theater",
            waiting: 285120,
            element: $("#place_theater")
          }
        ];

        var ready = false;
        var index = 0;
        var next_ready = 300;

        var checkNextBox = function (box: any) {
          console.log(Culture.town.name, index, ready, Culture.checkAvailable(Culture.town.id), box);
          if (index >= 3) {
            if (!ready) {
              console.log(Culture.town.name + " not ready yet.", 2);
            }
            Culture.finished(next_ready);
            return false;
          }

          if (box.name == "triumph" && (!window.tr.culture[Culture.town.id].triumph || !Culture.checkAvailable(Culture.town.id).triumph || window.MM.getModelByNameAndPlayerId("PlayerKillpoints").getUnusedPoints() < 300)) {
            console.log("a");
            index++;
            checkNextBox(boxes[index]);
            return false;
          } else if (box.name == "party" && (!window.tr.culture[Culture.town.id].party || !Culture.checkAvailable(Culture.town.id).party)) {
            console.log("ab");
            index++;
            checkNextBox(boxes[index]);
            return false;
          } else if (box.name == "theater" && (!window.tr.culture[Culture.town.id].theater || !Culture.checkAvailable(Culture.town.id).theater)) {
            console.log("ac");
            index++;
            checkNextBox(boxes[index]);
            return false;
          }

          if (box.element.find("#countdown_" + box.name).length) {
            console.log("ad");
            var timeInSeconds = HelperFunctions.timeToSeconds(box.element.find("#countdown_" + box.name).html());
            if (next_ready == 300) {
              next_ready = timeInSeconds;
            } else if (next_ready > timeInSeconds) {
              next_ready = timeInSeconds;
            }
            index++;
            checkNextBox(boxes[index]);
            return false;
          } else if (box.element.find(".button_new").hasClass("disabled")) {
            console.log("aee");
            index++;
            checkNextBox(boxes[index]);
            return false;
          } else if (!box.element.find(".button_new").hasClass("disabled")) {
            console.log("affff");
            Culture.interval = setTimeout(function () {
              ready = true;
              Culture.startCelebration(box, function (time) {
                if (next_ready == 300) {
                  next_ready = time;
                } else if (next_ready >= time) {
                  next_ready = time;
                }
                index++;
                checkNextBox(boxes[index]);
              });
            }, (index + 1) * HelperFunctions.randomize(1000, 2000));
            return false;
          }
          index++;
          checkNextBox(boxes[index]);
        };

        checkNextBox(boxes[index]);
      }
    }, HelperFunctions.randomize(2000, 4000));
  },
  startCelebration: function (box, callback) {
    if (!Culture.checkEnabled()) {
      return false;
    }

    window.BuildingPlace.startCelebration(box.name);
    var timeRemaining = 0;

    if (Culture.town.id == window.Game.townId) {
      var buildingWindows = window.GPWindowMgr.getByType(window.GPWindowMgr.TYPE_BUILDING_PLACE);
      for (var i = 0; i < buildingWindows.length; i++) {
        buildingWindows[i].getHandler().refresh();
      }
    }

    timeRemaining = box.waiting;
    callback(timeRemaining);
  },
  stop: function () {
    clearInterval(Culture.interval);
  },
  finished: function (next_ready: any) {
    if (!Culture.checkEnabled()) {
      return false;
    }
    Culture.town.modules.Culture.isReadyTime = Timestamp.now() + next_ready;
    ModuleHub.Queue.next();
  },
  checkEnabled: function () {
    return ModuleHub.modules.Culture.isOn;
  },
  deactivate: function () {
    $("#tr_culture_button").remove();
    $("#tr_culture_divider").remove();
  }
};

export default Culture;
