/*******************************************************************************************************************************
 * Automated Window
 *******************************************************************************************************************************/

import Log from "../utils/log";

var AutoWindow = {
  isOpen(fn: WindowType): boolean {
    if (fn == "academy") {
      return document.querySelector(".classic_window.academy") == null ? false : true;
    } else if (fn == "senate") {
      return document.querySelector(".main_window_background") == null ? false : true;
    } else if (fn == "hide") {
      return document.querySelector(".classic_window.hide") == null ? false : true;
    } else {
      throw new Error("Nom inconnu en paramètre de la fonction 'isOpen'.");
    }
  },

  open(fn: WindowType) {
    let cpt = 0;

    // Académie
    if (fn == "academy") {
      window.tr.automation.windows.academy.auto = false;
      window.AcademyWindowFactory.openAcademyWindow();

      // On regarde si la fenêtre s'ouvre
      let _wait = setInterval(function () {
        const res = document.querySelector(".window_curtain .academy");
        cpt++;

        // Si la fenêtre s'ouvre
        if (res != null) {
          clearInterval(_wait);
          window.tr.automation.windows.academy.auto = true;
          Log.log("WINDOW", `<span class="debut">🔆 Academy Window opened</span>`);
        }

        // Si 10 tentatives sans succés
        else if (cpt >= 10) {
          clearInterval(_wait);
          window.tr.automation.windows.academy.error = true;
          Log.log("WINDOW", `<span class="error">⚠ Cannot detect Academy Window!</span>`);
        }
      }, window.tr.automation.windows.timeVerif);
    }

    // Sénat
    else if (fn == "senate") {
      window.tr.automation.windows.senate.auto = false;

      window.MainWindowFactory.openMainWindow();
      let _wait = setInterval(function () {
        const res = document.querySelector(".main_window_background");
        cpt++;

        // Si la fenêtre s'ouvre
        if (res != null) {
          clearInterval(_wait);
          // On ajoute un id pour retrouver le button close de la fenêtre
          // @ts-ignore
          res.parentElement.parentElement.parentElement.id = "_senate";
          window.tr.automation.windows.senate.auto = true;
          Log.log("WINDOW", `<span class="debut">🔆 Senate Window opened</span>`);
        }

        // Si 10 tentatives sans succés
        else if (cpt >= 10) {
          clearInterval(_wait);
          window.tr.automation.windows.senate.error = true;
          Log.log("WINDOW", `<span class="error">⚠ Cannot detect Senate Window!</span>`);
        }
      }, window.tr.automation.windows.timeVerif);
    } else if (fn == "hide") {
      window.tr.automation.windows.hide.auto = false;
      window.HideWindowFactory.openHideWindow();

      // On regarde si la fenêtre s'ouvre
      let _wait = setInterval(function () {
        const res = document.querySelector(".window_curtain .hide");
        cpt++;

        // Si la fenêtre s'ouvre
        if (res != null) {
          clearInterval(_wait);
          window.tr.automation.windows.hide.auto = true;
          Log.log("WINDOW", `<span class="debut">🔆 Hide Window opened</span>`);
        }

        // Si 10 tentatives sans succés
        else if (cpt >= 10) {
          clearInterval(_wait);
          window.tr.automation.windows.hide.error = true;
          Log.log("WINDOW", `<span class="error">⚠ Cannot detect Hide Window!</span>`);
        }
      }, window.tr.automation.windows.timeVerif);
    }

    // Si aucun
    else {
      throw new Error("Error in AutoWindow: Parameter Error.");
    }
  },

  resetFn(fn: WindowType) {
    window.tr.automation.windows[fn].error = false;
    window.tr.automation.windows[fn].auto = false;
    window.tr.automation.windows[fn].finished.free = true;
    window.tr.automation.windows[fn].nbEvent = 0;
  },

  close(fn: WindowType) {
    // Académie
    if (fn == "academy") {
      const res: HTMLElement | null = document.querySelector(".academy .close");
      if (res != null) {
        res?.click();
        Log.log("WINDOW", `<span class="fin">🔻 Academy Window Closed.</span>`);
      }
      window.tr.automation.windows.academy.auto = false;
    }

    // Sénat
    else if (fn == "senate") {
      const res: HTMLElement | null = document.querySelector("#_senate .ui-dialog-titlebar-close");
      if (res != null) {
        res?.click();
        Log.log("WINDOW", `<span class="fin">🔻 Senate Window Closed.</span>`);
      }
      window.tr.automation.windows.senate.auto = false;
    }

    // Hide
    else if (fn == "hide") {
      const res: HTMLElement | null = document.querySelector("#_hide .ui-dialog-titlebar-close");
      if (res != null) {
        res?.click();
        Log.log("WINDOW", `<span class="fin">🔻 Hide Window Closed.</span>`);
      }
      window.tr.automation.windows.hide.auto = false;
    }

    // Si aucun
    else {
      throw new Error("Error in AutoWindow: Parameter Error.");
    }
  }
};

export default AutoWindow;
