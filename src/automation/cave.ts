/*******************************************************************************************************************************
 * Auto Cave
 *******************************************************************************************************************************/

import AutoWindow from "./windows";

var Caving = {
  cavingObserve: new MutationObserver(function (mutationsList, observer) {
    if (window.tr.settings.tr_cave) {
      console.log("ON", (window.ITowns.towns[window.Game.townId].resources().iron / window.ITowns.towns[window.Game.townId].resources().storage) * 100, window.tr.settings.tr_cave_percentage);

      if ((window.ITowns.towns[window.Game.townId].resources().iron / window.ITowns.towns[window.Game.townId].resources().storage) * 100 >= window.tr.settings.tr_cave_percentage) {
        console.log("if");
        Caving.caveSilver("hide");
      }
    }
  }),
  activate: function () {
    var silver_el = $(".amount.ui-game-selectable").get(2);

    // ! sometimes it catches event
    //$.Observer(window.GameEvents.town.resources.update).subscribe('AUTO_CAVE', function (e, data) {

    Caving.cavingObserve.observe(silver_el, {
      characterData: false,
      childList: true,
      attributes: false
    });
  },
  refresh_silver_total: function (xhr: { responseText: string }) {
    var JQ_silver_total = $("#dio_hides_silver_total .silver_amount");
    var silver_total = parseInt(JQ_silver_total.text());
    var silver_stored = JSON.parse(xhr.responseText).json.iron_stored;
    silver_total += silver_stored;
    JQ_silver_total.text(silver_total);
  },
  caveSilver: function (fn: WindowType) {
    if (!AutoWindow.isOpen(fn)) {
      AutoWindow.open(fn);
    } else {
      window.tr.automation.windows[fn].auto = true;
    }
    var timeout;
    // Attente fenêtre opene puis recherche
    let _search = setInterval(function () {
      if (window.tr.automation.windows[fn].auto) {
        clearInterval(_search);
        console.log("CAVIN WINDOW ON");
        timeout = setInterval(() => {
          if ($("#hide_espionage").length || $("#hides_overview_wrapper").length) {
            // @ts-ignore
            if ($("#hide_espionage").length & !$("#tr_hidesIndexIron").get(0)) {
              // @ts-ignore
              if (!$("#tr_hidesIndexIron").is(":visible") & "#hide_espionage".length) {
                $('<div id="tr_hidesIndexIron" style="width: 100px; height: 33px; position: absolute; left: 22px; top: 170px;"></div>').appendTo("#hide_espionage");
              }

              var b = window.ITowns.towns[window.Game.townId].resources().iron;
              if (b > (b / 100) * window.tr.settings.tr_cave_store_percentage) {
                $("#hide_espionage :input").val((b / 100) * window.tr.settings.tr_cave_store_percentage);
                setTimeout(function () {
                  $("#hide_espionage :input").select().blur();
                }, 10);
              } else {
                $("#hide_espionage :input").val("0");
                setTimeout(function () {
                  $("#hide_espionage :input").select().blur();
                }, 10);
              }
            }

            //$("#hide_espionage .textbox.order_input input").get(0).value = (window.ITowns.towns[window.Game.townId].resources().iron / 100) * window.tr.settings.tr_cave_store_percentage
          }
        }, 80);

        $("#hide_espionage .order_confirm.button_new.square")?.get(0)?.click();
        clearInterval(timeout);
      }

      // Si error sur la fenêtre
      else if (window.tr.automation.windows[fn].error) {
        clearInterval(_search);
        if (++window.tr.automation.windows[fn].nbEvent >= 2) {
          AutoWindow.resetFn(fn);
        }
      }
    }, window.tr.automation.windows.timeVerif);

    // Attente recherche finishede puis fermeture
    let _close = setInterval(function () {
      if (window.tr.automation.windows[fn].finished.free) {
        clearInterval(_close);
        AutoWindow.close(fn);
      }

      // Si error sur la fenêtre
      else if (window.tr.automation.windows[fn].error) {
        clearInterval(_close);
        if (++window.tr.automation.windows[fn].nbEvent >= 2) {
          AutoWindow.resetFn(fn);
        }
      }
    }, window.tr.automation.windows.timeVerif);

    // À la fin il reste finished.free à TRUE et .auto à FALSE
  },
  deactivate: function () {
    Caving.cavingObserve.disconnect();
  }
};

export default Caving;
