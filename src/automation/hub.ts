/*******************************************************************************************************************************
 * ModuleHub
 *******************************************************************************************************************************/
// @ts-nocheck

import Science from "../misc/science";
import Settings from "../settings";
import HelperFunctions from "../utils/helpers";
import Log from "../utils/log";
import UI from "../utils/ui";
import Caving from "./cave";
import Culture from "./culture";
import Farming from "./farming";
import Queues from "./queues";

const ModuleHub = {
  models: {
    Town: function () {
      this.key = null;
      this.id = null;
      this.name = null;
      this.farmTowns = {};
      this.relatedTowns = [];
      this.currentFarmCount = 0;
      this.modules = {
        Queues: {
          isReadyTime: 0
        },
        Culture: {
          isReadyTime: 0
        }
      };

      this.startQueues = function () {
        Queues.startBuild(this);
      };

      this.startSpying = function () {
        Caving.start(this);
      };

      this.startCulture = function () {
        Culture.startCulture(this);
      };

      this.startScience = function () {
        Science.start(this);
      };
    }
  },
  Queue: {
    total: 0,
    queue: [],
    /**
     * Add item to the queue
     * @param {Item addded to queue} _item
     */
    add: function (_item: any) {
      this.total++;
      this.queue.push(_item);
    },
    /**
     * Start the queue
     */
    start: function () {
      this.next();
    },
    /**
     * Stop the queue
     */
    stop: function () {
      this.queue = [];
      console.log("debug stop", ModuleHub);
    },
    isRunning: function () {
      console.log("isrunning", this.queue.length, this.total, this.queue.length > 0 || this.total > 0);
      return this.queue.length > 0 || this.total > 0;
    },
    /**
     * Execute the next item in queue
     */
    next: function () {
      console.log("debug next 1", ModuleHub);
      ModuleHub.updateTimer();
      console.log("debug next 2", ModuleHub);
      var _nextQueueItem = this.queue.shift();
      console.log("debug next 3", ModuleHub);
      // if this is not null, execute it
      if (_nextQueueItem) {
        _nextQueueItem.fx();
        //Queue is empty
      } else {
        if (this.queue.length <= 0) {
          this.total = 0;
          ModuleHub.finished();
        }
      }
      console.log("debug next 4", ModuleHub);
    }
  },
  currentTown: null,
  playerTowns: [],
  /**
   * ID of the interval timing the next cycle
   */
  interval: false,
  modules: {
    Queues: {
      isOn: false
    },
    Culture: {
      isOn: false
    },
    Farming: {
      isOn: false
    }
  },
  /**
   * Initilize the Builder feature
   */
  init: function () {
    $(
      "<style id='modulehub'>" +
        ".nui_bot_toolbox {position: absolute;top: 276px;z-index: 10;left: 0;}" +
        ".nui_bot_toolbox .bot_menu {background: url(https://raw.githubusercontent.com/alexandre2311/rex1/master/images/toolbox.png) no-repeat;width: 145px;height: 25px}" +
        ".nui_bot_toolbox .bot_menu ul {padding: 1px 7px 0 3px;list-style: none;display: block}" +
        ".nui_bot_toolbox .bot_menu ul li {float: left;width: 24px;height: 24px;padding-right: 3px}" +
        ".nui_bot_toolbox .bot_menu ul li:hover {background: url(https://raw.githubusercontent.com/alexandre2311/rex1/master/images/iconhover.png) no-repeat}" +
        ".nui_bot_toolbox .bot_menu ul li span {background: url(https://raw.githubusercontent.com/alexandre2311/rex1/master/images/items-sprite.png) no-repeat;display: block !important;cursor: pointer;width: 24px !important;height: 24px !important}" +
        ".nui_bot_toolbox .bot_menu ul li span.autofarm {background-position: 0 0 !important;filter: grayscale(100%)}" +
        ".nui_bot_toolbox .bot_menu ul li.on span.autofarm {filter: none}" +
        ".nui_bot_toolbox .bot_menu ul li#Culture_onoff {margin: auto !important}" +
        ".nui_bot_toolbox .bot_menu ul li span.culture {background-position: -29px 0 !important;filter: grayscale(100%)}" +
        ".nui_bot_toolbox .bot_menu ul li.on span.culture {filter: none}" +
        ".nui_bot_toolbox .bot_menu ul li span.autobuild {background-position: -56px 0 !important;filter: grayscale(100%)}" +
        ".nui_bot_toolbox .bot_menu ul li.on span.autobuild {filter: none}" +
        ".nui_bot_toolbox .bot_menu ul li span.autoattack {padding: 0 !important;background-position: -83px 0 !important;filter: grayscale(100%)}" +
        ".nui_bot_toolbox .bot_menu ul li.on span.autoattack {filter: none}" +
        ".nui_bot_toolbox .bot_menu ul li span.botsettings {background-position: -109px 0 !important}" +
        ".nui_bot_toolbox .bot_menu ul li.disabled span {cursor: default}" +
        ".nui_bot_toolbox .bot_menu ul li.disabled:hover {background: 0 0}" +
        ".nui_bot_toolbox .time_row {/* display: none; */background: url(https://raw.githubusercontent.com/alexandre2311/rex1/master/images/toolbox.png) 0 -25px no-repeat;width: 134px;height: 25px;padding: 5px 8px 0 2px;}" +
        "#building_tasks_main.active .ui_various_orders {width: 736px}" +
        "#building_tasks_main.active .various_orders_queue .various_orders_content {width: 736px;overflow: hidden}" +
        ".construction_queue_order_container.active {overflow: hidden;width: 520px}" +
        ".ui_various_orders.type_building_queue {width: 778px}" +
        "#buildings .build {cursor: pointer}" +
        "#building_tasks_main.active .ui_various_orders .queued_building_order {margin-bottom: 3px}" +
        "#building_tasks_main.active .ui_various_orders .item_icon.build_queue," +
        "#unit_orders_queue.active .ui_various_orders .item_icon.build_queue," +
        ".construction_queue_order_container.active .ui_various_orders .item_icon.build_queue {filter: url('data:image/svg+xml;utf8,&lt;svg xmlns='http://www.w3.org/2000/svg'&gt;&lt;filter id='grayscale'&gt;&lt;feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/&gt;&lt;/filter&gt;&lt;/svg&gt;#grayscale');filter: gray;-webkit-filter: grayscale(100%)}" +
        "#building_tasks_main.active .ui_various_orders .queued_building_order .btn_cancel_order.build_queue {top: -47px;left: 24px}" +
        "#unit_orders_queue .ui_various_orders {width: 781px}" +
        "#unit_orders_queue .various_orders_queue .various_orders_content {width: 781px;overflow: hidden}" +
        "a#unit_order_addqueue {position: absolute;top: 60px;left: 10px;background-position: -542px 0}" +
        "a#unit_order_addqueue:hover {background-position: -542px -23px}" +
        "#map_towns .flag .alliance_name," +
        "#map_towns .flag .player_name," +
        "#map_towns .flag .town_name," +
        ".construction_queue_order_container.active .advisor_container {display: none}" +
        "#building_tasks_main.active .instant_buy .ui_various_orders .queued_building_order .btn_cancel_order {top: 0;left: 47px}" +
        "#building_tasks_main.active .ui_various_orders .empty_slot," +
        "#building_tasks_main.active .ui_various_orders.curator_not_active .empty_slot," +
        "#building_tasks_main.active .ui_various_orders.curator_not_active.one_order .empty_slot," +
        "#unit_orders_queue .ui_various_orders .empty_slot," +
        "#unit_orders_queue .ui_various_orders.curator_not_active .empty_slot," +
        "#unit_orders_queue .ui_various_orders.curator_not_active.one_order .empty_slot," +
        ".construction_queue_order_container.active .ui_various_orders .empty_slot," +
        ".construction_queue_order_container.active .ui_various_orders.curator_not_active .empty_slot," +
        ".construction_queue_order_container.active .ui_various_orders.curator_not_active.one_order .empty_slot {margin-left: 10px !important;margin-bottom: 3px;margin-right: 10px !important}" +
        "#building_tasks_main.active .build {cursor: pointer}" +
        "#map_towns .flag {text-shadow: 1px 1px 1px #000;font: 10px/12px verdana}" +
        "#map_towns .flag.active_alliance .alliance_name {position: absolute;top: -17px;left: -153px;width: 314px;color: #7ef;text-align: center;display: block}" +
        "#map_towns .flag.active_town .town_name {display: block;position: absolute;text-align: right;color: #fb0;right: 15px;white-space: nowrap;top: -2px}" +
        "#map_towns .flag.active_player .player_name {display: block;position: absolute;color: #fff;white-space: nowrap;text-align: left;left: 15px;top: -2px}" +
        ".attack_planner.attacks.attack_bot .game_border {margin: 3px}" +
        ".attack_planner.attacks.attack_bot .attacks_list {height: 227px}" +
        ".attack_planner.attack_bot .attacks_list .attack_type32x32 {top: 3px;left: 3px}" +
        ".attack_planner.attack_bot .attacks_list .arrow {top: 3px;left: 39px}" +
        ".attack_planner.attack_bot.attacks li {min-height: 38px}" +
        ".attack_planner.attack_bot .row1," +
        ".attack_planner.attack_bot .row2," +
        ".attack_planner.attack_bot .row3 {margin: 1px 0 0 53px;font-size: 10px}" +
        ".attack_planner.attack_bot .row2 span:hover {color: #893e0e;cursor: default}" +
        ".attack_planner.attack_bot .origin_town_units {padding: 3px 9px}" +
        ".attack_planner.attack_bot .row2.expired {color: #da2b00}" +
        ".attack_planner.attack_bot .attack_bot_timer {position: absolute;right: 20px;font-size: 10px;top: -2px;color: #da2b00}" +
        ".attack_planner.attack_bot .attack_bot_timer.success {color: #1b8e00}" +
        "</style>"
    ).appendTo("head");

    $(".nui_main_menu")["css"]("top", "282px");
    $("<div/>", {
      class: "nui_bot_toolbox"
    })
      ["append"](
        $("<div/>", {
          class: "bot_menu layout_main_sprite"
        })["append"](
          $("<ul/>")
            ["append"](
              $("<li/>", {
                id: "Farming_onoff",
                class: "disabled"
              })["append"](
                $("<span/>", {
                  class: "autofarm farm_town_status_0"
                })
              )
            )
            ["append"](
              $("<li/>", {
                id: "Culture_onoff",
                class: "disabled"
              })["append"](
                $("<span/>", {
                  class: "culture farm_town_status_0"
                })
              )
            )
            ["append"](
              $("<li/>", {
                id: "Queues_onoff",
                class: "disabled"
              })["append"](
                $("<span/>", {
                  class: "autobuild toolbar_activities_recruits"
                })
              )
            )
            ["append"](
              $("<li/>", {
                id: "Autoattack_onoff",
                class: "disabled"
              })["append"](
                $("<span/>", {
                  class: "autoattack sword_icon"
                })
              )
            )
            ["append"](
              $("<li/>")["append"](
                $("<span/>", {
                  href: "#",
                  class: "botsettings circle_button_settings"
                })
                  ["on"]("click", function () {
                    Settings.openWindow();
                  })
                  .mousePopup(new MousePopup(window.DM["getl10n"]("COMMON")["main_menu"]["settings"]))
              )
            )
        )
      )
      ["append"](
        $("<div/>", {
          id: "time_autobot",
          class: "time_row"
        })
      )
      ["append"](
        $("<div/>", {
          class: "bottom"
        })
      )
      ["insertAfter"](".nui_left_box");

    Queues.town_queues = window.tr.queue;

    ModuleHub.loadPlayerTowns();
    ModuleHub.initTimer();
    ModuleHub.initButtons("Queues");
    ModuleHub.initButtons("Culture");
    ModuleHub.initButtons("Farming");
  },

  initButtons: function (module: string) {
    var element = $("#" + module + "_onoff");
    element.removeClass("disabled");
    element.on("click", function (e: any) {
      e.preventDefault();

      if (ModuleHub.modules[module].isOn == true) {
        ModuleHub.modules[module].isOn = false;
        element.removeClass("on");

        element.find("span").mousePopup(new MousePopup("Start " + module));
        console.log(module + " is deactivated.", 0);
        if (module == "Farming") {
          //Farming.stop()
        } else {
          if (module == "Culture") {
            Culture.stop();
          } else {
            if (module == "Queues") {
              Queues.stop();
            } else {
              if (module == "Autoattack") {
                //Autoattack.stop()
              }
            }
          }
        }
      } else {
        if (ModuleHub.modules[module].isOn == false) {
          element.addClass("on");
          console.log(module + " is activated.", 0);

          element.find("span").mousePopup(new MousePopup("Stop " + module));

          ModuleHub.modules[module].isOn = true;
          //if (module == 'Autoattack') {
          //  Autoattack.start()
          //}
        }
      }
      if (module != "Autoattack") {
        ModuleHub.checkWhatToStart();
      }
    });

    element.find("span").mousePopup(new MousePopup("Start " + module));
  },
  /**
   * Start function to decide which feature should start or get the lowest timer to next start
   */
  start: function () {
    var _queueNotEmpty = false;

    var _nextTimestamp: number | null = null;
    $.each(ModuleHub.playerTowns, function (_each, _town) {
      //Farmer
      //if (typeof Farming !== "undefined") {
      //  var _readyStatus = Farming.checkReady(_town);
      //  if (_readyStatus == true) {
      //    _queueNotEmpty = true;
      //    ModuleHub.Queue.add({
      //
      //      townId: _town.id,
      //      fx: function () {
      //
      //      _town.startFarming();
      //      },
      //    });
      //  } else {
      //    if (
      //
      //      _readyStatus != false &&
      //
      //      (_nextTimestamp == null || _readyStatus < _nextTimestamp)
      //    ) {
      //      _nextTimestamp = _readyStatus;
      //    }
      //  }
      //}
      //Philosopher
      if (typeof Culture !== "undefined") {
        var _readyStatus = Culture.checkReady(_town);
        console.log("Culture", _readyStatus);
        if (_readyStatus == true) {
          _queueNotEmpty = true;
          ModuleHub.Queue.add({
            townId: _town.id,
            fx: function () {
              _town.startCulture();
            }
          });
        } else {
          if (_readyStatus != false && (_nextTimestamp == null || _readyStatus < _nextTimestamp)) {
            _nextTimestamp = _readyStatus;
          }
        }
      }
      //Builder
      if (typeof Queues !== "undefined") {
        console.log(_town);
        var _readyStatus = Queues.checkReady(_town);
        console.log("Builder", _readyStatus);
        if (_readyStatus == true) {
          _queueNotEmpty = true;
          ModuleHub.Queue.add({
            townId: _town.id,
            fx: function () {
              _town.startQueues();
            }
          });
        } else {
          if (_readyStatus != false && (_nextTimestamp == null || _readyStatus < _nextTimestamp)) {
            _nextTimestamp = _readyStatus;
          }
        }
      }
    });
    if (_nextTimestamp === null && !_queueNotEmpty) {
      Log.log("Ready", "Nothing is ready yet!");
      ModuleHub.startTimer(30, function () {
        ModuleHub.start();
      });
    } else {
      if (!_queueNotEmpty) {
        var _nextInterval = _nextTimestamp - window.Timestamp.now() + 10;
        ModuleHub.startTimer(_nextInterval, function () {
          ModuleHub.start();
        });
      } else {
        ModuleHub.Queue.start();
      }
    }
  },
  /**
   * Stop the bot.
   */
  stop: function () {
    clearInterval(ModuleHub.interval);
    ModuleHub.Queue.stop();
    $("#time_autobot .caption .value_container .curr").html("Stopped");
  },
  /**
   * On finish the queue cycle, call the start function to get the next timer
   */
  finished: function () {
    ModuleHub.start();
  },
  /**
   * Put the "Start Autobot" text into the timer window
   */
  initTimer: function () {
    $(".nui_main_menu").css("top", "326px");
    $("#time_autobot")
      .append(
        UI.timerBoxSmall({
          id: "Farmer_timer",
          styles: "",
          text: "Start Bot"
        })
      )
      .show();
  },
  /**
   * Updates the timer for progress of actual queue
   */
  updateTimer: function (/*_0xa6b2xb, _0xa6b2xc*/) {
    var _progress = 0;
    /*if (typeof _0xa6b2xb !== 'undefined' && typeof _0xa6b2xc !== 'undefined') {*/
    //_0xa6b2xd = (((ModuleHub.Queuetotal - (ModuleHub.Queue.queue.length + 1)) + (_0xa6b2xc / _0xa6b2xb)) / ModuleHub['Queue']['total'] * 100)
    //} else {
    _progress = ((ModuleHub.Queue.total - ModuleHub.Queue.queue.length) / ModuleHub.Queue.total) * 100;
    //};
    if (!isNaN(_progress)) {
      $("#time_autobot .progress .indicator").width(_progress + "%");
      $("#time_autobot .caption .value_container .curr").html(Math.round(_progress) + "%");
    }
  },
  checkAutostart: function () {
    if (window.tr.settings.tr_autobuild_strt) {
      ModuleHub.modules.Queues.isOn = true;
      var el = $("#Queues_onoff");
      el.addClass("on");

      el.find("span").mousePopup(new MousePopup("Stop Queues"));
      ModuleHub.start();
    }
    if (window.tr.settings.tr_farmin) {
      ModuleHub.modules.Farming.isOn = true;
      var el = $("#Farming_onoff");
      el.addClass("on");

      el.find("span").mousePopup(new MousePopup("Stop Farming"));
      Farming.activate();
    }
  },
  /**
   * Timer that ticks every second to check if Queue has to start
   * @param {*} _interval
   * @param {Starts after the interval elapsed} _callback
   */

  startTimer: function (_interval: number, _callback: () => void) {
    var _ActInterval = _interval;

    ModuleHub.interval = setInterval(function () {
      $("#time_autobot .caption .value_container .curr").html(HelperFunctions.toHHMMSS(_interval));
      $("#time_autobot .progress .indicator").width(((_ActInterval - _interval) / _ActInterval) * 100 + "%");
      _interval--;
      if (_interval < 0) {
        clearInterval(ModuleHub.interval);
        _callback();
      }
    }, 1000);
  },
  checkWhatToStart: function () {
    var i = 0;
    $.each(ModuleHub.modules, function (idx, module) {
      console.log("loop ii", i);
      if (module.isOn && module != "Commander") {
        console.log("+++ ii", i);
        i++;
      }
    });
    if (i == 0) {
      console.log("whattostart - stop", i);
      ModuleHub.stop();
    } else {
      console.log("whattostart ii", i);
      console.log("whattostart", ModuleHub.Queue.isRunning());
      if (!ModuleHub.Queue.isRunning()) {
        clearInterval(ModuleHub.interval);
        ModuleHub.start();
      }
    }
  },
  loadPlayerTowns: function () {
    var i = 0;

    $.each(window.ITowns.towns, function (idx, city) {
      var _city = new ModuleHub.models.Town();
      _city.key = i;
      _city.id = city.id;
      _city.name = city.name;

      $.each(window.ITowns.towns, function (idx, _city2) {
        if (city.getIslandCoordinateX() == _city2.getIslandCoordinateX() && city.getIslandCoordinateY() == _city2.getIslandCoordinateY() && city.id != _city2.id) {
          _city.relatedTowns.push(_city2.id);
        }
      });

      ModuleHub.playerTowns.push(_city);
      i++;
    });

    ModuleHub.playerTowns.sort(function (city1, city2) {
      var _city1_name = city1.name,
        _city2_name = city2.name;
      if (_city1_name == _city2_name) {
        return 0;
      }
      return _city1_name > _city2_name ? 1 : -1;
    });
  }
};

export default ModuleHub;
