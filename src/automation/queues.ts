/*******************************************************************************************************************************
 * Queues Feature
 *******************************************************************************************************************************/

import { buildings } from "../constants";
import CityModel from "../misc/city_model";
import TrStorage from "../storage";
import HelperFunctions from "../utils/helpers";
import ModuleHub from "./hub";

var Queues: { [key: string]: any } = {
  building_queue: {},
  town_queues: [],
  town: null,
  iTown: null,
  interval: null,
  currentWindow: null,
  isCaptain: false,
  isCurator: false,
  Queue: 0,

  /**
   * Initilize Queues
   */
  init: function () {
    Queues.initFunction();
    Queues.checkCaptain();
    Queues.activateCss();
  },

  /**
   * Add css class to show Queues is active
   */
  activateCss: function () {
    $(".construction_queue_order_container").addClass("active");
  },

  initFunction: function () {
    var _renderQueue = (_originalRenderQueue: any) => {
      return function () {
        // @ts-ignore
        _originalRenderQueue.apply(this, arguments);
        // @ts-ignore
        if (this["$el"].selector == "#building_tasks_main .various_orders_queue .frame-content .various_orders_content" || this["$el"].selector == "#ui_box .ui_construction_queue .construction_queue_order_container") {
          // @ts-ignore
          Queues.initQueue(this.$el, "building");
        }

        // @ts-ignore
        if (this["$el"].selector == "#unit_orders_queue .js-researches-queue") {
          // @ts-ignore
          var queueUI = this["$el"].find(".ui_various_orders");
          if (queueUI.hasClass("barracks")) {
            // @ts-ignore
            Queues.initQueue(this.$el, "unit");
          } else {
            if (queueUI.hasClass("docks")) {
              // @ts-ignore
              Queues.initQueue(this.$el, "ship");
            }
          }
        }
      };
    };

    window.GameViews.ConstructionQueueBaseView.prototype.renderQueue = _renderQueue(window.GameViews.ConstructionQueueBaseView.prototype.renderQueue);
  },
  /**
   * Check if Captain is active: Set Queue length to 7.
   */
  checkCaptain: function () {
    if ($(".advisor_frame.captain div").hasClass("captain_active")) {
      Queues.isCaptain = true;
    }
    if ($(".advisor_frame.curator div").hasClass("curator_active")) {
      Queues.isCurator = true;
    }
    Queues.Queue = Queues.Curator ? 7 : 2;
  },
  /**
   * Check if current town can build
   * @param {Current Town} _town
   */
  checkReady: function (_town: any) {
    var _iTown = window.ITowns.towns[_town.id];
    //if Queues is off
    if (!ModuleHub.modules.Queues.isOn) {
      return false;
    }
    //Town has conqueror
    if (_iTown.hasConqueror()) {
      return false;
    }
    //nothing is enabled for town
    if (!window.tr.settings.tr_autobuild_building && !window.tr.settings.tr_autobuild_barracks && !window.tr.settings.tr_autobuild_harbor) {
      return false;
    }
    //is not ready
    if (_town.modules.Queues.isReadyTime >= window.Timestamp.now()) {
      return _town.modules.Queues.isReadyTime;
    }
    //is any bot queue not empty
    if (Queues.town_queues.filter((e: any) => e.town_id === _town.id).length > 0) {
      let current_town = Queues.town_queues.find((e: any) => e.town_id === _town.id);

      return current_town.building_queue.length > 0 || current_town.unit_queue.length > 0 || current_town.ship_queue.length > 0 || window.GameDataInstantBuy.isEnabled();
    }
    //Instant buy is enabled
    return window.GameDataInstantBuy.isEnabled();
  },

  /**
   * Start point for Queues cycle
   * @param {Town} _town
   */
  startBuild: function (_town: any) {
    //if not enabled
    if (!Queues.checkEnabled()) {
      return false;
    }
    Queues.town = _town;
    Queues.iTown = window.ITowns.towns[Queues.town.id];

    if (ModuleHub.currentTown != Queues.town.key) {
      console.log(Queues.town.name + " move to town.", 3);
      ModuleHub.currentTown = Queues.town.key;
    }
    window.HelperTown.townSwitch(Queues.town.id);
    Queues.startUpgrade();
  },

  /**
   * Check if a building is autocompletable
   */
  startUpgrade: function () {
    if (!Queues.checkEnabled()) {
      return false;
    }

    if (window.GameDataInstantBuy.isEnabled() && Queues.checkInstantComplete(Queues.town.id)) {
      window.HelperTown.townSwitch(Queues.town.id);

      //Timeout before instant buy
      Queues.interval = setTimeout(function () {
        //send request to instant buy town
        var index_params = {
          model_url: "BuildingOrder/" + Queues.instantBuyTown.order_id,
          action_name: "buyInstant",
          town_id: Queues.town.id,
          arguments: {
            order_id: Queues.instantBuyTown.order_id
          }
        };

        window.gpAjax.ajaxPost("frontend_bridge", "execute", index_params, false, function (_response: { success: string; error: string }) {
          if (_response.success) {
            //refresh queue
            if (Queues.town.id == window.Game.townId) {
              let _buildingWindows = window.GPWindowMgr.getByType(window.GPWindowMgr.TYPE_BUILDING_SENATE);
              for (let i = 0; _buildingWindows.length > i; i++) {
                _buildingWindows[i].getHandler().refresh();
              }
            }
            //if current town is not the selected town, sometimes the completed building remains in the queue
            if (window.MM.getModels().BuildingOrder[Queues.instantBuyTown.order_id]) {
              window.MM.removeModel(window.MM.getModels().BuildingOrder[Queues.instantBuyTown.order_id]);
            }

            console.log('<span style="color: #ffa03d;">' + HelperFunctions.capitalize(Queues.instantBuyTown.building_name) + " - " + _response.success + "</span>", 3);
          }

          if (_response.error) {
            console.log(Queues["town"]["name"] + " " + _response.error, 3);
          }
          //timeout before continuing queue
          Queues.interval = setTimeout(function () {
            Queues.instantBuyTown = false;
            Queues.startQueueing();
          }, HelperFunctions.randomize(500, 700));
        });
      }, HelperFunctions.randomize(1000, 2000));
      //if no instant buy is available or not activated
    } else {
      Queues.startQueueing();
    }
  },

  /**
   * Start to add items from bot queue to ingame queue
   */
  startQueueing: function () {
    if (!Queues.checkEnabled()) {
      return;
    }
    //If this town doesnt have any queues, finish
    if (Queues.town_queues.filter((e: any) => e.town_id === Queues.town.id).length <= 0) {
      Queues.finished();
      return;
    }

    //which to start next
    var _startNext = Queues.getReadyTime(Queues.town.id).shouldStart;
    switch (_startNext) {
      case "building":
        Queues.startBuildQueues();
        break;
      case "unit":
      case "ship":
        Queues.startBuildUnits(_startNext);
        break;
      default:
        Queues.finished();
        break;
    }
  },

  /**
   * Start to build buildings
   */
  startBuildQueues: function () {
    if (!Queues.checkEnabled()) {
      return false;
    }

    //check if town has building queues
    if (Queues.town_queues.filter((e: any) => e.town_id === Queues.town.id).length > 0) {
      let current_town_queue = Queues.town_queues.find((e: any) => e.town_id === Queues.town.id).building_queue;

      //if there is something to build
      if (current_town_queue.length > 0) {
        //timeout before build

        Queues.interval = setTimeout(function () {
          console.log(Queues.town.name + " getting building information.", 3);

          window.gpAjax.ajaxGet(
            "building_main",
            "index",
            {},
            false,

            function (_response_building_main: any) {
              if (Queues.hasFreeBuildingSlots(_response_building_main)) {
                var _firstBuilding = current_town_queue[0];
                //search the building in the response
                var _building_from_resp = Queues.getBuildings(_response_building_main)[_firstBuilding.item_name];
                //if the building is upgradeable
                if (_building_from_resp.can_upgrade) {
                  window.gpAjax.ajaxPost(
                    "frontend_bridge",
                    "execute",
                    {
                      model_url: "BuildingOrder",
                      action_name: "buildUp",
                      arguments: {
                        building_id: _firstBuilding.item_name
                      }
                    },
                    false,

                    function (_response_buildUp: { success: string; error: string }) {
                      if (_response_buildUp.success) {
                        if (Queues.town.id == window.Game.townId) {
                          let _buildingWindows = window.GPWindowMgr.getByType(window.GPWindowMgr.TYPE_BUILDING_SENATE);
                          for (let i = 0; _buildingWindows.length > i; i++) {
                            _buildingWindows[i].getHandler().refresh();
                          }
                        }
                        console.log('<span style="color: #ffa03d;">' + HelperFunctions.capitalize(_firstBuilding.item_name) + " - " + _response_buildUp.success + "</span>", 3);

                        Queues.saveBuilding({
                          type: "remove",
                          town_id: window.Game.townId,
                          item_id: _firstBuilding.id
                        });

                        $(".queue_id_" + _firstBuilding.id).remove();
                      }

                      if (_response_buildUp.error) {
                        console.log(Queues.town.name + " " + _response_buildUp.error, 3);
                      }
                      Queues.finished();
                    }
                  );
                } else {
                  if (!_building_from_resp.enough_population) {
                    console.log(Queues.town.name + " not enough population for " + _firstBuilding.item_name + ".", 3);
                    Queues.finished();
                  } else {
                    if (!_building_from_resp.enough_resources) {
                      console.log(Queues.town.name + " not enough resources for " + _firstBuilding.item_name + ".", 3);

                      Queues.finished();
                    } else {
                      console.log(Queues.town.name + " " + _firstBuilding.item_name + " can not be started due dependencies.", 3);

                      Queues.saveBuilding({
                        type: "remove",
                        town_id: window.Game.townId,
                        item_id: _firstBuilding.id
                      });

                      $(".queue_id_" + _firstBuilding.id).remove();
                      Queues.finished();
                    }
                  }
                }
              } else {
                console.log(Queues.town.name + " no free building slots available.", 3);
                Queues.finished();
              }
            }
          );
        }, HelperFunctions.randomize(1000, 2000));
      } else {
        Queues.finished();
      }
    } else {
      Queues.finished();
    }
  },

  /**
   * Start to build units (barracks or ships)
   * @param {Which queue to start} _queue
   */
  startBuildUnits: function (_queue: any) {
    if (!Queues.checkEnabled()) {
      return false;
    }
    //check if town has building queues

    if (Queues.town_queues.filter((e: any) => e.town_id === Queues.town.id).length > 0) {
      //get unit or ship queue
      let current_town_queue = Queues.town_queues.find((e: any) => e.town_id === Queues.town.id)[_queue + "_queue"];
      //if queue is not empty

      if (current_town_queue.length > 0) {
        var _firstQueueItem = current_town_queue[0];

        if (window.GameDataUnits.getMaxBuildForSingleUnit(_firstQueueItem.item_name) >= _firstQueueItem.count) {
          Queues.interval = setTimeout(function () {
            window.gpAjax.ajaxPost(
              "building_barracks",
              "build",
              {
                unit_id: _firstQueueItem.item_name,
                amount: _firstQueueItem.count,

                town_id: Queues.town.id,
                nl_init: true
              },
              false,

              function (_response: { error: string }) {
                if (_response.error) {
                  console.log(Queues.town.name + " " + _response.error, 3);
                } else {
                  //if successfull: remove the item from the bot queue

                  if (Queues.town.id == window.Game.townId) {
                    var _buildingWindows = window.GPWindowMgr.getByType(window.GPWindowMgr.TYPE_BUILDING_BARRACKS);
                    for (let i = 0; _buildingWindows.length > i; i++) {
                      _buildingWindows[i].getHandler().refresh();
                    }
                    _buildingWindows = window.GPWindowMgr.getByType(window.GPWindowMgr.TYPE_BUILDING_DOCKS);
                    for (let i = 0; _buildingWindows.length > i; i++) {
                      _buildingWindows[i].getHandler().refresh();
                    }
                    _buildingWindows = window.GPWindowMgr.getByType(window.GPWindowMgr.TYPE_BUILDING_SENATE);
                    for (let i = 0; _buildingWindows.length > i; i++) {
                      _buildingWindows[i].getHandler().refresh();
                    }
                  }
                  console.log('<span style="color: ' + (_queue == "unit" ? "#ffe03d" : "#3dadff") + ';">Units - ' + _firstQueueItem.count + " " + window.GameData.units[_firstQueueItem.item_name].name_plural + " added.</span>", 3);

                  Queues.saveUnits({
                    action: "remove",

                    town_id: window.Game.townId,
                    item_id: _firstQueueItem.id,
                    type: _queue
                  });

                  $(".queue_id_" + _firstQueueItem.id).remove();
                }
                Queues.finished();
              }
            );
          }, HelperFunctions.randomize(1000, 2000));
        } else {
          console.log(Queues.town.name + " recruiting " + _firstQueueItem.count + " " + window.GameData.units[_firstQueueItem.item_name].name_plural + " not ready.", 3);
          Queues.finished();
        }
      } else {
        Queues.finished();
      }
    } else {
      Queues.finished();
    }
  },

  /**
   * Calculate the next queue to build and the time left until the building should start
   * @param {ID of the town} _townId
   */
  getReadyTime: function (_townId: any) {
    var _queues: _queuesType = {
      building: {
        queue: [],
        timeLeft: 0
      },
      unit: {
        queue: [],
        timeLeft: 0
      },
      ship: {
        queue: [],
        timeLeft: 0
      }
    };

    if (window.MM.getModels().BuildingOrder) {
      $.each(window.MM.getModels().BuildingOrder, function (_index, _element) {
        if (_townId == _element.getTownId()) {
          _queues.building.queue.push({ type: "building", model: _element });

          if (_queues.building.timeLeft == 0) {
            _queues.building.timeLeft = _element.getTimeLeft();
          }
        }
      });
    }

    if (window.MM.getModels().UnitOrder) {
      $.each(window.MM.getModels().UnitOrder, function (_index, _element) {
        if (_townId == _element.attributes.town_id) {
          if (_element.attributes.kind == "ground") {
            _queues.unit.queue.push({ type: "unit", model: _element });

            if (_queues.unit.timeLeft == 0) {
              _queues.unit.timeLeft = _element.getTimeLeft();
            }
          }

          if (_element.attributes.kind == "naval") {
            _queues.ship.queue.push({ type: "ship", model: _element });
            if (_queues.ship.timeLeft == 0) {
              _queues.ship.timeLeft = _element.getTimeLeft();
            }
          }
        }
      });
    }
    var _readyTime = -1;
    var _doNext = "nothing";
    //check which bot queue has elements and take the one which has the lowest timeLeft
    $.each(_queues, function (_type, _model) {
      if (Queues.town_queues.filter((e: any) => e.town_id === _townId).length > 0) {
        let current_town = Queues.town_queues.find((e: any) => e.town_id === _townId);
        if ((_type == "building" && current_town.building_queue.length > 0) || (_type == "unit" && current_town.unit_queue.length > 0) || (_type == "ship" && current_town.ship_queue.length > 0)) {
          if (_readyTime == -1) {
            _readyTime = _model.timeLeft;
            _doNext = _type;
          } else {
            if (_model.timeLeft < _readyTime) {
              _readyTime = _model.timeLeft;
              _doNext = _type;
            }
          }
          //if there is space in the queue, start after the interval
          if (_queues[_type].queue.length < Queues.Queue) {
            _readyTime = +window.tr.settings.tr_autobuild_timer;
            _doNext = _type;
          }
        }
      }
    });
    //if instant buy is enabled

    if (window.GameDataInstantBuy.isEnabled() && window.tr.settings.tr_autobuild_instant) {
      //if there are buildings in the queue
      if (_queues.building.queue.length > 0) {
        let _firstBuildingTime = _queues.building.queue[0].model.getTimeLeft() - 300;
        if (_firstBuildingTime <= 0) {
          _firstBuildingTime = 0;
        }
        if (_firstBuildingTime < _readyTime || _firstBuildingTime <= +window.tr.settings.tr_autobuild_timer) {
          _readyTime = _firstBuildingTime;
        }
      }
    }
    return {
      readyTime: window.Timestamp.now() + (_readyTime >= 0 ? _readyTime : +window.tr.settings.tr_autobuild_timer),
      shouldStart: _doNext
    };
  },

  /**
   * Stop Building
   */
  stop: function () {
    clearInterval(Queues.interval);
  },

  /**
   * Check if Building is enabled
   */
  checkEnabled: function () {
    return ModuleHub.modules.Queues.isOn;
  },

  /**
   * Called on the end of the Building Queue.
   */
  finished: function () {
    if (!Queues.checkEnabled()) {
      return false;
    }

    Queues.town.modules.Queues.isReadyTime = Queues.getReadyTime(Queues.town.id).readyTime;
    ModuleHub.Queue.next();
  },

  /**
   * Check if a Building in the town has only 5 minutes left
   * @param {Town ID} _townId
   */
  checkInstantComplete: function (_townId: any) {
    Queues.instantBuyTown = false;

    $.each(window.MM.getModels().BuildingOrder, function (_index, _element) {
      if (_townId == _element.getTownId() && _element.getTimeLeft() < 300) {
        Queues.instantBuyTown = {
          order_id: _element.id,
          building_name: _element.getBuildingId()
        };
        return false;
      }
    });

    return Queues.instantBuyTown;
  },

  /**
   * Check if the building can be upgraded
   * @param {ID of the building} _buildingId
   * @param {iTown Object of the town} _iTown
   */
  checkBuildingDependencies: function (_buildingId: string, _iTown: { getBuildings: () => any }) {
    var _building = window.GameData.buildings[_buildingId],
      _AllDependencies = _building.dependencies,
      _iTownBuildings = _iTown.getBuildings(),
      _buildingsLevel = _iTownBuildings.getBuildings();

    var _dependencies: { building_id: string | number | symbol; level: any }[] = [];
    $.each(_AllDependencies, function (_index, _requiredLevel) {
      var _dependencieBuildingLevel = _buildingsLevel[_index];
      if (_dependencieBuildingLevel < _requiredLevel) {
        _dependencies.push({
          building_id: _index,
          level: _requiredLevel
        });
      }
    });

    return _dependencies;
  },

  /**
   * Update the buildingqueue.
   * @param {Object with Building Data} _building_data
   */
  saveBuilding: function (_building_data: { town_id: any; type: string; item_name: any; count: any; item_id: any }) {
    let newBuilding: { id: any; item_name: any; count: any };
    //if town doesnt exists in town_queues, add them
    if (Queues.town_queues.filter((e: any) => e.town_id === _building_data.town_id).length <= 0) {
      Queues.town_queues.push({
        town_id: _building_data.town_id,
        building_queue: [],
        unit_queue: [],
        ship_queue: []
      });
    }
    //Add new item to building queue
    if (_building_data.type === "add") {
      newBuilding = {
        id: window.Timestamp.now(),
        item_name: _building_data.item_name,
        count: _building_data.count
      };

      Queues.town_queues.find((e: any) => e.town_id === _building_data.town_id).building_queue.push(newBuilding);
    } else if (_building_data.type === "remove") {
      let current_town_queue = Queues.town_queues.find((e: any) => e.town_id === _building_data.town_id).building_queue;
      current_town_queue.splice(
        current_town_queue.findIndex((e: { id: any }) => e.id === _building_data.item_id),
        1
      );
    }

    $("#building_tasks_main .ui_various_orders, .construction_queue_order_container .ui_various_orders").each(function () {
      // @ts-ignore
      $(this).find(".empty_slot").remove();
      //Add new item to building queue
      if (_building_data.type === "add") {
        // @ts-ignore
        $(this).append(Queues.buildingElement(newBuilding));
      }
      // @ts-ignore
      Queues.setEmptyItems($(this));
    });
    window.tr.queue = Queues.town_queues;
    let tr_storage = new TrStorage();
    tr_storage.save("queue", window.tr.queue);
  },

  /**
   * Check if the main building has free slots
   * @param {Response} _buildingMainResponse
   */
  hasFreeBuildingSlots: function (_buildingMainResponse: { html: string } | undefined) {
    var _hasFreeSlots = false;
    if (_buildingMainResponse != undefined) {
      if (/BuildingMain\.full_queue = false;/g.test(_buildingMainResponse.html)) {
        _hasFreeSlots = true;
      }
    }
    return _hasFreeSlots;
  },

  /**
   * Get buildings from the main building respons.
   * @param {Response} _buildingMainResponse
   */
  getBuildings: function (_buildingMainResponse: { html: string | undefined }) {
    var _buildings = null;
    if (_buildingMainResponse.html != undefined) {
      var _match: RegExpMatchArray | null | undefined = _buildingMainResponse.html.match(/BuildingMain\.buildings = (.*);/g);
      if (_match && _match[0] != undefined) {
        _buildings = JSON.parse(_match[0].substring(25, _match[0].length - 1));
      }
    }
    return _buildings;
  },

  /**
   * Add the bot queue items to the queue
   * @param {JQuery Element to add the Items to} _jqueryElement
   * @param {building, unit or ship} _queueType
   */
  initQueue: function (_jqueryElement: { find: (arg0: string) => any }, _queueType: any) {
    var _guiQueue = _jqueryElement.find(".ui_various_orders");
    _guiQueue.find(".empty_slot").remove();

    switch (_queueType) {
      case "building":
        $("#building_tasks_main").addClass("active");

        if (Queues.town_queues.filter((e: any) => e.town_id == window.Game.townId).length > 0) {
          let current_town_queue = Queues.town_queues.find((e: any) => e.town_id == window.Game.townId).building_queue;
          $.each(current_town_queue, function (_index, _element) {
            _guiQueue.append(Queues.buildingElement(_element));
          });
        }
        break;
      case "unit":
        $("#unit_orders_queue").addClass("active");

        if (Queues.town_queues.filter((e: any) => e.town_id == window.Game.townId).length > 0) {
          let current_town_queue = Queues.town_queues.find((e: any) => e.town_id == window.Game.townId).unit_queue;
          $.each(current_town_queue, function (_index, _element) {
            _guiQueue.append(Queues.unitElement(_element, _queueType));
          });
        }
        break;
      case "ship":
        $("#unit_orders_queue").addClass("active");

        if (Queues.town_queues.filter((e: any) => e.town_id == window.Game.townId).length > 0) {
          let current_town_queue = Queues.town_queues.find((e: any) => e.town_id == window.Game.townId).ship_queue;
          $.each(current_town_queue, function (_index, _element) {
            _guiQueue.append(Queues.unitElement(_element, _queueType));
          });
        }
        break;
      default:
        break;
    }

    Queues.setEmptyItems(_guiQueue);

    _guiQueue.parent().mousewheel((_event: { preventDefault: () => void }, _speed: number) => {
      this.scrollLeft -= _speed * 30;
      _event.preventDefault();
    });
  },
  initUnitOrder: function (selectedUnit: any, type: any) {
    // Accessing unit details and interface elements
    var unitDetails = selectedUnit.units[selectedUnit.unit_id];
    var orderConfirmElement = selectedUnit.$el.find("#unit_order_confirm");
    var orderAddQueueElement = selectedUnit.$el.find("#unit_order_addqueue");
    var orderSliderElement = selectedUnit.$el.find("#unit_order_slider");

    // Checking missing dependencies and hiding elements accordingly
    if (orderAddQueueElement.length >= 0 && (unitDetails.missing_building_dependencies.length >= 1 || unitDetails.missing_research_dependencies.length >= 1)) {
      orderAddQueueElement.hide();
    }

    // Checking if there are no missing dependencies
    if (unitDetails.missing_building_dependencies.length == 0 && unitDetails.missing_research_dependencies.length == 0) {
      var playerTown = window.ITowns.towns[window.Game.townId];
      var maxBuildCount = unitDetails.max_build;

      var maxResourceCount = Math.max(unitDetails.resources.wood, unitDetails.resources.stone, unitDetails.resources.iron);
      var maxCounts = [];

      // Calculating maximum build counts based on available resources and population
      maxCounts.push(Math.floor(playerTown.getStorage() / maxResourceCount));
      maxCounts.push(Math.floor((playerTown.getAvailablePopulation() - Queues.checkPopulationBeingBuild()) / unitDetails.population));

      if (unitDetails.favor > 0) {
        maxCounts.push(Math.floor(500 / unitDetails.favor));
      }

      var maxPossibleCount = Math.min(...maxCounts);

      // Setting maximum units allowed in the slider
      if (maxPossibleCount > 0 && maxPossibleCount >= maxBuildCount) {
        selectedUnit.slider.setMax(maxPossibleCount);
      }

      // Handling the 'Add to queue' button functionality
      if (orderAddQueueElement.length == 0) {
        orderAddQueueElement = $("<a/>", {
          href: "#",
          id: "unit_order_addqueue",
          class: "confirm"
        });

        orderConfirmElement.after(orderAddQueueElement);

        orderAddQueueElement.mousePopup(new MousePopup("Add to recruit queue")).on("click", function (event: { preventDefault: () => void }) {
          event.preventDefault();
          Queues.addUnitQueueItem(unitDetails, type);
        });
      } else {
        orderAddQueueElement.unbind("click").on("click", function (event: { preventDefault: () => void }) {
          event.preventDefault();
          Queues.addUnitQueueItem(unitDetails, type);
        });
      }

      // Show/hide elements based on slider values and conditions
      if (maxPossibleCount <= 0) {
        orderAddQueueElement.hide();
      } else {
        orderAddQueueElement.show();
      }

      orderConfirmElement.show();

      // Slider functionality based on maximum build count
      orderSliderElement.slider({
        slide: function (event: any, ui: { value: number }) {
          console.log(123456789, "---", ui.value, maxBuildCount, maxPossibleCount, selectedUnit.slider.getValue(), selectedUnit.slider.getMax());
          let newValue = selectedUnit.slider.getValue() > maxBuildCount ? maxBuildCount : selectedUnit.slider.getValue();
          if (newValue > maxBuildCount) {
            orderConfirmElement.hide();
          } else {
            if (selectedUnit.slider.getValue() >= 0 && selectedUnit.slider.getValue() <= maxBuildCount) {
              orderConfirmElement.show();
            }
          }

          if (selectedUnit.slider.getValue() == 0) {
            orderAddQueueElement.hide();
          } else {
            if (selectedUnit.slider.getValue() > 0 && maxPossibleCount > 0) {
              orderAddQueueElement.show();
            }
          }
        }
      });
    }
  },
  checkBuildingLevel: function (_building: { [x: string]: any }) {
    var lvl = window.ITowns.towns[window.Game["townId"]].getBuildings().attributes[_building.item_name];

    $.each(window.ITowns.towns[window.Game.townId].buildingOrders().models, function (idx, building_item) {
      if (building_item.attributes.building_type == _building.item_name) {
        lvl++;
      }
    });

    if (Queues.town_queues.filter((e: any) => e.town_id == window.Game.townId).length > 0) {
      $.each(Queues.town_queues.find((e: any) => e.town_id === window.Game.townId).building_queue, function (idx, building_item) {
        if (building_item.id == _building.id) {
          return false;
        }
        if (building_item.item_name == _building.item_name) {
          lvl++;
        }
      });
    }
    lvl++;
    return lvl;
  },

  /**
   * Calculate the population being build in the unit and ship queue
   */
  checkPopulationBeingBuild: function () {
    var _count = 0;

    if (Queues.town_queues.filter((e: any) => e.town_id == window.Game.townId).length > 0) {
      let current_town = Queues.town_queues.find((e: any) => e.town_id == window.Game.townId);
      //unit queue

      $.each(current_town.unit_queue, function (_index, _element) {
        _count += _element.count * window.GameData.units[_element.item_name].population;
      });
      //ships queue

      $.each(current_town.ship_queue, function (_index, _element) {
        _count += _element.count * window.GameData.units[_element.item_name].population;
      });
    }
    return _count;
  },

  /**
   * Add unit or ship to the the queue
   * @param {Unit} _unit
   * @param {ship or unit} _type
   */

  addUnitQueueItem: function (_unit: { id: any }, _type: string) {
    Queues.saveUnits({
      action: "add",
      town_id: window.Game.townId,
      item_name: _unit.id,
      type: _type,
      count: _type == "ship" ? window.UnitOrder.docks_unit_order.slider.getValue() : window.UnitOrder.barracks_unit_order.slider.getValue()
    });
  },

  /**
   * Add or remove the Unit from the bot queue and render it
   * @param {Unit Data} _unitData
   */
  saveUnits: function (_unitData: any) {
    //if town doesnt exists in town_queues, add them
    if (Queues.town_queues.filter((e: any) => e.town_id === _unitData.town_id).length <= 0) {
      Queues.town_queues.push({
        town_id: _unitData.town_id,
        building_queue: [],
        unit_queue: [],
        ship_queue: []
      });
    }

    let newUnit: { id: any; item_name: any; count: any };
    //Add new item to unit queue
    if (_unitData.action === "add") {
      newUnit = {
        id: window.Timestamp.now(),
        item_name: _unitData.item_name,
        count: _unitData.count
      };

      Queues.town_queues.find((e: any) => e.town_id === _unitData.town_id)[_unitData.type + "_queue"].push(newUnit);
    } else if (_unitData.action === "remove") {
      let current_town_queue = Queues.town_queues.find((e: any) => e.town_id === _unitData.town_id)[_unitData.type + "_queue"];
      current_town_queue.splice(
        current_town_queue.findIndex((e: { id: any }) => e.id === _unitData.item_id),
        1
      );
    }

    let _alreadyAdded = false;
    $("#unit_orders_queue .ui_various_orders").each(function () {
      // @ts-ignore
      $(this).find(".empty_slot").remove();
      //Add new item to gui queue
      if (_unitData.action === "add" && !_alreadyAdded) {
        // @ts-ignore
        $(this).append(Queues.unitElement(newUnit, _unitData.type));
        _alreadyAdded = true;
      }
      // @ts-ignore
      Queues.setEmptyItems($(this));
      _unitData.type == "ship" ? window.UnitOrder.docks_unit_order.selectUnit(window.UnitOrder.unit_id) : window.UnitOrder.barracks_unit_order.selectUnit(window.UnitOrder.unit_id);
    });

    localStorage.setItem("Queues.Queue", Queues.town_queues);
  },

  /**
   * Add empty queue item to the Element
   * @param {Element to add the empty items to} _jqueryElement
   */
  setEmptyItems: function (_jqueryElement: any) {
    var _width = 0;
    var _queueParentWidth = _jqueryElement["parent"]()["width"]();
    $.each(_jqueryElement.find(".js-tutorial-queue-item"), function () {
      _width += $(this).outerWidth(true);
    });
    var _freeWidth = _queueParentWidth - _width;
    if (_freeWidth >= 0) {
      _jqueryElement.width(_queueParentWidth);
      for (let i = 1; i <= Math.floor(_freeWidth) / 60; i++) {
        _jqueryElement.append(
          $("<div/>", {
            class: "js-queue-item js-tutorial-queue-item construction_queue_sprite empty_slot"
          })
        );
      }
    } else {
      _jqueryElement.width(_width + 25);
    }
  },

  /**
   * Render the building element for the queue
   * @param {Building Object} _building
   */
  buildingElement: function (_building: any) {
    return $("<div/>", {
      class: "js-tutorial-queue-item queued_building_order last_order " + _building.item_name + " queue_id_" + _building.id
    })
      .append(
        $("<div/>", {
          class: "construction_queue_sprite frame"
        })
          .mousePopup(new MousePopup(HelperFunctions.capitalize(_building.item_name) + " queued"))
          .append(
            $("<div/>", {
              class: "item_icon building_icon40x40 js-item-icon build_queue " + _building.item_name
            }).append(
              $("<div/>", {
                class: "building_level"
              }).append('<span class="construction_queue_sprite arrow_green_ver"></span>' + Queues.checkBuildingLevel(_building))
            )
          )
      )
      .append(
        $("<div/>", {
          class: "btn_cancel_order button_new square remove js-item-btn-cancel-order build_queue"
          //remove Event
        })
          .on("click", function (_event: any) {
            _event.preventDefault();

            Queues.saveBuilding({
              type: "remove",
              town_id: window.Game.townId,
              item_id: _building.id
            });

            $(".queue_id_" + _building.id).remove();
          })
          .append(
            $("<div/>", {
              class: "left"
            })
          )
          .append(
            $("<div/>", {
              class: "right"
            })
          )
          .append(
            $("<div/>", {
              class: "caption js-caption"
            }).append(
              $("<div/>", {
                class: "effect js-effect"
              })
            )
          )
      );
  },

  /**
   * Render the unit element to the queue
   * @param {Unit Object} _unit
   * @param {Ship or unit} _queueType
   */
  unitElement: function (_unit: { [x: string]: string; item_name: string; id: string; count: any }, _queueType: any) {
    return $("<div/>", {
      class: "js-tutorial-queue-item queued_building_order last_order " + _unit.item_name + " queue_id_" + _unit.id
    })
      ["append"](
        $("<div/>", {
          class: "construction_queue_sprite frame"
        })
          .mousePopup(new MousePopup(HelperFunctions.capitalize(_unit.item_name).replace("_", " ") + " queued"))
          .append(
            $("<div/>", {
              class: "item_icon unit_icon40x40 js-item-icon build_queue " + _unit["item_name"]
            }).append(
              $("<div/>", {
                class: "unit_count text_shadow"
              }).html(_unit.count)
            )
          )
      )
      .append(
        $("<div/>", {
          class: "btn_cancel_order button_new square remove js-item-btn-cancel-order build_queue"
          //remove event
        })
          .on("click", function (_event: any) {
            _event.preventDefault();

            Queues.saveUnits({
              action: "remove",

              town_id: window.Game.townId,
              item_id: _unit.id,
              type: _queueType
            });

            $(".queue_id_" + _unit.id).remove();
          })
          .append(
            $("<div/>", {
              class: "left"
            })
          )
          .append(
            $("<div/>", {
              class: "right"
            })
          )
          .append(
            $("<div/>", {
              class: "caption js-caption"
            }).append(
              $("<div/>", {
                class: "effect js-effect"
              })
            )
          )
      );
  },

  /**
   * Window events on open main building and barracks
   */
  windows: {
    executeAndCheckBarracks: (callback: any) => {
      return function () {
        // @ts-ignore
        callback.apply(this, arguments);
        // @ts-ignore
        if (this.barracks) {
          // @ts-ignore
          Queues.initUnitOrder(this, "unit");
        } else {
          // @ts-ignore
          if (!this.barracks) {
            // @ts-ignore
            Queues.initUnitOrder(this, "ship");
          }
        }
      };
    },
    queues_main_index: function () {
      var town_queue = Queues.town_queues.filter((e: any) => e.town_id === window.Game.townId);

      if (town_queue.length > 0) {
        town_queue[0].building_queue.map((building: { id: any; item_name: any; count: any }) => {
          let newBuilding = {
            id: building.id,
            item_name: building.item_name,
            count: building.count
          };
          $(".construction_queue_order_container .ui_various_orders").each(function () {
            // @ts-ignore
            $(this).find(".empty_slot").remove();
            //Add new item to building queue
            // @ts-ignore
            $(this).append(Queues.buildingElement(newBuilding));
            // @ts-ignore
            Queues.setEmptyItems($(this));
          });
        });
      }
    },
    building_main_index: function () {
      if (window.GPWindowMgr && window.GPWindowMgr.getOpenFirst(window.Layout.wnd.TYPE_BUILDING_SENATE)) {
        Queues.currentWindow = window.GPWindowMgr.getOpenFirst(window.Layout.wnd.TYPE_BUILDING_SENATE).getJQElement().find(".gpwindow_content");

        var _mainTasks = Queues.currentWindow.find("#main_tasks h4");
        //replace max count of building tasks with infinit
        _mainTasks.html(_mainTasks.html().replace(/\/.*\)/, "/&infin;)"));

        var town_queue = Queues.town_queues.filter((e: any) => e.town_id === window.Game.townId);

        if (town_queue.length > 0) {
          town_queue[0].building_queue.map((building: { id: any; item_name: any; count: any }) => {
            let newBuilding = {
              id: building.id,
              item_name: building.item_name,
              count: building.count
            };
            $("#building_tasks_main .ui_various_orders").each(function () {
              // @ts-ignore
              $(this).find(".empty_slot").remove();
              //Add new item to building queue

              // @ts-ignore
              $(this).append(Queues.buildingElement(newBuilding));
              // @ts-ignore
              Queues.setEmptyItems($(this));
            });
          });
        }

        var _specialBuildings = ["theater", "thermal", "library", "lighthouse", "tower", "statue", "oracle", "trade_office"];
        //replace the "cant upgrade" with "add to queue"
        $.each($("#buildings .button_build.build_grey.build_up.small.bold"), function () {
          var _buildingId = $(this).parent().parent().attr("id").replace("building_main_", "");
          // if there are no more dependendencies

          var dep = Queues.checkBuildingDependencies(_buildingId, window.ITowns.getTown(window.Game.townId));
          if (dep.length <= 0) {
            //if the building is not a special building

            if ($.inArray(_buildingId, _specialBuildings) == -1 && buildings[_buildingId].hasOwnProperty(window.ITowns.getTown(window.Game.townId).getBuildings().attributes[_buildingId] + 1)) {
              $(this)
                .removeClass("build_grey")
                .addClass("build")
                .html("Add to queue")
                .on("click", function (_event: any) {
                  _event.preventDefault();

                  Queues.saveBuilding({
                    type: "add",
                    town_id: window.Game.townId,
                    item_name: _buildingId,
                    count: 1
                  });
                });
            }
          }
        });
      }
    },
    //replace max count of building tasks with infinit
    building_barracks_harbor_index: function (type: string) {
      if (window.GPWindowMgr != null && (window.GPWindowMgr.getOpenFirst(window.Layout.wnd.TYPE_BUILDING_BARRACKS) != null || window.GPWindowMgr.getOpenFirst(window.Layout.wnd.TYPE_BUILDING_DOCKS) != null)) {
        if (type == "ship") {
          Queues.currentWindow = window.GPWindowMgr.getOpenFirst(window.Layout.wnd.TYPE_BUILDING_DOCKS).getJQElement().find(".gpwindow_content");
          window.UnitOrder.docks_unit_order.selectUnit = Queues.windows.executeAndCheckBarracks(window.UnitOrder.docks_unit_order.selectUnit);
        } else {
          Queues.currentWindow = window.GPWindowMgr.getOpenFirst(window.Layout.wnd.TYPE_BUILDING_BARRACKS).getJQElement().find(".gpwindow_content");
          window.UnitOrder.barracks_unit_order.selectUnit = Queues.windows.executeAndCheckBarracks(window.UnitOrder.barracks_unit_order.selectUnit);
        }

        var _unitQueue = Queues.currentWindow.find("#unit_orders_queue h4");
        _unitQueue.find(".js-max-order-queue-count").html("&infin;");

        var town_queue = Queues.town_queues.filter((e: any) => e.town_id === window.Game.townId);

        if (town_queue.length > 0) {
          if (type == "unit") {
            town_queue[0].unit_queue.map((unit: { id: any; item_name: any; count: any }) => {
              let newUnit = {
                id: unit.id,
                item_name: unit.item_name,
                count: unit.count
              };
              $("#unit_orders_queue .ui_various_orders.barracks").each(function () {
                // @ts-ignore
                $(this).find(".empty_slot").remove();
                //Add new item to gui queue
                // @ts-ignore
                $(this).append(Queues.unitElement(newUnit, type));
                // @ts-ignore
                Queues.setEmptyItems($(this));
                window.UnitOrder.barracks_unit_order.selectUnit(window.UnitOrder.barracks_unit_order.unit_id);
              });
            });
          } else {
            town_queue[0].ship_queue.map((unit: { id: any; item_name: any; count: any }) => {
              let newUnit = {
                id: unit.id,
                item_name: unit.item_name,
                count: unit.count
              };
              $("#unit_orders_queue .ui_various_orders.docks").each(function () {
                // @ts-ignore
                $(this).find(".empty_slot").remove();
                //Add new item to gui queue
                // @ts-ignore
                $(this).append(Queues.unitElement(newUnit, type));
                // @ts-ignore
                Queues.setEmptyItems($(this));
                window.UnitOrder.docks_unit_order.selectUnit(window.UnitOrder.docks_unit_order.unit_id);
              });
            });
          }
        }
      }
    }
  }
};

export default Queues;
