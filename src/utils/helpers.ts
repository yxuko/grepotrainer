/*******************************************************************************************************************************
 * Helper Functions
 *******************************************************************************************************************************/

const HelperFunctions = {
  capitalize: function (str: string) {
    const lower = str.toLowerCase();
    return str.charAt(0).toUpperCase() + lower.slice(1);
  },
  beep: function (duration: number, frequency: number, volume: number) {
    const myAudioContext = new AudioContext();
    return new Promise<void>((resolve, reject) => {
      // Set default duration if not provided
      duration = duration || 200;
      frequency = frequency || 440;
      volume = volume || 100;

      try {
        let oscillatorNode = myAudioContext.createOscillator();
        let gainNode = myAudioContext.createGain();
        oscillatorNode.connect(gainNode);

        // Set the oscillator frequency in hertz
        oscillatorNode.frequency.value = frequency;

        // Set the type of oscillator
        oscillatorNode.type = "square";
        gainNode.connect(myAudioContext.destination);

        // Set the gain to the volume
        gainNode.gain.value = volume * 0.01;

        // Start audio with the desired duration
        oscillatorNode.start(myAudioContext.currentTime);
        oscillatorNode.stop(myAudioContext.currentTime + duration * 0.001);

        // Resolve the promise when the sound is finished
        oscillatorNode.onended = () => {
          resolve();
        };
      } catch (error) {
        reject(error);
      }
    });
  },
  toHHMMSS: function (_time: number) {
    var _hours = ~~(_time / 3600);
    var _minuts = ~~((_time % 3600) / 60);
    var _seconds = _time % 60;
    var ret: string = "";
    if (_hours > 0) {
      ret += "" + _hours + ":" + (_minuts < 10 ? "0" : "");
    }
    ret += "" + _minuts + ":" + (_seconds < 10 ? "0" : "");
    ret += "" + _seconds;
    return ret;
  },

  randomize: function (a: number, b: number) {
    return Math.floor(Math.random() * (b - a + 1)) + a;
  },

  pointNumber: function (number: number) {
    var sep = ".";
    let num = number.toString();
    if (num.length > 3) {
      var mod = num.length % 3;
      var output = mod > 0 ? num.substring(0, mod) : "";

      for (var i = 0; i < Math.floor(num.length / 3); i++) {
        if (mod == 0 && i == 0) {
          output += num.substring(mod + 3 * i, mod + 3 * i + 3);
        } else {
          output += sep + num.substring(mod + 3 * i, mod + 3 * i + 3);
        }
      }
      num = output;
    }
    return number;
  },
  dateDiff: function (date1: Date, date2: Date) {
    var diff: { [key: string]: any } = {};
    // @ts-ignore
    var tmp = date2 - date1;
    tmp = Math.floor(tmp / 1000);
    diff.sec = tmp % 60;
    tmp = Math.floor((tmp - diff.sec) / 60);
    diff.min = tmp % 60;
    tmp = Math.floor((tmp - diff.min) / 60);
    diff.hour = tmp % 24;
    tmp = Math.floor((tmp - diff.hour) / 24);
    diff.day = tmp;
    return diff;
  },
  timeToSeconds: function (time: string) {
    let timeArray = time.split(":");
    let seconds = 0;
    let multiplier = 1;

    while (timeArray.length > 0) {
      seconds += multiplier * parseInt(timeArray.pop(), 10);
      multiplier *= 60;
    }

    return seconds;
  }
};

export default HelperFunctions;
