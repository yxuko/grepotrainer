/*******************************************************************************************************************************
 * UI
 *******************************************************************************************************************************/

var UI = {
  // @ts-ignore
  createWindowType: function (name, title, width, height, minimizable, position) {
    // Create Window Type
    // @ts-ignore
    function WndHandler(wndhandle) {
      // @ts-ignore
      this.wnd = wndhandle;
    }
    // @ts-ignore
    Function.prototype.inherits.call(WndHandler, WndHandlerDefault);
    // @ts-ignore
    WndHandler.prototype.getDefaultWindowOptions = function () {
      //if (MID == 'zz') {
      return {
        //position: "10px",
        hidden: !1,
        left: 297,
        top: 101,
        width: width,
        height: height,
        minimizable: minimizable,
        title: title
      };
      // } else {
      //     return {
      //         position: position,
      //         width: width,
      //         height: height,
      //         minimizable: minimizable,
      //         title: "<img class='dio_title_img' src='https://www.tuto-de-david1327.com/medias/images/icon.gif' /><div class='dio_title'>" + title + "</div>"
      //     };
      // }
    };
    // @ts-ignore
    window.GPWindowMgr.addWndType(name, "75623", WndHandler, 1);
  },
  button: function (element: any) {
    return $("<div/>").append(
      $("<a/>", {
        class: "button_new" + (element.class || ""),
        href: "#",
        style: "margin-top:1px;float:left;" + (element.style || "")
      })
        .append(
          $("<span/>", {
            class: "left"
          })
        )
        .append(
          $("<span/>", {
            class: "right"
          })
        )
        .append(
          $("<div/>", {
            class: "caption js-caption"
          }).text(element.name)
        )
    );
  },
  checkbox: function (element: any) {
    return $("<div/>", {
      class: "checkbox_new" + (element.checked ? " checked" : "") + (element.disabled ? " disabled" : ""),
      style: "padding: 5px;" + (element.style || ""),
      id: element.id
    })
      .append(
        $("<input/>", {
          type: "checkbox",
          name: element.name,
          id: element.id,
          checked: element.checked,
          style: "display: none;"
        })
      )
      .append(
        $("<div/>", {
          class: "cbx_icon",
          rel: element.name
        })
      )
      .append(
        $("<div/>", {
          class: "cbx_caption"
        }).text(element.text)
      );
    //.bind("click", function () {
    //  if (!element.disabled) {
    //    $(this).toggleClass("checked");
    //    $(this).find($('input[type="checkbox"]')).prop("checked", $(this).hasClass("checked"));
    //    if ($(this).hasClass("checked")) {
    //      if (uncheckFunction != undefined) {
    //        uncheckFunction();
    //      }
    //    } else {
    //      if (checkFunction != undefined) {
    //        checkFunction();
    //      }
    //    }
    //  }
    //});
  },
  settingsCheckbox: function (element: any, checkFunction: any, uncheckFunction: any) {
    return $("<div/>", {
      class: "checkbox_new" + (element.checked ? " checked" : "") + (element.disabled ? " disabled" : ""),
      style: "padding: 5px;" + (element.style || "")
    })
      .append(
        $("<img/>", {
          src: element.src
        })
      )
      .append(
        $("<input/>", {
          type: "checkbox",
          name: element.name,
          id: element.id,
          checked: element.checked,
          style: "display: none;"
        })
      )
      .append(
        $("<div/>", {
          class: "cbx_icon",
          rel: element.name
        })
      )
      .append(
        $("<div/>", {
          class: "cbx_caption"
        }).text(element.text)
      )
      .bind("click", function () {
        $(this).toggleClass("checked");
        $(this).find($('input[type="checkbox"]')).prop("checked", $(this).hasClass("checked"));
        if ($(this).hasClass("checked")) {
          if (checkFunction != undefined) {
            checkFunction();
          }
        } else {
          if (uncheckFunction != undefined) {
            uncheckFunction();
          }
        }
      })
      .append(
        $("<p/>", {
          class: "cbx_caption"
        }).text(element.desc)
      );
  },
  input: function (element: any) {
    return $("<div/>", {
      style: "padding: 5px;"
    })
      .append(
        $("<label/>", {
          for: element.id
        }).text(element.name + ": ")
      )
      .append(
        $("<div/>", {
          class: "textbox",
          style: element.style
        })
          .append(
            $("<div/>", {
              class: "left"
            })
          )
          .append(
            $("<div/>", {
              class: "right"
            })
          )
          .append(
            $("<div/>", {
              class: "middle"
            }).append(
              $("<div/>", {
                class: "ie7fix"
              }).append(
                $("<input/>", {
                  type: element.type,
                  tabindex: "1",
                  id: element.id,
                  name: element.id,
                  value: element.value
                }).attr("size", element.size)
              )
            )
          )
      );
  },
  textarea: function (element: any) {
    return $("<div/>", {
      style: "padding: 5px;"
    })
      .append(
        $("<label/>", {
          for: element.id
        }).text(element.name + ": ")
      )
      .append(
        $("<div/>").append(
          $("<textarea/>", {
            name: element.id,
            id: element.id
          })
        )
      );
  },
  inputMinMax: function (element: any) {
    return $("<div/>", {
      class: "textbox"
    })
      .append(
        $("<span/>", {
          class: "tr_spinner_btn tr_spinner_down",
          rel: element.name
        }).click(function () {
          var rel = $(this)
            .parent()
            .find("#" + $(this).attr("rel"));
          // @ts-ignore
          if (parseInt($(rel).attr("min")) < parseInt($(rel).attr("value"))) {
            // @ts-ignore
            $(rel).attr("value", parseInt($(rel).attr("value")) - 1);
          }
        })
      )
      .append(
        $("<div/>", {
          class: "textbox",
          style: element.style
        })
          .append(
            $("<div/>", {
              class: "left"
            })
          )
          .append(
            $("<div/>", {
              class: "right"
            })
          )
          .append(
            $("<div/>", {
              class: "middle"
            }).append(
              $("<div/>", {
                class: "ie7fix"
              }).append(
                $("<input/>", {
                  type: "text",
                  tabindex: "1",
                  id: element.name,
                  value: element.value,
                  min: element.min,
                  max: element.max
                })
                  .attr("size", element.size || 10)
                  .css("text-align", "right")
              )
            )
          )
      )
      .append(
        $("<span/>", {
          class: "tr_spinner_btn tr_spinner_up",
          rel: element.name
        }).click(function () {
          var rel = $(this)
            .parent()
            .find("#" + $(this).attr("rel"));
          // @ts-ignore
          if (parseInt($(rel).attr("max")) > parseInt($(rel).attr("value"))) {
            // @ts-ignore
            $(rel).attr("value", parseInt($(rel).attr("value")) + 1);
          }
        })
      );
  },
  inputSlider: function (element: any) {
    return $("<div/>", {
      id: "tr_" + element.name + "_config"
    })
      .append(
        $("<div/>", {
          class: "slider_container"
        })
          .append(
            $("<div/>", {
              style: "float:left;width:120px;"
            }).html(element.name)
          )
          .append(
            UI.input({
              name: "tr_" + element.name + "_value",
              style: "float:left;width:33px;"
            }).hide()
          )
          .append(
            $("<div/>", {
              class: "windowmgr_slider",
              style: "width: 200px;float: left;"
            }).append(
              $("<div/>", {
                class: "grepo_slider sound_volume"
              })
            )
          )
      )
      .append(
        $("<script/>", {
          type: "text/javascript"
        }).text(
          "RepConv.slider = $('#tr_" +
            element.name +
            "_config .sound_volume').grepoSlider({\x0Amin: 0,\x0Amax: 100,\x0Astep: 5,\x0Avalue: " +
            element.volume +
            ",\x0Atemplate: 'tpl_tr_slider'\x0A}).on('sl:change:value', function (e, _sl, value) {\x0A$('#tr_" +
            element.name +
            "_value').attr('value',value);\x0Aif (RepConv.audio.test != undefined){\x0ARepConv.audio.test.volume = value/100;\x0A}\x0A}),\x0A$('#tr_" +
            element.name +
            "_config .button_down').css('background-position','-144px 0px;'),\x0A$('#tr_" +
            element.name +
            "_config .button_up').css('background-position','-126px 0px;')\x0A"
        )
      );
  },
  selectBox: function (element: any) {
    var str = '<div style="display: flex;align-content: space-between;align-items: center;"><span class="grepo_input"><span class="left"><span class="right"><select class="tr_grepo_down" name="' + element.id + '" id="' + element.id + '" type="text">';
    if (element.label) {
      str = '<div style="display: flex;align-content: space-between;align-items: center;"><label style="font-weight:bold;flex: 1;">' + element.label + '</label><span class="grepo_input"><span class="left"><span class="right"><select class="tr_grepo_down" name="' + element.id + '" id="' + element.id + '" type="text">';
    }
    $.each(element.options, function (a, b) {
      if (b[1]) {
        str += '<option value="' + b[0] + '">' + b[1] + "</option>";
      } else {
        str += '<option value="' + b + '">' + b + "</option>";
      }
    });
    str += "</select></span></span></span></div>";
    return str;
  },
  timerBoxFull: function (element: any) {
    return $("<div/>", {
      class: "single-progressbar instant_buy js-progressbar type_building_queue",
      id: element.id,
      style: element.styles
    })
      .append(
        $("<div/>", {
          class: "border_l"
        })
      )
      .append(
        $("<div/>", {
          class: "border_r"
        })
      )
      .append(
        $("<div/>", {
          class: "body"
        })
      )
      .append(
        $("<div/>", {
          class: "progress"
        }).append(
          $("<div/>", {
            class: "indicator",
            style: "width: 0%;"
          })
        )
      )
      .append(
        $("<div/>", {
          class: "caption"
        })
          .append(
            $("<span/>", {
              class: "text"
            })
          )
          .append(
            $("<span/>", {
              class: "value_container"
            }).append(
              $("<span/>", {
                class: "curr"
              }).html("0%")
            )
          )
      );
  },
  timerBoxSmall: function (element: any) {
    return $("<div/>", {
      class: "single-progressbar instant_buy js-progressbar type_building_queue",
      id: element.id,
      style: element.styles
    })
      .append(
        $("<div/>", {
          class: "progress"
        }).append(
          $("<div/>", {
            class: "indicator",
            style: "width: 0%;"
          })
        )
      )
      .append(
        $("<div/>", {
          class: "caption"
        })
          .append(
            $("<span/>", {
              class: "text"
            })
          )
          .append(
            $("<span/>", {
              class: "value_container"
            }).append(
              $("<span/>", {
                class: "curr"
              }).html(element.text ? element.text : "-")
            )
          )
      );
  },
  GameWrapper: function (title: string, id: string, inner: HTMLElement | string, element: any) {
    return $("<div/>", {
      class: "game_inner_box",
      style: element,
      id: id
    }).append(
      $("<div/>", {
        class: "game_border"
      })
        .append(
          $("<div/>", {
            class: "game_border_top"
          })
        )
        .append(
          $("<div/>", {
            class: "game_border_bottom"
          })
        )
        .append(
          $("<div/>", {
            class: "game_border_left"
          })
        )
        .append(
          $("<div/>", {
            class: "game_border_right"
          })
        )
        .append(
          $("<div/>", {
            class: "game_border_top"
          })
        )
        .append(
          $("<div/>", {
            class: "game_border_corner corner1"
          })
        )
        .append(
          $("<div/>", {
            class: "game_border_corner corner2"
          })
        )
        .append(
          $("<div/>", {
            class: "game_border_corner corner3"
          })
        )
        .append(
          $("<div/>", {
            class: "game_border_corner corner4"
          })
        )
        .append(
          $("<div/>", {
            class: "game_header bold",
            id: "settings_header"
          }).html(title)
        )
        .append($("<div/>").append(inner))
    );
  }
};

export default UI;
