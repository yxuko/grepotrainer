interface StorageType {
  [key: string]: any;
}

class TrStorage {
  constructor() {}

  getStorage = () => {
    const worldId = window.Game.world_id;
    const savedValue = localStorage.getItem(`${worldId}_tr`);
    let storage: StorageType = {};

    if (savedValue !== null && savedValue !== undefined) {
      try {
        storage = JSON.parse(savedValue);
      } catch (error) {
        console.error(`Error parsing localStorage data: ${error}`);
      }
    }
    return storage;
  };

  saveStorage = (storage: StorageType) => {
    try {
      const worldId = window.Game.world_id;
      localStorage.setItem(`${worldId}_tr`, JSON.stringify(storage));
      return true;
    } catch (error) {
      console.error(`Error saving data to localStorage: ${error}`);
      return false;
    }
  };

  save = (key: string, content: any) => {
    const storage = this.getStorage();
    storage[key] = content;
    window.tr[key] = content;
    return this.saveStorage(storage);
  };

  load = (key: string, defaultValue = null) => {
    const storage = this.getStorage();
    const savedValue = storage[key];
    return savedValue !== undefined ? savedValue : defaultValue;
  };
}

export default TrStorage;
