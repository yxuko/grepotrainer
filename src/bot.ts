import OceanNumbers from "./ui/ocean_numbers";
import Taskbar from "./ui/taskbar";
import MouseWheelZoom from "./ui/zoom";
import UnitComparison from "./misc/unit_comparison";
import PremiumHide from "./misc/premium_advisors_ads";
import TrSettings from "./storage";
import MarketSimplifier from "./ui/market_simplify_gold";
import Caving from "./automation/cave";
import Farming from "./automation/farming";
import FarmingVillagesHelper from "./ui/farming_villages_helper";
import Queues from "./automation/queues";
import Culture from "./automation/culture";
import TownPopup from "./ui/town_popup";
import Science from "./misc/science";
import FavorPopup from "./ui/favor_popup";
import TownIcons from "./ui/town_icons";
import MapWorld from "./ui/map_world";
import resCounter from "./misc/resources_counter";
import TownList from "./ui/town_list";
import spells from "./misc/barracks_harbor_spells";
import buildingsLvlPts from "./ui/buildings_level_points";
import ModuleHub from "./automation/hub";
import Settings from "./settings";
import Simulator, { afterSimulation, setStrengthSimulator } from "./misc/simulator";
import UnitStrength from "./misc/unit_strength";
import AvailableUnits, { getAllUnits } from "./misc/get_all_units";
import SentUnits from "./misc/sent_units";
import ShortDuration from "./misc/short_duration";
import checkGold from "./misc/gold";
import UnitsSelectorOverload from "./misc/units_selector_overload";
import TransportCapacity from "./ui/transport_capacity";
import cultureOverview from "./misc/culture_overview";
import cultureProgress from "./ui/culture_progress_bar";
import culturePoints from "./ui/culture_points";
import CityModel from "./misc/city_model";
import addPercentTrade from "./misc/percent_trade";
import addTradeMarks from "./misc/trade_progress_marks";
import RecruitingTrade from "./misc/recruiting_trade";
import townTradeImprovement from "./misc/festival_trade";

/*******************************************************************************************************************************
 * Config Data
 *******************************************************************************************************************************/

var tr_storage = new TrSettings();
tr_storage.getStorage();

window.tr = {
  settings: tr_storage.load("settings", {
    tr_auto_militia: false,
    tr_auto_dodger: false,
    tr_auto_spells: false,
    tr_balance_recs_villages: false,
    tr_custom_spell: "",
    tr_cave_percentage: 60,
    tr_cave_store_percentage: 10,
    tr_farmin_timer: 0,
    tr_farmin_wait_timer: 6000,
    tr_ava: true,
    tr_ava2: true,
    tr_cave: false,
    tr_clt: true,
    tr_com: true,
    tr_farmin: false,
    tr_freebtns: false,
    tr_fvrpop: true,
    tr_mousewheel: true,
    tr_newsimulator: true,
    tr_oceanNumbers: true,
    tr_per: true,
    tr_rec: true,
    tr_sci: true,
    tr_sen: true,
    tr_stopfarmin: true,
    tr_tih: true,
    tr_til: true,
    tr_tim: true,
    tr_tir: true,
    tr_tis: true,
    tr_tiw: true,
    tr_tkb: true,
    tr_tpt: true,
    tr_tra: true,
    tr_tti: true,
    tr_twnicn: true,
    tr_unitstrength: true,
    tr_way: true,
    tr_autobuild_barracks: true,
    tr_autobuild_building: true,
    tr_autobuild_harbor: true,
    tr_autobuild_instant: true,
    tr_autobuild_strt: true,
    tr_autobuild_timer: 120
  }),

  automation: {
    windows: {
      timeVerif: 100,
      academy: {
        auto: false,
        error: false,
        nbEvent: 0,
        finished: {
          free: false
        }
      },
      senate: {
        auto: false,
        error: false,
        nbEvent: 0,
        finished: {
          free: false
        }
      },
      hide: {
        auto: false,
        error: false,
        nbEvent: 0,
        finished: {
          free: false
        }
      },
      farmTown: {
        open: false
      }
    },
    farmTowns: {
      current: false
    }
  },

  city_model: tr_storage.load("city_model", {
    group_id: null,
    buildings: {
      main: 24,
      storage: 34,
      farm: 40,
      academy: 30,
      temple: 8,
      barracks: 10,
      docks: 10,
      market: 12,
      hide: 10,
      lumber: 15,
      stoner: 15,
      ironer: 15,
      wall: 25
    },
    special_buildings: {
      1: null,
      2: null
    }
  }),

  science: tr_storage.load("science", {}),
  culture: tr_storage.load("culture", {}),
  queue: tr_storage.load("queue", []),

  autoTownTypes: {},
  manuTownTypes: tr_storage.load("manuTownTypes", {}),
  manuTownAuto: tr_storage.load("manuTownAuto", {}),

  population: {},
  sentUnitsArray: tr_storage.load("sentUnits", { attack: {}, support: {} }),
  biriArray: tr_storage.load("biremes", {}),

  unitsGround: { att: {}, def: {} },
  unitsNaval: { att: {}, def: {} },
  groupUnitArray: {},

  spellbox: tr_storage.load("spellbox", { top: "23%", left: "-150%", show: false }),
  commandbox: tr_storage.load("commandbox", { top: 55, left: 250 }),
  tradebox: tr_storage.load("tradebox", { top: 55, left: 450 }),

  worldWonder: tr_storage.load("worldWonder", { ratio: {}, storage: {}, map: {} }),
  worldWonderTypes: tr_storage.load("worldWonderTypes", {}),

  bullseyeUnit: tr_storage.load("bullseyeUnit", { current_group: -1 }), // new

  overviews: tr_storage.load("overviews", { Buildings: "", Culture: "", Gods: "" }),

  WID: window.Game.world_id,
  PID: window.Game.player_id,
  MID: window.Game.market_id,
  LID: window.Game.locale_lang.split("_")[0],

  strength: {
    name: "",
    def: true,
    blunt: 0,
    sharp: 0,
    dist: 0,
    naval_attc: 0,
    naval_def: 0,
    shipsize: false
  },

  trade: {
    rest_count: 0,
    res: {}
  },

  arrival_interval: {},
  hades_interval: {}
};

function addFunctionToITowns() {
  // Copy function and prevent an error
  window.ITowns.townGroups.getGroupsTR = function () {
    var town_groups_towns,
      town_groups,
      groups: any = {};

    // #Grepolis Fix: 2.75 -> 2.76

    if (window.MM.collections) {
      town_groups_towns = window.MM.collections.TownGroupTown[0];
      town_groups = window.MM.collections.TownGroup[0];
    } else {
      town_groups_towns = window.MM.getCollections().TownGroupTown[0];
      town_groups = window.MM.getCollections().TownGroup[0];
    }

    town_groups_towns.each(function (town_group_town: { getGroupId: () => any; getTownId: () => any }) {
      var gid = town_group_town.getGroupId(),
        group = groups[gid],
        town_id = town_group_town.getTownId();

      if (!group) {
        groups[gid] = group = {
          id: gid,
          //name: town_groups.get(gid).getName(), // hier tritt manchmal ein Fehler auf: TypeError: Cannot read property "getName" of undefined at https://_.grepolis.com/cache/js/merged/game.js?1407322916:8298:525
          towns: {}
        };
      }

      group.towns[town_id] = { id: town_id };
      //groups[gid].towns[town_id]={id:town_id};
    });
    //console.log(groups);
    return groups;
  };

  window.ITowns.getHeroTR = function () {
    var town_groups_towns,
      town_groups,
      groups: any = {};

    // #Grepolis Fix: 2.75 -> 2.76
    var PlayerHero;

    if (window.MM.collections) {
      PlayerHero = window.MM.collections.PlayerHero[0];
    } else {
      PlayerHero = window.MM.getCollections().PlayerHero[0];
    }

    PlayerHero.each(function (PlayerHero: { getId: () => any; getLevel: () => any; getHomeTownId: () => any; getOriginTownName: () => any }) {
      var hero_name = PlayerHero.getId(),
        hero_level = PlayerHero.getLevel(),
        town_id = PlayerHero.getHomeTownId(),
        town_name = PlayerHero.getOriginTownName(),
        group = groups[town_id];

      if (!group) {
        groups[town_id] = group = {
          town_id: town_id,
          town: town_name,
          hero_name,
          hero_level: hero_level
        };
      }
    });
    return groups;
  };
}

/*******************************************************************************************************************************
 * Town window
 * ----------------------------------------------------------------------------------------------------------------------------
 * | ● TownTabHandler (trade, attack, support,...)
 * | ● Sent units box
 * | ● Short duration: Display of 30% troop speed improvement in attack/support tab
 * | ● Trade options:
 * |    - Ressource marks on possibility of city festivals
 * |    - Percentual Trade: Trade button
 * |    - Recruiting Trade: Selection boxes (ressource ratio of unit type + share of the warehouse capacity of the target town)
 * | ● Town Trade Improvement : Click on it and it is only exchanged towards a festival. Quack function
 * ----------------------------------------------------------------------------------------------------------------------------
 *******************************************************************************************************************************/

function TownTabHandler(action: string) {
  try {
    var wndArray, wndID, wndA;

    wndArray = window.Layout.wnd.getOpen(window.Layout.wnd.TYPE_TOWN);
    //console.log(wndArray);
    for (var e in wndArray) {
      if (wndArray.hasOwnProperty(e)) {
        //console.log(wndArray[e].getHandler());
        //console.log(wndArray[e].getAction());
        //console.log(wndArray[e].getHandler());
        //wndA = wndArray[e].getAction();

        wndID = "#gpwnd_" + wndArray[e].getID() + " ";

        var wnd = wndArray[e].getID();
        if (!$(wndID).get(0)) {
          wndID = "#gpwnd_" + (wndArray[e].getID() + 1) + " ";

          wnd = wndArray[e].getID() + 1;
        }
        //console.log(wndID);
        //if (wndA === action) {
        switch (action) {
          case "trading":
            if ($(wndID + "#trade_tab").get(0)) {
              if (!$(wndID + ".dio_rec_trade").get(0) && window.tr.settings.tr_rec) {
                RecruitingTrade.add(wndID);
              }
              if (!$(wndID + ".dio_btn_trade").get(0) && window.tr.settings.tr_per) {
                addPercentTrade(wndID, false);
              }
            }
            if (window.tr.settings.tr_tti) {
              townTradeImprovement.add(wndID, wnd);
            }
            addTradeMarks(15, 18, 15, "red"); // town festival
            break;
          case "info":
            //console.log(wndArray[e].getAction());
            break;
          case "support":
          case "attack":
            //if(!window.tr.arrival_interval[wndID]){
            if (window.tr.settings.tr_way && !($(".js-casted-powers-viewport .unit_movement_boost").get(0) || $(wndID + ".short_duration").get(0))) {
              //if(window.tr.arrival_interval[wndID]) console.log("add " + wndID);
              ShortDuration.add(wndID, action);
            } else $("#dio_short_duration_stylee").remove();
            if (window.tr.settings.tr_sen) {
              SentUnits.add(wndID, action);
            }
            UnitsSelectorOverload.activate();

            //}
            break;
          case "rec_mark":
            //addTradeMarks(wndID, 15, 18, 15, "lime");
            break;
        }
        //}
      }
    }
  } catch (error) {
    console.error(error, "TownTabHandler");
  }
}

function WWTradeHandler() {
  var wndArray, wndID, wndA;

  wndArray = window.GPWindowMgr.getOpen(window.GPWindowMgr.TYPE_WONDERS);
  for (var e in wndArray) {
    if (wndArray.hasOwnProperty(e)) {
      wndID = "#gpwnd_" + wndArray[e].getID() + " ";
      if (window.tr.settings.tr_per && !($(wndID + ".dio_btn_trade").get(0) || $(wndID + ".next_building_phase").get(0) || $(wndID + "#ww_time_progressbar").get(0))) {
        addPercentTrade(wndID, true);
      }
    }
  }
}

/*******************************************************************************************************************************
 * window.Game Injection Events
 *******************************************************************************************************************************/

// * Captcha Bypass
$.Observer(window.GameEvents.bot_check.update_started_at_change).subscribe("bot_check", function (e: any, data: any) {
  Farming.deactivate();
  Caving.deactivate();
});

$.Observer(window.GameEvents.town.town_switch).subscribe("TR_UNIT", function (e: any, data: any) {
  Queues.windows.queues_main_index();
  // Fix icon update when switching cities
  if (window.tr.settings.tr_twnicn) TownIcons.updateTownIcon();
});

$.Observer(window.GameEvents.map.town.click).subscribe("TR_MAP", function (e: any, data: any) {
  Queues.windows.queues_main_index();
});

$.Observer(window.GameEvents.ui.layout_mode.city_overview.activate).subscribe("TR_LAYOUT", function (e: any, data: any) {
  Queues.windows.queues_main_index();
});

$.Observer(window.GameEvents.notification.report.arrive).subscribe("TR_SPELLS", function (e: any, data: any) {
  if (window.tr.settings.tr_auto_spells && data.subject && data.subject.toLowerCase().includes("has enacted Narcissism")) {
    window.us
      .filter(
        window.MM.getModels().CastedPowers,
        function (power: any) {
          return power.getPowerId() === "call_of_the_ocean";
        },
        this
      )
      .map((narcissism: any) => {
        let townId = narcissism.getTownId();
        let power = window.HelperPower.createCastedPowerModel("cleanse", townId);
        power.cast();
        power = window.HelperPower.createCastedPowerModel("town_protection", townId);
        power.cast();
      });
  }
});

$.Observer(window.GameEvents.attack.incoming).subscribe("TR_WAR", function (e: any, data: any) {
  if (false) {
    $("#building_main_area_farm").click();
    window.BuildingFarm.action_request_militia();
  }
});

function loadFeatures() {
  // Implement old jQuery method (version < 1.9)

  $.fn.toggleClick = function () {
    var methods = arguments; // Store the passed arguments for future reference
    var count = methods.length; // Cache the number of methods

    // Use return this to maintain jQuery chainability
    // For each element you bind to
    return this.each(function (i, item) {
      // Create a local counter for that element
      var index = 0;

      // Bind a click handler to that element
      $(item).on("click", function () {
        // That when called will apply the 'index'th method to that element
        // the index % count means that we constrain our iterator between 0
        // and (count-1)
        return methods[index++ % count].apply(this, arguments);
      });
    });
  };

  if (typeof window.ITowns !== "undefined") {
    if (window.tr.overviews.Culture == "") {
      window.tr.overviews.Culture = window.DM.getl10n("mass_recruit").sort_by.name;
      window.tr.overviews.Culture_Dif = ">";

      window.tr.overviews.Buildings = window.DM.getl10n("mass_recruit").sort_by.name;
      window.tr.overviews.Buildings_Dif = ">";

      window.tr.overviews.Gods = window.DM.getl10n("mass_recruit").sort_by.name;
      tr_storage.save("overviews", window.tr.overviews);
    }

    PremiumHide.activate();

    addFunctionToITowns();
    Queues.windows.queues_main_index();

    if (window.tr.settings.tr_tkb) {
      //$("#tr_tkb").toggleClass("checked");
      setTimeout(function () {
        Taskbar.activate();
      }, 0);
    }

    setTimeout(function () {
      var waitCount = 0;

      // No comment... it's Grepolis... i don't know... *rolleyes*
      function waitForGrepoLazyLoading() {
        if (typeof window.ITowns.townGroups.getGroupsTR()[-1] !== "undefined" && typeof window.ITowns.getTown(window.Game.townId).getBuildings !== "undefined") {
          try {
            // Funktion wird manchmal nicht ausgeführt:
            var units = window.ITowns.getTown(window.Game.townId).units();

            getAllUnits();

            setInterval(function () {
              getAllUnits();
            }, 5000); // 15min

            if (window.tr.settings.tr_cave) {
              setTimeout(function () {
                Caving.activate();
              }, 5000);
            }

            if (window.tr.settings.tr_ava) {
              setTimeout(function () {
                AvailableUnits.activate();
              }, 5000);
            }

            if (window.tr.settings.tr_ava2) {
              setTimeout(function () {
                AvailableUnits.ocean.activate();
              }, 5000);
            }

            if (window.tr.settings.tr_newsimulator) {
              setTimeout(function () {
                Simulator.activate();
              }, 0);
            }

            if (window.tr.settings.tr_rec) {
              setTimeout(function () {
                RecruitingTrade.activate();
              }, 0);
            }

            if (window.tr.settings.tr_fvrpop) {
              setTimeout(function () {
                FavorPopup.activate();
              }, 0);
            }

            if (window.tr.settings.tr_unitstrength) {
              setTimeout(function () {
                UnitStrength.Menu.activate();
              }, 0);
            }

            if (window.tr.settings.tr_twnicn) {
              setTimeout(function () {
                TownIcons.activate();
                TownIcons.auto.activate();
              }, 0);
            }

            if (window.tr.settings.tr_til) {
              setTimeout(function () {
                TownList.activate();
              }, 0);
            }

            if (window.tr.settings.tr_tim) {
              setTimeout(function () {
                MapWorld.activate();
              }, 0);
            }

            if (window.tr.settings.tr_tiw) {
              setTimeout(function () {
                TownPopup.activate();
              }, 0);
            }

            if (window.tr.settings.tr_sen) {
              setTimeout(function () {
                SentUnits.activate();
              }, 0);
            }

            if (window.tr.settings.tr_com) {
              setTimeout(function () {
                UnitComparison.activate();
              }, 0);
            }

            if (window.tr.settings.tr_tra) {
              setTimeout(function () {
                TransportCapacity.activate();
              }, 0);
            }

            if (window.tr.settings.tr_sci) {
              setTimeout(function () {
                Science.activate();
                CityModel.activate();
              }, 0);
            }

            if (window.tr.settings.tr_tti) {
              setTimeout(function () {
                townTradeImprovement.activate();
              }, 0);
            }

            if (window.tr.settings.tr_clt) {
              setTimeout(function () {
                Culture.activate();
              }, 0);
            }

            if (window.tr.settings.tr_way) {
              setTimeout(function () {
                ShortDuration.activate();
              }, 0);
            }

            if (window.tr.settings.tr_oceanNumbers) {
              setTimeout(function () {
                OceanNumbers.activate();
              }, 0);
            }

            Settings.activate();
            ModuleHub.init();
            Queues.init();
            ModuleHub.checkAutostart();
          } catch (e: any) {
            if (waitCount < 12) {
              waitCount++;

              console.warn(e);

              // Ausführung wiederholen
              setTimeout(function () {
                waitForGrepoLazyLoading();
              }, 5000); // 5s
            } else {
              console.error(e);
            }
          }
        } else {
          var e = {
            stack: "getGroups() = " + typeof window.ITowns.townGroups.getGroupsTR()[-1] + ", getBuildings() = " + typeof window.ITowns.getTown(window.Game.townId).getBuildings
          };

          if (waitCount < 12) {
            waitCount++;

            console.warn(e.stack);

            // Ausführung wiederholen
            setTimeout(function () {
              waitForGrepoLazyLoading();
            }, 5000); // 5s
          } else {
            console.log(e);
          }
        }
      }
      waitForGrepoLazyLoading();
    }, 0);

    if (window.tr.settings.tr_mousewheel) {
      setTimeout(function () {
        MouseWheelZoom.activate();
      }, 0);
    }

    if (true)
      setTimeout(() => {
        cultureOverview.activate();
      }, 100);
    if (true)
      setTimeout(() => {
        cultureProgress.activate();
      }, 0);

    CityModel.createTownGroup();

    setInterval(() => {
      checkGold();
    }, 60000);
  } else {
    setTimeout(function () {
      loadFeatures();
    }, 100);
  }
}

loadFeatures();

$(document).ajaxComplete(function (e: any, xhr: { responseText: string }, opt: { url: string }) {
  try {
    var url = opt.url.split("?"),
      action = "";
    if (typeof url[1] !== "undefined" && typeof url[1].split(/&/)[1] !== "undefined") {
      action = url[0].substr(5) + "/" + url[1].split(/&/)[1].substr(7);
    }
    console.log("actions: ", action);

    var wnd = window.GPWindowMgr.getFocusedWindow() || false;
    if (wnd) {
      window.tr.wndId = wnd.getID();
      window.tr.wnd = wnd.getJQElement().find(".gpwindow_content");
    }

    switch (action) {
      case "/map_data/get_chunks":
      case "/frontend_bridge/fetch":
        MapWorld.add();
        break;
      case "/frontend_bridge/execute":
        MarketSimplifier.activate();
        break;
      case "/building_main/index":
      case "/building_main/build":
      case "/building_main/cancel":
      case "/building_main/tear_down":
        Queues.windows.building_main_index();
        buildingsLvlPts();
        break;
      case "/building_barracks/index":
      case "/building_barracks/build":
      case "/building_barracks/cancel":
        setTimeout(() => {
          Queues.windows.building_barracks_harbor_index("unit");
        }, 500);
        spells("barracks");
        if (window.tr.settings.tr_unitstrength) {
          UnitStrength.Barracks.add();
        }
        break;
      case "/building_docks/index":
      case "/building_docks/build":
      case "/building_docks/cancel":
      case "/building_barracks/units":
        setTimeout(() => {
          Queues.windows.building_barracks_harbor_index("ship");
        }, 500);
        spells("docks");
        if (window.tr.settings.tr_unitstrength) {
          UnitStrength.Harbor.add();
        }
        break;
      case "/building_place/simulator":
        setStrengthSimulator();
        if (window.tr.settings.tr_newsimulator) {
          Simulator.activate();
        }
        break;
      case "/building_place/culture":
        culturePoints.activate();
        cultureProgress.add();
        cultureOverview.add();
        break;
      case "/index/switch_town":
        if (window.tr.settings.tr_unitstrength) {
          setTimeout(function () {
            UnitStrength.Menu.update();
          }, 0);
        }
        if (window.tr.settings.tr_tra) {
          setTimeout(function () {
            TransportCapacity.update();
          }, 0);
        }
        break;
      case "/building_place/simulate":
      case "/building_place/insertSurvivesDefUnitsAsNewDefender":
        if (window.tr.settings.tr_newsimulator) {
          afterSimulation();
        }
        break;
      case "/town_info/trading":
      case "/town_info/info":
      case "/town_info/attack":
      case "/town_info/support":
        TownTabHandler(action.split("/")[2]);
      case "/wonders/index":
        if (window.Game.features.end_game_type == "end_game_type_world_wonder") {
          WWTradeHandler();
        }
      // Premium
      case "/command_info/conquest_info":
        if (window.tr.settings.tr_unitstrength) {
          UnitStrength.Conquest.add();
        }
        break;
      case "/farm_town_overviews/index":
        FarmingVillagesHelper.islandHeader();
        break;
      case "/farm_town_overviews/claim_loads":
        FarmingVillagesHelper.rememberloot();
        FarmingVillagesHelper.indicateLoot();
        break;
      case "/farm_town_overviews/get_farm_towns_for_town":
        FarmingVillagesHelper.setloot();
        break;
      case "/town_overviews/culture_overview":
      case "/town_overviews/start_celebration":
      case "/town_overviews/start_all_celebrations":
        cultureOverview.add();
      case "/town_overviews/store_iron":
        if (window.tr.settings.tr_cave) {
          Caving.refresh_silver_total(xhr);
        }
        break;
      case "/town_overviews/trade_overview":
        resCounter.init();
        addPercentTrade(1234, false); // TODO
        break;
    }
  } catch (error) {
    console.error(error, "handleAjaxCompleteObserver");
  }
});
