/********************************************************************************************************************************
 * Unit strength (blunt/sharp/distance) and Transport Capacity
 * ----------------------------------------------------------------------------------------------------------------------------
 * | ● Unit strength: Menu
 * |	- Switching of def/off display with buttons
 * |	- Possible Selection of certain unit types
 * | ● Unit strength: Conquest
 * | ● Unit strength: Barracks
 * | ● Transport capacity: Menu
 * |	- Switching of transporter speed (+/- big transporter)
 * ----------------------------------------------------------------------------------------------------------------------------
 * ******************************************************************************************************************************/

var UnitStrength = {
  // Calculate defensive strength
  calcDef: function (units: any) {
    var e;
    window.tr.strength.blunt = window.tr.strength.sharp = window.tr.strength.dist = 0;
    for (e in units) {
      if (units.hasOwnProperty(e)) {
        if (!window.GameData.units[e].is_naval) {
          window.tr.strength.blunt += units[e] * (window.GameData.units[e].def_hack || 0);
          window.tr.strength.sharp += units[e] * (window.GameData.units[e].def_pierce || 0);
          window.tr.strength.dist += units[e] * (window.GameData.units[e].def_distance || 0);
        } else {
          window.tr.strength.naval_def += units[e] * (window.GameData.units[e].defense || 0);
        }
      }
    }
  },
  // Calculate offensive strength
  calcOff: function (units: any, selectedUnits: any) {
    var e;
    window.tr.strength.blunt = window.tr.strength.sharp = window.tr.strength.dist = 0;
    for (e in selectedUnits) {
      if (selectedUnits.hasOwnProperty(e)) {
        if (!window.GameData.units[e].is_naval) {
          var attack = (units[e] || 0) * window.GameData.units[e].attack;

          switch (window.GameData.units[e].attack_type) {
            case "hack":
              window.tr.strength.blunt += attack;
              break;
            case "pierce":
              window.tr.strength.sharp += attack;
              break;
            case "distance":
              window.tr.strength.dist += attack;
              break;
          }
        } else {
          window.tr.strength.naval_attc += units[e] * (window.GameData.units[e].attack || 0);
        }
      }
    }
  },
  /*******************************************************************************************************************************
   * ● Unit strength: Unit menu
   *******************************************************************************************************************************/
  Menu: {
    activate: function () {
      $(
        //'<a id="tr_Caserne" style="position: absolute; width: 70px; height: 70px; left: 0px;"></a>' +
        '<div id="tr_strength" class="cont def" style="display:none;",><hr>' +
          '<span class="bold text_shadow cont_left strength_font">' +
          '<table style="margin:0px;">' +
          '<tr><td><div class="ico units_info_sprite img_hack"></td><td id="tr_blunt">0</td></tr>' +
          '<tr><td><div class="ico units_info_sprite img_pierce"></td><td id="tr_sharp">0</td></tr>' +
          '<tr><td><div class="ico units_info_sprite img_dist"></td><td id="tr_dist">0</td></tr>' +
          "</table>" +
          "</span>" +
          '<div class="cont_right">' +
          '<img id="tr_def_button" class="active img" src="https://gpall.innogamescdn.com/images/game/unit_overview/support.png">' +
          '<img id="tr_off_button" class="img" src="https://gpall.innogamescdn.com/images/game/unit_overview/attack.png">' +
          "</div></div>" +
          '<div id= "tr_tr_btn" class="">Unit Strength</div>'
      ).appendTo(".units_land .content");

      $(".units_land .nav .border_top").on("click", function () {
        window.BarracksWindowFactory.openBarracksWindow();
      });

      $("#tr_tr_btn").on("click", function () {
        if ($("#tr_strength").css("display") == "none") {
          UnitStrength.Menu.update();
        }
        $("#tr_strength").slideToggle();
      });
      $("#tr_tr_btn").hover(
        function () {
          $("#tr_tr_btn").css({
            color: "#ECB44D"
          });
        },
        function () {
          $("#tr_tr_btn").css({
            color: "#EEDDBB"
          });
        }
      );
      /*$("#tr_strength").css({
                       "display" : "none",
                   });*/
      $("#tr_strength").on("click", function () {
        UnitStrength.Menu.update();
      });
      $("#tr_tr_btn").css({
        cursor: "pointer",
        //"position" : "relative",
        height: "12px",
        width: "127px",
        "font-size": "10px",
        "font-weight": "bold",
        color: "#EEDDBB",
        background: "url(https://gpfr.innogamescdn.com/images/game/layout/progressbars-sprite_2.90_compressed.png) no-repeat 0 -100px"
        //background: url(https://gpfr.innogamescdn.com/images/game/layout/progressbars-sprite_2.90_compressed.png) no-repeat 0 0;
      });

      // Style
      $(
        '<style id="tr_strength_style">' +
          "#tr_strength.def #off_button, #tr_strength.off #def_button { filter:url(#Sepia); -webkit-filter:sepia(1); }" +
          "#tr_strength.off #off_button, #tr_strength.def #def_button { filter:none; -webkit-filter:none; } " +
          "#tr_strength.off .img_hack 		{ background-position:0% 36%;} " +
          "#tr_strength.def .img_hack 		{ background-position:0%  0%;} " +
          "#tr_strength.off .img_pierce 	{ background-position:0% 27%;} " +
          "#tr_strength.def .img_pierce 	{ background-position:0%  9%;} " +
          "#tr_strength.off .img_dist 		{ background-position:0% 45%;} " +
          "#tr_strength.def .img_dist 		{ background-position:0% 18%;} " +
          "#tr_strength .strength_font 	{ font-size: 0.8em; } " +
          "#tr_strength.off .strength_font { color:#edb;} " +
          "#tr_strength.def .strength_font { color:#fc6;} " +
          "#tr_strength .ico 				{ height:20px; width:20px; } " +
          "#tr_strength .units_info_sprite { background:url(https://gpall.innogamescdn.com/images/game/units/units_info_sprite2.51.png); background-size:100%; } " +
          "#tr_strength .img_pierce { background-position:0px -20px; } " +
          "#tr_strength .img_dist 	 { background-position:0px -40px; } " +
          "#tr_strength hr 		 { margin:0px; background-color:#5F5242; height:2px; border:0px solid; } " +
          "#tr_strength .cont_left  { width:65%;  display:table-cell; } " +
          "#tr_strength.cont { background:url(https://gpall.innogamescdn.com/images/game/layout/layout_units_nav_border.png); } " +
          "#tr_strength .cont_right { width:30%; display:table-cell; vertical-align:middle; } " +
          "#tr_strength .img 		 { float:right; background:none; margin:2px 8px 2px 0px; } " +
          "</style>"
      ).appendTo("head");

      // Button events
      $(".units_land .units_wrapper, .btn_gods_spells .checked").on("click", function () {
        setTimeout(function () {
          UnitStrength.Menu.update();
        }, 100);
      });

      $("#tr_off_button").on("click", function () {
        $("#tr_strength").addClass("off").removeClass("def");

        window.tr.strength.def = false;
        UnitStrength.Menu.update();
      });
      $("#tr_def_button").on("click", function () {
        $("#tr_strength").addClass("def").removeClass("off");

        window.tr.strength.def = true;
        UnitStrength.Menu.update();
      });
      $("#tr_def_button, #tr_off_button").hover(function () {
        $(this).css("cursor", "pointer");
      });

      UnitStrength.Menu.update();

      $.Observer(window.GameEvents.town.town_switch).subscribe("TR_UNIT", function (e: any, data: any) {
        UnitStrength.Menu.update();
      });
    },
    deactivate: function () {
      $("#tr_strength").remove();
      $("#tr_strength_style").remove();
      $("#tr_Caserne").remove();
      $("#tr_tr_btn").remove();
    },
    update: function () {
      var unitsIn = window.ITowns.getTown(window.Game.townId).units(),
        units = UnitStrength.Menu.getSelected();

      // Calculation
      if (window.tr.strength.def === true) {
        UnitStrength.calcDef(units);
      } else {
        UnitStrength.calcOff(unitsIn, units);
      }

      $("#tr_blunt").get(0).innerHTML = window.tr.strength.blunt;
      $("#tr_sharp").get(0).innerHTML = window.tr.strength.sharp;
      $("#tr_dist").get(0).innerHTML = window.tr.strength.dist;
    },
    getSelected: function () {
      var units: any[] = [];
      if ($(".units_land .units_wrapper .selected").length > 0) {
        $(".units_land .units_wrapper .selected").each(function () {
          // @ts-ignore
          units[this.className.split(" ")[1]] = this.children[0].innerHTML;
        });
      } else {
        $(".units_land .units_wrapper .unit").each(function () {
          // @ts-ignore
          units[this.className.split(" ")[1]] = this.children[0].innerHTML;
        });
      }

      return units;
    }
  },
  /*******************************************************************************************************************************
   * ● Unit strength: Conquest
   *******************************************************************************************************************************/
  Conquest: {
    add: function () {
      var units: any[] = [],
        str;

      // units of the siege
      $("#conqueror_units_in_town .unit").each(function () {
        str = $(this).attr("class").split(" ")[4];

        if (!window.GameData.units[str].is_naval) {
          // @ts-ignore
          units[str] = parseInt(this.children[0].innerHTML, 10);
          //console.log($(this).attr("class").split(" ")[4]);
        }
      });
      // calculation

      UnitStrength.calcDef(units);

      $(
        '<div id="tr_strength_eo" class="game_border" style="width:90px; margin: 20px; align:center;">' +
          '<div class="game_border_top"></div><div class="game_border_bottom"></div>' +
          '<div class="game_border_left"></div><div class="game_border_right"></div>' +
          '<div class="game_border_corner corner1"></div><div class="game_border_corner corner2"></div>' +
          '<div class="game_border_corner corner3"></div><div class="game_border_corner corner4"></div>' +
          '<span class="bold" style="color:#000;font-size: 0.8em;"><table style="margin:0px;background:#f7dca2;width:100%;align:center;">' +
          '<tr><td width="1%"><div class="ico units_info_sprite img_hack"></div></td><td id="tr_bl" align="center" width="100%">0</td></tr>' +
          '<tr><td><div class="ico units_info_sprite img_pierce"></div></td><td id="tr_sh" align="center">0</td></tr>' +
          '<tr><td><div class="ico units_info_sprite img_dist"></div></td><td id="tr_di" align="center">0</td></tr>' +
          "</table></span>" +
          "</div>"
      ).appendTo("#conqueror_units_in_town");

      $("#tr_strength_eo").mousePopup(new MousePopup("Gesamteinheitenstärke der Belagerungstruppen"));

      // Veröffentlichung-Button-Text
      $("#conqueror_units_in_town .publish_conquest_public_id_wrap").css({
        marginLeft: "130px"
      });

      $("#tr_strength_eo .ico").css({
        height: "20px",
        width: "20px"
      });
      $("#tr_strength_eo .units_info_sprite").css({
        background: "url(https://gpall.innogamescdn.com/images/game/units/units_info_sprite2.51.png)",
        backgroundSize: "100%"
      });
      $("#tr_strength_eo .img_pierce").css({ backgroundPosition: "0% 9%" });
      $("#tr_strength_eo .img_dist").css({ backgroundPosition: "0% 18%" });

      // @ts-ignore
      $("#tr_bl").get(0)?.innerHTML = window.tr.strength.blunt;
      // @ts-ignore
      $("#tr_sh").get(0)?.innerHTML = window.tr.strength.sharp;
      // @ts-ignore
      $("#tr_di").get(0)?.innerHTML = window.tr.strength.dist;
    }
  },
  /*******************************************************************************************************************************
   * ● Unit strength: Barracks
   *******************************************************************************************************************************/
  Barracks: {
    add: function () {
      if (!$("#tr_strength_baracks").get(0)) {
        var units: any[] = [],
          pop = 0;

        // whole units of the town
        $("#units .unit_order_total").each(function () {
          // @ts-ignore
          units[$(this).parent().parent().attr("id")] = this.innerHTML;
        });
        // calculation
        UnitStrength.calcDef(units);

        // population space of the units
        for (var e in units) {
          if (units.hasOwnProperty(e)) {
            pop += units[e] * window.GameData.units[e].population;
          }
        }
        $(
          '<div id="tr_strength_baracks" class="game_border" style="float:right; width:70px; align:center;">' +
            '<div class="game_border_top"></div><div class="game_border_bottom"></div>' +
            '<div class="game_border_left"></div><div class="game_border_right"></div>' +
            '<div class="game_border_corner corner1"></div><div class="game_border_corner corner2"></div>' +
            '<div class="game_border_corner corner3"></div><div class="game_border_corner corner4"></div>' +
            '<span class="bold" style="color:#000;font-size: 0.8em;"><table style="margin:0px;background:#f7dca2;width:100%;align:center;">' +
            '<tr><td width="1%"><div class="ico units_info_sprite img_def_hack"></div></td><td id="tr_def_b" align="center" width="100%">0</td></tr>' +
            '<tr><td><div class="ico units_info_sprite img_def_pierce"></div></td><td id="tr_def_s" align="center">0</td></tr>' +
            '<tr><td><div class="ico units_info_sprite img_def_dist"></div></td><td id="tr_def_d" align="center">0</td></tr>' +
            "</table></span>" +
            "</div>"
        ).appendTo(".ui-dialog #units");

        $(
          '<div id="tr_strength_baracks" class="game_border" style="float:right; width:70px; align:center;">' +
            '<div class="game_border_top"></div><div class="game_border_bottom"></div>' +
            '<div class="game_border_left"></div><div class="game_border_right"></div>' +
            '<div class="game_border_corner corner1"></div><div class="game_border_corner corner2"></div>' +
            '<div class="game_border_corner corner3"></div><div class="game_border_corner corner4"></div>' +
            '<span class="bold" style="color:#000;font-size: 0.8em;"><table style="margin:0px;background:#f7dca2;width:100%;align:center;">' +
            '<tr><td width="1%"><div class="ico units_info_sprite img_attc_hack"></div></td><td id="tr_attc_b" align="center" width="100%">0</td></tr>' +
            '<tr><td><div class="ico units_info_sprite img_attc_pierce"></div></td><td id="tr_attc_s" align="center">0</td></tr>' +
            '<tr><td><div class="ico units_info_sprite img_attc_dist"></div></td><td id="tr_attc_d" align="center">0</td></tr>' +
            "</table></span>" +
            "</div>"
        ).appendTo(".ui-dialog #units");

        $(
          '<div id="tr_pop_baracks" class="game_border" style="float:right; width:60px; align:center;">' +
            '<div class="game_border_top"></div><div class="game_border_bottom"></div>' +
            '<div class="game_border_left"></div><div class="game_border_right"></div>' +
            '<div class="game_border_corner corner1"></div><div class="game_border_corner corner2"></div>' +
            '<div class="game_border_corner corner3"></div><div class="game_border_corner corner4"></div>' +
            '<span class="bold" style="color:#000;font-size: 0.8em;"><table style="margin:0px;background:#f7dca2;width:100%;align:center;">' +
            '<tr><td width="1%"><img class="ico" src="https://gpall.innogamescdn.com/images/game/res/pop.png"></td><td id="tr_p" align="center" width="100%">0</td></tr>' +
            "</table></span>" +
            "</div>"
        ).appendTo(".ui-dialog #units");

        $(".ui-dialog #units .ico").css({
          height: "20px",
          width: "20px"
        });
        $(".ui-dialog #units .units_info_sprite").css({
          background: "url(https://gpall.innogamescdn.com/images/game/units/units_info_sprite2.51.png)",
          backgroundSize: "100%"
        });
        $(".ui-dialog #units .img_def_pierce").css({
          backgroundPosition: "0% 9%"
        });
        $(".ui-dialog #units .img_def_dist").css({
          backgroundPosition: "0% 18%"
        });

        $(".ui-dialog #units .img_attc_pierce").css({
          backgroundPosition: "0% 27%"
        });
        $(".ui-dialog #units .img_attc_hack").css({
          backgroundPosition: "0% 36%"
        });
        $(".ui-dialog #units .img_attc_dist").css({
          backgroundPosition: "0% 45%"
        });

        //$('#tr_pop_baracks').mousePopup(new MousePopup('Bevölkerungszahl aller Landeinheiten der Stadt'));
        //$('#tr_strength_baracks').mousePopup(new MousePopup('Gesamteinheitenstärke stadteigener Truppen'));

        // @ts-ignore
        $("#tr_def_b").get(0)?.innerHTML = window.tr.strength.blunt;
        // @ts-ignore
        $("#tr_def_s").get(0)?.innerHTML = window.tr.strength.sharp;
        // @ts-ignore
        $("#tr_def_d").get(0)?.innerHTML = window.tr.strength.dist;
        // @ts-ignore
        $("#tr_p").get(0)?.innerHTML = pop;

        // calculation
        UnitStrength.calcOff(units, units);
        // @ts-ignore
        $("#tr_attc_b").get(0)?.innerHTML = window.tr.strength.blunt;
        // @ts-ignore
        $("#tr_attc_s").get(0)?.innerHTML = window.tr.strength.sharp;
        // @ts-ignore
        $("#tr_attc_d").get(0)?.innerHTML = window.tr.strength.dist;
      }
    }
  },

  /*******************************************************************************************************************************
   * ● Unit strength: Harbor
   *******************************************************************************************************************************/
  Harbor: {
    add: function () {
      if (!$("#tr_strength_harbor").get(0)) {
        var units: any[] = [],
          pop = 0;

        // whole units of the town
        $("#units .unit_order_total").each(function () {
          // @ts-ignore
          units[$(this).parent().parent().attr("id")] = this.innerHTML;
        });
        // calculation
        UnitStrength.calcDef(units);

        // population space of the units
        for (var e in units) {
          if (units.hasOwnProperty(e)) {
            pop += units[e] * window.GameData.units[e].population;
          }
        }

        $(
          '<div id="tr_strength_harbor" class="game_border" style="float:right; width:70px; align:center;">' +
            '<div class="game_border_top"></div><div class="game_border_bottom"></div>' +
            '<div class="game_border_left"></div><div class="game_border_right"></div>' +
            '<div class="game_border_corner corner1"></div><div class="game_border_corner corner2"></div>' +
            '<div class="game_border_corner corner3"></div><div class="game_border_corner corner4"></div>' +
            '<span class="bold" style="color:#000;font-size: 0.8em;"><table style="margin:0px;background:#f7dca2;width:100%;align:center;">' +
            '<tr><td width="1%"><div class="ico units_info_sprite img_attc"></div></td><td id="tr_attc" align="center" width="100%">0</td></tr>' +
            '<tr><td><div class="ico units_info_sprite img_def"></div></td><td id="tr_def" align="center">0</td></tr>' +
            "</table></span>" +
            "</div>"
        ).appendTo(".ui-dialog #units");

        $(
          '<div id="tr_pop_harbor" class="game_border" style="float:right; width:60px; align:center;">' +
            '<div class="game_border_top"></div><div class="game_border_bottom"></div>' +
            '<div class="game_border_left"></div><div class="game_border_right"></div>' +
            '<div class="game_border_corner corner1"></div><div class="game_border_corner corner2"></div>' +
            '<div class="game_border_corner corner3"></div><div class="game_border_corner corner4"></div>' +
            '<span class="bold" style="color:#000;font-size: 0.8em;"><table style="margin:0px;background:#f7dca2;width:100%;align:center;">' +
            '<tr><td width="1%"><img class="ico" src="https://gpall.innogamescdn.com/images/game/res/pop.png"></td><td id="tr_p" align="center" width="100%">0</td></tr>' +
            "</table></span>" +
            "</div>"
        ).appendTo(".ui-dialog #units");

        $(".ui-dialog #units .ico").css({
          height: "20px",
          width: "20px"
        });
        $(".ui-dialog #units .units_info_sprite").css({
          background: "url(https://gpall.innogamescdn.com/images/game/units/units_info_sprite2.51.png)",
          backgroundSize: "100%"
        });

        $(".ui-dialog #units .img_attc").css({ backgroundPosition: "0% 73%" });
        $(".ui-dialog #units .img_def").css({ backgroundPosition: "0% 81%" });

        // @ts-ignore
        $("#tr_p").get(0)?.innerHTML = pop;
        // @ts-ignore
        $("#tr_def").get(0)?.innerHTML = window.tr.strength.naval_def;
        // calculation
        UnitStrength.calcOff(units, units);
        // @ts-ignore
        $("#tr_attc").get(0)?.innerHTML = window.tr.strength.naval_attc;
      }
    }
  }
};

export default UnitStrength;
