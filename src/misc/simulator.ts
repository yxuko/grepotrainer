import UnitStrength from "./unit_strength";

/*******************************************************************************************************************************
 * Simulator
 * ----------------------------------------------------------------------------------------------------------------------------
 * | ● Layout adjustment
 * | ● Unit strength for entered units (without modificator influence yet)
 * ----------------------------------------------------------------------------------------------------------------------------
 *******************************************************************************************************************************/
var Simulator = {
  activate: function () {
    $(
      '<style id="tr_simulator_style" type="text/css">' +
        "#place_simulator { overflow: hidden !important} " +
        "#place_simulator .place_sim_bonuses_heroes .place_symbol.place_def_losts { margin-bottom: 27px; } " +
        "#place_simulator_form .game_body { padding: 0px 1px;!important; } " +
        "#place_simulator .place_sim_heroes_container .place_simulator_table { height: 64px!important; } " +
        // Bonus container
        "#place_simulator .place_sim_wrap_mods { margin-bottom: 0px!important; } " + // Hide modifier box button
        ".place_sim_wrap_mods .place_simulator_table tbody tr:last-child { height: 47px; vertical-align: top; } " +
        // Sea unit box
        "#place_sim_naval_units tbody tr:last-child { height:auto !important;}" +
        "#place_sim_naval_units { margin-bottom: 91px;}" +
        // Select boxes
        ".place_sim_select_gods_wrap { position:absolute; bottom: 178px; left: 160px;} " +
        ".place_sim_select_gods_wrap .place_symbol, .place_sim_select_strategies .place_symbol { margin: 1px 2px 0px 5px !important} " +
        ".place_sim_select_gods_wrap select { max-width: 200px; width: auto!important;} " +
        //adaptation of the layout
        "#place_simulator_form a.unit, #place_simulator_form div.unit { margin: 3px 2px;}" +
        ".place_simulator_table .place_image { margin: 1px 2px;" +
        "</style>"
    ).appendTo("head");

    if ($("#place_simulator").get(0)) {
      setStrengthSimulator();
    }

    SimulatorStrength.activate();
  },
  deactivate: function () {
    $("#tr_simulator_style").remove();
    if ($("#simu_table").get(0)) {
      $("#simu_table").remove();
    }

    SimulatorStrength.deactivate();
  }
};

function afterSimulation() {
  let lossArray: { [key: string]: { [key: string]: any } } = {
      att: { res: 0, fav: 0, pop: 0 },
      def: { res: 0, fav: 0, pop: 0 }
    },
    wall_level = parseInt($('.place_sim_wrap_mods .place_insert_field[name="sim[mods][def][wall_level]"]').val(), 10),
    wall_damage = parseInt($("#building_place_def_losses_wall_level").get(0).innerHTML, 10),
    wall_iron = [0, 200, 429, 670, 919, 1175, 1435, 1701, 1970, 2242, 2518, 2796, 3077, 3360, 3646, 3933, 4222, 4514, 4807, 5101, 5397, 5695, 5994, 6294, 6596, 6899];

  // Calculate unit losses
  $("#place_sim_ground_units .place_losses, #place_sim_naval_units .place_losses").each(function () {
    const loss = parseInt(this.innerHTML, 10) || 0;
    //console.log(this.innerHTML);
    if (loss > 0) {
      const unit = this.id.substring(26);
      const side = this.id.split("_")[2]; // att / def

      lossArray[side].res += loss * (window.GameData.units[unit].resources.wood + window.GameData.units[unit].resources.stone + window.GameData.units[unit].resources.iron);

      lossArray[side].fav += loss * window.GameData.units[unit].favor;

      lossArray[side].pop += loss * window.GameData.units[unit].population;
    }
  });
  // Calculate wall resource losses
  for (let w = wall_level; w > wall_level - wall_damage; w--) {
    lossArray.def.res += 400 + w * 350 + wall_iron[w]; // wood amount is constant, stone amount is multiplicative and iron amount is irregular for wall levels
  }

  // Insert losses into table
  for (let x in lossArray) {
    if (lossArray.hasOwnProperty(x)) {
      for (var z in lossArray[x]) {
        if (lossArray[x].hasOwnProperty(z)) {
          //console.log(((z === "res") && (lossArray[x][z] > 10000)) ? (Math.round(lossArray[x][z] / 1000) + "k") : lossArray[x][z]);

          $("#tr_" + x + "_" + z).get(0).innerHTML = z === "res" && lossArray[x][z] > 10000 ? Math.round(lossArray[x][z] / 1000) + "k" : lossArray[x][z];
        }
      }
    }
  }
}

// Stärkeanzeige: Simulator
var SimulatorStrength: SimulatorStrengthType = {
  timeout: null,

  activate: function () {
    $(
      '<style id="tr_simulator_strength_style">' +
        "#tr_simulator_strength { position:absolute; top:188px; font-size:0.8em; width:63%; } " +
        "#tr_simulator_strength .ico { height:20px; width:20px; margin:auto; } " +
        "#tr_simulator_strength .units_info_sprite { background:url(https://gpall.innogamescdn.com/images/game/units/units_info_sprite2.51.png); background-size:100%; } " +
        "#tr_simulator_strength .img_hack { background-position:0% 36%; } " +
        "#tr_simulator_strength .img_pierce { background-position:0% 27%; } " +
        "#tr_simulator_strength .img_dist { background-position:0% 45% !important; } " +
        "#tr_simulator_strength .img_ship { background-position:0% 72%; } " +
        "#tr_simulator_strength .img_fav { background: url(https://gpall.innogamescdn.com/images/game/res/favor.png) !important; background-size: 100%; } " +
        "#tr_simulator_strength .img_res { background: url(https://gpall.innogamescdn.com/images/game/units/units_info_sprite2.51.png) 0% 54%; background-size: 100%; } " +
        "#tr_simulator_strength .img_pop { background: url(https://gpall.innogamescdn.com/images/game/res/pop.png); background-size:100%; } " +
        "#tr_simulator_strength .left_border { width: 46px; } " +
        "</style>"
    ).appendTo("head");

    SimulatorStrength.timeout = setInterval(() => {
      if ($("#tr_simulator_strength").length) {
        afterSimulation();
      }
    }, 900);
  },
  deactivate: function () {
    $("#tr_simulator_strength_style").remove();
    $("#tr_simulator_strength").remove();

    clearTimeout(SimulatorStrength.timeout);
    SimulatorStrength.timeout = null;
  }
};

function setStrengthSimulator() {
  $(
    '<div id="tr_simulator_strength" style="width: 49%;">' +
      '<div style="float:left; margin-right: -4px;"><h4>Unit Strength</h4>' +
      '<table class="place_simulator_table strength" cellpadding="0px" cellspacing="0px" style="align:center;">' +
      "<tr>" +
      '<td class="place_simulator_even"></td>' +
      '<td class="left_border place_simulator_odd"><div class="ico units_info_sprite img_hack"></div></td>' +
      '<td class="left_border place_simulator_even"><div class="ico units_info_sprite img_pierce"></div></td>' +
      '<td class="left_border place_simulator_odd"><div class="ico units_info_sprite img_dist"></div></td>' +
      '<td class="left_border place_simulator_even"><div class="ico units_info_sprite img_ship"></div></td>' +
      "</tr><tr>" +
      '<td class="place_simulator_even"><div class="place_symbol place_att"></div></td>' +
      '<td class="left_border place_simulator_odd" id="tr_att_b">0</td>' +
      '<td class="left_border place_simulator_even" id="tr_att_s">0</td>' +
      '<td class="left_border place_simulator_odd" id="tr_att_d">0</td>' +
      '<td class="left_border place_simulator_even" id="tr_att_ship">0</td>' +
      "</tr><tr>" +
      '<td class="place_simulator_even"><div class="place_symbol place_def"></div></td>' +
      '<td class="left_border place_simulator_odd" id="tr_def_b">0</td>' +
      '<td class="left_border place_simulator_even" id="tr_def_s">0</td>' +
      '<td class="left_border place_simulator_odd" id="tr_def_d">0</td>' +
      '<td class="left_border place_simulator_even" id="tr_def_ship">0</td>' +
      "</tr>" +
      "</table>" +
      "</div><div><h4>" +
      "Loss" +
      "</h4>" +
      '<table class="place_simulator_table loss" cellpadding="0px" cellspacing="0px" style="align:center; height: 64px;">' +
      "<tr>" +
      //'<td class="place_simulator_even"></td>' +
      '<td class="left_border place_simulator_odd"><div class="ico units_info_sprite img_res"></div></td>' +
      '<td class="left_border place_simulator_even"><div class="ico units_info_sprite img_fav"></div></td>' +
      '<td class="left_border place_simulator_odd"><div class="ico units_info_sprite img_pop"></div></td>' +
      "</tr><tr>" +
      //'<td class="place_simulator_even"><div class="place_symbol place_att"></div></td>' +
      '<td class="left_border place_simulator_odd" id="tr_att_res">0</td>' +
      '<td class="left_border place_simulator_even" id="tr_att_fav">0</td>' +
      '<td class="left_border place_simulator_odd" id="tr_att_pop">0</td>' +
      "</tr><tr>" +
      //'<td class="place_simulator_even"><div class="place_symbol place_def"></div></td>' +
      '<td class="left_border place_simulator_odd" id="tr_def_res">0</td>' +
      '<td class="left_border place_simulator_even" id="tr_def_fav">0</td>' +
      '<td class="left_border place_simulator_odd" id="tr_def_pop">0</td>' +
      "</tr>" +
      "</table>" +
      "</div></div>"
  ).appendTo("#simulator_body");

  $("#tr_simulator_strength .left_border").each(function () {
    $(this)[0].align = "center";
  });

  $("#tr_simulator_strength .strength").mousePopup(new MousePopup("Unit Strength (without modificator influence)"));

  $("#tr_simulator_strength .loss").mousePopup(new MousePopup("Loss"));

  // Klick auf Einheitenbild

  $(".index_unit").on("click", function () {
    const type = $(this).attr("class").split(" ")[4];
    $('.place_insert_field[name="sim[units][att][' + type + ']"]').change();
  });

  $("#place_sim_ground_units .place_insert_field, #place_sim_naval_units .place_insert_field").on("input change", function () {
    window.tr.strength.name = $(this).attr("name").replace(/\]/g, "").split("[");
    const str = this;
    setTimeout(function () {
      const unit_type = $(str).closest(".place_simulator_table").attr("id").split("_")[2];
      let val, e;

      val = parseInt($(str).val(), 10);
      val = val || 0;

      if (unit_type == "ground") {
        window.tr.unitsGround[window.tr.strength.name[2]][window.tr.strength.name[3]] = val;

        if (window.tr.strength.name[2] == "def") {
          UnitStrength.calcDef(window.tr.unitsGround.def);
        } else {
          UnitStrength.calcOff(window.tr.unitsGround.att, window.tr.unitsGround.att);
        }

        $("#tr_" + window.tr.strength.name[2] + "_b").get(0).innerHTML = window.tr.strength.blunt;

        $("#tr_" + window.tr.strength.name[2] + "_s").get(0).innerHTML = window.tr.strength.sharp;

        $("#tr_" + window.tr.strength.name[2] + "_d").get(0).innerHTML = window.tr.strength.dist;
      } else {
        let att = 0,
          def = 0;

        window.tr.unitsNaval[window.tr.strength.name[2]][window.tr.strength.name[3]] = val;

        if (window.tr.strength.name[2] == "def") {
          for (e in window.tr.unitsNaval.def) {
            if (window.tr.unitsNaval.def.hasOwnProperty(e)) {
              def += window.tr.unitsNaval.def[e] * window.GameData.units[e].defense;
            }
          }

          $("#tr_def_ship").get(0).innerHTML = def;
        } else {
          for (e in window.tr.unitsNaval.att) {
            if (window.tr.unitsNaval.att.hasOwnProperty(e)) {
              att += window.tr.unitsNaval.att[e] * window.GameData.units[e].attack;
            }
          }

          $("#tr_att_ship").get(0).innerHTML = att;
        }
      }
    }, 100);
  });

  // Abfrage wegen eventueller Spionageweiterleitung
  getUnitInputs();
  setTimeout(function () {
    setChangeUnitInputs("def");
  }, 100);

  $("#select_insert_units").change(function () {
    var side = $(this).find("option:selected").val();

    setTimeout(function () {
      getUnitInputs();
      if (side === "att" || side === "def") {
        setChangeUnitInputs(side);
      }
    }, 200);
  });
}

function getUnitInputs() {
  $("#place_sim_ground_units .place_insert_field, #place_sim_naval_units .place_insert_field").each(function () {
    window.tr.strength.name = $(this).attr("name").replace(/\]/g, "").split("[");
    const str = this;
    const unit_type = $(str).closest(".place_simulator_table").attr("id").split("_")[2];
    let val = parseInt($(str).val(), 10);
    val = val || 0;
    if (unit_type === "ground") {
      window.tr.unitsGround[window.tr.strength.name[2]][window.tr.strength.name[3]] = val;
    } else {
      window.tr.unitsNaval[window.tr.strength.name[2]][window.tr.strength.name[3]] = val;
    }
  });
}

function setChangeUnitInputs(side: string) {
  $('.place_insert_field[name="sim[units][' + side + '][godsent]"]').change();
  setTimeout(function () {
    $('.place_insert_field[name="sim[units][' + side + '][colonize_ship]"]').change();
  }, 100);
}

export default Simulator;
export { SimulatorStrength, setChangeUnitInputs, afterSimulation, setStrengthSimulator, getUnitInputs };
