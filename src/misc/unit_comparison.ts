/*******************************************************************************************************************************
 * Comparison box
 * ----------------------------------------------------------------------------------------------------------------------------
 * | ● Compares the units of each unit type
 * ----------------------------------------------------------------------------------------------------------------------------
 *******************************************************************************************************************************/

import UI from "../utils/ui";

var UnitComparison = {
  activate: function () {
    //UnitComparison.addBox();
    UnitComparison.addButton();
    // Create Window Type
    UI.createWindowType("DIO_COMPARISON", "Unit comparison", 500, 405, true, ["center", "center", 100, 100]);
    //Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON).setHeight('800');
    // Style
    $(
      '<style id="dio_comparison_style"> ' +
        // Button
        "#dio_comparison_button { top:51px; left:120px; z-index:11; position:absolute; } " +
        "#dio_comparison_button .ico_comparison { margin:5px 0px 0px 4px; width:24px; height:24px; z-index: 15;" +
        "background:url(https://www.tuto-de-david1327.com/medias/images/casque.png) no-repeat 0px 0px; background-size:100%; filter:url(#Hue1); -webkit-filter:hue-rotate(60deg); } " +
        "#dio_comparison_button.checked .ico_comparison { margin-top:6px; } " +
        // Window
        "#dio_comparison a { float:left; background-repeat:no-repeat; background-size:25px; line-height:2; margin-right:10px; } " +
        "#dio_comparison .box_content { text-align:center; font-style:normal; } " +
        // Menu tabs
        "#dio_comparison_menu .tab_icon { left: 23px;} " +
        "#dio_comparison_menu .tab_label { margin-left: 18px; } " +
        // Content
        "#dio_comparison .hidden { display:none; } " +
        "#dio_comparison table { width:500px; } " +
        "#dio_comparison .hack .t_hack, #dio_comparison .pierce .t_pierce, #dio_comparison .distance .t_distance, #dio_comparison .sea .t_sea { display:inline-table; } " +
        "#dio_comparison .box_content { background:url(https://www.tuto-de-david1327.com/medias/images/casque-1.png) 94% 94% no-repeat; background-size:140px; } " +
        "#dio_comparison .compare_type_icon { height:25px; width:25px; background:url(https://gpall.innogamescdn.com/images/game/units/units_info_sprite2.51.png); background-size:100%; } " +
        "#dio_comparison .compare_type_icon.booty { background:url(https://www.tuto-de-david1327.com/medias/images/butin-sac.png); background-size:100%; } " +
        "#dio_comparison .compare_type_icon.time { background:url(https://gpall.innogamescdn.com/images/game/res/time.png); background-size:100%; } " + //   // https://www.tuto-de-david1327.com/medias/images/time.png
        "#dio_comparison .compare_type_icon.favor { background:url(https://gpall.innogamescdn.com/images/game/res/favor.png); background-size:100%; } " + // https://www.tuto-de-david1327.com/medias/images/faveur.png
        "#dio_comparison .compare_type_icon.wood { background:url(https://gpall.innogamescdn.com/images/game/res/wood.png); background-size:100%; } " +
        "#dio_comparison .compare_type_icon.stone { background:url(https://gpall.innogamescdn.com/images/game/res/stone.png); background-size:100%; } " +
        "#dio_comparison .compare_type_icon.iron { background:url(https://gpall.innogamescdn.com/images/game/res/iron.png); background-size:100%; } " +
        ".tr_icon_small2 { position:relative; height:20px; width:25px; margin-left:-25px; }" +
        "</style>"
    ).appendTo("head");
  },
  deactivate: function () {
    $("#dio_comparison_button").remove();
    $("#dio_comparison_style").remove();
    // @ts-ignore
    if (window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON)) {
      // @ts-ignore
      window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON).close();
    }
  },
  addButton: function () {
    $('<div id="dio_comparison_button" class="circle_button"><div class="ico_comparison js-caption"></div></div>').appendTo(".bull_eye_buttons");
    // Events
    /*
     $('#dio_comparison_button').on('mousedown', function(){
     $('#dio_comparison_button').addClass("checked");
     }, function(){
     $('#dio_comparison_button').removeClass("checked");
     });
     */
    $("#dio_comparison_button").on("click", function () {
      // @ts-ignore
      if (!window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON)) {
        UnitComparison.openWindow();
        $("#dio_comparison_button").addClass("checked");
      } else {
        UnitComparison.closeWindow();
        $("#dio_comparison_button").removeClass("checked");
      }
    });
    // Tooltip
    // @ts-ignore
    $("#dio_comparison_button").mousePopup(new MousePopup('<div class="dio_icon b"></div>Unit comparison'));
  },
  openWindow: function () {
    var content =
      // Title tabs
      '<ul id="dio_comparison_menu" class="menu_inner" style="top: -36px; right: 94px;">' +
      '<li><a class="submenu_link sea" href="#"><span class="left"><span class="right"><span class="middle">' +
      '<span class="tab_icon tr_icon_small2 townicon_so"></span><span class="tab_label">Sea</span>' +
      "</span></span></span></a></li>" +
      '<li><a class="submenu_link distance" href="#"><span class="left"><span class="right"><span class="middle">' +
      '<span class="tab_icon tr_icon_small2 townicon_di"></span><span class="tab_label">Distance</span>' +
      "</span></span></span></a></li>" +
      '<li><a class="submenu_link pierce" href="#"><span class="left"><span class="right"><span class="middle">' +
      '<span class="tab_icon tr_icon_small2 townicon_sh"></span><span class="tab_label">Sharp</span>' +
      "</span></span></span></a></li>" +
      '<li><a class="submenu_link hack active" href="#"><span class="left"><span class="right"><span class="middle">' +
      '<span class="tab_icon tr_icon_small2 townicon_lo"></span><span class="tab_label">Blunt</span>' +
      "</span></span></span></a></li>" +
      "</ul>" +
      // Content
      '<div id="dio_comparison" style="margin-bottom:5px; font-style:italic;"><div class="box_content hack"></div></div>';
    // @ts-ignore
    window.Layout.wnd.Create(window.GPWindowMgr.TYPE_DIO_COMPARISON).setContent(content);
    UnitComparison.addComparisonTable("hack");
    UnitComparison.addComparisonTable("pierce");
    UnitComparison.addComparisonTable("distance");
    UnitComparison.addComparisonTable("sea");
    // Tooltips
    // @ts-ignore
    var labelArray = window.DM.getl10n("barracks"),
      // @ts-ignore
      labelAttack = window.DM.getl10n("context_menu", "titles").attack,
      // @ts-ignore
      labelDefense = window.DM.getl10n("place", "tabs")[0];
    // @ts-ignore
    $(".tr_att").mousePopup(new MousePopup(labelAttack));
    // @ts-ignore
    $(".tr_def").mousePopup(new MousePopup(labelDefense + " (Ø)"));
    // @ts-ignore
    $(".tr_def_sea").mousePopup(new MousePopup(labelDefense));
    // @ts-ignore
    $(".tr_spd").mousePopup(new MousePopup(labelArray.tooltips.speed));
    // @ts-ignore
    $(".tr_bty").mousePopup(new MousePopup(labelArray.tooltips.booty.title));
    // @ts-ignore
    $(".tr_bty_sea").mousePopup(new MousePopup(labelArray.tooltips.ship_transport.title));
    // @ts-ignore
    $(".tr_res").mousePopup(new MousePopup(labelArray.costs + " (" + labelArray.cost_details.wood + " + " + labelArray.cost_details.stone + " + " + labelArray.cost_details.iron + ")"));
    // @ts-ignore
    $(".tr_woo").mousePopup(new MousePopup(labelArray.costs + " (" + labelArray.cost_details.wood + ")"));
    // @ts-ignore
    $(".tr_sto").mousePopup(new MousePopup(labelArray.costs + " (" + labelArray.cost_details.stone + ")"));
    // @ts-ignore
    $(".tr_iro").mousePopup(new MousePopup(labelArray.costs + " (" + labelArray.cost_details.iron + ")"));
    // @ts-ignore
    $(".tr_fav").mousePopup(new MousePopup(labelArray.costs + " (" + labelArray.cost_details.favor + ")"));
    // @ts-ignore
    $(".tr_tim").mousePopup(new MousePopup(labelArray.cost_details.buildtime_barracks + " (s)"));
    // @ts-ignore
    $(".tr_tim_sea").mousePopup(new MousePopup(labelArray.cost_details.buildtime_docks + " (s)"));
    UnitComparison.switchComparisonTables();
    // Close button event - uncheck available units button
    /*if (typeof window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON).getJQCloseButton() != 'undefined') {
    //window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON).getJQCloseButton().get(0).onclick = function () {
        $('#dio_comparison_button').removeClass("checked");
        $('.ico_comparison').get(0).style.marginTop = "5px";
    //};
    };*/
  },
  closeWindow: function () {
    // @ts-ignore
    window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON).close();
  },
  switchComparisonTables: function () {
    $("#dio_comparison_menu .hack, #dio_comparison_menu .pierce, #dio_comparison_menu .distance, #dio_comparison_menu .sea").click(function () {
      // @ts-ignore
      $("#dio_comparison .box_content").removeClass($("#dio_comparison .box_content")?.get(0).className.split(" ")[1]);
      //console.debug(this.className.split(" ")[1]);
      $("#dio_comparison .box_content").addClass(this.className.split(" ")[1]);
      $("#dio_comparison_menu .active").removeClass("active");
      $(this).addClass("active");
    });
  },
  tooltips: [],
  t: 0,
  // @ts-ignore
  addComparisonTable: function (type) {
    var pos = {
      att: { hack: "36%", pierce: "27%", distance: "45.5%", sea: "72.5%" },
      def: { hack: "18%", pierce: "18%", distance: "18%", sea: "81.5%" }
    };
    var unitIMG = "https://gpall.innogamescdn.com/images/game/units/units_info_sprite2.51.png";
    var strArray = [
      "<td></td>",
      // @ts-ignore
      '<td><div class="compare_type_icon" style="background-position: 0% ' + pos.att[type] + ';"></div></td>',
      // @ts-ignore
      '<td><div class="compare_type_icon" style="background-position: 0% ' + pos.def[type] + ';"></div></td>',
      '<td><div class="compare_type_icon" style="background-position: 0% 63%;"></div></td>',
      type !== "sea" ? '<td><div class="compare_type_icon booty"></div></td>' : '<td><div class="compare_type_icon" style="background-position: 0% 91%;"></div></td>',
      '<td><div class="compare_type_icon" style="background-position: 0% 54%;"></div></td>',
      '<td><div class="compare_type_icon favor"></div></td>',
      '<td><div class="compare_type_icon time"></div></td>',
      '<td><div class="compare_type_icon wood"></div></td>',
      '<td><div class="compare_type_icon stone"></div></td>',
      '<td><div class="compare_type_icon iron"></div></td>'
    ];
    // @ts-ignore
    for (var e in window.GameData.units) {
      // @ts-ignore
      if (window.GameData.units.hasOwnProperty(e)) {
        // @ts-ignore
        var valArray = [];
        // @ts-ignore
        if (type === (window.GameData.units[e].attack_type || "sea") && e !== "militia") {
          // @ts-ignore
          valArray.att = Math.round((window.GameData.units[e].attack * 10) / window.GameData.units[e].population) / 10;
          // @ts-ignore
          valArray.def = Math.round(((window.GameData.units[e].def_hack + window.GameData.units[e].def_pierce + window.GameData.units[e].def_distance) * 10) / (3 * window.GameData.units[e].population)) / 10;
          // @ts-ignore
          valArray.def = valArray.def || Math.round((window.GameData.units[e].defense * 10) / window.GameData.units[e].population) / 10;
          // @ts-ignore
          valArray.speed = window.GameData.units[e].speed;
          // @ts-ignore
          valArray.booty = Math.round((window.GameData.units[e].booty * 10) / window.GameData.units[e].population) / 10;
          // @ts-ignore
          valArray.booty = valArray.booty || Math.round(((window.GameData.units[e].capacity ? window.GameData.units[e].capacity + 6 : 0) * 10) / window.GameData.units[e].population) / 10;
          // @ts-ignore
          valArray.res = Math.round((window.GameData.units[e].resources.wood + window.GameData.units[e].resources.stone + window.GameData.units[e].resources.iron) / window.GameData.units[e].population);
          // @ts-ignore
          valArray.wood = Math.round(window.GameData.units[e].resources.wood / window.GameData.units[e].population);
          // @ts-ignore
          valArray.stone = Math.round(window.GameData.units[e].resources.stone / window.GameData.units[e].population);
          // @ts-ignore
          valArray.iron = Math.round(window.GameData.units[e].resources.iron / window.GameData.units[e].population);
          // @ts-ignore
          valArray.favor = Math.round((window.GameData.units[e].favor * 10) / window.GameData.units[e].population) / 10;
          // @ts-ignore
          valArray.time = Math.round(window.GameData.units[e].build_time / window.GameData.units[e].population);
          // World without Artemis? -> grey griffin and boar
          // @ts-ignore
          valArray.heroStyle = "";
          // @ts-ignore
          valArray.heroStyleIMG = "";
          // @ts-ignore
          if (!window.Game.gods_active.artemis && (e === "griffin" || e === "calydonian_boar")) {
            // @ts-ignore
            valArray.heroStyle = "color:black;opacity: 0.4;";
            // @ts-ignore
            valArray.heroStyleIMG = "filter: url(#GrayScale); -webkit-filter:grayscale(100%);";
          }
          // @ts-ignore
          if (!window.Game.gods_active.aphrodite && (e === "siren" || e === "satyr")) {
            // @ts-ignore
            valArray.heroStyle = "display: none;";
          }
          // @ts-ignore
          if (!window.Game.gods_active.ares && (e === "spartoi" || e === "ladon")) {
            // @ts-ignore
            valArray.heroStyle = "display: none;";
          }
          // @ts-ignore
          strArray[0] += '<td class="un' + UnitComparison.t + '"style="' + valArray.heroStyle + '"><span class="unit index_unit unit_icon40x40 ' + e + '" style="' + valArray.heroStyle + valArray.heroStyleIMG + '"></span></td>';
          // @ts-ignore
          strArray[1] += '<td class="bold" style="color:' + (valArray.att > 19 ? "green;" : valArray.att < 10 && valArray.att !== 0 ? "red;" : "black;") + valArray.heroStyle + '">' + valArray.att + "</td>";
          // @ts-ignore
          strArray[2] += '<td class="bold" style="color:' + (valArray.def > 19 ? "green;" : valArray.def < 10 && valArray.def !== 0 ? "red;" : "black;") + valArray.heroStyle + '">' + valArray.def + "</td>";
          // @ts-ignore
          strArray[3] += '<td class="bold" style="' + valArray.heroStyle + '">' + valArray.speed + "</td>";
          // @ts-ignore
          strArray[4] += '<td class="bold" style="' + valArray.heroStyle + '">' + valArray.booty + "</td>";
          // @ts-ignore
          strArray[5] += '<td class="bold" style="' + valArray.heroStyle + '">' + valArray.res + "</td>";
          // @ts-ignore
          strArray[8] += '<td class="bold" style="' + valArray.heroStyle + '">' + valArray.wood + "</td>";
          // @ts-ignore
          strArray[9] += '<td class="bold" style="' + valArray.heroStyle + '">' + valArray.stone + "</td>";
          // @ts-ignore
          strArray[10] += '<td class="bold" style="' + valArray.heroStyle + '">' + valArray.iron + "</td>";
          // @ts-ignore
          strArray[6] += '<td class="bold" style="color:' + (valArray.favor > 0 ? "rgb(0, 0, 214);" : "black;") + valArray.heroStyle + ';">' + valArray.favor + "</td>";
          // @ts-ignore
          strArray[7] += '<td class="bold" style="' + valArray.heroStyle + '">' + valArray.time + "</td>";
          // @ts-ignore
          UnitComparison.tooltips[UnitComparison.t] = window.GameData.units[e].name;
          UnitComparison.t++;
          // @ts-ignore
          $("#dio_help_UnitComparison").mousePopup(new MousePopup('Wiki (<div class="dio_icon b"></div> Unit Comparison)'));
        }
      }
    }
    $(
      '<table class="hidden t_' +
        type +
        '" cellpadding="1px">' +
        "<tr>" +
        strArray[0] +
        "</tr>" +
        '<tr class="tr_att">' +
        strArray[1] +
        '</tr><tr class="tr_def' +
        (type == "sea" ? "_sea" : "") +
        '">' +
        strArray[2] +
        "</tr>" +
        '<tr class="tr_spd">' +
        strArray[3] +
        '</tr><tr class="tr_bty' +
        (type == "sea" ? "_sea" : "") +
        '">' +
        strArray[4] +
        "</tr>" +
        '<tr class="tr_res">' +
        strArray[5] +
        '</tr><tr class="tr_woo">' +
        strArray[8] +
        "</tr>" +
        '<tr class="tr_sto">' +
        strArray[9] +
        '</tr><tr class="tr_iro">' +
        strArray[10] +
        "</tr>" +
        '<tr class="tr_fav">' +
        strArray[6] +
        '</tr><tr class="tr_tim' +
        (type == "sea" ? "_sea" : "") +
        '">' +
        strArray[7] +
        "</tr>" +
        "</table>"
    ).appendTo("#dio_comparison .box_content");
    for (var i = 0; i <= UnitComparison.t; i++) {
      // @ts-ignore
      $(".un" + i).mousePopup(new MousePopup(UnitComparison.tooltips[i]));
    }
  }
};

export default UnitComparison;
