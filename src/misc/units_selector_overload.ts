/*******************************************************************************************************************************
 * select unit shelper
 *******************************************************************************************************************************/

var UnitsSelectorOverload = {
  activate: () => {
    try {
      var wnds = window.GPWindowMgr.getOpen(window.Layout.wnd.TYPE_TOWN);
      for (var e in wnds) {
        if (wnds.hasOwnProperty(e)) {
          var wndid = wnds[e].getID();

          var testel = $("DIV#gpwnd_" + wndid + " A.dio_balanced");
          if (testel.length > 0) continue;

          var handler = wnds[e].getHandler();

          $("DIV#gpwnd_" + wndid + " A.select_all_units").after(' | <a class="dio_balanced" style="position:relative; top:4px" href="#">No Overload</a> | <a class="dio_delete" style="position:relative; top:4px" href="#">' + window.DM.getl10n("market").delete_all_market_offers + "</a>");

          $(".gtk-deselect-units").css({ display: "none" });
          $(".attack_support_window .town_units_wrapper .ship_count").css({
            "margin-left": "0px"
          });

          var dio_bl_groundUnits = new Array("sword", "slinger", "archer", "hoplite", "rider", "chariot", "catapult", "minotaur", "zyklop", "medusa", "cerberus", "fury", "centaur", "calydonian_boar", "godsent");

          $("DIV#gpwnd_" + wndid + " A.dio_balanced").click(function () {
            var units = new Array();
            var item;

            for (var i = 0; i < dio_bl_groundUnits.length; i++) {
              if (handler.data.units[dio_bl_groundUnits[i]]) {
                item = {
                  name: dio_bl_groundUnits[i],
                  count: handler.data.units[dio_bl_groundUnits[i]].count,
                  population: handler.data.units[dio_bl_groundUnits[i]].population
                };
                units.push(item);
              }
            }
            var berth: number = 0;
            if (handler.data.researches && handler.data.researches.berth) {
              berth = parseFloat(handler.data.researches.berth);
            }

            var totalCap = handler.data.units.big_transporter.count * (handler.data.units.big_transporter.capacity + berth) + handler.data.units.small_transporter.count * (handler.data.units.small_transporter.capacity + berth);
            units.sort(function (a, b) {
              return b.population - a.population;
            });

            for (i = 0; i < units.length; i++) {
              if (units[i].count == 0) {
                units.splice(i, 1);
                i = i - 1;
              }
            }

            var restCap = totalCap;
            var sendUnits: any[] = [];
            for (i = 0; i < units.length; i++) {
              item = { name: units[i].name, count: 0 };
              sendUnits[units[i].name] = item;
            }

            var hasSent;
            var k = 0;
            while (units.length > 0) {
              hasSent = false;
              k = k + 1;
              for (i = 0; i < units.length; i++) {
                if (units[i].population <= restCap) {
                  hasSent = true;
                  units[i].count = units[i].count - 1;
                  sendUnits[units[i].name].count = sendUnits[units[i].name].count + 1;
                  restCap = restCap - units[i].population;
                }
              }
              for (i = 0; i < units.length; i++) {
                if (units[i].count == 0) {
                  units.splice(i, 1);
                  i = i - 1;
                }
              }
              if (!hasSent) {
                break;
              }
            }

            handler.getUnitInputs().each(function () {
              if (!sendUnits[this.name]) {
                if (handler.data.units[this.name].count > 0) {
                  this.value = handler.data.units[this.name].count;
                } else {
                  this.value = "";
                }
              }
            });

            for (i = 0; i < dio_bl_groundUnits.length; i++) {
              if (sendUnits[dio_bl_groundUnits[i]]) {
                if (sendUnits[dio_bl_groundUnits[i]].count > 0) {
                  $("DIV#gpwnd_" + wndid + " INPUT.unit_type_" + dio_bl_groundUnits[i]).val(sendUnits[dio_bl_groundUnits[i]].count);
                } else {
                  $("DIV#gpwnd_" + wndid + " INPUT.unit_type_" + dio_bl_groundUnits[i]).val("");
                }
              }
            }

            $("DIV#gpwnd_" + wndid + " INPUT.unit_type_sword").trigger("change");
          });

          $("DIV#gpwnd_" + wndid + " A.dio_delete").click(function () {
            handler.getUnitInputs().each(function () {
              this.value = "";
            });
            $("DIV#gpwnd_" + wndid + " INPUT.unit_type_sword").trigger("change");
          });
        }
      }
    } catch (error) {
      console.error(error, "UnitsSelectorOverload");
    }
  },
  deactivate: () => {
    $("#dio-ship_count").remove();
  }
};

export default UnitsSelectorOverload;
