/*******************************************************************************************************************************
 * ● Percentual Trade
 *******************************************************************************************************************************/

function addPercentTrade(wndID: string, ww: boolean) {
  var a,
    b = "";
  var content = wndID + ".content .btn_trade_button.button_new";
  if (ww) {
    a = "ww_";
    b = "_ww";
    content = wndID + ".trade .send_res .button.send_resources_btn";
  }

  $(content).after('<div id="dio_Percentual_trade' + b + '" class="btn dio_btn_trade"><a class="button">' + '<span class="left"><span class="right">' + '<span class="middle mid">' + '<span class="img_trade"></span></span></span></span>' + '<span style="clear:both;"></span>' + "</a></div>");
  $(wndID + ".dio_btn_trade").mousePopup(new MousePopup('<div class="dio_icon b"></div>Percentage trade'));
  setPercentTrade(wndID, ww);

  // Style
  $('<style id="dio_wonder_button">' + "div.wonder_res_container fieldset.send_res .button { margin-top: 0px!important; } " + "</style>").appendTo("head");

  $("#trade_tab #duration_container").css({
    "margin-bottom": "-15px"
  });

  $(wndID + ".dio_btn_trade").css({
    position: "relative",
    top: "13px",
    display: "inline",
    "margin-left": "10px"
  });

  $(wndID + ".mid").css({ minWidth: "26px" });

  $(wndID + ".img_trade").css({
    width: "27px",
    height: "27px",
    top: "-3px",
    float: "left",
    position: "relative",
    background: 'url("https://www.tuto-de-david1327.com/medias/images/echange.png") no-repeat'
  });
}

function setPercentTrade(wndID: string, ww: any) {
  var a = ww ? "ww_" : "",
    own_town = $(wndID + ".town_info").get(0) ? true : false;

  $(wndID + ".dio_btn_trade").toggleClick(
    function () {
      window.tr.trade.res.wood = {};
      window.tr.trade.res.stone = {};
      window.tr.trade.res.iron = {};
      window.tr.trade.res.sum = {};
      window.tr.trade.res.sum.amount = 0;
      // Set amount of resources to 0
      setAmount(true, a, wndID);
      // Total amount of resources // TODO: ITowns.getTown(Game.townId).getCurrentResources(); ?
      for (var e in window.tr.trade.res) {
        if (window.tr.trade.res.hasOwnProperty(e) && e != "sum") {
          window.tr.trade.res[e].rest = false;

          window.tr.trade.res[e].amount = parseInt($(".ui_resources_bar ." + e + " .amount").get(0).innerHTML, 10);

          window.tr.trade.res.sum.amount += window.tr.trade.res[e].amount;
        }
      }
      // Percentage of total resources
      window.tr.trade.res.wood.percent = (100 / window.tr.trade.res.sum.amount) * window.tr.trade.res.wood.amount;
      window.tr.trade.res.stone.percent = (100 / window.tr.trade.res.sum.amount) * window.tr.trade.res.stone.amount;
      window.tr.trade.res.iron.percent = (100 / window.tr.trade.res.sum.amount) * window.tr.trade.res.iron.amount;
      // Total trading capacity
      window.tr.trade.res.sum.cur = parseInt($(wndID + "#" + a + "big_progressbar .caption .curr").get(0).innerHTML, 10);
      // Amount of resources on the percentage of trading capacity (%)
      (window.tr.trade.res.wood.part = (window.tr.trade.res.sum.cur / 100) * window.tr.trade.res.wood.percent), 10;
      (window.tr.trade.res.stone.part = (window.tr.trade.res.sum.cur / 100) * window.tr.trade.res.stone.percent), 10;
      (window.tr.trade.res.iron.part = (window.tr.trade.res.sum.cur / 100) * window.tr.trade.res.iron.percent), 10;
      // Get rest warehouse capacity of each resource type
      for (var f in window.tr.trade.res) {
        if (window.tr.trade.res.hasOwnProperty(f) && f != "sum") {
          if (!ww && own_town) {
            // Own town

            var curr =
                parseInt(
                  $(wndID + "#town_capacity_" + f + " .amounts .curr")
                    .get(0)
                    .innerHTML.replace("+", "")
                    .trim(),
                  10
                ) || 0,
              curr2 =
                parseInt(
                  $(wndID + "#town_capacity_" + f + " .amounts .curr2")
                    .get(0)
                    .innerHTML.replace("+", "")
                    .trim(),
                  10
                ) || 0,
              max =
                parseInt(
                  $(wndID + "#town_capacity_" + f + " .amounts .max")
                    .get(0)
                    .innerHTML.replace("+", "")
                    .trim(),
                  10
                ) || 0;

            window.tr.trade.res[f].cur = curr + curr2;
            window.tr.trade.res[f].max = max - window.tr.trade.res[f].cur;
            if (window.tr.trade.res[f].max < 0) {
              window.tr.trade.res[f].max = 0;
            }
          } else {
            // World wonder or foreign town
            window.tr.trade.res[f].max = 30000;
          }
        }
      }
      // Rest of fraction (0-2 units) add to stone amount
      window.tr.trade.res.stone.part += window.tr.trade.res.sum.cur - (window.tr.trade.res.wood.part + window.tr.trade.res.stone.part + window.tr.trade.res.iron.part);

      window.tr.trade.res.sum.rest = 0;
      window.tr.trade.rest_count = 0;
      calcRestAmount();
      setAmount(false, a, wndID);
    },
    function () {
      setAmount(true, a, wndID);
    }
  );
}

function calcRestAmount() {
  // Subdivide rest
  if (window.tr.trade.res.sum.rest > 0) {
    for (var e in window.tr.trade.res) {
      if (window.tr.trade.res.hasOwnProperty(e) && e != "sum" && window.tr.trade.res[e].rest != true) {
        window.tr.trade.res[e].part += window.tr.trade.res.sum.rest / (3 - window.tr.trade.rest_count);
      }
    }

    window.tr.trade.res.sum.rest = 0;
  }
  // Calculate new rest
  for (var f in window.tr.trade.res) {
    if (window.tr.trade.res.hasOwnProperty(f) && f != "sum" && window.tr.trade.res[f].rest != true) {
      if (window.tr.trade.res[f].max <= window.tr.trade.res[f].part) {
        window.tr.trade.res[f].rest = true;
        window.tr.trade.res.sum.rest += window.tr.trade.res[f].part - window.tr.trade.res[f].max;
        window.tr.trade.rest_count += 1;
        window.tr.trade.res[f].part = window.tr.trade.res[f].max;
      }
    }
  }
  // Recursion
  if (window.tr.trade.res.sum.rest > 0 && window.tr.trade.rest_count < 3) {
    calcRestAmount();
  }
}

function setAmount(clear: boolean, a: string, wndID: string) {
  for (var e in window.tr.trade.res) {
    if (window.tr.trade.res.hasOwnProperty(e) && e != "sum") {
      if (clear == true) {
        window.tr.trade.res[e].part = 0;
      }

      $(wndID + "#" + a + "trade_type_" + e + ' [type="text"]')
        .select()
        .val(window.tr.trade.res[e].part)
        .blur();
    }
  }
}

export default addPercentTrade;
