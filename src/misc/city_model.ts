/*******************************************************************************************************************************
 * City Model Feature
 *******************************************************************************************************************************/

import TrStorage from "../storage";
import UI from "../utils/ui";
import Queues from "../automation/queues";

var CityModel = {
  activate: function () {
    $(
      '<style id="tr_model_city" type="text/css">' +
        ".toolbar_activities .middle .activity.model_city .icon, .toolbar_activities .middle .activity.model_city .icon.active {background: url(https://raw.githubusercontent.com/alexandre2311/rex1/master/images/items-sprite.png) no-repeat;background-position: -56px 0 !important;width: 26px;height: 27px;}" +
        ".auto_build_up_arrow {background: url(https://gpit.innogamescdn.com/images/game/academy/up.png) no-repeat -2px -2px;width: 18px;height: 18px;position: absolute;right: -2px;bottom: 12px;transform: scale(0.8);cursor: pointer;}" +
        ".auto_build_down_arrow {background: url(https://gpit.innogamescdn.com/images/game/academy/up.png) no-repeat -2px -2px;width: 18px;height: 18px;position: absolute;right: -2px;bottom: -3px;transform: scale(0.8) rotate(180deg);cursor: pointer;  } " +
        ".auto_build_box {background: url(https://gpit.innogamescdn.com/images/game/academy/tech_frame.png) no-repeat 0 0;width: 58px;height: 59px;position: relative;overflow: hidden;display: inline-block;vertical-align: middle;  } " +
        ".auto_build_building {position: absolute;top: 4px;left: 4px;width: 50px;height: 50px;  background: url(https://gpit.innogamescdn.com/images/game/main/buildings_sprite_50x50.png) no-repeat 0 0;} " +
        ".auto_build_lvl {position: absolute;bottom: 3px;left: 3px;margin: 0;font-weight: bold;font-size: 12px;color: white;text-shadow: 0px 0px 2px black, 1px 1px 2px black, 0px 2px 2px black;} " +
        "#buildings_lvl_buttons {padding: 5px; max-height: 400px; user-select: none;} " +
        ".disabled_build {filter: grayscale(100%);-webkit-filter: grayscale(100%);} " +
        ".buildings_group_special {background: url(https://gpen.innogamescdn.com/images/game/main/senate_sprite_2.37.png) no-repeat 0 -267px;height: 40px;width: 176px;position: relative;}" +
        ".buildings_group_special .image {background: url(https://gpit.innogamescdn.com/images/game/main/buildings_sprite_50x50.png) no-repeat 0 0; position: relative;height: 34px;width: 40px;left: 3px;top: 3px;text-align: right;vertical-align: bottom;float: left;margin-right: 2px;}" +
        "</style>"
    ).appendTo("head");

    CityModel.addButton();

    // Create Window Type
    UI.createWindowType("TR_MODEL_CITY", "City Model", 800, 217, true, ["center", "center", 100, 100]);
    //Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_MODEL_CITY).setHeight('800');

    window.tr.city_model.editBuildingLevel = CityModel.editBuildingLevel;
    window.tr.city_model.editSpecialBuilding = CityModel.editSpecialBuilding;
  },
  start: function () {
    let townIds: any[] = [];
    window.ITowns.town_group_towns.models.map((town: any) => {
      if (town.getGroupId() == window.tr.city_model.group_id) {
        townIds.push(town.getTownId());
      }
    });
    townIds.map((townId: number) => {
      if (window.ITowns.towns[townId].getBuildings().attributes["storage"] < window.tr.city_model.buildings["storage"]) {
        Queues.saveBuilding({
          type: "add",
          town_id: townId,
          item_name: "storage",
          count: 2
        });
      }
    });
  },
  createTownGroup: function () {
    let groupExists = false;
    for (const town_group of window.ITowns.town_groups.models) {
      if (town_group.getName() == "New Cities") {
        groupExists = true;
        window.tr.city_model.group_id = town_group.getId();
        let tr_storage = new TrStorage();
        tr_storage.save("city_model", window.tr.city_model);
      }
    }
    if (!groupExists) {
      window.gpAjax.ajaxPost(
        "town_group_overviews",
        "add_town_group",
        {
          town_group_name: "New Cities"
        },
        false,
        (data: any) => {
          window.tr.city_model.group_id = data.town_group_id;
          let tr_storage = new TrStorage();
          tr_storage.save("city_model", window.tr.city_model);
        },
        () => {}
      );
    }
  },
  addButton: function () {
    $('<div id="tr_model_city_divider" class="divider"></div><div id="tr_model_city_button" class="activity_wrap"><div class="activity model_city"><div class="hover_state"><div class="icon"></div></div></div><div class="ui_highlight" data-type="bubble_menu"></div></div>').appendTo(".tb_activities.toolbar_activities .middle");

    $("#tr_model_city_button").on("click", function () {
      if (!window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_MODEL_CITY)) {
        CityModel.openWindow();
      } else {
        CityModel.closeWindow();
      }
    });
  },
  openWindow: function () {
    var content = '<div id="tr_model_city" style="margin-bottom:5px; font-style:italic;"><div class="box_content"></div></div>';

    window.Layout.wnd.Create(window.GPWindowMgr.TYPE_TR_MODEL_CITY).setContent(content);
    CityModel.addContent();

    // Close button event - uncheck available units button
    /*if (typeof window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_MODEL_CITY).getJQCloseButton() != 'undefined') {
//window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_MODEL_CITY).getJQCloseButton().get(0).onclick = function () {
    $('#tr_settings_button').removeClass("checked");
    $('.ico_comparison').get(0).style.marginTop = "5px";
//};
};*/
  },
  closeWindow: function () {
    window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_MODEL_CITY).close();
  },
  getBuildingHtml: (building: string, bg: any[]) => {
    return `
    <div class="auto_build_box" style="cursor: pointer">
      <div class="item_icon auto_build_building" style="background-position: -${bg[0]}px -${bg[1]}px;">
        <div class="auto_build_up_arrow" onclick="event.stopPropagation(); window.tr.city_model.editBuildingLevel('${building}', 1)"></div>
        <div class="auto_build_down_arrow" onclick="event.stopPropagation(); window.tr.city_model.editBuildingLevel('${building}', -1)"></div>
        <p style="color: lime" id="build_lvl_${building}" class="auto_build_lvl"> ${window.tr.city_model.buildings[building]} <p>
      </div>
    </div>`;
  },
  getSpecialBuildingHtml: (id: number, building: string, bg: any[]) => {
    return `<div id="special_building_${building}" onclick="event.stopPropagation(); window.tr.city_model.editSpecialBuilding(${id}, '${building}')" class="image enable_build_button ${
      ((window.tr.city_model.special_buildings[id] && window.tr.city_model.special_buildings[id] != building) || window.tr.city_model.special_buildings[id] === null) && "disabled_build"
    } special_build" style="background-position: -${bg[0]}px -${bg[1]}px;"></div>`;
  },
  addContent: function () {
    $("#tr_model_city .box_content").html(`
    <div id="build_settings">
      <div style="width: 600px; margin-bottom: 3px">
        <p style="font-weight: bold; margin: 0px 5px">City model for new cities</p>
        <p style="margin: 0px 5px">Choose building level</p>
      </div>
      <div style="width: 100%; display: inline-flex; gap: 6px;">
          ${CityModel.getBuildingHtml("main", [450, 0])}
          ${CityModel.getBuildingHtml("storage", [250, 50])}
          ${CityModel.getBuildingHtml("farm", [150, 0])}
          ${CityModel.getBuildingHtml("academy", [0, 0])}
          ${CityModel.getBuildingHtml("temple", [300, 50])}
          ${CityModel.getBuildingHtml("barracks", [50, 0])}
          ${CityModel.getBuildingHtml("docks", [100, 0])}
          ${CityModel.getBuildingHtml("market", [0, 50])}
          ${CityModel.getBuildingHtml("hide", [200, 0])}
          ${CityModel.getBuildingHtml("lumber", [400, 0])}
          ${CityModel.getBuildingHtml("stoner", [200, 50])}
          ${CityModel.getBuildingHtml("ironer", [250, 0])}
          ${CityModel.getBuildingHtml("wall", [50, 100])}
      </div>
      <div style="width: 100%; display: inline-flex; justify-content: space-between; gap: 6px;">
        <div id="special_buildings_group_1">
          <div class="buildings_group_special">
          ${CityModel.getSpecialBuildingHtml(1, "theater", [350, 50])}
          ${CityModel.getSpecialBuildingHtml(1, "thermal", [400, 50])}
          ${CityModel.getSpecialBuildingHtml(1, "library", [300, 0])}
          ${CityModel.getSpecialBuildingHtml(1, "lighthouse", [350, 0])}
          </div>
        </div>
        <div id="special_buildings_group_2">
          <div class="buildings_group_special">
          ${CityModel.getSpecialBuildingHtml(2, "tower", [450, 50])}
          ${CityModel.getSpecialBuildingHtml(2, "statue", [150, 50])}
          ${CityModel.getSpecialBuildingHtml(2, "oracle", [50, 50])}
          ${CityModel.getSpecialBuildingHtml(2, "trade_office", [0, 100])}
          </div>
        </div>
      </div>
    </div>`);
  },
  editBuildingLevel: function (name: string, d: number) {
    const { max_level, min_level } = window.GameData.buildings[name];
    const current_lvl = window.tr.city_model.buildings[name];

    if (d) {
      /* Check if bottom or top overflow */
      window.tr.city_model.buildings[name] = Math.min(Math.max(current_lvl + d, min_level), max_level);
    } else {
      window.tr.city_model.buildings[name] = Math.min(Math.max(50, min_level), max_level);
    }
    let tr_storage = new TrStorage();
    tr_storage.save("city_model", window.tr.city_model);
    //const color = window.tr.city_model.buildings[name] > current_lvl ? "orange" : window.tr.city_model.buildings[name] < current_lvl ? "red" : "lime";
    $(`#build_settings #build_lvl_${name}`).text(window.tr.city_model.buildings[name]);
  },
  editSpecialBuilding: function (id: number, building: string) {
    if (window.tr.city_model.special_buildings[id] && window.tr.city_model.special_buildings[id] == building) {
      window.tr.city_model.special_buildings[id] = null;
      $(`#special_buildings_group_${id} #special_building_${building}`).addClass("disabled_build");
    } else {
      window.tr.city_model.special_buildings[id] = building;
      $(`#special_buildings_group_${id} .enable_build_button`).addClass("disabled_build");
      $(`#special_buildings_group_${id} #special_building_${building}`).removeClass("disabled_build");
    }
    let tr_storage = new TrStorage();
    tr_storage.save("city_model", window.tr.city_model);
  },
  deactivate: function () {
    $("#tr_model_city_button").remove();
    $("#tr_model_city_divider").remove();
  }
};

export default CityModel;
