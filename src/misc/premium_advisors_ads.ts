/*******************************************************************************************************************************
 * Hide Premium Containers
 *******************************************************************************************************************************/

var PremiumHide = {
  activate: function () {
    $("<style id='hide_premium' type='text/css'>#barracks_commander_hint, #mines_trader_hint, #temple_priest_hint, .advisor_container, .advisor_hint{display:none;}</script> ").appendTo("head");
  }
};

export default PremiumHide;
