/*******************************************************************************************************************************
 *  Culture overview : Add a counter for the party in the culture view. Quack function
 *******************************************************************************************************************************/

import HelperFunctions from "../utils/helpers";

var cultureOverview: CultureOverviewType = {
  timeout: null,
  activate: () => {
    if ($("#culture_points_overview_bottom").length) {
      cultureOverview.add();
    }

    cultureOverview.timeout = setInterval(() => {
      if ($("#culture_points_overview_bottom").length) {
        cultureOverview.add();
      }
    }, 50000); //0.

    $(
      '<style id="dio_cultureOverview_style"> ' +
        "#joe_cultureBTN_wrapper { display: none; } " +
        "#dio_cultureBTN_wrapper { color: rgb(255, 255, 255); font-family: Verdana; font-weight: bold; font-size: 12px; text-align: center; line-height: 25px; text-shadow: rgb(0, 0, 0) 1px 1px 0px; }" +
        ".dio_cultureBTN_wrapper_left { position: absolute; top: 0px; left: 0px; }" +
        ".dio_cultureBTN_wrapper_right { position: absolute; top: 0px; right: 0px; z-index: 2000; }" +
        '.dio_cultureBTN_l, .dio_cultureBTN_r { cursor: pointer; max-width: 40px; min-width: 25px; float: right; position: relative; margin-left: 3px; border: 2px groove gray; background: url("https://gp' +
        window.tr.LID +
        '.innogamescdn.com/images/game/overviews/celebration_bg_new.png") }' +
        ".dio_cultureBTN_cityfestival { background-position: 0 -110px; }" +
        ".dio_cultureBTN_olympicgames { background-position: 0 -140px; }" +
        ".dio_cultureBTN_triumph { background-position: 0 -201px; }" +
        ".dio_cultureBTN_theather { background-position: 0 -170px; }" +
        "</style>"
    ).appendTo("head");
  },
  add: () => {
    try {
      var a = $("ul#cultur_overview_towns");
      var b, c, d, e, f, i, j, k;
      f = 0;

      var g = $("ul#culture_overview_towns span.eta");
      var h = $("#culture_points_overview_bottom #place_culture_count").text();

      e = 0;
      b = $('a[class~="confirm"][class~="type_triumph"]');
      d = $('a[class~="confirm"][class~="type_triumph"][class~="disabled"]');
      if (d.length > 0) {
        for (f = 0; f < b.length; f++) {
          if ($(b[f]).attr("class").indexOf("disabled") > 1) {
            continue;
            c = $(b[f]).parents('li[id^="ov_town_"]');
            var eltext = c[0].previousSibling;
            $(c).insertBefore($(d[0]).parents('li[id^="ov_town_"]'));
            $(eltext).insertBefore($(d[0]).parents('li[id^="ov_town_"]'));
          }
        }
      }

      e = 0;
      b = $('a[class~="confirm"][class~="type_theater"]');
      d = $('a[class~="confirm"][class~="type_theater"][class~="disabled"]');
      if (d.length > 0) {
        for (f = 0; f < b.length; f++) {
          if ($(b[f]).attr("class").indexOf("disabled") > 1) {
            continue;
            c = $(b[f]).parents('li[id^="ov_town_"]');
            eltext = c[0].previousSibling;
            $(c).insertBefore($(d[0]).parents('li[id^="ov_town_"]'));
            $(eltext).insertBefore($(d[0]).parents('li[id^="ov_town_"]'));
          }
        }
      }

      e = 0;
      b = $('a[class~="confirm"][class~="type_party"]');
      d = $('a[class~="confirm"][class~="type_party"][class~="disabled"]');
      if (d.length > 0) {
        for (f = 0; f < b.length; f++) {
          if ($(b[f]).attr("class").indexOf("disabled") > 1) {
            continue;
            c = $(b[f]).parents('li[id^="ov_town_"]');
            eltext = c[0].previousSibling;
            $(c).insertBefore($(d[0]).parents('li[id^="ov_town_"]'));
            $(eltext).insertBefore($(d[0]).parents('li[id^="ov_town_"]'));
          }
        }
      }
      ///////
      if ($("#dio_cultureBTN_wrapper").length == 0) {
        $("#culture_overview_wrapper")
          .parent()
          .append(
            '<div id="dio_cultureBTN_wrapper"><div class="dio_cultureBTN_wrapper_right"><div id="dio_cultureBTN_theather_r" class="dio_cultureBTN_r dio_cultureBTN_theather"></div>' +
              '<div id="dio_cultureBTN_triumph_r" class="dio_cultureBTN_r dio_cultureBTN_triumph"></div><div id="dio_cultureBTN_olympicgames_r" class="dio_cultureBTN_r dio_cultureBTN_olympicgames"></div>' +
              '<div id="dio_cultureBTN_cityfestival_r" class="dio_cultureBTN_r dio_cultureBTN_cityfestival"></div></div></div>'
          );

        if (!$("#dio_culture_sort_control").is(":visible"))
          $("#culture_overview_wrapper").css({
            top: "35px",
            height: "+=-35px"
          });

        var dio_cultureBTN_r_clicked_last = "";

        function hideTownElements(JQelement: HTMLElement) {
          var dio_cultureBTN_mode = "";
          switch (JQelement.id) {
            case "dio_cultureBTN_cityfestival_r":
              dio_cultureBTN_mode = "ul li:eq(0)";
              break;
            case "dio_cultureBTN_olympicgames_r":
              dio_cultureBTN_mode = "ul li:eq(1)";
              break;
            case "dio_cultureBTN_triumph_r":
              dio_cultureBTN_mode = "ul li:eq(2)";
              break;
            case "dio_cultureBTN_theather_r":
              dio_cultureBTN_mode = "ul li:eq(3)";
              break;
            default:
              setTimeout(() => {
                window.HumanMessage.error("<div class='dio_icon b'></div>Error");
              }, 0);
              break;
          }
          if (dio_cultureBTN_r_clicked_last === JQelement.id) {
            $("ul#culture_overview_towns li")
              .filter(function () {
                return !!$(dio_cultureBTN_mode, this).find(".eta").length;
              })
              .toggle();
            $("ul#culture_overview_towns li")
              .filter(function () {
                return !!$(dio_cultureBTN_mode, this).find(".celebration_progressbar:not(:has(>.eta))").length;
              })
              .removeClass("hidden");
            $(JQelement).toggleClass("culture_red");
          } else {
            $("ul#culture_overview_towns li")
              .show()
              .filter(function () {
                return !!$(dio_cultureBTN_mode, this).find(".eta").length;
              })
              .hide();
            $(".dio_cultureBTN_r").removeClass("culture_red");
            $(JQelement).addClass("culture_red");
          }
          dio_cultureBTN_r_clicked_last = JQelement.id;
          $(".dio_cultureBTN_r").css({ border: "2px groove #808080" });
          $(".culture_red").css({ border: "2px groove #CC0000" });
        }
        $(".dio_cultureBTN_r").click(function () {
          hideTownElements(this);
        });
      }

      var dio_cultureCounter = {
        cityfestivals: 0,
        olympicgames: 0,
        triumph: 0,
        theather: 0
      };

      var dio_bashpoints = $("#culture_points_overview_bottom .points_count").text().split("/");

      var dio_goldforgames = Math.floor($("#ui_box .gold_amount").text() / 50);
      dio_cultureCounter.triumph = Math.floor((parseInt(dio_bashpoints[0]) - parseInt(dio_bashpoints[1])) / 300) + 1;
      if (dio_cultureCounter.triumph < 0) {
        dio_cultureCounter.triumph = 0;
      }
      dio_cultureCounter.cityfestivals = $('a[class~="confirm"][class~="type_party"]:not(.disabled)').length;
      dio_cultureCounter.olympicgames = $('a[class~="confirm"][class~="type_games"]:not(.disabled)').length;
      if (dio_goldforgames < dio_cultureCounter.olympicgames) {
        dio_cultureCounter.olympicgames = dio_goldforgames;
      }
      dio_cultureCounter.theather = $('a[class~="confirm"][class~="type_theater"]:not(.disabled)').length;

      $("#dio_cultureBTN_cityfestival_r").text(dio_cultureCounter.cityfestivals);
      $("#dio_cultureBTN_olympicgames_r").text(dio_cultureCounter.olympicgames);
      $("#dio_cultureBTN_triumph_r").text(dio_cultureCounter.triumph);
      $("#dio_cultureBTN_theather_r").text(dio_cultureCounter.theather);

      $(".dio_cultureBTN_cityfestival").mousePopup(new MousePopup('<div class="dio_icon b"></div> City Festivals (' + HelperFunctions.pointNumber(dio_cultureCounter.cityfestivals) + ") "));
      $(".dio_cultureBTN_olympicgames").mousePopup(new MousePopup('<div class="dio_icon b"></div> Olympic Games (' + HelperFunctions.pointNumber(dio_cultureCounter.olympicgames) + ") "));
      $(".dio_cultureBTN_triumph").mousePopup(new MousePopup('<div class="dio_icon b"></div> Victory Processions (' + HelperFunctions.pointNumber(dio_cultureCounter.triumph) + ") "));
      $(".dio_cultureBTN_theather").mousePopup(new MousePopup('<div class="dio_icon b"></div> Theater Plays (' + HelperFunctions.pointNumber(dio_cultureCounter.theather) + ") "));
    } catch (error) {
      console.error(error, "cultureOverview (add)");
    }
  },
  deactivate: () => {
    $("#dio_cultureOverview_style").remove();
    $("#dio_cultureBTN_wrapper").remove();

    clearTimeout(cultureOverview.timeout);
    cultureOverview.timeout = null;
    if (!$("#dio_culture_sort_control").is(":visible")) $("#culture_overview_wrapper").css({ top: "0px", height: "+=35px" });
  }
};

export default cultureOverview;
