function spells(type: string) {
  var spells: { [key: string]: string } = {},
    _power_div = $("<div/>", { class: "powers_container clearfix" }),
    spell_id = undefined,
    god: string | undefined = undefined,
    spell_ena = false;

  switch (type) {
    case "barracks":
      $.each(
        window.MM.checkAndPublishRawModel("PlayerGods", {
          id: window.Game.player_id
        }).getProductionOverview(),
        function (gd, _) {
          gd == "hera" && ((spell_id = "fertility_improvement"), (god = "hera"), (spells[gd] = spell_id));
          gd == "ares" && ((spell_id = "spartan_training"), (god = "ares"), (spells[gd] = spell_id));
        }
      );
      spell_ena = window.MM.checkAndPublishRawModel("Town", { id: window.Game.townId }).getBuildings().getBuildingLevel("barracks") > 0;
      break;
    case "docks":
      $.each(
        window.MM.checkAndPublishRawModel("PlayerGods", {
          id: window.Game.player_id
        }).getProductionOverview(),
        function (gd, ba) {
          gd == "poseidon" && ((spell_id = "call_of_the_ocean"), (god = "poseidon"), (spells[gd] = spell_id));
        }
      );
      spell_ena = window.MM.checkAndPublishRawModel("Town", { id: window.Game.townId }).getBuildings().getBuildingLevel("docks") > 0;
  }

  if (spell_ena && $("#unit_order .tr_power").length == 0) {
    $.each(spells, function (_god, power_id) {
      var casted_power = window.HelperPower.createCastedPowerModel(power_id, window.Game.townId),
        _disable = window.MM.checkAndPublishRawModel("PlayerGods", { id: window.Game.player_id }).get(_god + "_favor") < window.GameData.powers[power_id].favor,
        _classAdd = _disable ? " disabled" : "",
        casted = window.HelperPower.createCastedPowerModel(power_id, window.Game.townId);
      $.each(window.MM.checkAndPublishRawModel("Town", { id: window.Game.townId }).getCastedPowers(), function (ind, elem) {
        if (elem.getPowerId() == power_id) {
          (casted = elem), (_classAdd = " active_animation extendable animated_power_icon animated_power_icon_45x45");
        }
      });
      $(_power_div).append(
        $("<div/>", { class: "js-power-icon power_icon45x45 " + power_id + " power" + _classAdd, "data-power_id": power_id, rel: _god })
          .append(
            $("<div/>", { class: "extend_spell" })
              .append($("<div/>", { class: "gold" }))
              .append($("<div/>", { class: "amount" }))
          )
          .append($("<div/>", { class: "js-caption" }))
          .on("mouseover", function (e) {
            var _tooltipParam = {
              show_costs: true
            };
            if (typeof casted.getId != "undefined") {
              (_tooltipParam.casted_power_end_at = casted.getEndAt()), (_tooltipParam.extendable = casted.isExtendable());
            }
            $(this).tooltip(window.TooltipFactory.createPowerTooltip(casted_power.getPowerId(), _tooltipParam)).showTooltip(e);
          })
          .on("click", function (e) {
            window.CM.unregister({ main: window.tr.wndId, sub: "castedspells" }, "tr_power_" + casted.getId());
            var _btn = window.CM.register({ main: window.tr.wndId, sub: "castedspells" }, "tr_power_" + casted.getId(), window.tr.wnd.find($(".tr_power .new_ui_power_icon .gold")).button()),
              power = window.HelperPower.createCastedPowerModel(power_id, window.Game.townId);
            if (casted.getId() == undefined) {
              power.cast();
            } else {
              if (casted.isExtendable()) {
                window.BuyForGoldWindowFactory.openExtendPowerForGoldWindow(_btn, casted);
                $(this).addClass(_classAdd);
              }
            }
          })
      );
      window.tr.wnd.find($(".game_inner_box")).append($("<div/>", { class: "tr_power" }).append(_power_div));
      var power = window.GameData.powers[power_id],
        god = window.MM.checkAndPublishRawModel("PlayerGods", { id: window.Game.player_id }).getCurrentProductionOverview()[_god],
        _godCurr = window.MM.checkAndPublishRawModel("PlayerGods", { id: window.Game.player_id })[_god + "_favor_delta_property"].calculateCurrentValue().unprocessedCurrentValue,
        marg = 33,
        _elem = $("<div/>", {
          style: "margin-top:" + marg + "px;color:black;text-shadow: 2px 2px 2px gray;font-size:10px;z-index:3000;font-weight: bold;",
          name: "counter"
        });
      window.CM.unregister({ main: window.tr.wndId, sub: "castedspells" }, "tr_countdown");
      window.CM.register(
        { main: window.tr.wndId, sub: "castedspells" },
        "tr_countdown",
        _elem
          .countdown2({
            value: ((power.favor - _godCurr) /*god.current*/ / god.production) * 60 * 60,
            display: "readable_seconds_with_days"
          })
          .on("cd:finish", function () {
            $(this).parent().removeClass("disabled"), $(this).remove();
          })
      );
      window.tr.wnd.find($(".power_icon45x45.power." + power_id)).append(_elem);
    });
  }
}
export default spells;
