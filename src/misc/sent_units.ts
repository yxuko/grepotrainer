import TrStorage from "../storage";

/*******************************************************************************************************************************
 * ● Sent units box
 *******************************************************************************************************************************/
var SentUnits = {
  activate: function () {
    $.Observer(window.GameEvents.command.send_unit).subscribe("DIO_SEND_UNITS", function (e: any, data: { params: { [x: string]: any; hasOwnProperty: (arg0: string) => any }; sending_type: string }) {
      for (var z in data.params) {
        if (data.params.hasOwnProperty(z) && data.sending_type !== "") {
          if (window.GameData.units[z]) {
            window.tr.sentUnitsArray[data.sending_type][z] = window.tr.sentUnitsArray[data.sending_type][z] == undefined ? 0 : window.tr.sentUnitsArray[data.sending_type][z];

            window.tr.sentUnitsArray[data.sending_type][z] += data.params[z];
          }
        }
      }
      //SentUnits.update(data.sending_type); ????
    });
  },
  deactivate: function () {
    $.Observer(window.GameEvents.command.send_unit).unsubscribe("DIO_SEND_UNITS");
    $("#dio_table_box").remove();
    $("#dio_table_box2").remove();
    $("#dio_table_box3").remove();
    $("#dio_table_box4").remove();
  },

  add: function (wndID: string, action: string) {
    $(
      '<style id="dio_table_box">' +
        ".attack_support_window .send_units_form .button_wrapper { text-align:left; padding-left:70px; }" +
        //'#btn_attack_town { margin-left: 55px; } ' +
        "#gt_delete { display: none; }" +
        ".attack_support_window .additional_info_wrapper .town_info_duration_pos_alt { min-height: 50px;; } " +
        ".attack_support_window .additional_info_wrapper .town_info_duration_pos { min-height: 62px!important; } " +
        "</style>"
    ).appendTo(wndID + ".attack_support_window");

    if ((window.ITowns.getTown(window.Game.townId).researches().attributes.breach == true || window.ITowns.getTown(window.Game.townId).researches().attributes.stone_storm == true) & window.tr.settings.tr_sen) {
      $('<style id="dio_table_box3">' + ".attack_support_window .send_units_form .attack_type_wrapper .attack_table_box { text-align:left;  transform:scale(0.94); margin: -5px 0px -5px -20px;}" + ".attack_support_window .table_box .table_box_content .content_box { min-width:137px ; }" + "</style>").appendTo(wndID + ".attack_support_window");
    } else {
      $('<style id="dio_table_box4">' + ".attack_support_window .send_units_form .attack_type_wrapper .attack_table_box { text-align:left; transform:scale(1); margin: -8px 0px -2px 12px;}" + ".attack_support_window .table_box .table_box_content .content_box { min-width:180px; }" + "</style>").appendTo(wndID + ".attack_support_window");
    }
    if (!$(wndID + ".sent_units_box").get(0)) {
      $(
        '<div id="dio_table_box2">' +
          '<div class="game_inner_box sent_units_box ' +
          action +
          '"><div class="game_border ">' +
          '<div class="game_border_top"></div><div class="game_border_bottom"></div><div class="game_border_left"></div><div class="game_border_right"></div>' +
          '<div class="game_border_corner corner1"></div><div class="game_border_corner corner2"></div>' +
          '<div class="game_border_corner corner3"></div><div class="game_border_corner corner4"></div>' +
          '<div class="game_header bold">' +
          '<div class="dio_icon b" style="opacity: 0.7;"></div>' +
          '<div class="icon_sent townicon_' +
          (action == "attack" ? "lo" : "ld") +
          '"></div><span>Sent units (' +
          (action == "attack" ? "OFF" : "DEF") +
          ")</span>" +
          "</div>" +
          '<div class="troops"><div class="units_list"></div><hr style="width: 172px;border: 1px solid rgb(185, 142, 93);margin: 3px 0px 2px -1px;">' +
          '<div id="btn_sent_units_reset" class="button_new">' +
          '<div class="left"></div>' +
          '<div class="right"></div>' +
          '<div class="caption js-caption">Reset<div class="effect js-effect"></div></div>' +
          "</div>" +
          "</div></div></div>"
      ).appendTo(wndID + ".attack_support_window");
      SentUnits.update(action);
      $(wndID + ".icon_sent").css({
        height: "20px",
        marginTop: "-2px",
        width: "20px",
        backgroundPositionY: "-26px",
        paddingLeft: "0px",
        marginLeft: "0px"
      });
      $(wndID + ".sent_units_box").css({
        position: "absolute",
        right: "0px",
        bottom: "26px",
        width: "192px"
      });
      $(wndID + ".troops").css({ padding: "6px 0px 6px 6px" });
      $(wndID + "#btn_sent_units_reset").click(function () {
        // Overwrite old array
        window.tr.sentUnitsArray[action] = {};
        SentUnits.update(action);
      });
    }
  },

  update: function (action: string) {
    try {
      // Remove old unit list
      $(".sent_units_box." + action + " .units_list").each(function () {
        this.innerHTML = "";
      });
      // Add new unit list

      for (var x in window.tr.sentUnitsArray[action]) {
        if (window.tr.sentUnitsArray[action].hasOwnProperty(x)) {
          if ((window.tr.sentUnitsArray[action][x] || 0) > 0) {
            $(".sent_units_box." + action + " .units_list").each(function () {
              $(this).append('<div class="unit_icon25x25 ' + x + (window.tr.sentUnitsArray[action][x] >= 1000 ? (window.tr.sentUnitsArray[action][x] >= 10000 ? " five_digit_number" : " four_digit_number") : "") + '">' + '<span class="count text_shadow">' + window.tr.sentUnitsArray[action][x] + "</span>" + "</div>");
            });
          }
        }
      }
      let tr_storage = new TrStorage();
      tr_storage.save("sentUnits", window.tr.sentUnitsArray);
    } catch (error) {
      console.error(error, "updateSentUnitsBox");
    }
  }
};

export default SentUnits;
