import HelperFunctions from "../utils/helpers";

export default function checkGold() {
  Object.keys(window.ITowns.towns).map((id) => {
    window.gpAjax.ajaxGet("frontend_bridge", "execute", { model_url: "PremiumExchange", action_name: "read", town_id: id }, !0, {
      success: function (T: any, V: { stone: { capacity: number; stock: number }; wood: { capacity: number; stock: number }; iron: { capacity: number; stock: number } }) {
        let stone_stock = V.stone.capacity - V.stone.stock;
        let wood_stock = V.wood.capacity - V.wood.stock;
        let iron_stock = V.iron.capacity - V.iron.stock;
        let min = window.GameDataMarket.getMinTradingSum();
        let max = Math.max(stone_stock, wood_stock, iron_stock);
        if (max >= min) {
          console.log("gold!");
          HelperFunctions.beep(200, 700, 10);
        } else {
          console.log("no gold");
        }
      },
      error: function (T: any, V: any) {
        console.log(V);
      }
    });
  });
}
