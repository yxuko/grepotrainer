/*******************************************************************************************************************************
 * ● Short duration
 *******************************************************************************************************************************/
var ShortDuration = {
  activate: () => {
    $(
      '<style id="dio_short_duration_style">' +
        ".attack_support_window .dio_duration { border-spacing:0px; margin-bottom:2px; float: left; } " +
        ".attack_support_window .way_icon { padding:30px 0px 0px 30px; background:transparent url(https://gp" +
        window.tr.LID +
        ".innogamescdn.com/images/game/towninfo/traveltime.png) no-repeat 0 0; } " +
        ".attack_support_window .arrival_icon { padding:30px 0px 0px 30px; background:transparent url(https://gp" +
        window.tr.LID +
        ".innogamescdn.com/images/game/towninfo/arrival.png) no-repeat 0 0; } " +
        ".attack_support_window .short_icon { background: url(https://dio-david1327.github.io/img/dio/logo/short_icon.png) 11px -1px / 21px no-repeat; filter: hue-rotate(50deg); -webkit-filter: hue-rotate(50deg); margin: 100px; width: 27px; transform: translateX(-7px); } " +
        ".attack_support_window .dio_duration .power_icon45x45.cap_of_invisibility { transform: scale(0.4); margin: -12px -20px -13px -20px; width: 44px; } " +
        ".attack_support_window .additional_info_wrapper .town_info_duration_pos_alt { min-height: 50px;; } " +
        ".attack_support_window .additional_info_wrapper .town_info_duration_pos { min-height: 55px; } " +
        ".attack_support_window .additional_info_wrapper { margin-top: -8px; } " +
        ".attack_support_window .send_units_form .attack_type_wrapper .attack_table_box { margin-top: -2px;}" +
        ".attack_support_window .way_duration, " +
        ".attack_support_window .arrival_time { padding:0px 0px 0px 0px; background:none!important;; } " +
        "</style>"
    ).appendTo("head");
  },
  deactivate: () => {
    $("#dio_short_duration_style").remove();
    $("#dio_short_duration_stylee").remove();
    $("#dio_duration").remove();
  },

  add: (wndID: string, action: string) => {
    try {
      $('<style id="dio_short_duration_stylee">' + ".attack_support_window .additional_info_wrapper .nightbonus { position: absolute; left: 242px; top: 45px; } " + ".attack_support_window .fight_bonus.morale { position: absolute; left: 238px; top: 23px; } " + ".attack_support_window span.max_booty { margin-left: -2px; } " + "</style>").appendTo(wndID + ".attack_support_window");

      $(
        '<table class="dio_duration">' +
          '<tr><td class="way_icon"></td><td class="dio_way"></td><td class="arrival_icon"></td><td class="dio_arrival"></td><td colspan="2" class="dio_night"></td></tr>' +
          '<tr class="short_duration_row" style="color:darkgreen">' +
          '<td>&nbsp;╚&gt;&nbsp;</td><td><span class="short_duration">~0:00:00</span></td>' +
          '<td>&nbsp;&nbsp;&nbsp;╚&gt;</td><td><span class="short_arrival">~00:00:00</span></td>' +
          '<td class="short_icon"></td><td></td></tr>' +
          (action == "attack" ? '<tr class="hades_duration_row" style="color:#774b33">' + '<td>&nbsp;╚&gt;&nbsp;</td><td><span class="hades_duration">~0:00:00</span></td>' + '<td>&nbsp;&nbsp;&nbsp;╚&gt;</td><td><span class="hades_visibility">~00:00:00 </span></td>' + '<td class="power_icon45x45 power cap_of_invisibility"></td><td></td></tr>' : "") +
          "</table>"
      ).prependTo(wndID + ".duration_container");
      //}
      $(wndID + ".nightbonus").appendTo(wndID + ".dio_night");
      $(wndID + ".way_duration").appendTo(wndID + ".dio_way");
      $(wndID + ".arrival_time").appendTo(wndID + ".dio_arrival");

      // Tooltip

      $(wndID + ".hades_duration_row").mousePopup(new MousePopup('<div class="dio_icon b"></div> cap of invisibility'));

      // Detection of changes
      ShortDuration.change(wndID, action);
      ShortDuration.calculate(wndID, action);
    } catch (error) {
      console.error(error, "addShortDuration");
    }
  },

  change: (wndID: string, action: any) => {
    var duration = new MutationObserver(function (mutations) {
      mutations.forEach(function (mutation) {
        if (mutation.addedNodes[0]) ShortDuration.calculate(wndID, action);
      });
    });
    if ($(wndID + ".way_duration").get(0)) {
      duration.observe($(wndID + ".way_duration").get(0), {
        attributes: false,
        childList: true,
        characterData: false
      });
    }
  },

  calculate: (wndID: string, action: string) => {
    try {
      var setup_time = 900 / window.Game.game_speed,
        duration_time = $(wndID + ".duration_container .way_duration")
          .get(0)
          .innerHTML.replace("~", "")
          .split(":"),
        // TODO: hier tritt manchmal Fehler auf TypeError: Cannot read property "innerHTML" of undefined at calcShortDuration (<anonymous>:3073:86)
        duration_time_short,
        duration_time_hades,
        arrival_time_short: number,
        arrival_time_hades: number,
        h,
        m,
        s,
        atalanta_factor = 0;
      //console.log(setup_time)

      var hasCartography = window.ITowns.getTown(window.Game.townId).getResearches().get("cartography");

      var hasMeteorology = window.ITowns.getTown(window.Game.townId).getResearches().get("meteorology");

      var hasSetSail = window.ITowns.getTown(window.Game.townId).getResearches().get("set_sail");

      var hasLighthouse = window.ITowns.getTown(window.Game.townId).buildings().get("lighthouse");
      // Atalanta aktiviert?
      if ($(wndID + ".unit_container.heroes_pickup .atalanta").get(0)) {
        if ($(wndID + ".cbx_include_hero").hasClass("checked")) {
          // Beschleunigung hängt vom Level ab, Level 1 = 11%, Level 20 = 30%

          var atalanta_level = window.MM.getCollections().PlayerHero[0].models[1].get("level");
          atalanta_factor = (atalanta_level + 10) / 100;
        }
      }

      // Sekunden, Minuten und Stunden zusammenrechnen (-> in Sekunden)

      duration_time = (parseInt(duration_time[0], 10) * 60 + parseInt(duration_time[1], 10)) * 60 + parseInt(duration_time[2], 10);

      // Verkürzte Laufzeit berechnen

      duration_time_short = ((duration_time - setup_time) * (1 + atalanta_factor)) / (1 + 0.3 + atalanta_factor) + setup_time;

      duration_time_hades = duration_time / 10;

      h = Math.floor(duration_time_short / 3600);
      m = Math.floor((duration_time_short - h * 3600) / 60);
      s = Math.floor(duration_time_short - h * 3600 - m * 60);

      if (h < 10) {
        h = "0" + h;
      }
      if (m < 10) {
        m = "0" + m;
      }
      if (s < 10) {
        s = "0" + s;
      }

      $(wndID + ".short_duration").get(0).innerHTML = "~" + h + ":" + m + ":" + s;

      // Ankunftszeit errechnen

      arrival_time_short = Math.round(Timestamp.server() + window.Game.server_gmt_offset) + duration_time_short;

      h = Math.floor(arrival_time_short / 3600);
      m = Math.floor((arrival_time_short - h * 3600) / 60);
      s = Math.floor(arrival_time_short - h * 3600 - m * 60);

      h %= 24;

      if (h < 10) {
        h = "0" + h;
      }
      if (m < 10) {
        m = "0" + m;
      }
      if (s < 10) {
        s = "0" + s;
      }

      $(wndID + ".short_arrival").get(0).innerHTML = "~" + h + ":" + m + ":" + s;

      clearInterval(window.tr.arrival_interval[wndID]);

      window.tr.arrival_interval[wndID] = setInterval(function () {
        arrival_time_short += 1;

        h = Math.floor(arrival_time_short / 3600);

        m = Math.floor((arrival_time_short - h * 3600) / 60);

        s = Math.floor(arrival_time_short - h * 3600 - m * 60);

        h %= 24;

        if (h < 10) {
          h = "0" + h;
        }
        if (m < 10) {
          m = "0" + m;
        }
        if (s < 10) {
          s = "0" + s;
        }

        if ($(wndID + ".short_arrival").get(0)) {
          $(wndID + ".short_arrival").get(0).innerHTML = "~" + h + ":" + m + ":" + s;
        } else {
          clearInterval(window.tr.arrival_interval[wndID]);
        }
      }, 1000);
      if (action == "attack") {
        h = Math.floor(duration_time_hades / 3600);
        m = Math.floor((duration_time_hades - h * 3600) / 60);
        s = Math.floor(duration_time_hades - h * 3600 - m * 60);

        if (h < 10) {
          h = "0" + h;
        }
        if (m < 10) {
          m = "0" + m;
        }
        if (s < 10) {
          s = "0" + s;
        }

        $(wndID + ".hades_duration").get(0).innerHTML = "~" + h + ":" + m + ":" + s;

        // Ankunftszeit errechnen

        arrival_time_hades = Math.round(Timestamp.server() + window.Game.server_gmt_offset) + duration_time_hades;

        h = Math.floor(arrival_time_hades / 3600);
        m = Math.floor((arrival_time_hades - h * 3600) / 60);
        s = Math.floor(arrival_time_hades - h * 3600 - m * 60);

        h %= 24;

        if (h < 10) {
          h = "0" + h;
        }
        if (m < 10) {
          m = "0" + m;
        }
        if (s < 10) {
          s = "0" + s;
        }

        $(wndID + ".hades_visibility").get(0).innerHTML = "~" + h + ":" + m + ":" + s;

        clearInterval(window.tr.hades_interval[wndID]);

        window.tr.hades_interval[wndID] = setInterval(function () {
          arrival_time_hades += 1;

          h = Math.floor(arrival_time_hades / 3600);

          m = Math.floor((arrival_time_hades - h * 3600) / 60);

          s = Math.floor(arrival_time_hades - h * 3600 - m * 60);

          h %= 24;

          if (h < 10) {
            h = "0" + h;
          }
          if (m < 10) {
            m = "0" + m;
          }
          if (s < 10) {
            s = "0" + s;
          }

          if ($(wndID + ".hades_visibility").get(0)) {
            $(wndID + ".hades_visibility").get(0).innerHTML = "~" + h + ":" + m + ":" + s;
          } else {
            clearInterval(window.tr.hades_interval[wndID]);
          }
        }, 1000);
      }
    } catch (error) {
      console.error(error, "ShortDuration.calculate");
    }
  }
};

export default ShortDuration;
