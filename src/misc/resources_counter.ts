/*******************************************************************************************************************************
 * Resource Counter
 * Adding total resource counter to trade overview
 *******************************************************************************************************************************/
var resCounter = {
  activate: function () {
    $(
      '<style id="dio_trade_overview_style">' +
        "#dio_resource_counter {position: absolute; right: 40px; margin-top: 4px; height: 16px; background-color: #ffe2a1; border: 1px solid #e1af55; display: inline-block}" +
        "#dio_wood_counter {float: left;}" +
        "#dio_wood_counter .resource_wood_icon {padding-left: 24px; width: auto;}" +
        "#dio_wood_counter .wood_amount {display: inline; padding-bottom: 1px; padding-right: 5px; font-size: 10px}" +
        "#dio_stone_counter {float: left;}" +
        "#dio_stone_counter .resource_stone_icon {padding-left: 24px; width: auto;}" +
        "#dio_stone_counter .stone_amount {display: inline; padding-right: 5px; font-size: 10px}" +
        "#dio_silver_counter {float: left; margin-right: 3px;}" +
        "#dio_silver_counter .resource_iron_icon {padding-left: 24px; width: auto;}" +
        "#dio_silver_counter .iron_amount {display: inline; padding-right: 2px; font-size: 10px}" +
        "</style>"
    ).appendTo("head");
  },
  //Counting resources in town right now
  init: function () {
    let wood_total = 0;
    let stone_total = 0;
    let silver_total = 0;
    const city_boxes = $("#trade_overview_wrapper").find(".trade_town");
    for (var a = 0; a < city_boxes.length - 2; a++) {
      wood_total += parseInt($(city_boxes[a]).find(".resource_wood_icon").text());
      stone_total += parseInt($(city_boxes[a]).find(".resource_stone_icon").text());
      silver_total += parseInt($(city_boxes[a]).find(".resource_iron_icon").text());
    }
    //Appending counter to trade window

    const wnd = window.GPWindowMgr.getFocusedWindow() || false;
    const dio_wnd = wnd.getJQElement().find(".overview_search_bar");
    dio_wnd.append(
      '<div id="dio_resource_counter" nobr><div id="dio_wood_counter"><span class="resource_wood_icon wood"><span class="wood_amount">' +
        wood_total +
        "</span></span></div>" +
        '<div id="dio_stone_counter"><span class="resource_stone_icon stone"><span class="stone_amount">' +
        stone_total +
        "</span></span></div>" +
        '<div id="dio_silver_counter"><span class="resource_iron_icon iron"><span class="iron_amount">' +
        silver_total +
        "</span></span></div></div>"
    );
  },
  deactivate: function () {
    $("#dio_trade_overview_style").remove();
    $("#dio_resource_counter").remove();
  }
};

export default resCounter;
