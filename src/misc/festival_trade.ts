/*******************************************************************************************************************************
 * ● Town Trade Improvement : Click on it and it is only exchanged towards a festival. Quack function
 *******************************************************************************************************************************/

var townTradeImprovement = {
  activate: function () {
    $(
      '<style id="dio_style_Improvement_trade" type="text/css">' +
        "#dio_Improvement_trade { position: relative; top: -259px; left: 59px; } " +
        "#dio_Improvement_trade .dio_trade { position: absolute; height: 16px; width: 22px; background-repeat: no-repeat; background-position: 0px -1px; } " +
        "#dio_Improvement_trade .dio_trade:hover { background-position: 0px -17px; } " +
        "#dio_Improvement_trade .dio_trade.dio_max { right: 105px; background-image: url(https://dio-david1327.github.io/img/dio/btn/trade-arrow.png);} " +
        "#dio_Improvement_trade .dio_trade.dio_send_cult { right: 85px; background-image: url(https://dio-david1327.github.io/img/dio/btn/trade-cult.png);} " +
        "#dio_Improvement_trade .dio_trade.dio_send_cult_reverse { left: 105px; background-image: url(https://dio-david1327.github.io/img/dio/btn/trade-cultr.png);} " +
        ".q_trade { display: none; } " +
        "</style>"
    ).appendTo("head");
  },
  add: function (wndID: any, wnd: any) {
    $(wndID + " .tripple-progress-progressbar").each(function () {
      var res_type = this.id.split("_")[2];
      var res = townTradeImprovement.getRes(res_type, wnd);
      $(this)
        .find(".amounts")
        .append('<span id="dio_needed" class="dio_needed_' + res_type + "_" + wnd + '"> &#9658; ' + res.needed + "</span>");
    });

    $($(wndID + "#trade > div > div.content > .town-capacity-indicator")[2]).before(
      '<div id="dio_Improvement_trade">' +
        '<a id="dio_wood_' +
        wnd +
        '_max" 	class="dio_trade dio_max" 		style="top:200px"></a>' +
        '<a id="dio_stone_' +
        wnd +
        '_max" 	class="dio_trade dio_max" 		style="top:234px"></a>' +
        '<a id="dio_iron_' +
        wnd +
        '_max" 	class="dio_trade dio_max" 		style="top:268px"></a>' +
        '<a id="dio_wood_' +
        wnd +
        '_cult" 	class="dio_trade dio_send_cult" style="top:200px"></a>' +
        '<a id="dio_stone_' +
        wnd +
        '_cult" 	class="dio_trade dio_send_cult" style="top:234px"></a>' +
        '<a id="dio_iron_' +
        wnd +
        '_cult" 	class="dio_trade dio_send_cult" style="top:268px"></a>' +
        /*'<a id="dio_wood_'+wnd+'_cultreverse" 	class="dio_trade dio_send_cult_reverse" style="top:200px"></a>' +
            '<a id="dio_stone_'+wnd+'_cultreverse" 	class="dio_trade dio_send_cult_reverse" style="top:234px"></a>' +
            '<a id="dio_iron_'+wnd+'_cultreverse" 	class="dio_trade dio_send_cult_reverse" style="top:268px"></a>'*/ "</div>"
    );

    $(wndID + " .dio_trade").click(function () {
      var id = this.id.split("_");
      var res = townTradeImprovement.getRes(id[1], id[2], id[3]);
      if (res.needed - res.amounts.curr3 <= 0 || res.caption.curr <= 0 || res.amounts.curr3 > 0) {
        res.send = 0;
      } else if (res.needed - res.amounts.curr3 > res.caption.curr) {
        res.send = res.caption.curr + res.amounts.curr3;
      } else {
        res.send = res.needed;
      }
      res.wnd
        .find("#trade_type_" + id[1] + " input")
        .val(res.send)
        .select()
        .blur();
    });

    // Tooltip
    $(".dio_max").mousePopup(new MousePopup('<div class="dio_icon b"></div>' + "max"));
    $(".dio_send_cult").mousePopup(new MousePopup('<div class="dio_icon b"></div>City festivals'));
    //$('.dio_send_cult_reverse').mousePopup(new MousePopup(getTexts("Quack", "delete")));
  },
  getRes: function (res_type: string, wnd_id: string, mode: string | undefined) {
    var res: any = {};
    res.wnd = $("#gpwnd_" + wnd_id);
    res.selector = res.wnd.find("#town_capacity_" + res_type);
    res.caption = {
      curr: parseInt(res.wnd.find("#big_progressbar .caption .curr").html()),
      max: parseInt(res.wnd.find("#big_progressbar .caption .max").html()),
      now: parseInt(res.wnd.find("#trade_type_" + res_type + " input").val())
    };
    res.amounts = {
      curr: parseInt(res.selector.find(".curr").html()) || 0,
      curr2: parseInt(res.selector.find(".curr2").html().substring(3)) || 0,
      curr3: parseInt(res.selector.find(".curr3").html().substring(3)) || 0,
      max: parseInt(res.selector.find(".max").html()) || 0
    };
    if (mode === "cult" || mode === "cultreverse") {
      res.amounts.max = res_type === "stone" ? 18000 : 15000;
    }
    if (mode === "cultreverse") {
      var townrescurrent = $("div#ui_box div.ui_resources_bar div.indicator[data-type='" + res_type + "'] div.amount").text();

      res.needed = townrescurrent - res.amounts.max;
    } else {
      res.needed = res.amounts.max - res.amounts.curr - res.amounts.curr2;
    }
    return res;
  },
  deactivate: function () {
    $(".tripple-progress-progressbar #dio_needed").remove();
    $("#dio_Improvement_trade").remove();
    $("#dio_style_Improvement_trade").remove();
  }
};

export default townTradeImprovement;
