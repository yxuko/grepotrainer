import addTradeMarks from "./trade_progress_marks";

/*******************************************************************************************************************************
 * ● Recruiting Trade
 * *****************************************************************************************************************************/
var trade_count = 0,
  unit = "FS",
  unit2 = "",
  percent = "0.0"; // Recruiting Trade

if (typeof window.GameData.units.attack_ship == "undefined") {
  unit2 = "Attack ship";
  setTimeout(() => {
    unit2 = window.GameData.units.attack_ship.name;
  }, 200);
} else unit2 = window.GameData.units.attack_ship.name;

var RecruitingTrade = {
  activate: () => {
    $(
      '<style id="dio_style_recruiting_trade" type="text/css">' +
        "#dio_recruiting_trade .option_s { filter:grayscale(85%); -webkit-filter:grayscale(85%); margin:0px; cursor:pointer; } " +
        "#dio_recruiting_trade .option_s:hover { filter:unset !important; -webkit-filter:unset !important; } " +
        "#dio_recruiting_trade .select_rec_unit .sel { filter:sepia(100%); -webkit-filter:sepia(100%); } " +
        "#dio_recruiting_trade .option {color:#000; background:#FFEEC7; } " +
        "#dio_recruiting_trade .option:hover {color:#fff; background:#328BF1; } " +
        "#dio_recruiting_trade .select_rec_unit { position:absolute; display:none; } " +
        "#dio_recruiting_trade .select_rec_perc { display:none; margin: 22px 0 0 -55px; } " +
        "#dio_recruiting_trade .select_rec_unit.open { display:block !important; } " +
        "#dio_recruiting_trade .select_rec_perc.open { display: initial !important; } " +
        "div#trade_tab div.content { min-height: 340px; } " +
        "#dio_recruiting_trade .item-list { max-height:237px; } " +
        "#dio_recruiting_trade .arrow { width:18px; height:18px; background:url(https://dio-david1327.github.io/img/dio/btn/drop-out.png) no-repeat -1px -1px; position:absolute; } " +
        "#dio_recruiting_trade .dio_drop_rec_unit .caption { padding: 4px 20px 0 2px; } " +
        "#dio_recruiting_trade .dio_drop_rec_unit { width: auto; left: 10px; } " +
        "#dio_recruiting_trade .dio_drop_rec_perc { width: 55px; } " +
        "#dio_recruiting_trade .dio_rec_count { display: none; } " +
        "</style>"
    ).appendTo("head");
  },
  deactivate: function () {
    $("#dio_style_recruiting_trade").remove();
    $("#dio_recruiting_trade").remove();
  },
  add: function (wndID: any) {
    var max_amount: number;
    $(wndID + "#duration_container").before(
      '<div id="dio_recruiting_trade" class="dio_rec_trade">' +
        // DropDown-Button for ratio
        '<div class="dio_drop_rec_perc dropdown default">' +
        '<div class="border-left"></div>' +
        '<div class="border-right"></div>' +
        '<div class="caption" name="' +
        percent +
        '">' +
        Math.round(percent * 100) +
        "%</div>" +
        '<div class="arrow"></div>' +
        "</div>" +
        // DropDown-Button for unit
        '<div class="dio_drop_rec_unit dropdown default">' +
        '<div class="border-left"></div>' +
        '<div class="border-right"></div>' +
        '<div class="caption" name="' +
        unit +
        '">' +
        unit2 +
        "</div>" +
        '<div class="arrow"></div>' +
        '</div><span class="dio_rec_count">(' +
        trade_count +
        ")</span></div>"
    ); //<span class="dio_rec_count">(' + trade_count + ')</span></div>

    // Select boxes for unit and ratio
    $(
      '<div id="dio_Select_boxes" class="select_rec_unit dropdown-list default active">' +
        '<div class="item-list">' +
        //Ship
        '<div id="dioattack_ship" class="option_s unit index_unit unit_icon40x40 attack_ship" 		name="FS"></div>' + // Light ship
        '<div id="diobireme"		class="option_s unit index_unit unit_icon40x40 bireme" 			name="BI"></div>' + // Bireme
        '<div id="diotrireme" 	class="option_s unit index_unit unit_icon40x40 trireme" 			name="TR"></div>' + // Trireme
        '<div id="diotransporter" class="option_s unit index_unit unit_icon40x40 big_transporter" 	name="BT"></div>' + // Transport boat
        '<div id="diosmall_trans" class="option_s unit index_unit unit_icon40x40 small_transporter" name="BE"></div>' + // Fast transport ship
        '<div id="diocolonize" 	class="option_s unit index_unit unit_icon40x40 colonize_ship" 		name="CE"></div>' + // Colony ship
        '<div id="diodemolition" 	class="option_s unit index_unit unit_icon40x40 demolition_ship" name="DS"></div>' + // Fire ship
        //Troop
        '<div id="diosword" 		class="option_s unit index_unit unit_icon40x40 sword" 			name="SK"></div>' + // Swordsman
        '<div id="dioslinger" 	class="option_s unit index_unit unit_icon40x40 slinger" 			name="SL"></div>' + // Slinger
        '<div id="dioarcher" 		class="option_s unit index_unit unit_icon40x40 archer" 			name="BS"></div>' + // Archer
        '<div id="diohoplite" 	class="option_s unit index_unit unit_icon40x40 hoplite" 			name="HO"></div>' + // Hoplite
        '<div id="diorider" 		class="option_s unit index_unit unit_icon40x40 rider" 			name="RE"></div>' + // Horseman
        '<div id="diochariot" 	class="option_s unit index_unit unit_icon40x40 chariot" 			name="SW"></div>' + // Chariot
        '<div id="diocatapult" 	class="option_s unit index_unit unit_icon40x40 catapult" 			name="CA"></div>' + // catapult
        //Fly
        '<div id="diocentaur" 	class="option_s unit index_unit unit_icon40x40 centaur" 			name="CT"></div>' + // Centaur
        '<div id="diocerberus" 	class="option_s unit index_unit unit_icon40x40 cerberus" 			name="CB"></div>' + // Cerberus
        '<div id="diozyklop" 		class="option_s unit index_unit unit_icon40x40 zyklop" 			name="CL"></div>' + // Cyclop
        '<div id="diofury" 		class="option_s unit index_unit unit_icon40x40 fury" 				name="EY"></div>' + // Erinys
        '<div id="diomedusa" 		class="option_s unit index_unit unit_icon40x40 medusa" 			name="MD"></div>' + // Medusa
        '<div id="diominotaur" 	class="option_s unit index_unit unit_icon40x40 minotaur" 			name="MT"></div>' + // Minotaur
        '<div id="diosea_monster" class="option_s unit index_unit unit_icon40x40 sea_monster" 		name="HD"></div>' + // Hydra
        '<div id="dioharpy" 		class="option_s unit index_unit unit_icon40x40 harpy" 			name="HP"></div>' + // Harpy
        '<div id="diomanticore" 	class="option_s unit index_unit unit_icon40x40 manticore" 		name="MN"></div>' + // Manticore
        '<div id="diopegasus" 	class="option_s unit index_unit unit_icon40x40 pegasus" 			name="PG"></div>' + // Pegasus
        '<div id="diogriffin" 	class="option_s unit index_unit unit_icon40x40 griffin" 			name="GF"></div>' + // Griffin
        '<div id="diocalydonian" 	class="option_s unit index_unit unit_icon40x40 calydonian_boar" name="CY"></div>' + // Calydonian boar
        (window.Game.gods_active.aphrodite
          ? '<div id="diosiren" 	class="option_s unit index_unit unit_icon40x40 siren" 			name="SE"></div>' + // Siren
            '<div id="diosatyr" 	class="option_s unit index_unit unit_icon40x40 satyr" 			name="ST"></div>'
          : "") + // Satyr
        (window.Game.gods_active.ares
          ? '<div id="dioladon" 	class="option_s unit index_unit unit_icon40x40 ladon" 			name="LD"></div>' + // Ladon
            '<div id="diospartoi" 	class="option_s unit index_unit unit_icon40x40 spartoi" 		name="SR"></div>'
          : "") + // Spartoi
        //Other
        '<div id="diowall" 		class="option_s unit index_unit place_image wall_level" 			name="WA"></div>' + // City wall Lv 5
        '<div id="diowall2" 		class="option_s unit index_unit place_image wall_level" 		name="WA2"></div>' + // City wall Lv 15
        '<div id="diohide" 		class="option_s unit index_unit building_icon40x40 hide" 			name="HI"></div>' + // City hide Lv 5
        '<div id="diohide2" 		class="option_s unit index_unit building_icon40x40 hide" 		name="HI2"></div>' + // City hide Lv 15
        '<div id="diofestivals" 	class="option_s unit index_unit place_image morale" 			name="FE"></div>' + // City festival
        "</div></div>"
    ).appendTo(wndID + ".dio_rec_trade");

    $(wndID + ".dio_drop_rec_perc").after(
      '<div class="select_rec_perc dropdown-list default inactive">' +
        '<div class="item-list">' +
        '<div class="option sel" name="0.0">&nbsp;&nbsp;0%</div>' +
        '<div class="option" name="0.01">&nbsp;&nbsp;1%</div>' +
        '<div class="option" name="0.02">&nbsp;&nbsp;2%</div>' +
        '<div class="option" name="0.04">&nbsp;&nbsp;4%</div>' +
        '<div class="option" name="0.05">&nbsp;&nbsp;5%</div>' +
        '<div class="option" name="0.06">&nbsp;&nbsp;6%</div>' +
        '<div class="option" name="0.08">&nbsp;&nbsp;8%</div>' +
        '<div class="option" name="0.10">10%</div>' +
        '<div class="option" name="0.14">14%</div>' +
        '<div class="option" name="0.17">17%</div>' +
        '<div class="option" name="0.20">20%</div>' +
        '<div class="option" name="0.25">25%</div>' +
        '<div class="option" name="0.33">33%</div>' +
        '<div class="option" name="0.50">50%</div>' +
        "</div></div>"
    );

    $(wndID + ".dio_rec_trade [name='" + unit + "']").toggleClass("sel");

    // click events of the drop menu
    $(wndID + " .select_rec_unit .option_s").each(function () {
      $(this).click(function (e: any) {
        $(".select_rec_unit .sel").toggleClass("sel");
        $("." + this.className.split(" ")[4]).toggleClass("sel");

        unit = $(this).attr("name");

        unit2 = ratio[unit].name;
        $(".dio_drop_rec_unit .caption").attr("name", unit);
        $(".dio_drop_rec_unit .caption").each(function () {
          this.innerHTML = unit2;
        });

        $($(this).parent().parent().get(0)).removeClass("open");
        $(".dio_drop_rec_unit .caption").change();
      });
    });
    $(wndID + " .select_rec_perc .option").each(function () {
      $(this).click(function (e: any) {
        $(this).parent().find(".sel").toggleClass("sel");
        $(this).toggleClass("sel");

        percent = $(this).attr("name");
        $(".dio_drop_rec_perc .caption").attr("name", percent);
        $(".dio_drop_rec_perc .caption").each(function () {
          this.innerHTML = Math.round(percent * 100) + "%";
        });

        $($(this).parent().parent().get(0)).removeClass("open");
        $(".dio_drop_rec_perc .caption").change();
      });
    });

    // show & hide drop menus on click
    $(wndID).click(function (e: any) {
      var clicked = $(e.target),
        element = $("#" + this.id + " .dropdown-list.open").get(0);
      if (clicked[0].parentNode.className.split(" ")[1] !== "dropdown" && element) {
        $(element).removeClass("open");
      }
    });

    // hover arrow change
    $(wndID + ".dropdown").hover(
      function (e: any) {
        $(e.target)[0].parentNode.childNodes[3].style.background = "url(https://dio-david1327.github.io/img/dio/btn/drop-out.png) no-repeat -1px -1px";
      },
      function (e: any) {
        $(e.target)[0].parentNode.childNodes[3].style.background = "url(https://dio-david1327.github.io/img/dio/btn/drop-out.png) no-repeat -1px -1px";
      }
    );

    $(wndID + ".dio_drop_rec_unit .caption").attr("name", unit);
    $(wndID + ".dio_drop_rec_perc .caption").attr("name", percent);

    $(wndID + ".dio_drop_rec_unit").mousePopup(new MousePopup('<div class="dio_icon b"></div> Resource ratio of an unit type'));
    $(wndID + ".dio_drop_rec_perc").mousePopup(new MousePopup('<div class="dio_icon b"></div> Share of the storage capacity of the target city'));

    var res = RecruitingTrade.resources;
    var ratio = {
      //MO: RecruitingTrade.addd(""),
      //Ship

      FS: res("attack_ship"), // Light ship
      BI: res("bireme"), // Bireme
      TR: res("trireme"), // Trireme
      BT: res("big_transporter"), // Transport boat
      BE: res("small_transporter"), // Fast transport ship
      CE: res("colonize_ship"), // Colony ship
      DS: res("demolition_ship"), // Fire ship
      //Troop
      SK: res("sword"), // Swordsman
      SL: res("slinger"), // Slinger
      BS: res("archer"), // Archer
      HO: res("hoplite"), // Hoplite
      RE: res("rider"), // Horseman
      SW: res("chariot"), // Chariot
      CA: res("catapult"), // Catapult
      //Fly
      CT: res("centaur"), // Centaur
      CB: res("cerberus"), // Cerberus
      CL: res("zyklop"), // Cyclop
      EY: res("fury"), // Erinys
      MD: res("medusa"), // Medusa
      MT: res("minotaur"), // Minotaur
      HD: res("sea_monster"), // Hydra
      HP: res("harpy"), // Harpy
      MN: res("manticore"), // Manticore
      PG: res("pegasus"), // Pegasus
      GF: res("griffin"), // Griffin
      SE: res("siren"), // Siren
      ST: res("satyr"), // Satyr
      LD: res("ladon"), // Ladon
      SR: res("spartoi"), // Spartoi
      CY: res("calydonian_boar"), // Calydonian boar
      //Other
      WA: {
        w: 0.2286,
        s: 1,
        i: 0.6714,
        name: window.GameData.buildings.wall.name + " Lv 5"
      }, // City wall Lv 5
      WA2: {
        w: 0.0762,
        s: 1,
        i: 0.7491,
        name: window.GameData.buildings.wall.name + " Lv 15"
      }, // City wall Lv 15
      HI: res(false, 1621, 2000, 2980, window.DM.getl10n("hide").index.hide + " Lv 5"), // City hide Lv 5
      HI2: res(false, 3991, 4000, 5560, window.DM.getl10n("hide").index.hide + " Lv 10"), // City hide Lv 10
      FE: { w: 0.8333, s: 1, i: 0.8333, name: "City Festivals" } // City festival
    };

    if ($("#town_capacity_wood .max").get(0)) {
      max_amount = parseInt($("#town_capacity_wood .max").get(0).innerHTML, 10);
    } else {
      max_amount = 25500;
    }

    $(wndID + ".caption").change(function (e: any) {
      if (!($(this).attr("name") === unit || $(this).attr("name") === percent)) {
        $(".dio_rec_count").get(0).innerHTML = "(" + trade_count + ")";
      }

      var tmp = $(this).attr("name");

      if ($(this).parent().attr("class").split(" ")[0] === "dio_drop_rec_unit") {
        unit = tmp;
      } else {
        percent = tmp;
      }

      var max = (max_amount - 100) / 1000;

      addTradeMarks(max * ratio[unit].w, max * ratio[unit].s, max * ratio[unit].i, "lime");

      var part = (max_amount - 1000) * parseFloat(percent); // -1000 als Puffer (sonst Überlauf wegen Restressies, die nicht eingesetzt werden können, vorallem bei FS und Biremen)
      var rArray = window.ITowns.getTown(window.Game.townId).getCurrentResources();
      var tradeCapacity = window.ITowns.getTown(window.Game.townId).getAvailableTradeCapacity();
      var wood = ratio[unit].w * part;
      var stone = ratio[unit].s * part;
      var iron = ratio[unit].i * part;
      if (wood > rArray.wood || stone > rArray.stone || iron > rArray.iron || wood + stone + iron > tradeCapacity) {
        wood = stone = iron = 0;
        $(".dio_drop_rec_perc .caption").css({ color: "#f00" });
      } else {
        $("." + e.target.parentNode.parentNode.className + " .dio_drop_rec_perc .caption").css({ color: "#000" });
      }
      $("#trade_type_wood [type='text']").select().val(wood).blur();
      $("#trade_type_stone [type='text']").select().val(stone).blur();
      $("#trade_type_iron [type='text']").select().val(iron).blur();
    });

    $("#trade_button").click(() => {
      trade_count++;

      $(".dio_rec_count").get(0).innerHTML = "(" + trade_count + ")";
    });

    $(wndID + ".dio_drop_rec_perc .caption").change();

    // Tooltip \\
    var units = window.GameData.units;

    //Ship
    $("#dioattack_ship").mousePopup(new MousePopup(units.attack_ship.name));
    $("#diobireme").mousePopup(new MousePopup(units.bireme.name));
    $("#diotrireme").mousePopup(new MousePopup(units.trireme.name));
    $("#diotransporter").mousePopup(new MousePopup(units.big_transporter.name));
    $("#diosmall_trans").mousePopup(new MousePopup(units.small_transporter.name));
    $("#diocolonize").mousePopup(new MousePopup(units.colonize_ship.name));
    $("#diodemolition").mousePopup(new MousePopup(units.demolition_ship.name));
    $("#diosword").mousePopup(new MousePopup(units.sword.name));
    //Troop
    $("#dioslinger").mousePopup(new MousePopup(units.slinger.name));
    $("#dioarcher").mousePopup(new MousePopup(units.archer.name));
    $("#diohoplite").mousePopup(new MousePopup(units.hoplite.name));
    $("#diorider").mousePopup(new MousePopup(units.rider.name));
    $("#diochariot").mousePopup(new MousePopup(units.chariot.name));
    $("#diocatapult").mousePopup(new MousePopup(units.catapult.name));
    //Fly
    $("#diocentaur").mousePopup(new MousePopup(units.centaur.name));
    $("#diocerberus").mousePopup(new MousePopup(units.cerberus.name));
    $("#diozyklop").mousePopup(new MousePopup(units.zyklop.name));
    $("#diofury").mousePopup(new MousePopup(units.fury.name));
    $("#diomedusa").mousePopup(new MousePopup(units.medusa.name));
    $("#diominotaur").mousePopup(new MousePopup(units.minotaur.name));
    $("#diosea_monster").mousePopup(new MousePopup(units.sea_monster.name));
    $("#dioharpy").mousePopup(new MousePopup(units.harpy.name));
    $("#diomanticore").mousePopup(new MousePopup(units.manticore.name));
    $("#diopegasus").mousePopup(new MousePopup(units.pegasus.name));
    $("#diogriffin").mousePopup(new MousePopup(units.griffin.name));
    $("#diocalydonian").mousePopup(new MousePopup(units.calydonian_boar.name));
    $("#diospartoi").mousePopup(new MousePopup(units.spartoi.name));
    $("#diosatyr").mousePopup(new MousePopup(units.satyr.name));
    $("#dioladon").mousePopup(new MousePopup(units.ladon.name));
    $("#diosiren").mousePopup(new MousePopup(units.siren.name));
    //Other
    $("#diowall").mousePopup(new MousePopup(window.GameData.buildings.wall.name + " Lv 5"));
    $("#diowall2").mousePopup(new MousePopup(window.GameData.buildings.wall.name + " Lv 15"));
    $("#diohide").mousePopup(new MousePopup(window.DM.getl10n("hide").index.hide + " Lv 5"));
    $("#diohide2").mousePopup(new MousePopup(window.DM.getl10n("hide").index.hide + " Lv 10"));
    $("#diofestivals").mousePopup(new MousePopup("City festivals"));
  },
  resources: function (res: string | number, W?: any, S?: any, I?: any, name?: any) {
    let w, s, i, a;
    if (res) {
      a = window.GameData.units[res].resources;
      w = a.wood;
      s = a.stone;
      i = a.iron;
      a = Math.max(w, s, i);

      name = window.GameData.units[res].name;
    } else {
      w = W;
      s = S;
      i = I;
      a = w + s + i;
    }
    w = w / a;
    s = s / a;
    i = i / a;
    return { w: w, s: s, i: i, name: name };
  }
};

export default RecruitingTrade;
