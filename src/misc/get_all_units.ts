/*******************************************************************************************************************************
 * Available units
 * ----------------------------------------------------------------------------------------------------------------------------
 * | ● GetAllUnits
 * | ● Shows all available units
 * ----------------------------------------------------------------------------------------------------------------------------
 *******************************************************************************************************************************/

import TrStorage from "../storage";
import UI from "../utils/ui";

function getAllUnits() {
  try {
    var townArray = window.ITowns.getTowns(),
      groupArray = window.ITowns.townGroups.getGroupsTR(),
      unitArray = {
        sword: 0,
        archer: 0,
        hoplite: 0,
        chariot: 0,
        godsent: 0,
        rider: 0,
        slinger: 0,
        catapult: 0,
        small_transporter: 0,
        big_transporter: 0,
        manticore: 0,
        harpy: 0,
        pegasus: 0,
        cerberus: 0,
        minotaur: 0,
        medusa: 0,
        zyklop: 0,
        centaur: 0,
        fury: 0,
        sea_monster: 0
      },
      unitArraySea = {
        bireme: 0,
        trireme: 0,
        attack_ship: 0,
        demolition_ship: 0,
        colonize_ship: 0
      };

    //console.debug("TR-TOOLS | getAllUnits | GROUP ARRAY", groupArray);

    if (window.Game.hasArtemis) {
      unitArray = $.extend(unitArray, { griffin: 0, calydonian_boar: 0 });
    }

    if (window.Game.gods_active.aphrodite) {
      unitArray = $.extend(unitArray, { siren: 0, satyr: 0 });
    }

    if (window.Game.gods_active.ares) {
      unitArray = $.extend(unitArray, { spartoi: 0, ladon: 0 });
    }
    unitArray = $.extend(unitArray, unitArraySea);

    for (var group in groupArray) {
      if (groupArray.hasOwnProperty(group)) {
        // Clone Object "unitArray"

        window.tr.groupUnitArray[group] = Object.create(unitArray);

        for (var town in groupArray[group].towns) {
          if (groupArray[group].towns.hasOwnProperty(town)) {
            var type = { lo: 0, ld: 0, so: 0, sd: 0, fo: 0, fd: 0 }; // Type for TownList

            for (var unit in unitArray) {
              if (unitArray.hasOwnProperty(unit)) {
                // All Groups: Available units

                var tmp = parseInt(window.ITowns.getTown(town).units()[unit], 10);

                window.tr.groupUnitArray[group][unit] += tmp || 0;
                // Only for group "All"

                if (group == -1) {
                  // Bireme counter // old
                  if (unit === "bireme" && (window.tr.biriArray[townArray[town].id] || 0) < (tmp || 0)) {
                    window.tr.biriArray[townArray[town].id] = tmp;
                  }
                  //TownTypes
                  if (!window.GameData.units[unit].is_naval) {
                    if (window.GameData.units[unit].flying) {
                      type.fd += ((window.GameData.units[unit].def_hack + window.GameData.units[unit].def_pierce + window.GameData.units[unit].def_distance) / 3) * (tmp || 0);
                      type.fo += window.GameData.units[unit].attack * (tmp || 0);
                    } else {
                      type.ld += ((window.GameData.units[unit].def_hack + window.GameData.units[unit].def_pierce + window.GameData.units[unit].def_distance) / 3) * (tmp || 0);
                      type.lo += window.GameData.units[unit].attack * (tmp || 0);
                    }
                  } else {
                    type.sd += window.GameData.units[unit].defense * (tmp || 0);
                    type.so += window.GameData.units[unit].attack * (tmp || 0);
                  }
                }
              }
            }
            // Only for group "All"
            if (group == -1) {
              // Icon: DEF or OFF?
              var z = type.sd + type.ld + type.fd <= type.so + type.lo + type.fo ? "o" : "d",
                temp = 0;

              for (var t in type) {
                if (type.hasOwnProperty(t)) {
                  // Icon: Land/Sea/Fly (t[0]) + OFF/DEF (z)
                  if (temp < type[t]) {
                    window.tr.autoTownTypes[townArray[town].id] = t[0] + z;
                    temp = type[t];
                  }
                  // Icon: Troops Outside (overwrite)
                  if (temp < 1000) {
                    window.tr.autoTownTypes[townArray[town].id] = "no";
                  }
                }
              }
              // Icon: Empty Town (overwrite)
              var popBuilding = 0,
                buildVal = window.GameData.buildings,
                levelArray = townArray[town].buildings().getLevels(),
                popMax = Math.floor(buildVal.farm.farm_factor * Math.pow(townArray[town].buildings().getBuildingLevel("farm"), buildVal.farm.farm_pow)), // Population from farm level
                popPlow = townArray[town].getResearches().attributes.plow ? 200 : 0,
                popFactor = townArray[town].getBuildings().getBuildingLevel("thermal") ? 1.1 : 1.0, // Thermal
                popExtra = townArray[town].getPopulationExtra();

              for (var b in levelArray) {
                if (levelArray.hasOwnProperty(b)) {
                  popBuilding += Math.round(buildVal[b].pop * Math.pow(levelArray[b], buildVal[b].pop_factor));
                }
              }
              window.tr.population[town] = {};
              window.tr.population[town].max = popMax * popFactor + popPlow + popExtra;
              window.tr.population[town].buildings = popBuilding;
              window.tr.population[town].units = parseInt(window.tr.population[town].max - (popBuilding + townArray[town].getAvailablePopulation()), 10);

              if (window.tr.population[town].units < 300) {
                window.tr.autoTownTypes[townArray[town].id] = "po";
              }

              window.tr.population[town].percent = Math.round((100 / (window.tr.population[town].max - popBuilding)) * window.tr.population[town].units);
              // ! if (!window.tr.manuTownAuto[townArray[town].id]) {
              // ! window.tr.autoTownTypes[townArray[town].id] = "OO";
              //! }
            }
          }
        }
      }
    }
    // Update Available Units
    AvailableUnits.updateBullseye();

    if (window.GPWindowMgr.TYPE_DIO_UNITS) {
      if (window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_UNITS)) {
        AvailableUnits.updateWindow();
      }
    }
  } catch (error) {
    console.error(error, "getAllUnits"); // TODO: Eventueller Fehler in Funktion
  }
}

// Neuer Einheitenzähler
var UnitCounter: UnitCounterType = {
  units: { total: {}, available: {}, outer: {}, foreign: {}, support: {} },
  count: function () {
    var tooltipHelper = __non_webpack_require__("helpers/units_tooltip_helper");
    var groups = window.ITowns.townGroups.getGroupsTR();

    for (var groupId in groups) {
      if (groups.hasOwnProperty(groupId)) {
        UnitCounter.units.total[groupId] = {};
        UnitCounter.units.available[groupId] = {};
        UnitCounter.units.outer[groupId] = {};
        UnitCounter.units.support[groupId] = {};
        for (var townId in groups[groupId].towns) {
          if (groups[groupId].towns.hasOwnProperty(townId)) {
            UnitCounter.units.total[groupId][townId] = window.ITowns.towns[townId].units();

            UnitCounter.units.available[groupId][townId] = window.ITowns.towns[townId].units();

            UnitCounter.units.outer[groupId][townId] = {};

            UnitCounter.units.support[groupId][townId] = window.ITowns.towns[townId].unitsSupport();

            var supports = tooltipHelper.getDataForSupportingUnitsInOtherTownFromCollection(window.MM.getTownAgnosticCollectionsByName("Units")[1].fragments[townId], window.MM.getOnlyCollectionByName("Town"));

            for (var supportId in supports) {
              if (supports.hasOwnProperty(supportId)) {
                for (var attributeId in supports[supportId].attributes) {
                  if (supports[supportId].attributes.hasOwnProperty(attributeId)) {
                    if (typeof window.GameData.units[attributeId] !== "undefined" && supports[supportId].attributes[attributeId] > 0) {
                      UnitCounter.units.outer[groupId][townId][attributeId] = (UnitCounter.units.outer[groupId][townId][attributeId] || 0) + supports[supportId].attributes[attributeId];

                      UnitCounter.units.total[groupId][townId][attributeId] = (UnitCounter.units.total[groupId][townId][attributeId] || 0) + supports[supportId].attributes[attributeId];
                    }
                  }
                }
              }
            }
          }
        }

        UnitCounter.summarize(groupId);
      }
    }
    return UnitCounter.units;
  },

  summarize: function (groupId: string | number) {
    var tooltipHelper = __non_webpack_require__("helpers/units_tooltip_helper");

    UnitCounter.units.total[groupId]["all"] = {};
    UnitCounter.units.available[groupId]["all"] = {};

    UnitCounter.units.outer[groupId]["all"] = {};

    UnitCounter.units.support[groupId]["all"] = {};

    for (var townId in UnitCounter.units.total[groupId]) {
      if (UnitCounter.units.total[groupId].hasOwnProperty(townId) && townId !== "all") {
        var unitId;
        // Einheiten gesamt

        for (unitId in UnitCounter.units.total[groupId][townId]) {
          if (UnitCounter.units.total[groupId][townId].hasOwnProperty(unitId)) {
            UnitCounter.units.total[groupId]["all"][unitId] = (UnitCounter.units.total[groupId]["all"][unitId] || 0) + UnitCounter.units.total[groupId][townId][unitId];
          }
        }
        // Einheiten verfügbar

        for (unitId in UnitCounter.units.available[groupId][townId]) {
          if (UnitCounter.units.available[groupId][townId].hasOwnProperty(unitId)) {
            UnitCounter.units.available[groupId]["all"][unitId] = (UnitCounter.units.available[groupId]["all"][unitId] || 0) + UnitCounter.units.available[groupId][townId][unitId];
          }
        }
        // Einheiten außerhalb

        for (unitId in UnitCounter.units.outer[groupId][townId]) {
          if (UnitCounter.units.outer[groupId][townId].hasOwnProperty(unitId)) {
            UnitCounter.units.outer[groupId]["all"][unitId] = (UnitCounter.units.outer[groupId]["all"][unitId] || 0) + UnitCounter.units.outer[groupId][townId][unitId];
          }
        }
        // Einheiten außerhalb

        for (unitId in UnitCounter.units.support[groupId][townId]) {
          if (UnitCounter.units.support[groupId][townId].hasOwnProperty(unitId)) {
            UnitCounter.units.support[groupId]["all"][unitId] = (UnitCounter.units.support[groupId]["all"][unitId] || 0) + UnitCounter.units.support[groupId][townId][unitId];
          }
        }
      }
    }
  }
};

var AvailableUnits: AvailableUnitsType = {
  timeout: null,
  wnd: null,
  activate: function () {
    AvailableUnits.timeout = setInterval(() => {
      UnitCounter.count();
    }, 1000);
    var DioMenuFix = !1;

    $("#dio_available_units_bullseye").length &&
      ((DioMenuFix = !0),
      $("#HMoleM").css({
        top: $("#ui_box .nui_left_box:first").offset().top + $("#ui_box .nui_left_box:first")[0].scrollHeight - 6
      }),
      !window.MH.nui_main_menu()),
      $("<style>" + "available_units_bullseye_addition { display:none!important; }" + "</style>").appendTo("head");

    var default_title = window.DM.getl10n("place", "support_overview").options.troop_count + " (" + window.DM.getl10n("hercules2014", "available") + ")";
    $(".dio_picomap_container").prepend("<div id='dio_available_units_bullseye' class='unit_icon90x90 " + (window.tr.bullseyeUnit[window.tr.bullseyeUnit.current_group] || "bireme") + "'><div class='amount'></div></div>");
    // Ab version 2.115
    if ($(".topleft_navigation_area").get(0)) {
      $(".topleft_navigation_area").prepend(
        "<div id='available_units_bullseye' style='display: none;'></div><div id='dio_available_units_bullseye_addition' class='picomap_area'><div class='dio_picomap_container'><div id='dio_available_units_bullseye' class='unit_icon90x90 " +
          (window.tr.bullseyeUnit[window.tr.bullseyeUnit.current_group] || "bireme") +
          "'><div class='amount'></div></div></div><div class='dio_picomap_overlayer'></div></div>"
      );
      $(
        '<style id="dio_available_units_style_addition">' +
          "#dio_ava2 { display:block!important; }" +
          //'#tr_mnu_list .nui_main_menu {top: 0px !important; }'+
          ".bull_eye_buttons, .rb_map { height:38px !important; }" +
          ".coords_box { top: 117px !important; } " +
          "#ui_box .btn_change_colors { top: 31px !important; }" +
          ".picomap_area { position: absolute; overflow: visible; top: 0; left: 0; width: 156px; height: 161px; z-index: 5; }" +
          ".picomap_area .dio_picomap_container, .picomap_area .dio_picomap_overlayer { position: absolute; top: 33px; left: -3px; width: 147px; height: 101px; }" +
          //'.picomap_area .dio_picomap_overlayer { background: url(https://gpde.innogamescdn.com/images/game/autogenerated/layout/layout_2.107.png) -145px -208px no-repeat; width: 147px; height: 101px; z-index: 5;} '+
          '.picomap_area .dio_picomap_overlayer { background: url("https://dio-david1327.github.io/img/dio/logo/dio-sprite-5.png");background-position: 473px 250px; width: 147px; z-index: 6;} ' +
          "</style>"
      ).appendTo("head");
      AvailableUnits.autre();
    }
    // Style
    $(
      '<style id="dio_available_units_style">' +
        "#dio_available_units .unit.index_unit.bold.unit_icon40x40:hover {-webkit-filter: brightness(1.1); box-shadow: 0px 0px 6px rgb(0 252 18);}" +
        "@-webkit-keyframes Z { 0% { opacity: 0; } 100% { opacity: 1; } } " +
        "@keyframes Z { 0% { opacity: 0; } 100% { opacity: 1; } } " +
        "@-webkit-keyframes blurr { 0% { -webkit-filter: blur(5px); } 100% { -webkit-filter: blur(0px); } } " +
        ".dio_picomap_overlayer { cursor:pointer; } " +
        ".picomap_area .bull_eye_buttons { height: 55px; } " +
        "#sea_id { background: none; font-size:25px; cursor:default; height:50px; width:50px; position:absolute; top:70px; left:157px; z-index: 30; } " +
        // Available bullseye unit
        "#dio_available_units_bullseye { margin: 10px 28px 0px 28px; -webkit-animation: blur 2s; animation: Z 1s; } " +
        "#dio_available_units_bullseye .amount { color:#826021; position:relative; top:28px; font-style:italic; width:79px; font-weight: bold; text-shadow: 0px 0px 2px black, 1px 1px 2px black, 0px 2px 2px black; -webkit-animation: blur 3s; } " +
        "#dio_available_units_bullseye.big_number { font-size: 0.90em; line-height: 1.4; } " +
        "#dio_available_units_bullseye.blur { -webkit-animation: blurr 0.6s; } " +
        // Land units
        "#dio_available_units_bullseye.sword	.amount	{ color:#E2D9C1; top:57px; width:90px;	} " +
        "#dio_available_units_bullseye.hoplite	.amount	{ color:#E2D9C1; top:57px; width:90px;	} " +
        "#dio_available_units_bullseye.archer	.amount	{ color:#E2D0C1; top:47px; width:70px;	} " +
        "#dio_available_units_bullseye.chariot			{ margin-top: 15px; } " +
        "#dio_available_units_bullseye.chariot	.amount	{ color:#F5E8B4; top:38px; width:91px;  } " +
        "#dio_available_units_bullseye.rider	.amount	{ color:#DFCC6C; top:52px; width:105px;	} " +
        "#dio_available_units_bullseye.slinger	.amount	{ color:#F5E8B4; top:53px; width:91px;	} " +
        "#dio_available_units_bullseye.catapult	.amount	{ color:#F5F6C5; top:36px; width:87px;	} " +
        "#dio_available_units_bullseye.godsent	.amount	{ color:#F5F6C5; top:57px; width:92px;	} " +
        // Mythic units
        "#dio_available_units_bullseye.medusa			.amount	{ color:#FBFFBB; top:50px; width:65px;	} " +
        "#dio_available_units_bullseye.manticore		.amount	{ color:#ECD181; top:50px; width:55px; 	} " +
        "#dio_available_units_bullseye.pegasus					{ margin-top: 16px;	} " +
        "#dio_available_units_bullseye.pegasus			.amount	{ color:#F7F8E3; top:36px; width:90px;	} " +
        "#dio_available_units_bullseye.minotaur			        { margin-top: 10px; } " +
        "#dio_available_units_bullseye.minotaur		    .amount	{ color:#EAD88A; top:48px; width:78px;	} " +
        "#dio_available_units_bullseye.zyklop					{ margin-top: 3px;	} " +
        "#dio_available_units_bullseye.zyklop			.amount	{ color:#EDE0B0; top:50px; width:95px;	} " +
        "#dio_available_units_bullseye.harpy					{ margin-top: 16px;	} " +
        "#dio_available_units_bullseye.harpy			.amount	{ color:#E7DB79; top:30px; width:78px;	} " +
        "#dio_available_units_bullseye.sea_monster		.amount	{ color:#D8EA84; top:58px; width:91px;	} " +
        "#dio_available_units_bullseye.cerberus		    .amount	{ color:#EC7445; top:25px; width:101px;	} " +
        "#dio_available_units_bullseye.centaur					{ margin-top: 15px;	} " +
        "#dio_available_units_bullseye.centaur			.amount	{ color:#ECE0A8; top:29px; width:83px;	} " +
        "#dio_available_units_bullseye.fury			    .amount	{ color:#E0E0BC; top:57px; width:95px;	} " +
        "#dio_available_units_bullseye.griffin					{ margin-top: 15px;	} " +
        "#dio_available_units_bullseye.griffin			.amount	{ color:#FFDC9D; top:40px; width:98px;	} " +
        "#dio_available_units_bullseye.calydonian_boar	.amount	{ color:#FFDC9D; top:17px; width:85px;	} " +
        "#dio_available_units_bullseye.siren		    .amount	{ color:#EAD88A; top:50px; width:78px;	} " +
        "#dio_available_units_bullseye.satyr			        { margin-top: 15px; } " +
        "#dio_available_units_bullseye.satyr		    .amount	{ color:#EAD88A; top:48px; width:78px;	} " +
        "#dio_available_units_bullseye.spartoi			        { margin-top: 10px; } " +
        "#dio_available_units_bullseye.spartoi		    .amount	{ color:#EAD88A; top:48px; width:78px;	} " +
        "#dio_available_units_bullseye.ladon			        { margin-top: 10px; } " +
        "#dio_available_units_bullseye.ladon		    .amount	{ color:#EAD88A; top:48px; width:78px;	} " +
        // Naval units
        "#dio_available_units_bullseye.attack_ship		    .amount	{ color:#FFCB00; top:26px; width:99px;	} " +
        "#dio_available_units_bullseye.bireme			    .amount	{ color:#DFC677; color:azure; top:28px; width:79px;	} " +
        "#dio_available_units_bullseye.trireme			    .amount	{ color:#F4FFD4; top:24px; width:90px;	} " +
        "#dio_available_units_bullseye.small_transporter	.amount { color:#F5F6C5; top:26px; width:84px;	} " +
        "#dio_available_units_bullseye.big_transporter	    .amount { color:#FFDC9D; top:27px; width:78px;	} " +
        "#dio_available_units_bullseye.colonize_ship		.amount { color:#F5F6C5; top:29px; width:76px;	} " +
        "#dio_available_units_bullseye.colonize_ship		.amount { color:#F5F6C5; top:29px; width:76px;	} " +
        "#dio_available_units_bullseye.demolition_ship	    .amount { color:#F5F6C5; top:35px; width:90px;	} " +
        // Available units window
        "#dio_available_units { overflow: auto;  } " +
        "#dio_available_units .unit { margin: 5px; cursor:pointer; overflow:visible; display: -webkit-inline-box; float: none; } " +
        "#dio_available_units .unit.active { border: 2px solid #7f653a; border-radius:30px; margin:4px; } " +
        "#dio_available_units .unit span { text-shadow: 1px 1px 1px black, 1px 1px 2px black;} " +
        "#dio_available_units hr { margin: 5px 0px 5px 0px; } " +
        "#dio_available_units .drop_box .option { float: left; margin-right: 30px; width:100%; } " +
        "#dio_available_units .drop_box { position:absolute; top: -38px; right: 98px; width:120px; z-index:10; } " +
        "#dio_available_units .drop_box .drop_group { width: 124px; } " +
        "#dio_available_units .drop_box .select_group.open { display:block; } " +
        "#dio_available_units .drop_box .item-list { overflow: auto; overflow-x: hidden; } " +
        '#dio_available_units .drop_box .arrow { width:18px; height:18px; background:url("https://www.tuto-de-david1327.com/medias/images/drop-out.png") no-repeat -1px -1px; position:absolute; } ' +
        // Available units button
        "#dio_btn_available_units { top:84px; left:120px; z-index:15; position:absolute; } " +
        "#dio_btn_available_units .ico_available_units { margin:5px 0px 0px 4px; width:24px; height:24px; " +
        "background:url(https://www.tuto-de-david1327.com/medias/images/couteau.png) no-repeat 0px 0px;background-size:100%; filter:url(#Hue1); -webkit-filter:hue-rotate(100deg);  } " +
        "</style>"
    ).appendTo("head");

    if (window.Game.gods_active.aphrodite || window.Game.gods_active.ares) {
      UI.createWindowType("DIO_UNITS", "Available Units", 430, 315, true, [240, 70]);
    } else {
      UI.createWindowType("DIO_UNITS", "Available Units", 378, 315, true, [240, 70]);
    }
    // Set Sea-ID beside the bull eye
    $("#sea_id").prependTo("#ui_box");
    AvailableUnits.addButton();
    UnitCounter.count();
    AvailableUnits.updateBullseye();

    $(".dio_picomap_overlayer").mousePopup(new MousePopup('<div class="dio_icon b"></div>' + "Available Units"));
  },
  autre: function () {
    if ($(".topleft_navigation_area").get(0)) {
      $("#dio_available_units_style_oceanautre").remove();
      $("#dio_available_units_style_oceanaut").remove();
      if (window.tr.settings.tr_ava2) {
        $(
          '<style id="dio_available_units_style_oceanautre">' +
            ".nui_grepo_score { top: 167px!important; } " +
            ".nui_left_box { top: 119px ; } " +
            ".nui_main_menu { top: 276px ; }" +
            "#HMoleM {top: 270px !important;}" +
            "#ui_box .ocean_number_box { position: absolute; top: 151px; left: 45px; }" +
            "#ui_box .ocean_number_box .ocean_number { font-weight: 700; z-index: 5; width: 100px; left: -13px;}" +
            ".picomap_area .dio_picomap_overlayer { height: 135px ; } " +
            "</style>"
        ).appendTo("head");
        //if ($('#MHOLE_MENU').is(':visible')) {$('<style id="dio_available_units_style_oceanautre">.nui_main_menu { top: 333px; }</style>').appendTo('head');}
      } else if (!window.tr.settings.tr_ava2) {
        $(
          '<style id="dio_available_units_style_oceanaut">' +
            ".nui_grepo_score { top: 150px!important; } " +
            ".nui_left_box { top: 102px ; } " +
            ".nui_main_menu { top: 260px ; }" +
            "#HMoleM {top: 253px !important;}" +
            "#ui_box .ocean_number_box { position: absolute; top: 65px; left: 44px; }" +
            "#ui_box .ocean_number_box .ocean_number { font-weight: 500; z-index: 1;}" +
            ".picomap_area .dio_picomap_overlayer { height: 101px;} " +
            "</style>"
        ).appendTo("head");
        //if ($('#MHOLE_MENU').is(':visible')) {$('<style id="dio_available_units_style_oceanautre">.nui_main_menu { top: 333px; }</style>').appendTo('head');}
      }
    }
  },
  ocean: {
    activate: function () {
      if (window.tr.settings.tr_ava) {
        setTimeout(function () {
          AvailableUnits.autre();
        }, 10);
      }
    },
    deactivate: function () {
      if (window.tr.settings.tr_ava) {
        setTimeout(function () {
          AvailableUnits.autre();
        }, 10);
      }
    }
  },
  deactivate: function () {
    $("#dio_available_units_bullseye").remove();
    $("#dio_available_units_bullseye_addition").remove();
    $("#dio_available_units_style_addition_main_menu").remove();
    $("#dio_available_units_style_oceanautre").remove();
    $("#dio_available_units_style_oceanaut").remove();
    $('<style id="dio_HMoleM">#HMoleM {top: 210px !important;}</style>').appendTo("head");
    $("#dio_available_units_style").remove();
    $("#dio_available_units_style_addition").remove();
    $("#dio_btn_available_units").remove();

    if (window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_UNITS)) {
      window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_UNITS).close();
    }
    $(".dio_picomap_overlayer").unbind();
    $("#sea_id").appendTo(".dio_picomap_container");
    $("#dio_available_units_style_ocean").remove();

    clearTimeout(AvailableUnits.timeout);
    AvailableUnits.timeout = null;
  },
  addButton: function () {
    var default_title = window.DM.getl10n("place", "support_overview").options.troop_count + " (" + window.DM.getl10n("hercules2014", "available") + ")";
    $('<div id="dio_btn_available_units" class="circle_button"><div class="ico_available_units js-caption"></div></div>').appendTo(".bull_eye_buttons");
    // Events
    $("#dio_btn_available_units")
      .on("mousedown", function () {
        $("#dio_btn_available_units, .ico_available_units").addClass("checked");
      })
      .on("mouseup", function () {
        $("#dio_btn_available_units, .ico_available_units").removeClass("checked");
      });
    $("#dio_btn_available_units, .dio_picomap_overlayer").click(function () {
      if (!window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_UNITS)) {
        AvailableUnits.openWindow();
        $("#dio_btn_available_units, .ico_available_units").addClass("checked");
      } else {
        AvailableUnits.closeWindow();
        $("#dio_btn_available_units, .ico_available_units").removeClass("checked");
      }
    });
    // Tooltip

    $("#dio_btn_available_units").mousePopup(new MousePopup('<div class="dio_icon b"></div> Units overview'));
  },
  openWindow: function () {
    var groupArray = window.ITowns.townGroups.getGroupsTR(),
      unitArray: { [key: string]: number } = {
        sword: 0,
        archer: 0,
        hoplite: 0,
        slinger: 0,
        rider: 0,
        chariot: 0,
        catapult: 0,
        godsent: 0,
        manticore: 0,
        harpy: 0,
        pegasus: 0,
        griffin: 0,
        cerberus: 0,
        minotaur: 0,
        medusa: 0,
        zyklop: 0,
        centaur: 0,
        calydonian_boar: 0,
        fury: 0,
        sea_monster: 0,
        spartoi: 0,
        ladon: 0,
        satyr: 0,
        siren: 0,
        small_transporter: 0,
        big_transporter: 0,
        bireme: 0,
        attack_ship: 0,
        trireme: 0,
        demolition_ship: 0,
        colonize_ship: 0
      };
    if (!groupArray[window.tr.bullseyeUnit.current_group]) {
      window.tr.bullseyeUnit.current_group = -1;
    }

    if (!window.Game.hasArtemis) {
      delete unitArray.calydonian_boar;
      delete unitArray.griffin;
    }

    if (!window.Game.gods_active.aphrodite) {
      delete unitArray.siren;
      delete unitArray.satyr;
    }

    if (!window.Game.gods_active.ares) {
      delete unitArray.spartoi;
      delete unitArray.ladon;
    }
    var land_units_str = "",
      content =
        '<div id="dio_available_units">' +
        // Dropdown menu
        '<div class="drop_box">' +
        '<div class="drop_group dropdown default">' +
        '<div class="border-left"></div><div class="border-right"></div>' +
        '<div class="caption" name="' +
        window.tr.bullseyeUnit.current_group +
        '">' +
        window.ITowns.town_groups._byId[window.tr.bullseyeUnit.current_group].attributes.name +
        "</div>" +
        //'<div class="caption" name="' + groupArray[window.tr.bullseyeUnit.current_group].name + '">dfghjk</div>' +
        '<div class="arrow"></div>' +
        "</div>" +
        '<div class="select_group dropdown-list default active"><div class="item-list"></div></div>' +
        "</div>" +
        '<table width="100%" class="radiobutton horizontal rbtn_visibility"><tr>' +
        '<td width="25%"><div class="option js-option" name="total"><div class="pointer"></div>Total</div></td>' +
        '<td width="25%"><div class="option js-option" name="available"><div class="pointer"></div>Available</div></td>' +
        '<td width="25%"><div class="option js-option" name="outer"><div class="pointer"></div>Outer</div></td>' +
        '<td width="25%"><div class="option js-option" name="support"><div class="pointer"></div>' +
        window.DM.getl10n("context_menu", "titles").support +
        "</div></td>" +
        "</tr></table>" +
        "<hr>" +
        // Content
        '<div class="box_content">';
    for (var unit in unitArray) {
      if (unitArray.hasOwnProperty(unit)) {
        land_units_str += '<div id="dio' + unit + '" class="unit index_unit bold unit_icon40x40 ' + unit + '"></div>';

        if (window.Game.gods_active.aphrodite) {
          if (unit == "siren") {
            land_units_str += '<div style="clear:left;"></div>'; // break
          }
        } else if (window.Game.gods_active.ares) {
          if (unit == "ladon") {
            land_units_str += '<div style="clear:left;"></div>';
          }
        } else if (unit == "sea_monster") {
          land_units_str += '<div style="clear:left;"></div>';
        }
      }
    }
    content += land_units_str + "</div></div>";

    AvailableUnits.wnd = window.Layout.wnd.Create(window.GPWindowMgr.TYPE_DIO_UNITS);

    AvailableUnits.wnd.setContent(content);

    // Add groups to dropdown menu
    for (var group in groupArray) {
      if (groupArray.hasOwnProperty(group)) {
        if (window.ITowns.town_groups._byId[group]) {
          var group_name = window.ITowns.town_groups._byId[group].attributes.name;

          $('<div class="option' + (group == -1 ? " sel" : "") + '" name="' + group + '">' + group_name + "</div>").appendTo("#dio_available_units .item-list");
        }
      }
    }
    // Set active mode
    if (typeof window.tr.bullseyeUnit.mode !== "undefined") {
      $('#dio_available_units .radiobutton .option[name="' + window.tr.bullseyeUnit.mode + '"]').addClass("checked");
    } else {
      $('#dio_available_units .radiobutton .option[name="available"]').addClass("checked");
    }
    // Update
    AvailableUnits.updateWindow();
    // Dropdown menu Handler
    $("#dio_available_units .drop_group").click(function () {
      $("#dio_available_units .select_group").toggleClass("open");
    });
    // Change group
    $("#dio_available_units .select_group .option").click(function () {
      window.tr.bullseyeUnit.current_group = $(this).attr("name");
      $("#dio_available_units .select_group").removeClass("open");
      $("#dio_available_units .select_group .option.sel").removeClass("sel");
      $(this).addClass("sel");
      $("#dio_available_units .drop_group .caption").attr("name", window.tr.bullseyeUnit.current_group);

      $("#dio_available_units .drop_group .caption")?.get(0)?.innerHTML = this.innerHTML;
      $("#dio_available_units .unit.active").removeClass("active");
      $("#dio_available_units .unit." + (window.tr.bullseyeUnit[window.tr.bullseyeUnit.current_group] || "bireme")).addClass("active");
      UnitCounter.count();
      AvailableUnits.updateWindow();
      AvailableUnits.updateBullseye();
      AvailableUnits.save();
    });
    // Change mode (total, available, outer)
    $("#dio_available_units .radiobutton .option").click(function () {
      window.tr.bullseyeUnit.mode = $(this).attr("name");
      $("#dio_available_units .radiobutton .option.checked").removeClass("checked");
      $(this).addClass("checked");
      UnitCounter.count();
      AvailableUnits.updateWindow();
      AvailableUnits.updateBullseye();
      AvailableUnits.save();
    });
    // Set active bullseye unit
    $("#dio_available_units .unit." + (window.tr.bullseyeUnit[window.tr.bullseyeUnit.current_group] || "bireme")).addClass("active");
    // Change bullseye unit
    $("#dio_available_units .unit").click(function () {
      window.tr.bullseyeUnit[window.tr.bullseyeUnit.current_group] = this.className.split(" ")[4].trim();
      $("#dio_available_units .unit.active").removeClass("active");
      $(this).addClass("active");
      AvailableUnits.updateBullseye();
      AvailableUnits.save();
    });
    // Close button event - uncheck available units button
    /*window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_UNITS).getJQCloseButton().get(0).onclick = function () {
            $('#dio_btn_available_units, .ico_available_units').removeClass("checked");
        };*/
    //tooltip

    $("#dio_help_available_units").mousePopup(new MousePopup('Wiki (<div class="dio_icon b"></div> Units overview)'));
    for (unit in unitArray) {
      if (unitArray.hasOwnProperty(unit)) {
        $("#dio" + unit).mousePopup(new MousePopup(window.GameData.units[unit].name));
      }
    }
  },
  closeWindow: function () {
    window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_UNITS).close();
  },
  save: function () {
    // console.debug("BULLSEYE SAVE", window.tr.bullseyeUnit);
    let tr_storage = new TrStorage();
    tr_storage.save("bullseyeUnit", window.tr.bullseyeUnit);
  },
  updateBullseye: function () {
    var sum = "0",
      str = "",
      fsize = ["1.4em", "1.2em", "1.15em", "1.1em", "1.0em", "0.95em"],
      i;
    if ($("#dio_available_units_bullseye").get(0)) {
      $("#dio_available_units_bullseye")?.get(0)?.className = "unit_icon90x90 " + (window.tr.bullseyeUnit[window.tr.bullseyeUnit.current_group] || "bireme");

      if (UnitCounter.units[window.tr.bullseyeUnit.mode || "available"][window.tr.bullseyeUnit.current_group]) {
        sum = UnitCounter.units[window.tr.bullseyeUnit.mode || "available"][window.tr.bullseyeUnit.current_group]["all"][window.tr.bullseyeUnit[window.tr.bullseyeUnit.current_group] || "bireme"] || 0;
      }

      sum = sum.toString();

      for (i = 0; i < sum.length; i++) {
        str += "<span style='font-size:" + fsize[i] + "'>" + sum[i] + "</span>";
      }

      $("#dio_available_units_bullseye .amount")?.get(0)?.innerHTML = str;
      if (sum >= 100000) {
        $("#dio_available_units_bullseye").addClass("big_number");
      } else {
        $("#dio_available_units_bullseye").removeClass("big_number");
      }
    }
  },
  updateWindow: function () {
    $("#dio_available_units .box_content .unit").each(function () {
      var unit = this.className.split(" ")[4];
      // TODO: Alte Variante entfernen
      // Alte Variante:
      //this.innerHTML = '<span style="font-size:0.9em">' + groupUnitArray[window.tr.bullseyeUnit.current_group][unit] + '</span>';
      // Neue Variante

      this.innerHTML = '<span style="font-size:0.9em">' + (UnitCounter.units[window.tr.bullseyeUnit.mode || "available"][window.tr.bullseyeUnit.current_group]["all"][unit] || 0) + "</span>";
    });
  }
};

export default AvailableUnits;
export { getAllUnits };
