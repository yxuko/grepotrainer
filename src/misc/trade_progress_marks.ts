/*******************************************************************************************************************************
 * ● Ressources marks
 *******************************************************************************************************************************/

function addTradeMarks(woodmark: number, stonemark: number, ironmark: number, color: string) {
  var max_amount: string,
    limit,
    wndArray = window.GPWindowMgr.getOpen(window.Layout.wnd.TYPE_TOWN),
    wndID;
  for (var e in wndArray) {
    if (wndArray.hasOwnProperty(e)) {
      wndID = "#gpwnd_" + wndArray[e].getID() + " ";
      if ($(wndID + ".town-capacity-indicator").get(0)) {
        max_amount = $(wndID + ".amounts .max").get(0).innerHTML;
        $("#trade_tab .c_" + color).each(function () {
          this.remove();
        });
        $("#trade_tab .progress").each(function () {
          if ($("p", this).length < 3) {
            if ($(this).parent().get(0).id != "big_progressbar") {
              limit = 1000 * (242 / parseInt(max_amount, 10));

              switch ($(this).parent().get(0).id.split("_")[2]) {
                case "wood":
                  limit = limit * woodmark;
                  break;
                case "stone":
                  limit = limit * stonemark;
                  break;
                case "iron":
                  limit = limit * ironmark;
                  break;
              }
              $('<p class="c_' + color + '"style="position:absolute;left: ' + limit + "px; background:" + color + ';width:2px;height:100%;margin:0px"></p>').appendTo(this);
            }
          }
        });
      }
    }
  }
}

export default addTradeMarks;
