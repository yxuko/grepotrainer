/*******************************************************************************************************************************
 * Science Feature
 *******************************************************************************************************************************/

import TownIcons from "../ui/town_icons";
import UI from "../utils/ui";

var Science = {
  _researchQueue: {},
  activate: function () {
    $(
      '<style id="tr_science" type="text/css">' +
        "#tr_science .box_content {overflow-y: auto;}" +
        ".center {text-align: center;}" +
        ".w5 {width: 53px}" +
        ".w8 {width: 8rem;overflow: hidden}" +
        "#bot_townsoverview_table_wrapper {min-height: 433px;max-height: 403px;}" +
        "#bot_townsoverview_table_wrapper li {display: inline-flex;flex-direction: row;align-items: center;}" +
        ".toolbar_activities .middle .activity.science .icon, .toolbar_activities .middle .activity.science .icon.active {background: url(https://gpen.innogamescdn.com/images/game/autogenerated/layout/layout_095495a.png) no-repeat -580px -337px;width: 26px;height: 27px;}" +
        "</style>"
    ).appendTo("head");

    Science.addButton();

    // Create Window Type
    UI.createWindowType("TR_SCIENCE", "Science", 800, 605, true, ["center", "center", 100, 100]);
    //Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_SCIENCE).setHeight('800');

    window.tr.science.research = Science.research;
  },
  start: function (town: any) {},
  checkReady: function (town: any) {
    return false;
  },
  addButton: function () {
    $('<div id="tr_science_divider" class="divider"></div><div id="tr_science_button" class="activity_wrap"><div class="activity science"><div class="hover_state"><div class="icon"><div class="count js-caption">*</div></div></div></div><div class="ui_highlight" data-type="bubble_menu"></div></div>').appendTo(".tb_activities.toolbar_activities .middle");

    $("#tr_science_button").on("click", function () {
      if (!window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_SCIENCE)) {
        Science.openWindow();
      } else {
        Science.closeWindow();
      }
    });
  },
  openWindow: function () {
    var content = '<div id="tr_science" style="margin-bottom:5px; font-style:italic;"><div class="box_content"></div></div>';

    window.Layout.wnd.Create(window.GPWindowMgr.TYPE_TR_SCIENCE).setContent(content);
    Science.addContent();

    // Close button event - uncheck available units button
    /*if (typeof window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_SCIENCE).getJQCloseButton() != 'undefined') {
      //window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_SCIENCE).getJQCloseButton().get(0).onclick = function () {
          $('#tr_settings_button').removeClass("checked");
          $('.ico_comparison').get(0).style.marginTop = "5px";
      //};
      };*/
  },
  closeWindow: function () {
    window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_SCIENCE).close();
  },
  addContent: function () {
    var content = '<ul class="game_list"><li class="even" style="display: inline-flex;">';
    content += '<div class="towninfo small tag_header col w8" id="header_town">City</div>';
    content += '<div class="towninfo small tag_header col w5" id="header_slinger"><div class="research_icon research slinger"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_archer"><div class="research_icon research archer"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_town_guard"><div class="research_icon research town_guard"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_hoplite"><div class="research_icon research hoplite active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_meteorology"><div class="research_icon research meteorology"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_espionage"><div class="research_icon research espionage active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_booty_bpv"><div class="research_icon research booty_bpv active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_pottery"><div class="research_icon research pottery"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_rider"><div class="research_icon research rider active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_architecture"><div class="research_icon research architecture"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_instructor"><div class="research_icon research instructor"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_bireme"><div class="research_icon research bireme"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_building_crane"><div class="research_icon research building_crane"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_shipwright"><div class="research_icon research shipwright active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_colonize_ship"><div class="research_icon research colonize_ship"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_chariot"><div class="research_icon research chariot"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_attack_ship"><div class="research_icon research attack_ship"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_conscription"><div class="research_icon research conscription active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_demolition_ship"><div class="research_icon research demolition_ship active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_catapult"><div class="research_icon research catapult active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_cryptography"><div class="research_icon research cryptography active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_small_transporter"><div class="research_icon research small_transporter active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_plow"><div class="research_icon research plow active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_berth"><div class="research_icon research berth active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_trireme"><div class="research_icon research trireme active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_phalanx"><div class="research_icon research phalanx active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_breach"><div class="research_icon research breach active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_mathematics"><div class="research_icon research mathematics active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_ram"><div class="research_icon research ram active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_cartography"><div class="research_icon research cartography active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_take_over"><div class="research_icon research take_over active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_stone_storm"><div class="research_icon research stone_storm active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_temple_looting"><div class="research_icon research temple_looting active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_divine_selection"><div class="research_icon research divine_selection active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_combat_experience"><div class="research_icon research combat_experience active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_strong_wine"><div class="research_icon research strong_wine active"></div></div>';
    content += '<div class="towninfo small tag_header col w5" id="header_set_sail"><div class="research_icon research set_sail active"></div></div>';
    content += '<div style="clear:both;"></div>';
    content += '</li></ul><div id="bot_townsoverview_table_wrapper">';
    content += '<ul class="game_list scroll_content">';
    var i = 0;

    var towns = Object.entries(window.ITowns.getTowns());
    for (const townE of towns) {
      var town: any = townE[1];
      var resources = town.resources();
      var researches = town.researches().attributes;
      let academyLvl = town.getBuildings().getBuildingLevel("academy");
      let researchPoints = Science.availablePoints(town.id);

      content += '<li class="' + (i % 2 ? "even" : "odd") + ' bottom" id="ov_town_' + town.id + '">';
      content += '<div class="towninfo small townsoverview col w8">';
      content += '<div class="town_name_culture">';
      content += '<span style="display: inline-flex;align-items: center;gap: 2px;">';
      if (window.tr.autoTownTypes.hasOwnProperty(town.id)) {
        content += '<div style="background: url(https://dio-david1327.github.io/img/dio/btn/town-icons.png) repeat;background-position: ' + TownIcons.types[window.tr.manuTownTypes[town.id] || window.tr.autoTownTypes[town.id]] * -25 + 'px -27px;height: 18px;width:18px;"></div>';
      }
      content += '<a href="#' + town.getLinkFragment() + '" class="gp_town_link">' + town.name + "</a></span>";
      content += "<span>(" + researchPoints + "/177, lvl. " + academyLvl + ")</span>";
      content += "</div></div>";
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "slinger", researches["slinger"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "archer", researches["archer"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "town_guard", researches["town_guard"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "hoplite", researches["hoplite"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "meteorology", researches["meteorology"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "espionage", researches["espionage"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "booty", researches["booty"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "pottery", researches["pottery"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "rider", researches["rider"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "architecture", researches["architecture"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "instructor", researches["instructor"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "bireme", researches["bireme"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "building_crane", researches["building_crane"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "shipwright", researches["shipwright"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "colonize_ship", researches["colonize_ship"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "chariot", researches["chariot"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "attack_ship", researches["attack_ship"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "conscription", researches["conscription"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "demolition_ship", researches["demolition_ship"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "catapult", researches["catapult"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "cryptography", researches["cryptography"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "small_transporter", researches["small_transporter"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "plow", researches["plow"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "berth", researches["berth"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "trireme", researches["trireme"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "phalanx", researches["phalanx"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "breach", researches["breach"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "mathematics", researches["mathematics"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "ram", researches["ram"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "cartography", researches["cartography"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "take_over", researches["take_over"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "stone_storm", researches["stone_storm"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "temple_looting", researches["temple_looting"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "divine_selection", researches["divine_selection"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "combat_experience", researches["combat_experience"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "strong_wine", researches["strong_wine"]);
      content += Science.getResearchBtn(town.id, researchPoints, academyLvl, resources, "set_sail", researches["set_sail"]);
      content += '<div style="clear:both;"></div>';
      content += "</li>";
      i++;
    }
    content += "</ul></div>";
    content += '<div class="game_list_footer">';
    content += '<div id="bot_culture_settings"></div>';
    content += "</div>";

    $(content).appendTo("#tr_science .box_content");
  },
  getResearchBtn: function (townId: number, points: number, academyLvl: number, resources: any, researchId: string, researchDone: boolean) {
    let _available = Science.checkResearchAvailability(points, academyLvl, resources, researchId);
    let pending = Object.keys(Science._researchQueue).includes(townId.toString()) && Object.keys(Science._researchQueue[townId]).includes(researchId);
    let text = researchDone ? "Done" : _available ? "Research" : "-";
    if (pending) {
      return '<div class="col w5 center"><div style="background-image: url(https://gpen.innogamescdn.com/images/game/academy/points_25x25.png);margin: 0 auto;width: 24px;height: 22px;"></div></div>';
    } else if (researchDone) {
      return '<div class="towninfo small townsoverview col w5 center"><div class="' + researchId + '">' + text + "</div></div>";
    } else {
      let css = _available ? "" : "disabled";
      return `<div id="research_btn_${townId}_${researchId}" class="col w5 button_new ${css}" onclick="event.stopPropagation(); window.tr.science.research('${researchId}', ${townId})"><div class="left"></div><div class="right"></div><div class="caption js-caption" style="padding:0;font-size:8px;"><span>${text}</span><div class="effect js-effect"></div></div></div>`;
    }
  },
  getResearchQueue: function () {
    Science._researchQueue = {};
    if (window.MM.getModels().ResearchOrder && Object.keys(window.MM.getModels().ResearchOrder).length > 0) {
      $.each(window.MM.getModels().ResearchOrder, function (ind, ord) {
        (Science._researchQueue[ord.getTownId()] = Science._researchQueue[ord.getTownId()] || {}), (Science._researchQueue[ord.getTownId()][ord.getType()] = ord);
      });
    }
  },
  availablePoints: function (townId: number) {
    var town = window.MM.getCollections().Town[0].get(townId),
      _maxPoints = town.getBuildings().getBuildingLevel("academy") * window.GameDataResearches.getResearchPointsPerAcademyLevel() + town.getBuildings().getBuildingLevel("library") * window.GameDataResearches.getResearchPointsPerLibraryLevel(),
      _availablePoints = _maxPoints;
    $.each(window.GameData.researches, function (ind, elem) {
      if (town.getResearches().get(ind)) {
        _availablePoints -= elem.research_points;
      }
    });
    Science.getResearchQueue();
    console.log(Science._researchQueue);
    if (Object.keys(Science._researchQueue).length > 0 && Science._researchQueue[townId]) {
      $.each(Science._researchQueue[townId], function (iRO, _ord) {
        _availablePoints -= window.GameData.researches[iRO].research_points;
      });
    }
    return _availablePoints;
  },
  checkResearchAvailability: function (points: number, academyLvl: number, resources: any, researchId: string) {
    let academyAvailable = window.GameData.researches[researchId].building_dependencies.academy <= academyLvl;
    let resourcesCosts = window.GameData.researches[researchId].resources;
    let pointsCosts = window.GameData.researches[researchId].research_points;
    let resourcesAvailable = resources.iron >= resourcesCosts.iron && resources.wood >= resourcesCosts.wood && resources.stone >= resourcesCosts.stone;
    let pointsAvailable = points >= pointsCosts;
    return pointsAvailable && resourcesAvailable && academyAvailable;
  },
  research: function (researchId: string, townId: number) {
    window.gpAjax.ajaxPost(
      "frontend_bridge",
      "execute",
      {
        model_url: "ResearchOrder",
        action_name: "research",
        arguments: { id: researchId },
        town_id: townId
      },
      !0,
      {
        success: function (h, i) {
          Science.getResearchQueue();
          $("#research_btn_" + townId + "_" + researchId).html('<div class="col w5 center"><div style="background-image: url(https://gpen.innogamescdn.com/images/game/academy/points_25x25.png);margin: 0 auto;width: 24px;height: 22px;"></div></div>');
        },
        error: function (a, b) {
          console.log(a, b);
        }
      }
    );
  },
  deactivate: function () {
    $("#tr_science_button").remove();
    $("#tr_science_divider").remove();
  }
};

export default Science;
