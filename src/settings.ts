/*******************************************************************************************************************************
 * Settings
 *******************************************************************************************************************************/

import { sort_options, wait_options, cave_warehouse_options, cave_store_options, autobuild_timer } from "./constants";
import UI from "./utils/ui";
import WorldWonderIcons from "./ui/world_wonder_icons";
import OceanNumbers from "./ui/ocean_numbers";
import Taskbar from "./ui/taskbar";
import MouseWheelZoom from "./ui/zoom";
import UnitComparison from "./misc/unit_comparison";
import Caving from "./automation/cave";
import Farming from "./automation/farming";
import Culture from "./automation/culture";
import TownPopup from "./ui/town_popup";
import Science from "./misc/science";
import FavorPopup from "./ui/favor_popup";
import TownIcons from "./ui/town_icons";
import MapWorld from "./ui/map_world";
import TownList from "./ui/town_list";
import TrStorage from "./storage";
import Simulator from "./misc/simulator";
import AvailableUnits from "./misc/get_all_units";
import SentUnits from "./misc/sent_units";
import ShortDuration from "./misc/short_duration";
import UnitStrength from "./misc/unit_strength";
import TransportCapacity from "./ui/transport_capacity";
import RecruitingTrade from "./misc/recruiting_trade";

var Settings = {
  tooltips: [],
  t: 0,

  activate: function () {
    //document.querySelector(".forum.main_menu_item.last")?.classList.remove("last");
    //$('<li id="tr_open_settings" class="forum main_menu_item last"><span class="content_wrapper"><span class="button_wrapper"><span class="button"><span class="icon" style="background:url(https://www.tuto-de-david1327.com/medias/images/city-view.png) no-repeat -36px -0px"></span></span><div class="ui_highlight"></div><span class="indicator"></span></span></span><span class="name_wrapper"><span class="name">Trainer</span></span></li>').appendTo(".nui_main_menu .content ul")

    //$("#tr_open_settings").on("click", () => {
    //  Settings.openWindow()
    //});

    //document.querySelector(".nui_main_menu .content ul")?.style.height = "330px";

    // Create Window Type
    UI.createWindowType("TR_SETTINGS", "Grepotrainer", 650, 495, true, ["center", "center", 100, 100]);
    //Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON).setHeight('800');

    // Style
    $(
      '<style id="tr_settings_style"> ' +
        // Button
        "#tr_settings_button { top:51px; left:120px; z-index:11; position:absolute; } " +
        "#tr_settings_button .ico_comparison { margin:5px 0px 0px 4px; width:24px; height:24px; z-index: 15;" +
        "background:url(https://www.tuto-de-david1327.com/medias/images/casque.png) no-repeat 0px 0px; background-size:100%; filter:url(#Hue1); -webkit-filter:hue-rotate(60deg); } " +
        "#tr_settings_button.checked .ico_comparison { margin-top:6px; } " +
        // Window
        "#tr_settings a { float:left; background-repeat:no-repeat; background-size:25px; line-height:2; } " +
        "#tr_settings .box_content { font-style:normal; } " +
        // Content
        "#tr_settings .hidden { width:100%; } " +
        "#tr_settings .automation .t_automation, #tr_settings .merchant .t_merchant, #tr_settings .architect .t_architect, #tr_settings .philosopher .t_philosopher, #tr_settings .captain .t_captain { display:inline-table; } " +
        "#tr_settings .box_content { min-height: 423px; max-height: 423px; overflow-y: auto; overflow-x: hidden; background:url(https://www.tuto-de-david1327.com/medias/images/casque-1.png) 94% 94% no-repeat; background-size:140px; } " +
        "#tr_settings table tr :first-child { vertical-align:top; } " +
        "#tr_settings #dio_version_info { font-weight:bold;height: 35px;margin-top:-5px; } " +
        "#tr_settings #dio_version_info img { margin:-1px 2px -8px 0px; } " +
        "#tr_settings .icon_types_table { font-size:0.7em; line-height:2.5; border:1px solid green; border-spacing:10px 2px; border-radius:5px; } " +
        "#tr_settings .icon_types_table td { text-align:left; } " +
        "#tr_settings table p { margin:0.1em 0em; } " +
        "#tr_settings .checkbox_new .cbx_caption { white-space:nowrap; margin-right:10px; font-weight:bold; } " +
        "#tr_settings .tr_settings_tabs {width:auto; border:2px solid darkgreen; background:#2B241A; padding:1px 1px 0px 1px; right:auto; border-top-left-radius:5px; border-top-right-radius:5px; border-bottom:0px;} " +
        "#tr_settings .submenu_link {color: #000;} " +
        "#tr_settings .tr_settings_tabs li { float:left; } " +
        "#tr_settings .tr_icon_small { margin:0px; } " +
        "#tr_settings img { max-width:90px; max-height:90px; margin-right:10px; } " +
        "#tr_settings .content { border:2px solid darkgreen; border-radius:5px; border-top-left-radius:0px; background:rgba(31, 25, 12, 0.1); top:23px; position:relative; padding:10px; height:350px; overflow-y:auto; } " +
        "#tr_settings .content .content_category { display:none; } " +
        "#tr_settings .dio_options_table legend { font-weight:bold; } " +
        "#tr_settings .dio_options_table p { margin:0px; } " +
        "#tr_settings #dio_donate_btn { filter: hue-rotate(45deg); -webkit-filter: hue-rotate(45deg); } " +
        "#dio_hall table { border-spacing: 9px 3px; } " +
        "#dio_hall table th { text-align:left !important;color:green;text-decoration:underline;padding-bottom:10px; } " +
        "#dio_hall table td.value { text-align: right; } " +
        '#dio_hall table td.laurel.green { background: url("/images/game/ally/founder.png") no-repeat; height:18px; width:18px; background-size:100%; } ' +
        '#dio_hall table td.laurel.green2 { background: url("https://www.tuto-de-david1327.com/medias/images/laurel-sprite.png") no-repeat 0%; height:18px; width:18px; } ' +
        '#dio_hall table td.laurel.bronze { background: url("https://www.tuto-de-david1327.com/medias/images/laurel-sprite.png") no-repeat 25%; height:18px; width:18px; } ' +
        '#dio_hall table td.laurel.silver { background: url("https://www.tuto-de-david1327.com/medias/images/laurel-sprite.png") no-repeat 50%; height:18px; width:18px; } ' +
        '#dio_hall table td.laurel.gold { background: url("https://www.tuto-de-david1327.com/medias/images/laurel-sprite.png") no-repeat 75%; height:18px; width:18px; } ' +
        '#dio_hall table td.laurel.blue { background: url("https://www.tuto-de-david1327.com/medias/images/laurel-sprite.png") no-repeat 100%; height:18px; width:18px; } ' +
        "#tr_settings .radiobutton .disabled { color: #000000; } " +
        "</style>"
    ).appendTo("head");
  },
  openWindow: function () {
    var content =
      // Title tabs
      '<ul id="tr_settings_menu" class="menu_inner" style="top: -36px; right: 94px;">' +
      "</span></span></span></a></li>" +
      '<li><a class="submenu_link automation" href="#"><span class="left"><span class="right"><span class="middle">' +
      '<span class="tab_label">Automation Hub</span>' +
      "</span></span></span></a></li>" +
      '<li><a class="submenu_link architect" href="#"><span class="left"><span class="right"><span class="middle">' +
      '<span class="tab_label">Architect</span>' +
      "</span></span></span></a></li>" +
      '<li><a class="submenu_link philosopher" href="#"><span class="left"><span class="right"><span class="middle">' +
      '<span class="tab_label">Philosopher</span>' +
      "</span></span></span></a></li>" +
      '<li><a class="submenu_link merchant" href="#"><span class="left"><span class="right"><span class="middle">' +
      '<span class="tab_label">Merchant</span>' +
      "</span></span></span></a></li>" +
      '<li><a class="submenu_link captain active" href="#"><span class="left"><span class="right"><span class="middle">' +
      '<span class="tab_label">Captain</span>' +
      "</span></span></span></a></li>" +
      "</ul>" +
      // Content
      '<div id="tr_settings" style="margin-bottom:5px; font-style:italic;"><div class="box_content captain"></div></div>';

    window.Layout.wnd.Create(window.GPWindowMgr.TYPE_TR_SETTINGS).setContent(content);
    Settings.addContent();
    // Tooltips
    $(".tr_farmer").mousePopup(new MousePopup("Farm your Villages"));
    $(".tr_builder").mousePopup(new MousePopup("Fill your queues"));
    $(".tr_spy").mousePopup(new MousePopup("Fill your caves"));
    $(".tr_architect").mousePopup(new MousePopup("New Designs"));
    $(".tr_philosopher").mousePopup(new MousePopup("Culture"));
    $(".tr_merchant").mousePopup(new MousePopup("Trade"));
    $(".tr_captain").mousePopup(new MousePopup("Organize your units"));

    Settings.switchContent();
    // Close button event - uncheck available units button
    /*if (typeof window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON).getJQCloseButton() != 'undefined') {
      //window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_DIO_COMPARISON).getJQCloseButton().get(0).onclick = function () {
          $('#tr_settings_button').removeClass("checked");
          $('.ico_comparison').get(0).style.marginTop = "5px";
      //};
      };*/
  },
  closeWindow: function () {
    window.Layout.wnd.getOpenFirst(window.GPWindowMgr.TYPE_TR_SETTINGS).close();
  },
  switchContent: function () {
    $("#tr_settings_menu .merchant, #tr_settings_menu .automation, #tr_settings_menu .architect, #tr_settings_menu .philosopher, #tr_settings_menu .captain").click(function () {
      $("#tr_settings .box_content").removeClass($("#tr_settings .box_content")?.get(0).className.split(" ")[1]);
      //console.debug(this.className.split(" ")[1]);
      $("#tr_settings .box_content").addClass(this.className.split(" ")[1]);
      $("#tr_settings_menu .active").removeClass("active");
      $(this).addClass("active");
    });
  },
  addContent: function () {
    let tr_storage = new TrStorage();
    // Captain tab: DONE
    $(
      '<table class="hidden t_captain" cellpadding="1px"><tr>' +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/available-units.png" alt="available_units" /></td>' +
        '<td><div id="tr_ava" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Units overview</div></div><br>' +
        '<div id="tr_ava2" style="display:none; margin-top: 5px;" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">' +
        window.DM.getl10n("COMMON").ocean_number_tooltip +
        "</div></div>" +
        "<p>Counts the units of all cities</p>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/unites-envoyees.png" alt="sent_units" /></td>' +
        '<td><div id="tr_sen" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Sent units</div></div>' +
        "<p>Shows sent units in the attack/support window</p></td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/force-unitaire.png" alt="unit_strength" /></td>' +
        '<td><div id="tr_unitstrength" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Unit strength</div></div>' +
        "<p>Adds unit strength tables in various areas</p></td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/transport-capacity.png" alt="transport_capacity" /></td>' +
        '<td><div id="tr_tra" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">' +
        window.DM.getl10n("barracks").tooltips.ship_transport.title +
        "</div></div>" +
        "<p>Shows the occupied and available transport capacity in the unit menu</p></td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/comparaison-des-unites.png" alt="unit_comparison" /></td>' +
        '<td><div id="tr_com" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Unit Comparison</div></div>' +
        "<p>Adds unit comparison tables" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/simulateur.png" alt="simulator" style="max-width:100px !important;"/></td>' +
        '<td><div id="tr_newsimulator" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Simulator</div></div>' +
        "<p>Adaptation of the simulator layout & permanent display of the extended modifier box</p></td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/troop-speed.png" style="border: 1px solid rgb(158, 133, 78);" alt="troop_speed" /></td>' +
        '<td><div id="tr_way" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Troop speed</div></div>' +
        "<p>Displays improved troop speed in the attack/support window</p><br></td>" +
        "</tr><tr>" +
        "</tr></table>"
    ).appendTo("#tr_settings .box_content");

    // Architect tab: DONE
    $(
      '<table class="hidden t_architect" cellpadding="1px"><tr>' +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/barre-de-taches.png" alt="taskbar" style="max-width:100px !important;"/></td>' +
        '<td><div id="tr_tkb" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Taskbar</div></div>' +
        "<p>Increases the taskbar</p></td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/barre-de-taches.png" alt="taskbar" style="max-width:100px !important;"/></td>' +
        '<td><div id="tr_oceanNumbers" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Ocean Numbers Labels</div></div>' +
        "<p>Add Ocean Numbers Labels</p></td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/popup-de-faveur-1.png" alt="favor_popup" style="max-width:100px !important;"/></td>' +
        '<td><div id="tr_fvrpop" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Favor popup</div></div>' +
        "<p>Changes the favor popup</p></td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/mousewheel-zoom.png" alt="" /></td>' +
        '<td><div id="tr_mousewheel" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Mouse wheel</div></div>' +
        "<p>You can change the views with the mouse wheel</p><br></td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/icones-des-villes.gif" alt="townicons" style="transform: scale(1.25); margin-top: 10px;" /></td>' +
        '<td><div id="tr_twnicn" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Town icons</div></div>' +
        "<p>Each city receives an icon for the town type (automatic detection)</p>" +
        '<table class="icon_types_table">' +
        '<tr><td style="width:115px"><div class="tr_icon_small townicon_lo"></div>Land Offensive</td>' +
        '<td><div class="tr_icon_small townicon_fo"></div>Fly Offensive</td>' +
        '<td><div class="tr_icon_small townicon_so"></div>Navy Offensive</td>' +
        '<td><div class="tr_icon_small townicon_no"></div>Outside</td></tr>' +
        '<tr><td><div class="tr_icon_small townicon_ld"></div>Land Defensive</td>' +
        '<td><div class="tr_icon_small townicon_fd"></div>Fly Defensive' +
        '<td><div class="tr_icon_small townicon_sd"></div>Navy Defensive</td>' +
        '<td><div class="tr_icon_small townicon_po"></div>Empty</td></tr>' +
        "</table>" +
        "<p>Additional icons are available for manual selection:</p>" +
        '<div class="tr_icon_small townicon_sh"></div><div class="tr_icon_small townicon_di"></div><div class="tr_icon_small townicon_un"></div><div class="tr_icon_small townicon_ko"></div>' +
        '<div class="tr_icon_small townicon_ti"></div><div class="tr_icon_small townicon_gr"></div><div class="tr_icon_small townicon_dp"></div><div class="tr_icon_small townicon_re"></div>' +
        '<div class="tr_icon_small townicon_wd"></div><div class="tr_icon_small townicon_st"></div><div class="tr_icon_small townicon_si"></div><div class="tr_icon_small townicon_bu"></div>' +
        '<div class="tr_icon_small townicon_he"></div><div class="tr_icon_small townicon_ch"></div><div class="tr_icon_small townicon_bo"></div><div class="tr_icon_small townicon_fa"></div>' +
        '<div class="tr_icon_small townicon_wo"></div>' +
        "</td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/liste-de-ville-1.png" alt="townlist" style="border: 1px solid rgb(158, 133, 78);" /></td>' +
        '<td><div id="tr_til" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Town list</div></div>' +
        "<p>Adds the town icons to the town list</p></td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/map-1.png" alt="map" /></td>' +
        '<td><div id="tr_tim" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Map</div></div>' +
        '<div id="tr_tiw" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Icons Popup</div></div>' +
        "<p>Sets the town icons on the strategic map</p>" +
        '<table id="Town_Popup" width="60%"><tr>' +
        '<td width="20%"><div id="tr_tpt" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">' +
        window.DM.getl10n("context_menu").titles.units_info +
        "</div></div></td>" +
        '<td width="20%"><div id="tr_tis" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">' +
        window.DM.getl10n("context_menu").titles.support +
        "</div></div></td>" +
        '<td width="20%"><div id="tr_tih" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">' +
        window.DM.getl10n("heroes").collection.heroes +
        "</div></div></td>" +
        '<td width="20%"><div id="tr_tir" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Resources</div></div></td>' +
        "</tr></table></td>" +
        "</td></tr></table>"
    ).appendTo("#tr_settings .box_content");

    // Merchant tab: DONE
    $(
      '<table class="hidden t_merchant" cellpadding="1px"><tr>' +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/recruiting-trade.png" alt="recruiting_trade" /></td>' +
        '<td><div id="tr_rec" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Recruiting trade</div></div>' +
        '<p>Extends the trade window by a recruiting trade</p><br><img src="https://www.tuto-de-david1327.com/medias/images/commerce-de-pourcentage.png" style="border: 2px solid rgb(158, 133, 78); max-height:none; max-width:250px !important;" /></td>' +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/percentage-trade.png" alt="percentage_trade" /></td>' +
        '<td><div id="tr_per" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Percentual trade</div></div>' +
        "<p>Extends the trade window by a percentual trade</p><br></td>" +
        "</tr><tr>" +
        '<td><img src="https://www.tuto-de-david1327.com/medias/images/towntradeimprovement.jpg" alt="" style="border: 2px solid rgb(158, 133, 78);"/></td>' +
        '<td><div id="tr_tti" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Trade resources for festivals</div></div>' +
        "<p>Click on it and it is only exchanged towards a festival.</p><br></br></td>" +
        "</tr><tr>" +
        "</tr></table>"
    ).appendTo("#tr_settings .box_content");

    // Philosopher tab: DONE
    $(
      '<table class="hidden t_philosopher" cellpadding="1px"><tr>' +
        '<td><img src="" alt="" /></td>' +
        '<td><div id="tr_clt" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Culture Overview</div></div>' +
        "<p>Enable Culture overview of all cities.</p></td>" +
        "</tr><tr>" +
        '<td><img src="" alt="" /></td>' +
        '<td><div id="tr_sci" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Science Overview</div></div>' +
        "<p>Enable Science overview of all cities.</p><br></br></td>" +
        "</tr><tr>" +
        "</tr></table>"
    ).appendTo("#tr_settings .box_content");

    // Automation tab
    $(
      '<table class="hidden t_automation" cellpadding="1px">' +
        '<tr><td><div class="game_header bold">Builder</div></td></tr>' +
        "<tr><td>" +
        '<div id="tr_autobuild_strt" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Start Building on startup</div></div>' +
        "</tr></td>" +
        "<tr><td>" +
        '<div id="placeholder_autobuild_timer"></div>' +
        "</tr></td>" +
        "<tr><td>" +
        '<div id="builder" style="display: flex;flex-direction: row;justify-content: space-between;align-items: center;">' +
        '<div id="tr_autobuild_building" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Enable building queue</div></div>' +
        '<div id="tr_autobuild_barracks" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Enable barracks queue</div></div>' +
        '<div id="tr_autobuild_harbor" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Enable harbor queue</div></div>' +
        "</div>" +
        "</tr></td>" +
        '<tr><td><div class="game_header bold">Farmer</div></td></tr>' +
        "<tr><td>" +
        '<div id="tr_farmin" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Auto Farm</div></div>' +
        "</td></tr>" +
        "<tr><td>" +
        '<div id="placeholder_farmin_timer"></div>' +
        "</td></tr>" +
        "<tr><td>" +
        '<div id="placeholder_farmin_wait_timer"></div>' +
        "</td></tr>" +
        "<tr><td>" +
        '<div id="tr_stopfarmin" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Stop after warehouse is full</div></div>' +
        "</td></tr>" +
        '<tr><td><div class="game_header bold">Spy</div></td></tr>' +
        "<tr><td>" +
        '<div id="tr_cave" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Auto Cave</div></div>' +
        "</td></tr>" +
        "<tr><td>" +
        '<div id="placeholder_cave_percentage"></div>' +
        "</td></tr>" +
        "<tr><td>" +
        '<div id="placeholder_cave_store_options"></div>' +
        "</td></tr>" +
        '<tr><td><div class="game_header bold">Commander</div></td></tr>' +
        "<tr><td>" +
        '<div id="spells" style="display: flex;flex-direction: row;justify-content: space-between;align-items: center;">' +
        '<div id="tr_auto_militia" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Auto Militia</div></div>' +
        '<div id="tr_auto_dodger" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Auto Dodger</div></div>' +
        "</div>" +
        "</td></tr>" +
        '<tr><td><div class="game_header bold">Priestess</div></td></tr>' +
        "<tr><td>" +
        '<div id="tr_auto_spells" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Auto Spells</div></div>' +
        "</td></tr>" +
        "<tr><td>" +
        '<div id="spells" style="display: flex;flex-direction: row;justify-content: space-between;align-items: center;">' +
        '<div class="cbx_caption" style="font-weight:bold;">Purification</div>' +
        '<div class="cbx_caption" style="font-weight:bold;">&gt;</div>' +
        '<div id="placeholder_custom_spell"></div>' +
        '<div class="cbx_caption" style="font-weight:bold;">&gt;</div>' +
        '<div class="cbx_caption" style="font-weight:bold;">City Protection</div>' +
        "</div>" +
        "</tr></td>" +
        '<tr><td><div class="game_header bold">Merchant</div></td></tr>' +
        "<tr><td>" +
        '<div id="tr_balance_recs_cities" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Auto Balance Resources within cities</div></div>' +
        "</td></tr>" +
        "<tr><td>" +
        '<div id="tr_balance_recs_villages" class="checkbox_new"><div class="cbx_icon"></div><div class="cbx_caption">Auto Balance Resources with villages</div></div>' +
        "</td></tr>" +
        "</table>"
    ).appendTo("#tr_settings .box_content");

    $(
      UI.selectBox({
        id: "tr_autobuild_timer",
        label: "Check every: ",
        options: autobuild_timer
      })
    ).appendTo("#placeholder_autobuild_timer");

    $("#tr_autobuild_timer").val(window.tr.settings.tr_autobuild_timer);

    $("#tr_autobuild_timer").on("change", function () {
      // @ts-ignore
      window.tr.settings[this.id] = parseInt($("#tr_autobuild_timer").val());
      tr_storage.save("settings", window.tr.settings);
    });

    // Farmer
    $(
      UI.selectBox({
        id: "tr_farmin_timer",
        label: "Farm every:",
        options: sort_options
      })
    ).appendTo("#placeholder_farmin_timer");

    $(
      UI.selectBox({
        id: "tr_farmin_wait_timer",
        label: "Pause Time between each village ",
        options: wait_options
      })
    ).appendTo("#placeholder_farmin_wait_timer");

    $("#tr_farmin_timer").val(window.tr.settings.tr_farmin_timer);
    $("#tr_farmin_wait_timer").val(window.tr.settings.tr_farmin_wait_timer);

    $("#tr_farmin_timer").on("change", function () {
      // @ts-ignore
      window.tr.settings[this.id] = parseInt($("#tr_farmin_timer").val());
      tr_storage.save("settings", window.tr.settings);
    });

    $("#tr_farmin_wait_timer").on("change", function () {
      // @ts-ignore
      window.tr.settings[this.id] = parseInt($("#tr_farmin_wait_timer").val());
      tr_storage.save("settings", window.tr.settings);
    });

    $(
      UI.selectBox({
        id: "tr_cave_percentage",
        label: "Fill cave when warehouse is",
        options: cave_warehouse_options
      })
    ).appendTo("#placeholder_cave_percentage");

    $(
      UI.selectBox({
        id: "tr_cave_store_options",
        label: "Silver to store",
        options: cave_store_options
      })
    ).appendTo("#placeholder_cave_store_options");

    $("#tr_cave_percentage").val(window.tr.settings.tr_cave_percentage);
    $("#tr_cave_store_percentage").val(window.tr.settings.tr_cave_store_percentage);

    $("#tr_cave_percentage").on("change", function () {
      // @ts-ignore
      window.tr.settings[this.id] = parseInt($("#tr_cave_percentage").val());
      tr_storage.save("settings", window.tr.settings);
    });

    $("#tr_cave_store_percentage").on("change", function () {
      // @ts-ignore
      window.tr.settings[this.id] = parseInt($("#tr_cave_store_percentage").val());
      tr_storage.save("settings", window.tr.settings);
    });

    $("#tr_auto_militia").val(window.tr.settings.tr_auto_militia);
    $("#tr_auto_dodger").val(window.tr.settings.tr_auto_dodger);
    $("#tr_auto_spells").val(window.tr.settings.tr_auto_spells);
    $("#tr_balance_recs_cities").val(window.tr.settings.tr_balance_recs_cities);
    $("#tr_balance_recs_villages").val(window.tr.settings.tr_balance_recs_villages);
    $(
      UI.selectBox({
        id: "tr_custom_spell",
        options: sort_options
      })
    ).appendTo("#placeholder_custom_spell");

    $("#tr_settings .checkbox_new").click(function () {
      // @ts-ignore
      if ($(this).hasClass("checked")) {
        // @ts-ignore
        Settings.toggleActivation(this.id);

        // @ts-ignore
        window.tr.settings[this.id] = false;
        tr_storage.save("settings", window.tr.settings);

        // @ts-ignore
        $(this).removeClass("checked");
      } else {
        // @ts-ignore
        Settings.toggleActivation(this.id);

        // @ts-ignore
        window.tr.settings[this.id] = true;
        tr_storage.save("settings", window.tr.settings);

        // @ts-ignore
        $(this).toggleClass("checked");
      }
    });

    for (var e in window.tr.settings) {
      console.log(e);
      if (window.tr.settings.hasOwnProperty(e)) {
        if (window.tr.settings[e] == true) {
          console.log("true");
          $("#" + e).addClass("checked");
        } else {
          $("#" + e).addClass("unchecked");
        }
      }
    }
  },
  toggleActivation: function (opt: string | number) {
    var FEATURE,
      activation = true;
    switch (opt) {
      case "tr_farmin":
        FEATURE = Farming;
        break;
      case "tr_cave":
        FEATURE = Caving;
        break;
      case "tr_ava":
        FEATURE = AvailableUnits;
        break;
      case "tr_ava2":
        FEATURE = AvailableUnits.ocean;
        break;
      case "tr_sen":
        FEATURE = SentUnits;
        break;
      case "tr_unitstrength":
        FEATURE = UnitStrength.Menu;
        break;
      case "tr_way":
        FEATURE = ShortDuration;
        break;
      case "tr_com":
        FEATURE = UnitComparison;
        break;
      case "tr_tra":
        FEATURE = TransportCapacity;
        break;
      case "tr_newsimulator":
        FEATURE = Simulator;
        break;
      case "tr_twnicn":
        FEATURE = TownIcons;
        break;
      case "tr_tiw":
        FEATURE = TownPopup;
        break;
      case "tr_tim":
        FEATURE = MapWorld;
        break;
      case "tr_til":
        FEATURE = TownList;
        break;
      case "tr_tkb":
        FEATURE = Taskbar;
        break;
      case "tr_mousewheel":
        FEATURE = MouseWheelZoom;
        break;
      case "tr_fvrpop":
        FEATURE = FavorPopup;
        break;
      case "dio_wwi":
        FEATURE = WorldWonderIcons;
        break;
      case "tr_oceanNumbers":
        FEATURE = OceanNumbers;
        break;
      case "tr_clt":
        FEATURE = Culture;
        break;
      case "tr_sci":
        FEATURE = Science;
        break;
      case "tr_rec":
        FEATURE = RecruitingTrade;
        break;

      default:
        activation = false;
        break;
    }
    if (activation) {
      if (window.tr.settings[opt]) {
        FEATURE.deactivate();
      } else {
        FEATURE.activate();
      }
    }
  }
};

export default Settings;
