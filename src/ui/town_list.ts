var TownList = {
  activate: function () {
    // Style town list
    $(
      '<style id="tr_town_list" type="text/css">' +
        "#town_groups_list .item { text-align: left; } " +
        "#town_groups_list .inner_column { border: 1px solid rgba(100, 100, 0, 0.3);margin: -2px 0px 0px 2px; } " +
        "#town_groups_list .island_quest_icon { position: absolute; right: 37px; top: 3px; } " +
        "#town_groups_list .island_quest_icon.hidden_icon { display:none; } " +
        // Quacks Zentrier-Button verschieben
        "#town_groups_list .jump_town { right: 36px !important; } " +
        // Population percentage
        "#town_groups_list .pop_percent { position: absolute; right: 6px; top:0px; font-size: 0.7em; display:block !important;} " +
        "#town_groups_list .full { color: green; } " +
        "#town_groups_list .threequarter { color: darkgoldenrod; } " +
        "#town_groups_list .half { color: darkred; } " +
        "#town_groups_list .quarter { color: red; } " +
        "#town_groups_list .tr_icon_small { display:block !important;} " +
        ".tr_icon_small { position:relative; right:-2px; height:20px; width:25px; } " +
        "</style>"
    ).appendTo("head");

    // Open town list: hook to grepolis function render()
    var i = 0;

    while (window.layout_main_controller.sub_controllers[i].name != "town_name_area") {
      i++;
    }

    window.layout_main_controller.sub_controllers[i].controller.town_groups_list_view.render_old = window.layout_main_controller.sub_controllers[i].controller.town_groups_list_view.render;

    window.layout_main_controller.sub_controllers[i].controller.town_groups_list_view.render = function () {
      window.layout_main_controller.sub_controllers[i].controller.town_groups_list_view.render_old();
      TownList.change();
    };

    // Town List open?
    if ($("#town_groups_list").get(0)) {
      TownList.change();
    }
  },
  deactivate: function () {
    var i = 0;

    while (window.layout_main_controller.sub_controllers[i].name != "town_name_area") {
      i++;
    }

    window.layout_main_controller.sub_controllers[i].controller.town_groups_list_view.render = window.layout_main_controller.sub_controllers[i].controller.town_groups_list_view.render_old;

    $("#tr_town_list").remove();
    $("#town_groups_list .small_icon, #town_groups_list .pop_percent").css({
      display: "none"
    });
    $("#town_groups_list .town_group_town").unbind("mouseenter mouseleave");
  },
  change: function () {
    if (!$("#town_groups_list .tr_icon_small").get(0) && !$("#town_groups_list .pop_percent").get(0)) {
      $("#town_groups_list .town_group_town").each(function () {
        try {
          // @ts-ignore
          var town_item = $(this),
            town_id = town_item.attr("name"),
            townicon_div,
            percent_div = "",
            percent = -1,
            pop_space = "full";

          if (window.tr.population[town_id]) {
            percent = window.tr.population[town_id].percent;
          }
          if (percent < 75) {
            pop_space = "threequarter";
          }
          if (percent < 50) {
            pop_space = "half";
          }
          if (percent < 25) {
            pop_space = "quarter";
          }

          if (!town_item.find("tr_icon_small").length) {
            townicon_div = '<div class="tr_icon_small townicon_' + (window.tr.manuTownTypes[town_id] || window.tr.autoTownTypes[town_id] || "no") + '"></div>';
            // TODO: Notlösung...
            if (percent != -1) {
              percent_div = '<div class="pop_percent ' + pop_space + '">' + percent + "%</div>";
            }
            town_item.prepend(townicon_div + percent_div);
          }
        } catch (error) {
          console.error(error, "TownList.change");
        }
      });
    }

    // Hover Effect for Quacks Tool:
    $("#town_groups_list .town_group_town").hover(
      function () {
        // @ts-ignore
        $(this).find(".island_quest_icon").addClass("hidden_icon");
      },
      function () {
        // @ts-ignore
        $(this).find(".island_quest_icon").removeClass("hidden_icon");
      }
    );

    // Add change town list event handler
    //$.Observer(window.GameEvents.town.town_switch).subscribe('TR_SWITCH_TOWN', function () {
    //TownList.change();
    //});
  }
};

export default TownList;
