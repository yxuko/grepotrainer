/*******************************************************************************************************************************
 * Favor Popup
 * ----------------------------------------------------------------------------------------------------------------------------
 * | ● Improved favor popup
 * ----------------------------------------------------------------------------------------------------------------------------
 *******************************************************************************************************************************/
var FavorPopup = {
  godArray: {
    zeus: "zeus",
    athena: "athena",
    poseidon: "poseidon",
    hera: "hera",
    hades: "hades",
    artemis: "artemis",
    aphrodite: "aphrodite",
    ares: "ares"
  },
  activate: function () {
    $(".gods_area").bind("mouseover", function () {
      FavorPopup.setFavorPopup();
    });
    //Before update 2.231
    /*$('.gods_favor_button_area, #favor_circular_progress').bind('mouseover mouseout', function () {
            return false;
        });*/
    //Update 2.231
    $('<div id="dio_FavorPopup" style="width: 100px; height: 33px; position: absolute; left: 35px; top: 2px;"></div>').appendTo(".gods_favor_amount");
    $('<div id="dio_FuryPopup" class="fury_amount" style="width: 67px; height: 33px; position: absolute; right: 50px; top: 2px;"></div>').appendTo(".gods_favor_amount");
    //$('<div id="dio_FuryPopup" class="fury_amount" style="width: 57px; height: 33px; position: fixed; right: 70px; top: 202px;"></div>').appendTo('.gods_favor_amount');
  },
  deactivate: function () {
    $(".gods_area").unbind("mouseover");
    //Before update 2.231
    //$('.gods_favor_button_area, #favor_circular_progress').unbind('mouseover mouseout');
    //Update 2.231
    $("#dio_FavorPopup").remove();
    $("#dio_FuryPopup").remove();
    $("#dio-amount").remove();
  },
  setFavorPopup: function () {
    var pic_row = "",
      fav_row = "",
      prod_row = "",
      tooltip_str,
      textColor,
      tooltip_fury;
    for (var g in FavorPopup.godArray) {
      if (FavorPopup.godArray.hasOwnProperty(g)) {
        if (window.ITowns.player_gods.attributes.temples_for_gods[g]) {
          pic_row += '<td><div style="transform:scale(0.8); margin: -6px;"; class="god_mini ' + [g] + '";></td>';

          textColor = window.ITowns.player_gods.attributes[g + "_favor"] == window.ITowns.player_gods.attributes.max_favor ? (textColor = "color:red;") : (textColor = "color:blue");

          fav_row += '<td class="bold" style="' + textColor + '">' + window.ITowns.player_gods.attributes[g + "_favor"] + "</td>";

          prod_row += '<td class="bold">' + window.ITowns.player_gods.attributes.production_overview[g].production + "</td>";
        }
      }
    }
    var fury_row = "";

    var fury_max = window.ITowns.player_gods.attributes.max_fury;

    textColor = window.ITowns.player_gods.attributes.fury == fury_max ? (textColor = "color:red;") : (textColor = "color:blue");

    fury_row = '<td class="bold" style="' + textColor + '">' + window.ITowns.player_gods.attributes.fury + "/" + fury_max + "</td>";
    tooltip_str = $('<div id"tooltip"><table><tr><td><div class="dio_icon b" style="opacity: 0.30; width: 35px; height: 35px;"></td>' + pic_row + "</tr>" + '<tr align="center"><td><img src="https://gpall.innogamescdn.com/images/game/res/favor.png"></td>' + fav_row + "</tr>" + '<tr align="center"><td>+</td>' + prod_row + "</tr>" + "</table></div>");
    tooltip_fury = $('<div id"tooltip"><table><tr align="center"><td><img src="https://www.tuto-de-david1327.com/medias/images/fury.png"></td>' + fury_row + "</tr>" + "</table></div>");
    //Before update 2.231
    //$('.gods_favor_button_area, #favor_circular_progress').mousePopup(new MousePopup(tooltip_str));
    //Update 2.231

    $("#dio_FavorPopup").mousePopup(new MousePopup(tooltip_str));
    //if (Game.gods_active.ares) {

    $("#dio_FuryPopup").mousePopup(new MousePopup(tooltip_fury));
    //}
  }
};

export default FavorPopup;
