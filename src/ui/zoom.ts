/*******************************************************************************************************************************
 * Mouse Wheel Feature
 *******************************************************************************************************************************/

var MouseWheelZoom = {
  activate: function () {
    // Agora

    $(
      '<style id="tr_Agora_style">#tr_Agora {position: absolute; width: 144px; height: 26px; left: 1px; z-index: 5;}</style>'
    ).appendTo("head");

    $('<a id="tr_Agora"></a>').appendTo(".nui_battlepoints_container");

    $("#tr_Agora").on("click", function () {
      // @ts-ignore
      window.PlaceWindowFactory.openPlaceWindow();
    });

    // Scroll trough the views
    $(
      "#main_area, #tr_political_map, .viewport, .sjs-city-overview-viewport"
    ).bind("mousewheel", function (e) {
      e.stopPropagation();

      var current = $(".bull_eye_buttons .checked")
          ?.get(0)
          ?.getAttribute("name"),
        delta = 0,
        scroll,
        sub_scroll = 6;

      switch (current) {
        case "strategic_map":
          scroll = 3;
          break;
        case "island_view":
          scroll = 2;
          break;
        case "city_overview":
          scroll = 1;
          break;
      }
      // @ts-ignore
      delta = -e.originalEvent.detail || e.originalEvent.wheelDelta; // Firefox || Chrome & Opera

      //console.debug("cursor_pos", e.pageX, e.pageY);

      if (scroll !== 4) {
        if (delta < 0) {
          // @ts-ignore
          scroll += 1;
        } else {
          // @ts-ignore
          scroll -= 1;
        }
      } else {
        // Zoomstufen bei der Politischen Karte
        // @ts-ignore
        sub_scroll = $(".zoom_select")?.get(0)?.selectedIndex;

        if (delta < 0) {
          sub_scroll -= 1;
        } else {
          sub_scroll += 1;
        }
        if (sub_scroll === -1) {
          sub_scroll = 0;
        }
        if (sub_scroll === 7) {
          scroll = 3;
        }
      }
      switch (scroll) {
        case 3:
          $(".bull_eye_buttons .strategic_map").trigger("click");
          $("#popup_div").css("display", "none");
          break;
        case 2:
          $(".bull_eye_buttons .island_view").trigger("click");
          //TownPopup.remove();
          break;
        case 1:
          $(".bull_eye_buttons .city_overview").trigger("click");
          break;
      }

      // Prevent page from scrolling
      return false;
    });
  },

  deactivate: function () {
    $("#main_area, .ui_city_overview").unbind("mousewheel");

    $("#tr_Agora").remove();

    $("#tr_Agora_style").remove();
  }
};

export default MouseWheelZoom;
