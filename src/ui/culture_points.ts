/*******************************************************************************************************************************
 * | ● culturePoints :
 *******************************************************************************************************************************/

var culturePoints: CulturePointsType = {
  timeout: null,
  activate: () => {
    if ($("#culture_points_overview_bottom").length || $("#place_container").length) {
      culturePoints.add();
    }

    culturePoints.timeout = setInterval(() => {
      if ($("#culture_points_overview_bottom").length || $("#place_container").length) {
        culturePoints.add();
      }
    }, 50000); //0.
  },
  add: () => {
    try {
      let f, i, j, k, g, h;
      //let test = false;
      let test = 100;
      if ($("#culture_points_overview_bottom").length) {
        g = $("ul#culture_overview_towns span.eta");
        h = $("#culture_points_overview_bottom #place_culture_count").text();
        i = h.split("/");
        j = parseInt(i[0]) + g.length;
        k = parseInt(i[1]) - j;
        //console.log(k);
        if (test != 100) {
          k = k - test;
        }
        if (h.indexOf("[") < 1) {
          if (k > 0) {
            $("#culture_points_overview_bottom #place_culture_count").append(" <span id='dio_culture'>[-" + k + "]</span><span id='dio_cultureplus' style='color: #ECB44D'></span>");
          } else {
            $("#culture_points_overview_bottom #place_culture_count")
              .append(" [<span id='dio_culture'></span>]<span id='dio_cultureplus' style='color: #ECB44D'> +" + k * -1 + "</span>")
              .find("span#dio_culture")
              .countdown(culturePoints.heure(g, h, test));
          }
        } else {
          if (k > 0) {
            $("#dio_culture").text("[-" + k + "]");
            $("#dio_cultureplus").text("");
          } else {
            $("#dio_culture").countdown(culturePoints.heure(g, h, test));
            $("#dio_cultureplus").text(" +" + k * -1);
          }
        }
        h = $("#place_battle_points .points_background .points_count").text();
        i = h.split("/");
        j = parseInt(i[0]);
        k = parseInt(i[1]) - j;
        if (k > 0) {
          if (h.indexOf("[") < 1) {
            $("#place_battle_points .points_background .points_count").append(" <span id='dio_battle_points'>[-" + k + "]</span>");
          } else {
            $("#dio_battle_points").text("[-" + k + "]");
          }
        }
      }
      if ($("#place_container").length) {
        let h = $("#place_container #place_culture_count").text();
        let g = 0;

        let inProgress = parseInt(
          $("#place_culture_in_progress")
            .text()
            .match(/[0-9]+/)
        );
        if (inProgress > 0) {
          g = inProgress;
        }
        i = h.split("/");
        j = parseInt(i[0]) + g;
        k = parseInt(i[1]) - j;
        if (test != 100) {
          k = k - test;
        }
        if (h.indexOf("[") < 1) {
          if (k > 0) {
            $("#place_container #place_culture_count").append("<span id='dio_cultureA'>[-" + k + "]</span>");
          } else {
            $("#place_container #place_culture_count").append("<span id='dio_cultureplusA' style='color: #ECB44D'> [+" + k * -1 + "]</span>");
          }
        } else {
          if (k > 0) {
            $("#dio_cultureA").text("[-" + k + "]");
          } else {
            $("#dio_cultureplusA").text(" [+" + k * -1 + "]");
          }
        }
      }

      $("#dio_culture, #dio_cultureplus, #dio_cultureA, #dio_cultureplusA, #dio_battle_points, #dio_battle_pointsA").mousePopup(new MousePopup('<div class="dio_icon b"></div>'));
    } catch (error) {
      console.error(error, "culturePoints (add)");
    }
  },
  heure: (g: string | any[], h: string, test: number) => {
    try {
      var f, i, j, k;
      i = h.split("/");
      j = parseInt(i[0]) + g.length;
      k = parseInt(i[1]) - j;
      if (test != 100) {
        k = k - test;
      }
      if (k <= 0) {
        var l = new Array();
        for (f = 0; f < g.length; f++) {
          l.push(g[f].dataset.timestamp);
        }
        l.sort();
        var m = l[l.length + k - 1];
      } else {
        m = "";
        culturePoints.add();
      }
    } catch (error) {
      console.error(error, "culturePoints (heure)");
      m = "";
    }
    return m;
  },
  deactivate: () => {
    $("#dio_culture").remove();
    $("#dio_cultureplus").remove();
    $("#dio_cultureA").remove();
    $("#dio_cultureplusA").remove();

    clearTimeout(culturePoints.timeout);
    culturePoints.timeout = null;
  }
};

export default culturePoints;
