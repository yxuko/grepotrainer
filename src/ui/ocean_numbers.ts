/*******************************************************************************************************************************
 * Ocean Number
 *******************************************************************************************************************************/

var OceanNumbers = {
  activate: function () {
    if ($("div#map_oceanNumbers").length == 0) {
      $("#map_move_container").append(
        $("<div/>", {
          id: "map_oceanNumbers",
          style: "position:absolute; top:0; left:0;"
        })
      );
      // @ts-ignore
      var _mapSize = (__non_webpack_require__("map/helpers") || window.MapTiles).map2Pixel(100, 100);
      //MapTiles.map2Pixel(100,100);

      $("head").append(
        $("<style/>")
          .append(
            ".RepConvON {border: 1px solid #fff; position: absolute; display: block; z-index: 2; opacity: .1;width: " +
              _mapSize.x +
              "px; height: " +
              _mapSize.x +
              "px;}" +
              "#RepConvSpanPrev .outer_border {border: 2px solid black; font-size: 95%;}"
          )
          .append(
            "#ui_box.tr_ui_box .nui_units_box{ top:244px;}" +
              "#ui_box.tr_ui_box .nui_right_box #tr_pl{ " +
              " top:156px; " +
              //' background-position-y: -129px;'+
              " height: 30px;" +
              " line-height: 24px;" +
              " font-weight: 700;" +
              " color: rgb(255, 204, 102);" +
              " font-size: 11px;" +
              " text-align: center;" +
              //' width: 145px;'+
              "}" +
              "#ui_box.tr_ui_box.city-overview-enabled .nui_units_box{ top:223px !important;}" +
              "#ui_box.tr_ui_box.city-overview-enabled .nui_right_box #tr_pl{ top:135px;}"
          )
          // wygląd wyspy
          .append(
            "div.island_info_wrapper div.center1 {" +
              "top: 0px;" +
              "width: 435px;" +
              "float: left;" +
              "left: 270px;" +
              "}" +
              "div.island_info_towns {" +
              "margin-top: 25px;" +
              "}" +
              "div.island_info_wrapper .island_info_left .game_list {" +
              "height: 340px !important;" +
              "}" +
              "div.island_info_wrapper .island_info_right .game_list {" +
              "height: 370px;" +
              "}" +
              "#farm_town_overview_btn {" +
              "top: 75px !important" +
              "}"
          )
      );

      for (var _mapY = 0; _mapY < 10; _mapY++) {
        for (var _mapX = 0; _mapX < 10; _mapX++) {
          // @ts-ignore
          var _lt = (__non_webpack_require__("map/helpers") || window.MapTiles).map2Pixel(_mapX * 100, _mapY * 100);
          //MapTiles.map2Pixel(_mapX*100,_mapY*100);
          $("div#map_oceanNumbers").append(
            $("<div/>", {
              class: "RepConvON",
              style:
                "left:" +
                _lt.x +
                "px; top: " +
                _lt.y +
                "px; background-image: url(https://cdn.grcrt.net/map/" +
                _mapX +
                _mapY +
                ".png);"
            })
          );
        }
      }
    }
  },

  deactivate: function () {
    $("div#map_oceanNumbers").remove();
  }
};

export default OceanNumbers;