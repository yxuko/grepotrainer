/*******************************************************************************************************************************
 * world Wonder Icons
 *******************************************************************************************************************************/

var worldWonderIcon = {
  colossus_of_rhodes:
    "url(https://gpall.innogamescdn.com/images/game/map/wonder_colossus_of_rhodes.png) 38px -1px;",
  great_pyramid_of_giza:
    "url(https://gpall.innogamescdn.com/images/game/map/wonder_great_pyramid_of_giza.png) 34px -6px;",
  hanging_gardens_of_babylon:
    "url(https://gpall.innogamescdn.com/images/game/map/wonder_hanging_gardens_of_babylon.png) 34px -5px;",
  lighthouse_of_alexandria:
    "url(https://gpall.innogamescdn.com/images/game/map/wonder_lighthouse_of_alexandria.png) 37px -1px;",
  mausoleum_of_halicarnassus:
    "url(https://gpall.innogamescdn.com/images/game/map/wonder_mausoleum_of_halicarnassus.png) 37px -4px;",
  statue_of_zeus_at_olympia:
    "url(https://gpall.innogamescdn.com/images/game/map/wonder_statue_of_zeus_at_olympia.png) 36px -3px;",
  temple_of_artemis_at_ephesus:
    "url(https://gpall.innogamescdn.com/images/game/map/wonder_temple_of_artemis_at_ephesus.png) 34px -5px;"
};

var WorldWonderIcons = {
  activate: function () {
    try {
      if (!$("#tr_wondericons").get(0)) {
        var color = "orange";

        // style for world wonder icons
        var style_str = "<style id='tr_wondericons' type='text/css'>";
        // @ts-ignore
        for (var ww_type in wonder.map) {
          // @ts-ignore
          if (wonder.map.hasOwnProperty(ww_type)) {
            // @ts-ignore
            for (var ww in wonder.map[ww_type]) {
              // @ts-ignore
              if (wonder.map[ww_type].hasOwnProperty(ww)) {
                /*
                               if(wonder.map[ww_type][ww] !== AID){
                               color = "rgb(192, 109, 54)";
                               } else {
                               color = "orange";
                               }
                               */
                style_str +=
                  "#mini_i" +
                  ww +
                  ":before {" +
                  "content: '';" +
                  "background:" +
                  color +
                  " " +
                  // @ts-ignore
                  worldWonderIcon[ww_type] +
                  "background-size: auto 97%;" +
                  "padding: 8px 16px;" +
                  "top: 50px;" +
                  "position: relative;" +
                  "border-radius: 40px;" +
                  "z-index: 200;" +
                  "cursor: pointer;" +
                  "box-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5);" +
                  "border: 2px solid green; } " +
                  "#mini_i" +
                  ww +
                  ":hover:before { z-index: 201; " +
                  "filter: url(#Brightness12);" +
                  "-webkit-filter: brightness(1.2); } ";
              }
            }
          }
        }
        $(style_str + "</style>").appendTo("head");

        // Context menu on mouseclick
        $("#minimap_islands_layer").on("click", ".m_island", function (e) {
          var ww_coords = this.id.split("i")[3].split("_");
          // @ts-ignore
          window.Layout.contextMenu(e, "wonder", {
            ix: ww_coords[0],
            iy: ww_coords[1]
          });
        });
      }
    } catch (error) {
      console.log(error);
    }
  },
  deactivate: function () {
    $("#tr_wondericons").remove();
  }
};

export default WorldWonderIcons;