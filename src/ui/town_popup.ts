import HelperFunctions from "../utils/helpers";

var TownPopup = {
  activate: function () {
    $(
      '<style id="tr_town_popup_style" type="text/css">' +
        "#Town_Popup { display:block!important;} " +
        "#tr_town_popup { position:absolute; z-index:6003; max-width: 173px;} " +
        "#tr_town_popup .title { margin:5px;font-weight: bold; margin-right: 15px;} " +
        "#tr_town_popup .tr_branding { position:absolute; right:12px; top:8px; height: 20px; filter:sepia(1); -webkit-filter:sepia(1); opacity:0.5; } " +
        "#tr_town_popup .unit_content, " +
        "#tr_town_popup .move_counter_content, " +
        "#tr_town_popup .spy_content, " +
        "#tr_town_popup .god_content, " +
        "#tr_town_popup .hero_content, " +
        "#tr_town_popup .resources_content { background-color: #ffe2a1; border: 1px solid #e1af55; margin-top:2px; padding: 4px; font-family: Arial;font-weight: 700;font-size: 0.8em; } " +
        "#tr_town_popup .resources_content { text-align: right; margin-top:3px; } " +
        "#tr_town_popup .resources_content table { min-width:95% } " +
        "#tr_town_popup .footer_content { margin-top:3px;  } " +
        "#tr_town_popup .footer_content table { width:100%; } " +
        "#tr_town_popup .spy_content { height:25px; margin-right:3px; } " +
        "#tr_town_popup .god_content { width:25px; } " +
        "#tr_town_popup .hero_content { width:25px; } " +
        "#tr_town_popup .god_mini { transform:scale(0.4); margin: -19px; } " +
        "#tr_town_popup .count { position: absolute; bottom: -2px; right: 2px; font-size: 10px; font-family: Verdana,Arial,Helvetica,sans-serif; } " +
        "#tr_town_popup .four_digit_number .count { font-size:8px !important; } " +
        "#tr_town_popup .unit_icon25x25 { border: 1px solid #6e4b0b; margin: 1px; } " +
        "#tr_town_popup .wall { width:25px; height:25px; background-image:url(https://gpde.innogamescdn.com/images/game/main/wall.png); border: 1px solid #6e4b0b; margin: 1px; display: inline-block; vertical-align: middle; background-size: 100%; } " +
        // Spy Icon
        "#tr_town_popup .support_filter { margin: 0px 4px 0px 0px; float:left; } " +
        "#tr_town_popup .spy_text { line-height: 2.3em; float:left; } " +
        // fury Icon
        "#tr_town_popup .fury_icon { width:22px; height:15px; background: url(https://www.tuto-de-david1327.com/medias/images/fury.png) no-repeat; margin-left:2px; display: inline-block; vertical-align: middle; background-size: 75%;} " +
        // support Icon
        "#tr_town_popup .support_icon { width:16px; height:16px; background: url(https://www.tuto-de-david1327.com/medias/images/support-16px.png); margin: 1px; margin-bottom: 2px; display: inline-block; vertical-align: middle; background-size: 100%;} " +
        // attack Icon
        "#tr_town_popup .attack_icon { width:16px; height:16px; background: url(https://www.tuto-de-david1327.com/medias/images/attack-16px.png); margin: 1px; margin-bottom: 2px; display: inline-block; vertical-align: middle; background-size: 100%;} " +
        // Bei langen Stadtnamen wird sonst der Rand abgeschnitten:
        "#tr_town_popup .popup_middle_right { min-width: 11px; } " +
        // Mouseover Effect
        ".own_town .flagpole:hover, .m_town:hover { z-index: 101 !important; filter: brightness(1.2); -webkit-filter: brightness(1.2); font-size: 2em; margin-top: -1px; } " +
        ".own_town .flagpole, #main_area .m_town { cursor: pointer; } " +
        // Context menu on mouse click
        "#minimap_islands_layer .m_town { z-index: 99; cursor: pointer; } " +
        "</style>"
    ).appendTo("head");
    // }// Town Popups on Strategic map
    $("#minimap_islands_layer").off("mouseout", ".m_town");
    $("#minimap_islands_layer").on("mouseout", ".m_town", function () {
      TownPopup.remove();
    });
    $("#minimap_islands_layer").off("mouseover", ".m_town");
    $("#minimap_islands_layer").on("mouseover", ".m_town", function () {
      TownPopup.add(this);
    });

    // Town Popups on island view
    $("#map_towns").off("mouseout", ".own_town .flagpole");
    $("#map_towns").on("mouseout", ".own_town .flagpole", function () {
      TownPopup.remove();
    });
    $("#map_towns").off("mouseover", ".own_town .flagpole");
    $("#map_towns").on("mouseover", ".own_town .flagpole", function () {
      TownPopup.add(this);
    }); //}
  },
  deactivate: function () {
    $("#tr_town_popup_style").remove();
    // Events entfernen
    $("#minimap_islands_layer").off("click", ".m_town");
    $("#minimap_islands_layer").off("mousedown");

    $("#minimap_islands_layer").off("mouseout", ".m_town");
    $("#minimap_islands_layer").off("mouseover", ".m_town");
  },

  add: function (that: { id: string }) {
    var townID = 0;
    var popup_left = 0,
      popup_top = 0,
      classSize = "";
    //console.debug("TOWN", $(that).offset(), that.id);

    if (that.id === "") {
      // Island view
      townID = parseInt($(that).parent()[0].id.substring(10), 10);
      if (window.tr.settings.tr_tim) {
        popup_left = $(that).offset().left + 20;

        popup_top = $(that).offset().top + 20;
      } else {
        popup_left = $(that).offset().left + 15;

        popup_top = $(that).offset().top + 15;
      }
    } else {
      // Strategic map
      townID = parseInt(that.id.substring(6), 10);
      if (window.tr.settings.tr_tim) {
        popup_left = $(that).offset().left - 150;

        popup_top = $(that).offset().top + 20;
      } else {
        popup_left = $(that).offset().left - 145;

        popup_top = $(that).offset().top + 15;
      }
    }
    // Own town?

    if (typeof window.ITowns.getTown(townID) !== "undefined") {
      var units = window.ITowns.getTowns()[townID].units();

      var unitsSupport: { [key: string]: any } = window.ITowns.getTowns()[townID].unitsSupport();

      TownPopup.remove();
      // var popup = "<div id='tr_town_popup' style='left:" + ($(that).offset().left + 20) + "px; top:" + ($(that).offset().top + 20) + "px; '>";
      var popup = "<table class='popup' id='tr_town_popup' style='left:" + popup_left + "px; top:" + popup_top + "px; ' cellspacing='0px' cellpadding='0px'>";

      popup += "<tr class='popup_top'><td class='popup_top_left'></td><td class='popup_top_middle'></td><td class='popup_top_right'></td></tr>";

      popup += "<tr><td class='popup_middle_left'>&nbsp;</td><td style='width: auto;' class='popup_middle_middle'>";
      //var group_name="";

      var dadaa = "";
      /*var groupArrayy = window.ITowns.townGroups.getGroupsTRdav();
            for (var group in groupArrayy) {
  
                if (groupArrayy.hasOwnProperty(group)) {
  
                    if (Game.premium_features.curator > window.Timestamp.now() & groupArrayy[group].towns.hasOwnProperty(townID) ) {
                        //if (groupArrayy[townID].group < 3 ) {
                        ITowns.town_groups._byId[-1].attributes.name = ITowns.town_groups._byId[group].attributes.name;
                        ITowns.town_groups._byId[0].attributes.name = "";
                        setTimeout(function () {
                        ITowns.town_groups._byId[-1].attributes.name = ITowns.town_groups._byId[.1].attributes.name;
                        ITowns.town_groups._byId[0].attributes.name = ITowns.town_groups._byId[0].attributes.name;
                            }, 1);
                            var group_name = "";
  
                            group_name += ITowns.town_groups._byId[group].attributes.name;
                            // Title (town name)
                            //group_name = ITowns.town_groups._byId[group].attributes.name;
                            //if (ty) {group_name = ITowns.town_groups._byId[group].attributes.name;}
                        dadaa = " (" + group_name + ")";
  
  
                        //}else dadaa = " (...)";
                    }
                    //if (group == -1) {group_name="";}
                    //else {// Title (town name)
                    //    dadaa = "";
                    //}
                }
            }*/

      popup += "<h4><div style='margin-right:30px;'>" + window.ITowns.getTown(townID).name + (dadaa == " ()" ? "" : dadaa) + "</div></h4>";

      // Count movement
      var sup = 0,
        att = 0;

      var e,
        t = undefined,
        a = window.MM.getModels().MovementsUnits;
      for (e in a) {
        if (a.hasOwnProperty(e)) {
          t = a[e].attributes;
          if (t.target_town_id != townID) continue;
          "attack" == t?.type && att++, "support" == t?.type && sup++;
        }
      }

      if (sup > 0 || att > 0) {
        popup +=
          "<div class='move_counter_content' style=''><div style='float:left;margin-right:5px;'></div>" +
          (att > 0 ? "<div class='movement off'></div>" + "<div style='font-size: 12px;'><div class='attack_icon'></div><span class='movement' style='color:red;'> " + att + "</span> " + window.DM.getl10n("layout").toolbar_activities.incomming_attacks + "</div>" : "") +
          (sup > 0 ? "<div class='movement def'></div>" + "<div style='font-size: 12px;'><div class='support_icon'></div><span class='movement' style='color:green;'> " + sup + "</span> " + window.DM.getl10n("context_menu").titles.support + "</div>" : "") +
          "</div>";
      }

      // Unit Container
      if (window.tr.settings.tr_tpt) {
        popup += "<div class='unit_content'>";
        if (!$.isEmptyObject(units)) {
          for (var unit_id in units) {
            if (units.hasOwnProperty(unit_id)) {
              if (units[unit_id] > 1000) {
                classSize = "four_digit_number";
              }

              // Unit
              popup += '<div class="unit_icon25x25 ' + unit_id + " " + classSize + '"><span class="count text_shadow">' + units[unit_id] + "</span></div>";
            }
          }
        }

        // - Wall
        var wallLevel = window.ITowns.getTowns()[townID].getBuildings().attributes.wall;
        popup += '<div class="wall image bold"><span class="count text_shadow">' + wallLevel + "</span></div>";

        popup += "</div>";
      }

      //support
      if (!$.isEmptyObject(unitsSupport) & window.tr.settings.tr_tis) {
        // Title (support name)
        popup += "<h4><span style='white-space: nowrap;margin-right:35px;'>" + window.DM.getl10n("context_menu", "titles").support + "</span></h4>";

        // Unit support
        popup += "<div class='unit_content'>";

        for (var unitSupport_id in unitsSupport) {
          if (unitsSupport.hasOwnProperty(unitSupport_id)) {
            if (unitsSupport[unitSupport_id] > 1000) {
              classSize = "four_digit_number";
            }

            // Unit
            popup += '<div class="unit_icon25x25 ' + unitSupport_id + " " + classSize + '"><span class="count text_shadow">' + unitsSupport[unitSupport_id] + "</span></div>";
          }
        }
      }

      popup += "</div>";

      // Resources Container
      if (window.tr.settings.tr_tir) {
        popup += "<div class='resources_content'><table cellspacing='2px' cellpadding='0px'><tr>";

        var resources = window.ITowns.getTowns()[townID].resources();

        var storage = window.ITowns.getTowns()[townID].getStorage();

        var maxFavor = window.ITowns.getTowns()[townID].getMaxFavor();

        var Fury = window.ITowns.player_gods.attributes.fury;

        var fury_max = window.ITowns.player_gods.attributes.max_fury;

        // - Wood

        var textColor = resources.wood === storage ? "color:red;" : "";
        popup += '<td class="resources_small wood"></td><td style="' + textColor + '; width:1%;">' + resources.wood + "</td>";

        popup += '<td style="min-width:15px;"></td>';

        // - Population
        textColor = resources.population === 0 ? (textColor = "color:red;") : (textColor = "");
        popup += '<td class="resources_small population"></td><td style="' + textColor + ' width:1%">' + resources.population + "</td>";

        popup += "</tr><tr>";

        // - Stone
        textColor = resources.stone === storage ? (textColor = "color:red;") : (textColor = "");
        popup += '<td class="resources_small stone"></td><td style="' + textColor + '">' + resources.stone + "</td>";

        popup += '<td style="min-width:15px;"></td>';

        // - favor
        textColor = resources.favor === maxFavor ? (textColor = "color:red;") : (textColor = "");
        popup += '<td class="resources_small favor"></td><td style="' + textColor + '; width:1%">' + resources.favor + "</td>";

        popup += "</tr><tr>";

        // - Iron
        textColor = resources.iron === storage ? (textColor = "color:red;") : (textColor = "");
        popup += '<td class="resources_small iron"></td><td style="' + textColor + '">' + resources.iron + "</td>";

        if (window.ITowns.getTowns()[townID].god() == "ares") {
          popup += '<td style="min-width:15px;"></td>';

          // - fury
          textColor = Fury === fury_max ? (textColor = "color:red;") : (textColor = "");
          popup += '<td class="fury_icon"></td><td style="' + textColor + '; width:1%">' + Fury + "</td>";
        }

        popup += "</tr></table></div>";
      }

      // console.debug("TOWNINFO", ITowns.getTowns()[townID]);

      // Spy and God Container
      popup += "<div class='footer_content'><table cellspacing='0px'><tr>";

      var spy_storage = window.ITowns.getTowns()[townID].getEspionageStorage();

      // - Spy content
      popup += "<td class='spy_content'>";
      popup += '<div class="support_filter attack_spy"></div><div class="spy_text">' + HelperFunctions.pointNumber(spy_storage) + "</div>";
      popup += "</td>";

      popup += "<td></td>";

      // - hero Content
      if (window.tr.settings.tr_tih) {
        var HeroArray = window.ITowns.getHeroTR()[townID];
        if (HeroArray) {
          popup += "<td class='hero_content'>";
          popup += '<div class="hero_icon hero25x25 ' + HeroArray.hero_name + '"><span class="count text_shadow">' + HeroArray.hero_level + "</span></div>";
          popup += "</td>";
          popup += "<td></td>";
        }
      }

      // - God Content

      var god = window.ITowns.getTowns()[townID].god();

      popup += "<td class='god_content'>";
      popup += '<div class="god_mini ' + god + '"></div>';
      popup += "</td>";

      popup += "</tr></table></div>";

      popup += "</td><td class='popup_middle_right'>&nbsp;</td></tr>";

      popup += "<tr class='popup_bottom'><td class='popup_bottom_left'></td><td class='popup_bottom_middle'></td><td class='popup_bottom_right'></td></tr>";

      popup += "</table>";

      $(popup).appendTo("#popup_div_curtain");
    }
  },
  remove: function () {
    $("#tr_town_popup").remove();
  }
};

export default TownPopup;
