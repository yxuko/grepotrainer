/*******************************************************************************************************************************
 * Taskbar
 *******************************************************************************************************************************/

var Taskbar = {
  activate: function () {
    // @ts-ignore
    document.querySelector(".minimized_windows_area")?.style.width = "150%";
    // @ts-ignore
    document.querySelector(".minimized_windows_area").style.left = "-25%";
  },

  deactivate: function () {
    // @ts-ignore
    document.querySelector(".minimized_windows_area").style.width = "100%";
    // @ts-ignore
    document.querySelector(".minimized_windows_area").style.left = "0%";
  }
};

export default Taskbar;
