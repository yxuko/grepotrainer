var cultureProgress = {
  activate: () => {
    $(
      '<style id="dio-ProgressBar-style">' +
        '#dio-ProgressBar {position: absolute; height: 25px; top: 30px; background:url("https://gp' +
        window.tr.LID +
        '.innogamescdn.com/images/game/place/culture_bar-2.99.png") no-repeat 0px 0px;filter: hue-rotate(-40deg) brightness(2);}' +
        '#dio-Bar {position: absolute; height: 25px; top: 30px; background:url("https://gp' +
        window.tr.LID +
        '.innogamescdn.com/images/game/place/culture_bar-2.99.png") no-repeat 0px 0px;}' +
        "#culture_points_overview_bottom #place_culture_bar, #place_container #place_culture_bar { display: none; }" +
        "</style>"
    ).appendTo("head");
    cultureProgress.add();
  },
  add: () => {
    try {
      if ($("#place_culture_towns").is(":visible")) {
        let level = parseInt($("#place_culture_towns").text().split("/")[1]);

        let [currentCount, totalCount] = $("#place_culture_count")
          .text()
          .match(/[0-9]+/g);

        let inProgress = parseInt(
          $("#place_culture_in_progress")
            .text()
            .match(/[0-9]+/)
        );

        let nbNeeded = (level - 1) * 3;
        let nbLeft = totalCount - currentCount;
        let percentLeft = 100 - (nbLeft / nbNeeded) * 100;
        let percentInProgress = (inProgress / nbNeeded) * 100;

        //Progress Bar
        if (!$("#culture_points_overview_bottom #dio-ProgressBar").length) $('<div id="dio-ProgressBar"></div><div id="dio-Bar"></div>').insertBefore("#culture_points_overview_bottom #place_culture_bar");
        if (!$("#place_container #dio-ProgressBar").length) $('<div id="dio-ProgressBar"></div><div id="dio-Bar"></div>').insertBefore("#place_container #place_culture_bar");

        $("#culture_points_overview_bottom #dio-Bar, #place_container #dio-Bar").css({ width: "calc(" + percentLeft + "% - 1px)" });
        $("#culture_points_overview_bottom #dio-ProgressBar, #place_container #dio-ProgressBar").css({
          width: percentInProgress + "%",
          left: "calc(" + percentLeft + "% + 2px)",
          "max-width": "calc(" + (100 - percentLeft) + "% - 4px)"
        });
      }
    } catch (error) {
      console.error(error, "cultureProgress");
    }
  },
  deactivate: () => {
    $("#dio-ProgressBar-style").remove();
    $("#dio-ProgressBar").remove();
    $("#dio-Bar").remove();
  }
};

export default cultureProgress;
