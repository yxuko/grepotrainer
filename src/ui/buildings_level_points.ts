import { buildings } from "../constants";

function buildingsLvlPts() {
  try {
    // @ts-ignore
    $(".build_up").map((idx, button) => {
      // @ts-ignore
      if (button.onclick != null) {
        // @ts-ignore
        const fn = button.onclick.toString();
        const list = fn.substr(54, (fn.substr(54).match(/\)/) || []).index).split("', ");
        const building = list[0];
        const lvl = list[1];

        button.innerHTML = button.innerHTML + "<small>(" + buildings[building][lvl.toString()] + "pts)</small>";
      }
    });
    // @ts-ignore
    $(".tear_down").map((idx, button) => {
      // @ts-ignore
      if (button.onclick != null) {
        // @ts-ignore
        const fn = button.onclick.toString();
        const building = fn.substr(49, (fn.substr(49).match(/\'/) || []).index);

        // @ts-ignore
        const lvl = BuildingMain.buildings[building].level;

        button.innerHTML = button.innerHTML + "<small>(-" + buildings[building][lvl] + "pts)</small>";
      }
    });
  } catch (error) {
    console.error(error);
  }
}
export default buildingsLvlPts;
