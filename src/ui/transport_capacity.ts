/*******************************************************************************************************************************
 * ● Transporter capacity
 *******************************************************************************************************************************/
var TransportCapacity = {
  timeout: null,
  activate: function () {
    // transporter display
    $(
      '<div id="dio_tr_recruit" class="checkbox_new checked" style="right: 10px; bottom: 2px; position: absolute;"><div class="dio_tr_recruit"></div><div class="cbx_icon" style="margin-top:2px"></div></div></tr></table>' +
        //'<a id="dio_Port" style="position: absolute; width: 60px; height: 26px;"></a>' +
        '<div id="dio_transporter" class="cont" style="height:25px;">' +
        '<table style=" margin:0px;"><tr align="center" >' +
        '<td><img id="dio_ship_img" class="ico" src="https://www.tuto-de-david1327.com/medias/images/mini-bateau.png"></td>' +
        '<td><span id="dio_ship" class="bold text_shadow" style="color:#FFCC66;font-size: 10px;line-height: 2.1;"></span></td></tr>' +
        '<tr align="center"><td></td>' +
        '<td><span id="dio_ship-def" class="bold text_shadow" style="color:#FFCC66;font-size: 10px;line-height: 2.1;"></span></td></tr>' +
        //'<div id="tr_recruit" class="checkbox_new checked" style="margin-left:-1px"><div class="tr_options tr_recruit"></div><div class="cbx_icon" style="margin-top:2px"></div></div>' +
        "</div>"
    ).appendTo(".units_naval .content");
    $(".units_naval .nav .border_top").click(function () {
      window.DocksWindowFactory.openDocksWindow();
    });
    $(".dio_tr_recruit").css({
      background: "url(https://gpall.innogamescdn.com/images/game/units/units_info_sprite2.51.png)",
      "background-position-y": "0px",
      "background-size": "100%",
      width: "18px",
      height: "18px",
      float: "left"
    });
    $("#dio_transporter.cont").css({
      background: "url(https://gpall.innogamescdn.com/images/game/layout/layout_units_nav_border.png)"
    });
    $("#dio_transporter").hover(function () {
      $(this).css("cursor", "pointer");
    });
    $("#dio_tr_recruit.checkbox_new").click(function () {
      if ($(this).find("DIV.tr_deactivated").length === 0) {
        $(this).toggleClass("checked");
      }
    });

    TransportCapacity.timeout = setInterval(() => {
      TransportCapacity.update();
    }, 800);

    $("#dio_tr_recruit")
      .first()
      .toggleClick(function () {
        TransportCapacity.update();
      });

    $("#dio_transporter").toggleClick(
      function () {
        $("#dio_ship_img").get(0).src = "https://www.tuto-de-david1327.com/medias/images/mini-bateau-red.png";
        window.tr.strength.shipsize = true;
        TransportCapacity.update();
      },
      function () {
        $("#dio_ship_img").get(0).src = "https://www.tuto-de-david1327.com/medias/images/mini-bateau.png";
        window.tr.strength.shipsize = false;
        TransportCapacity.update();
      }
    );
    TransportCapacity.update();

    $("#dio_tr_recruit").mousePopup(new MousePopup('<div class="dio_icon b"></div>Count units in recruitment queue'));
  },
  deactivate: function () {
    $("#dio_transporter").remove();
    $("#dio_Port").remove();
    $("#dio_tr_recruit").remove();

    clearTimeout(TransportCapacity.timeout);
    TransportCapacity.timeout = null;
  },
  update: function () {
    try {
      const selected_town = window.ITowns.getTown(window.Game.townId),
        GD_units = window.GameData.units,
        GD_heroes = window.GameData.heroes,
        trans_small = GD_units.small_transporter,
        trans_big = GD_units.big_transporter;
      let bigTransp = 0,
        smallTransp = 0,
        pop = 0,
        pop_def = 0,
        ship = 0,
        berth,
        units = [];

      // Ship space (available)
      if (window.tr.strength.shipsize) {
        bigTransp = parseInt(window.ITowns.getTown(window.Game.townId).units().big_transporter, 10);
        if (isNaN(bigTransp)) bigTransp = 0;
      } else {
      smallTransp = parseInt(window.ITowns.getTown(window.Game.townId).units().small_transporter, 10);
      if (isNaN(smallTransp)) smallTransp = 0;

      }
      // Checking: Research berth
      berth = 0;

      if (window.ITowns.getTown(window.Game.townId).researches().hasBerth()) {
        berth = window.GameData.research_bonus.berth;
      }
      ship = bigTransp * (trans_big.capacity + berth) + smallTransp * (trans_small.capacity + berth);
      units = window.ITowns.getTown(window.Game.townId).units();

      function isOff(name: string) {
        return GD_units[name].unit_function === "function_off" || GD_units[name].unit_function === "function_both";
      }

      function isDef(name: string) {
        return GD_units[name].unit_function === "function_def" || GD_units[name].unit_function === "function_both";
      }
      // Ship space (required)
      for (var e in units) {
        if (units.hasOwnProperty(e)) {
          if (GD_units[e]) {
            if (isOff(e)) {
              if (e === "spartoi") {
                pop += units[e];
              } else if (!(GD_units[e].is_naval || GD_units[e].flying)) {
                pop += units[e] * GD_units[e].population;
              }
            }
            if (isDef(e)) {
              if (!(GD_units[e].is_naval || GD_units[e].flying)) {
                pop_def += units[e] * GD_units[e].population;
              }
            }
          }
        }
      }
      if ($(".dio_tr_recruit").parent().hasClass("checked")) {
        const recruits = selected_town.getUnitOrdersCollection().models;
        for (let i = 0; i < recruits.length; ++i) {
          const unitt = recruits[i].attributes.unit_type,
            number = recruits[i].attributes.units_left;
          // Landtruppen
          if (!(unitt in GD_heroes) && units[unitt] != 0 && !GD_units[unitt].flying && GD_units[unitt].capacity == undefined) {
            if (isOff(unitt)) {
              if (unitt === "spartoi") {
                pop += number;
              } else {
                pop += number * GD_units[unitt].population;
              }
            }
            if (isDef(unitt)) {
              if (unitt === "spartoi") {
                pop_def += number;
              } else {
                pop_def += number * GD_units[unitt].population;
              }
            }
          }
          // Transportschiffe
          else if (!(unitt in GD_heroes) && units[unitt] != 0 && !GD_units[unitt].flying && GD_units[unitt].capacity != 0) {
            if (!window.tr.strength.shipsize) {
              if (unitt === "small_transporter") {
                ship += number * (GD_units[unitt].capacity + berth);
              } else return;
            }
            if (window.tr.strength.shipsize) {
              ship += number * (GD_units[unitt].capacity + berth);
            }
          }
        }
      }
      $(".dio_tr_recruit").css({
        "background-position-y": pop_def > pop ? "-36px" : "0px"
      });
      if (pop_def > pop) {
        pop = pop_def;
      }
      const newHTML = `<span style="color:${(pop === 0) && (ship === 0) ? "#FFCC66" : pop > ship ? "#ffb4b4" : "#6eff5c"}">${pop}/${ship}</span>`;

      if ($("#dio_ship").get(0).innerHTML !== newHTML) {
        $("#dio_ship").get(0).innerHTML = newHTML;
        const popDiff = Math.abs(pop - ship);
        if (pop > ship) {
          let missing = 0
          let name = ""
          if (window.tr.strength.shipsize) {
            missing = Math.ceil(popDiff / (trans_big.capacity + berth));
            name = missing === 1 ? trans_big.name : trans_big.name_plural;
          } else {
             missing = Math.ceil(popDiff / (trans_small.capacity + berth));
             name = missing === 1 ? trans_small.name : trans_small.name_plural;
          }

          $("#dio_transporter").mousePopup(new MousePopup('<div class="dio_icon b"></div>' + "Lack " + missing + " " + name));
        } else {
          let name = trans_small.name_plural;
          if (window.tr.strength.shipsize) {
            name = trans_big.name_plural;
          }

          $("#dio_transporter").mousePopup(new MousePopup('<div class="dio_icon b"></div>' + "Still " + popDiff + " available population. For the " + name + "."));

          if (pop === 0 && ship === 0) {
            $("#dio_transporter").mousePopup(new MousePopup('<div class="dio_icon b"></div>You don\'t have an army.'));
          } else if (popDiff === 0) {
            $("#dio_transporter").mousePopup(new MousePopup('<div class="dio_icon b"></div>Optimal population for' + " " + name + "."));
          }
        }
      }
    } catch (error) {
      console.error(error, "TransportCapacity");
    }
  }
};

export default TransportCapacity;
