/*******************************************************************************************************************************
 * Town Icons
 *******************************************************************************************************************************/

import { getAllUnits } from "../misc/get_all_units";
import TrStorage from "../storage";
import MapWorld from "./map_world";
import TownList from "./town_list";

var TownIcons: TownIconsType = {
  types: {
    // Automatic Icons
    lo: 0,
    ld: 3,
    so: 6,
    sd: 7,
    fo: 10,
    fd: 9,
    bu: 14 /* Building */,
    po: 22,
    no: 12,

    // Manual Icons
    fa: 20 /* Favor */,
    re: 15 /* Resources */,
    di: 2 /* Distance */,
    sh: 1 /* Pierce */,
    lu: 13 /* ?? */,
    dp: 11 /* Diplomacy */,
    ha: 15 /* ? */,
    si: 18 /* Silber */,
    ra: 17,
    ch: 19 /* Research */,
    ti: 23 /* Time */,
    un: 5,
    wd: 16 /* Wood */,
    wo: 24 /* World */,
    bo: 13 /* Booty */,
    gr: 21 /* Lorbeer */,
    st: 17 /* Stone */,
    is: 26 /* ?? */,
    he: 4 /* Helmet */,
    ko: 8 /* Kolo */,
    O1: 58,
    O2: 59,
    O3: 60,
    O4: 61,
    O5: 62,
    O6: 63,

    OO: 25 /* Kolo */,

    SL: 26, // Slinger
    CB: 27, // Cerberus
    SW: 28, // Chariot
    CE: 29, // Colony ship
    CY: 30, // Calydonian boar
    DS: 31, // Fire ship
    FS: 32, // Light ship
    BI: 33, // Bireme
    TR: 34, // Trireme
    GF: 35, // Griffin
    MN: 36, // Manticore
    MD: 37, // Medusa
    EY: 38, // Erinys
    HP: 39, // Harpy
    HD: 40, // Hydra
    CT: 41, // Centaur
    PG: 42, // Pegasus
    RE: 43, // Horseman
    HO: 44, // Hoplite
    CA: 45, // Catapult
    ee: 46, //
    SK: 47, // Swordsman
    BS: 48, // Archer
    MT: 49, // Minotaur
    CL: 50, // Cyclop
    SE: 51, // Siren
    ST: 52, // Satyr
    LD: 53, // Ladon
    SR: 54, // Spartoi
    BT: 55,
    BE: 56,
    Bo: 57,

    a1: 64,
    a2: 65,
    a3: 66,
    a4: 67,
    a5: 68,
    a6: 69,
    a7: 70,
    a8: 71
  },

  deactivate: function () {
    $("#town_icon").remove();
    $("#tr_townicons_field").remove();

    //clearTimeout(TownIcons.timeout);
    //TownIcons.timeout = null;
  },

  activate: function () {
    try {
      $('<div id="town_icon"><div class="town_icon_bg"><div class="icon_big townicon_' + (window.tr.manuTownTypes[window.Game.townId] || (window.tr.autoTownTypes[window.Game.townId] || "no") + " auto") + '"></div></div></div>').appendTo(".town_name_area");

      // Town Icon Style
      $("#town_icon .icon_big").css({
        backgroundPosition: TownIcons.types[window.tr.manuTownTypes[window.Game.townId] || window.tr.autoTownTypes[window.Game.townId] || "no"] * -25 + "px 0px"
      });

      $(
        '<style id="tr_townicons_field" type="text/css">' +
          "#town_icon { background:url(https://dio-david1327.github.io/img/dio/logo/dio-sprite-5.png) 0 -125px no-repeat; position:absolute; width:69px; height:61px; left:-47px; top:0px; z-index: auto; } " +
          "#town_icon .town_icon_bg { background:url(https://dio-david1327.github.io/img/dio/logo/dio-sprite-5.png) -76px -129px no-repeat; width:43px; height:43px; left:25px; top:4px; cursor:pointer; position: relative; } " +
          "#town_icon .town_icon_bg:hover { filter:url(#Brightness11); -webkit-filter:brightness(1.1); box-shadow: 0px 0px 15px rgb(1, 197, 33); } " +
          "#town_icon .icon_big	{ position:absolute; left:9px; top:9px; height:25px; width:25px; } " +
          "#town_icon .select_town_icon {position: absolute; top:47px; left:23px; width:236px; display:none; padding:2px; border:3px inset rgb(7, 99, 12); box-shadow:rgba(0, 0, 0, 0.5) 4px 4px 6px; border-radius:0px 10px 10px 10px; z-index: 5003;" +
          "background:url(https://gpall.innogamescdn.com/images/game/popup/middle_middle.png); } " +
          "#town_icon .item-list {max-height: none; align: right; overflow: hidden; } " +
          "#town_icon .option_s { cursor:pointer; width:20px; height:20px; margin:0px; padding:2px 2px 3px 3px; border:2px solid rgba(0,0,0,0); border-radius:5px; background-origin:content-box; background-clip:content-box;} " +
          "#town_icon .option_s:hover { border: 2px solid rgb(59, 121, 81) !important;-webkit-filter: brightness(1.3); } " +
          "#town_icon .sel { border: 2px solid rgb(202, 176, 109); } " +
          "#town_icon hr { width:265px; margin:0px 0px 7px 0px; position:relative; top:3px; border:0px; border-top:2px dotted #000; float:left} " +
          "#town_icon .auto_s { width:160px; height:16px; float:left; margin-right: 3px;} " +
          "#town_icon .dio_icon.b { width: 26px; height: 22px; float: right; margin: 0;  margin-right: 3px;} " +
          "#town_icon .défaut_s { background:url(https://dio-david1327.github.io/img/dio/btn/arrows-20px.png) no-repeat; width:17px; height:17px; float: right; margin-right: 3px;} " +
          // Quickbar modification
          ".ui_quickbar .left, .ui_quickbar .right { width:46%; } " +
          // because of Kapsonfires Script and Beta Worlds bug report bar:
          ".town_name_area { z-index:11; left:52%; } " +
          ".town_name_area .left { z-index:20; left:-39px; } " +
          ".town_name_area .button.GMHADD {left: -134px !important; z-index: 100 !important;}" +
          "#town_groups_list {margin-left: -20px;}" +
          "#dio_tic2 { display:block!important; }" +
          "</style>"
      ).appendTo("head");

      // Style for town icons
      var style_str = '<style id="tr_townicons" type="text/css">';
      for (var ico in TownIcons.types) {
        if (TownIcons.types.hasOwnProperty(ico)) {
          style_str += ".townicon_" + ico + " { background:url(https://dio-david1327.github.io/img/dio/btn/town-icons.png) " + TownIcons.types[ico] * -25 + "px -26px repeat;float:left;} ";
        }
      }
      style_str += "</style>";

      $(style_str).appendTo("head");

      var icoArray = [
        "lo",
        "ld",
        "fo",
        "fd",
        "so",
        "sd",
        "no",
        "po",
        "sh",
        "di",
        "un",
        "ko",
        "ti",
        "gr",
        "dp",
        "re",
        "wd",
        "st",
        "si",
        "bu",
        "he",
        "ch",
        "bo",
        "fa",
        "wo",
        "OO",
        "O1",
        "O2",
        "O3",
        "O4",
        "O5",
        "O6",
        "hr",
        "FS",
        "BI",
        "TR",
        "BT",
        "BE",
        "CE",
        "DS",
        "SK",
        "SL",
        "BS",
        "HO",
        "RE",
        "SW",
        "CA",
        "CT",
        "CB",
        "CL",
        "EY",
        "MD",
        "MT",
        "HD",
        "HP",
        "MN",
        "PG",
        "GF",
        "CY",
        "SE",
        "ST",
        "LD",
        "SR",
        "ee",
        "Bo",
        "hr",
        "a1",
        "a2",
        "a3",
        "a4",
        "a5",
        "a6",
        "a7",
        "a8"
      ];

      // Fill select box with town icons

      $('<div class="select_town_icon dropdown-list default active"><div class="item-list"></div></div>').appendTo("#town_icon");
      for (var i in icoArray) {
        if (icoArray.hasOwnProperty(i)) {
          if (icoArray[i] === "hr") {
            $(".select_town_icon .item-list").append("<hr>");
          } else {
            $(".select_town_icon .item-list").append('<div class="option_s tr_icon_small townicon_' + icoArray[i] + '" name="' + icoArray[i] + '"></div>');
          }
        }
      }
      $('<hr><div class="option_s auto_s" name="auto"><b>Auto</b></div>' + '<div class="option_s défaut_s" name="défaut"></div>' + '<div class="dio_icon b"></div>').appendTo(".select_town_icon .item-list");

      //$('#town_icon .auto_s').dialog('<table style="width:600px" class="">' +
      //'<tr><td><div class="tr_icon_small townicon_lo"></div>Land Offensive</td>' +
      //'<td><div class="tr_icon_small townicon_fo"></div>Fly Offensive</td>' +
      //'<td><div class="tr_icon_small townicon_so"></div>Navy Offensive</td>' +
      //'<td><div class="tr_icon_small townicon_no"></div>Outside</td></tr>' +
      //'<tr><td><div class="tr_icon_small townicon_ld"></div>Land Defensive</td>' +
      //'<td><div class="tr_icon_small townicon_fd"></div>Fly Defensive' +
      //'<td><div class="tr_icon_small townicon_sd"></div>Navy Defensive</td>' +
      //'<td><div class="tr_icon_small townicon_po"></div>Empty</td></tr>' +
      //'</table>');

      $("#town_icon .défaut_s").mousePopup(new MousePopup('<div class="dio_icon b"></div> Reset'));

      $("#town_icon .option_s").on("click", function () {
        $("#town_icon .sel").removeClass("sel");
        // @ts-ignore
        $(this).addClass("sel");

        // @ts-ignore
        if ($(this).attr("name") === "auto") {
          delete window.tr.manuTownTypes[window.Game.townId];

          window.tr.manuTownAuto[window.Game.townId] = true;
          // @ts-ignore
        } else if ($(this).attr("name") === "défaut") {
          delete window.tr.manuTownTypes[window.Game.townId];

          delete window.tr.manuTownAuto[window.Game.townId];
          getAllUnits();
        } else {
          // @ts-ignore
          window.tr.manuTownTypes[window.Game.townId] = $(this).attr("name");
        }
        TownIcons.changeTownIcon();
        getAllUnits();
        TownList.change();

        // Update town icons on the map
        if (window.tr.settings.tr_tim) {
          MapWorld.add(); //setOnMap();
        }

        let tr_storage = new TrStorage();
        tr_storage.save("manuTownTypes", window.tr.manuTownTypes);
        tr_storage.save("manuTownAuto", window.tr.manuTownAuto);
      });

      // Show & hide drop menus on click
      $("#town_icon .town_icon_bg").on("click", function () {
        var el = $("#town_icon .select_town_icon").get(0);
        if (el?.style.display === "none") {
          el.style.display = "block";
        } else {
          // @ts-ignore
          el?.style.display = "none";
        }
      });

      $('#town_icon .select_town_icon [name="' + (window.tr.manuTownTypes[window.Game.townId] || (window.tr.autoTownTypes[window.Game.townId] ? "auto" : "")) + '"]').addClass("sel");
    } catch (error) {
      console.error(error);
    }
  },

  updateTownIcon: function () {
    setTimeout(function () {
      // @ts-ignore
      var townType = window.tr.manuTownTypes[window.Game.townId] || window.tr.autoTownTypes[window.Game.townId] || "no";

      $("#town_icon .icon_big")
        .removeClass()
        .addClass("icon_big townicon_" + townType + " auto");

      $("#town_icon .icon_big").css({
        // @ts-ignore
        backgroundPosition: TownIcons.types[townType] * -25 + "px 0px"
      });
      MapWorld.add(); //setOnMap();
    }, 200);
  },

  changeTownIcon: function () {
    var townType = window.tr.manuTownTypes[window.Game.townId] || window.tr.autoTownTypes[window.Game.townId] || "no";
    $("#town_icon .icon_big")
      .removeClass()
      .addClass("icon_big townicon_" + townType + " auto");

    $("#town_icon .sel").removeClass("sel");
    $('#town_icon .select_town_icon [name="' + (window.tr.manuTownTypes[window.Game.townId] || (window.tr.autoTownTypes[window.Game.townId] ? "auto" : "")) + '"]').addClass("sel");

    $("#town_icon .icon_big").css({
      backgroundPosition: TownIcons.types[townType] * -25 + "px 0px"
    });

    // @ts-ignore
    $("#town_icon .select_town_icon")?.get(0)?.style.display = "none";
  },

  auto: {
    activate: () => {
      setTimeout(() => {
        getAllUnits();
        TownIcons.updateTownIcon();
      }, 100);
    },
    deactivate: () => {
      setTimeout(() => {
        getAllUnits();
        TownIcons.changeTownIcon();
      }, 100);
    }
  }
};

export default TownIcons;
