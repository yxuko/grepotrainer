/*******************************************************************************************************************************
 * Farming Village Overview
 * ----------------------------------------------------------------------------------------------------------------------------
 * | ● FarmingVillagesHelper : Automatically hide the city. Quack function
 * ----------------------------------------------------------------------------------------------------------------------------
 * *****************************************************************************************************************************/

var FarmingVillagesHelper = {
  activeFarm: undefined,
  activate: () => {},
  rememberloot: () => {
    var activeFarmClass = $("#time_options_wrapper .active").attr("class").split(" ");

    FarmingVillagesHelper.activeFarm = activeFarmClass[1];
  },

  setloot: () => {
    setTimeout(() => {
      $("#time_options_wrapper ." + FarmingVillagesHelper.activeFarm).click(); /*global activeFarm*/
    }, 500);
  },
  islandHeader: () => {
    $("#fto_town_list li").each(function (index) {
      if (this.classList.length == 2) {
        $(this).addClass("dio_li_island");
        $(this).append('<div class="dio_colordivider" style="background-image: url(https://www.tuto-de-david1327.com/medias/images/13-4.jpg); display: block; height: 24px; margin: -4px -2px;"></div>' + '<div class="checkbox_new checked disabled" style="position: absolute; right: 2px; top: 5px; display: none;"><div class="cbx_icon"></div></div>');
        $(this).find("span").css({
          "margin-left": "2px"
        });
        $(this).find("a").css({
          color: "rgb(238, 221, 187)"
        });
      }
    });
    $(".dio_colordivider").click(function () {
      var el = $(this).parent().nextUntil(".dio_li_island");
      if ($('#fto_town_list li:first[style*="border-right"]').length == 0) {
        el.slideToggle();
      } else {
        el.toggleClass("hidden");
      }
    });
    $('<style type="text/css">#fto_town_list li.active {background: rgba(208, 190, 151, 0.60)} .dio_autoHideCitiesOff {background-position: 0px -11px}</style>').appendTo("head");
  },
  indicateLoot: () => {
    var activeIsland = $("#fto_town_list li.active").prevAll(".dio_li_island").first();
    activeIsland.find("div.checkbox_new").removeClass("disabled");
    activeIsland.find("div.dio_colordivider").trigger("click");
  },

  switchTown: (direction: string) => {
    var el;
    if (direction === "up") {
      el = $("#fto_town_list li.active").prevAll("li:not(.dio_li_island):visible").first();
    } else {
      el = $("#fto_town_list li.active").nextAll("li:not(.dio_li_island):visible").first();
    }
    el.click();
    if (el.get(0)) {
      el.get(0).scrollIntoView();

      var scrollPosition = el.get(0).parentNode.scrollTop;
      var scrollMax = (scrollPosition += 405);

      var scrollContainer = el.get(0).parentNode.scrollHeight;
      if (scrollMax != scrollContainer) {
        el.get(0).parentNode.scrollTop -= 160;
      }
    }
  },
  deactivate: () => {
    $("#dio_toggleAutohide").addClass("dio_autoHideCitiesOff");
  }
};

export default FarmingVillagesHelper;
