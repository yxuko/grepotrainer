import TownIcons from "./town_icons";

var MapWorld = {
  activate: function () {
    //style
    $(
      "<style id='tr_townicons_map' type='text/css'>" +
        ".own_town .flagpole, #main_area .m_town.player_" +
        window.tr.PID +
        " { z-index: 100; width:19px!important; height:19px!important; border-radius: 11px; border: 3px solid rgb(16, 133, 0); margin: -4px !important; font-size: 0em !important; box-shadow: 1px 1px 0px rgba(0, 0, 0, 0.5); } " +
        "#dio_town_popup .count { position: absolute; bottom: 1px; right: 1px; font-size: 10px; } " +
        "#minimap_islands_layer .m_town { text-shadow: 1px 1px 0px rgba(0, 0, 0, 0.7); } " +
        "#minimap_canvas.expanded.night, #map.night .flagpole { filter: brightness(0.7); -webkit-filter: brightness(0.7); } " +
        "#minimap_click_layer { display:none; }" +
        "</style>"
    ).appendTo("head");
    MapWorld.add();
    $("#ui_box > div.topleft_navigation_area > div.coords_box > div.btn_jump_to_coordination.circle_button.jump_to_coordination > div, #ui_box > div.topleft_navigation_area > div.bull_eye_buttons > div.btn_jump_to_town.circle_button.jump_to_town > div, #minimap_canvas, #map").click(() => {
      setTimeout(() => {
        MapWorld.add();
      }, 500);
    });
    $("#minimap_canvas").dblclick(() => {
      setTimeout(() => {
        MapWorld.add();
      }, 500);
    });
  },

  add: function () {
    try {
      // Style for own towns (town icons)
      for (var e in window.tr.autoTownTypes) {
        if (window.tr.autoTownTypes.hasOwnProperty(e)) {
          $("#mini_t" + e + ", #town_flag_" + e + " .flagpole").css({
            background: "rgb(255, 187, 0) url(https://dio-david1327.github.io/img/dio/btn/town-icons.png) repeat",
            "background-position": TownIcons.types[window.tr.manuTownTypes[e] || window.tr.autoTownTypes[e]] * -25 + "px -27px"
          });
        }

        $("#minimap_islands_layer").off("mousedown");
        $("#minimap_islands_layer").on("mousedown", function () {
          if (typeof $("#context_menu").get(0) !== "undefined") {
            $("#context_menu").get(0).remove();
          }
        });
      }
    } catch (error) {
      console.error(error);
    }
  },
  deactivate: function () {
    $("#tr_townicons_map").remove();
    $(".own_town .flagpole, #main_area .m_town").css({ background: "" });
  }
};

export default MapWorld;
