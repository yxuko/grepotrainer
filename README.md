# 🐱 Grepo Trainer
Trainer for [Grepolis](https://fr.grepolis.com)

→ JS script to paste in Grepolis Console (F12).

## Commands

### Generate on every save
```
yarn start
```

### Generate Minified JS
```
yarn mini
```
