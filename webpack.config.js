var path = require("path");
var webpack = require("webpack");
const TerserPlugin = require("terser-webpack-plugin");

var PATHS = {
  entryPoint: path.resolve(__dirname, "src/bot.ts"),
  bundles: path.resolve(__dirname, "dist")
};

var config = {
  mode: "production",
  entry: {
    bot: [PATHS.entryPoint]
  },
  optimization: {
    minimize: true,
    minimizer: [new TerserPlugin()]
  },
  // The output defines how and where we want the bundles. The special
  // value `[name]` in `filename` tell Webpack to use the name we defined above.
  // We target a UMD and name it MyLib. When including the bundle in the browser
  // it will be accessible at `window.MyLib`
  output: {
    path: PATHS.bundles,
    filename: "[name].js",
    libraryTarget: "umd",
    umdNamedDefine: true
  },
  // Add resolve for `tsx` and `ts` files, otherwise Webpack would
  // only look for common JavaScript file extension (.js)
  resolve: {
    extensions: [".ts", ".tsx", ".js"]
  },
  // Activate source maps for the bundles in order to preserve the original
  // source when the user debugs the application
  devtool: "source-map",
  plugins: [],
  externals: {
    "map/helpers": "map/helpers",
    "helpers/units_tooltip_helper": "helpers/units_tooltip_helper"
  },
  module: {
    // Webpack doesn't understand TypeScript files and a loader is needed.
    // `node_modules` folder is excluded in order to prevent problems with
    // the library dependencies, as well as `__tests__` folders that
    // contain the tests for the library
    rules: [
      {
        test: /\.ts?$/,
        loader: "ts-loader",
        exclude: /node_modules/,
        options: {
          transpileOnly: true
        }
      }
    ]
  }
};

module.exports = config;
